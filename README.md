# README #

## What is this repository for? ##

* PyramIDE is an IDE (Integrated Development Environment) for [Anubis language](http://anubis-language.org)
* Version 1.0.12.108

## How do I get set up? ##

### Summary of set up ###

PyramIDE is a full .NET 2.0 project, written in C#.
Will it can be built theorically with any Visual Studio since VS 2005, it's known to be successfully compiled with VS 2008 only.

### Configuration

### Dependencies

* .NET Framwork 2.0
* Bison / Flex
 


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact