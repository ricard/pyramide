;#define DeleteToFirstPeriod(str *S) /* macro declaration */ Local[1] = Copy(S, 1, (Local[0] = Pos(".", S)) - 1),��S = Copy(S, Local[0] + 1),��Local[1]

;#define GetShortVersionString(str S) /* macro declaration */ Local[0] = S, ��Local[1] = DeleteToFirstPeriod(Local[0]), ��Local[2] = DeleteToFirstPeriod(Local[0]), ��Local[1] + "." + Local[2]

#define AppName "PyramIDE"
#define AppVersion GetFileVersion(AddBackslash(SourcePath) + "..\bin\PyramIDE.exe")
#define AddinVersion GetFileVersion(AddBackslash(SourcePath) + "..\AddIns\AddIns\BackendBindings\AnubisBinding\AnubisBinding.dll")
#define AppPublisher "SoftArchi"

#define AppVersionNotProtected GetFileVersion(AddBackslash(SourcePath) + "..\bin\PyramIDE.exe")
#define AddinVersionNotProtected GetFileVersion(AddBackslash(SourcePath) + "..\AddIns\AddIns\BackendBindings\AnubisBinding\AnubisBinding.dll")

#if AppVersion != AppVersionNotProtected
# error Not protected !!
#endif
#if AddinVersion != AddinVersionNotProtected
# error Not protected !!
#endif

;#define AppShortVersion GetShortVersionString(S = AppVersion)
;#sub AppShortVersion
;	#emit GetShortVersionString(S = AppVersion)
;#endsub

; Set inclusion for languages:
;#define ISSI_Languages
;#define ISSI_English "doc\License-en.txt"
;#define ISSI_French "doc\License-fr.txt"

; Detect MicroSoft .NET Framework: (Valid options "1.1" and "2.0"
;#define ISSI_NetDetect "2.0"

;#define ISSI_AppStart
;#define ISSI_About "NameAndVersionDate"



[Setup]
AppName={#AppName}
AppVersion={#AppVersion}
AppPublisher={#AppPublisher}
AppVerName={#AppName} v{#AppVersion} by {#AppPublisher}
OutputBaseFilename={#AppName}-{#AppVersion}-Setup
DefaultDirName={pf}\{#AppPublisher}\{#AppName}
DefaultGroupName={#AppPublisher}\{#AppName}
SourceDir=..\
ShowLanguageDialog=yes
WizardImageFile=setup\wizard-image.bmp
OutputDir=setup\Output
Compression=lzma
VersionInfoVersion={#AppVersion}
VersionInfoCompany={#AppPublisher}
VersionInfoTextVersion={#AppVersion}
VersionInfoCopyright=Copyright � {#AppPublisher}
UninstallDisplayIcon={app}\bin\PyramIDE.exe
AppPublisherURL=http://www.softarchi.com
LicenseFile=doc\Licence-fr.rtf

[Dirs]
Name: {app}\bin
Name: {app}\AddIns
Name: {app}\data
Name: {app}\doc

[Files]
Source: bin\PyramIDE.exe; DestDir: {app}\bin; Flags: overwritereadonly ignoreversion
Source: bin\*.*; DestDir: {app}\bin; Excludes: .svn,*.pdb,*.vshost.*,PyramIDE.exe,PostBuild,*.CodeAnalysisLog.xml,*.lastcodeanalysissucceeded,*.xml,Wix; Flags: overwritereadonly ignoreversion recursesubdirs
Source: AddIns\ICSharpCode.SharpDevelop.addin; DestDir: {app}\AddIns; Flags: overwritereadonly ignoreversion recursesubdirs
Source: AddIns\AddIns\BackendBindings\AnubisBinding\Resources\*.*; DestDir: {app}\AddIns\AddIns\BackendBindings\AnubisBinding\Resources; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Language/Anubis
Source: AddIns\AddIns\BackendBindings\AnubisBinding\Templates\*.*; DestDir: {app}\AddIns\AddIns\BackendBindings\AnubisBinding\Templates; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Language/Anubis
Source: AddIns\AddIns\BackendBindings\AnubisBinding\AnubisBinding.addin; DestDir: {app}\AddIns\AddIns\BackendBindings\AnubisBinding; Flags: overwritereadonly ignoreversion; Components: AddIns/Language/Anubis
Source: AddIns\AddIns\BackendBindings\AnubisBinding\Anubis.Build.targets; DestDir: {app}\AddIns\AddIns\BackendBindings\AnubisBinding; Flags: overwritereadonly ignoreversion; Components: AddIns/Language/Anubis
Source: AddIns\AddIns\BackendBindings\AnubisBinding\Anubis.Build.Tasks.dll; DestDir: {app}\AddIns\AddIns\BackendBindings\AnubisBinding; Flags: overwritereadonly ignoreversion; Components: AddIns/Language/Anubis
Source: AddIns\AddIns\BackendBindings\AnubisBinding\AnubisBinding.dll; DestDir: {app}\AddIns\AddIns\BackendBindings\AnubisBinding; Flags: overwritereadonly ignoreversion; Components: AddIns/Language/Anubis
Source: AddIns\AddIns\BackendBindings\AnubisBinding\AnubisParser.dll; DestDir: {app}\AddIns\AddIns\BackendBindings\AnubisBinding; Flags: overwritereadonly ignoreversion; Components: AddIns/Language/Anubis
;Source: AddIns\AddIns\DisplayBindings\ResourceEditor\*.*; DestDir: {app}\AddIns\AddIns\DisplayBindings\ResourceEditor; Excludes: .svn,*.pdb; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Display/ResourceEditor
Source: AddIns\AddIns\DisplayBindings\XmlEditor\*.*; DestDir: {app}\AddIns\AddIns\DisplayBindings\XmlEditor; Excludes: .svn,*.pdb; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Display/XmlEditor
Source: AddIns\AddIns\Misc\AddInManager\*.*; DestDir: {app}\AddIns\AddIns\Misc\AddInManager; Excludes: .svn,*.pdb; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Misc/AddInManager
;Source: AddIns\AddIns\Misc\AddinScout\*.*; DestDir: {app}\AddIns\AddIns\Misc\AddinScout; Excludes: .svn,*.pdb; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Misc/AddinScout
Source: AddIns\AddIns\Misc\FiletypeRegisterer\*.*; DestDir: {app}\AddIns\AddIns\Misc\FiletypeRegisterer; Excludes: .svn,*.pdb; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Misc/FiletypeRegisterer
Source: AddIns\AddIns\Misc\HighlightingEditor\*.*; DestDir: {app}\AddIns\AddIns\Misc\HighlightingEditor; Excludes: .svn,*.pdb; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Misc/HighlightingEditor
Source: AddIns\AddIns\Misc\RegExpTk\*.*; DestDir: {app}\AddIns\AddIns\Misc\RegExpTk; Excludes: .svn,*.pdb; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Misc/RegExpTk
Source: AddIns\AddIns\Misc\StartPage\*.*; DestDir: {app}\AddIns\AddIns\Misc\StartPage; Excludes: .svn,*.pdb; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Misc/StartPage
Source: AddIns\AddIns\Misc\SubversionAddin\*.*; DestDir: {app}\AddIns\AddIns\Misc\SubversionAddin; Excludes: .svn,*.pdb; Flags: overwritereadonly ignoreversion recursesubdirs; Components: AddIns/Misc/Subversion
Source: doc\*.*; DestDir: {app}\doc; Excludes: .svn; Flags: overwritereadonly ignoreversion recursesubdirs
Source: doc\ChangeLog.xml; DestDir: {app}\doc; Flags: overwritereadonly ignoreversion
Source: doc\copyright.txt; DestDir: {app}\doc; Flags: overwritereadonly ignoreversion
Source: doc\LGPL_license.txt; DestDir: {app}\doc; Flags: overwritereadonly ignoreversion
Source: doc\readme.rtf; DestDir: {app}\doc; Flags: overwritereadonly ignoreversion
Source: data\ConversionStyleSheets\*.*; DestDir: {app}\data\ConversionStyleSheets; Excludes: .svn; Flags: overwritereadonly ignoreversion recursesubdirs
Source: data\modes\*.*; DestDir: {app}\data\modes; Excludes: .svn; Flags: overwritereadonly ignoreversion recursesubdirs
Source: data\options\*.*; DestDir: {app}\data\options; Excludes: .svn; Flags: overwritereadonly ignoreversion recursesubdirs
Source: data\resources\*.*; DestDir: {app}\data\resources; Excludes: .svn; Flags: overwritereadonly ignoreversion recursesubdirs
Source: data\schemas\*.*; DestDir: {app}\data\schemas; Excludes: .svn; Flags: overwritereadonly ignoreversion recursesubdirs
Source: data\templates\*.*; DestDir: {app}\data\templates; Excludes: .svn; Flags: overwritereadonly ignoreversion recursesubdirs
Source: setup\dependencies\*.*; DestDir: {tmp}; Excludes: .svn; Flags: overwritereadonly recursesubdirs dontcopy


[Run]
Filename: {ini:{tmp}\dep.ini,install,windowsInstaller31}; Parameters: /passive /norestart; Description: Windows Installer 3.1; Flags: skipifdoesntexist; StatusMsg: "{cm:ExternalSetup, ""Windows Installer 3.1""}"
Filename: {ini:{tmp}\dep.ini,install,dotnet20Redist}; Parameters: "/q:a /c:""install /qb!"""; Description: .NET 2.0 Install; Flags: skipifdoesntexist; StatusMsg: "{cm:ExternalSetup, ""Microsoft .NET 2.0""}"
Filename: {ini:{tmp}\dep.ini,install,dotnet20sdk}; Parameters: "/q:a /c:""install /qb!"""; Description: .NET 2.0 SDK Install; Flags: skipifdoesntexist; StatusMsg: "{cm:ExternalSetup, ""Microsoft .NET 2.0 SDK""}"
Filename: {ini:{tmp}\dep.ini,install,vc8redist}; Parameters: ; Description: Visual Studio 8 Redistribuables Install; Flags: skipifdoesntexist; StatusMsg: "{cm:ExternalSetup, ""Visual Studio 8 Redistribuables""}"
Filename: {ini:{tmp}\dep.ini,install,anubis}; Parameters: /silent; Description: Anubis Language Install; Flags: skipifdoesntexist; StatusMsg: "{cm:ExternalSetup, ""Anubis Language""}"
Filename: {app}\bin\setup\PostInstallTasks.bat; WorkingDir: {app}\bin\setup; StatusMsg: {cm:Compiling}; Flags: runhidden
Filename: {app}\bin\PyramIDE.exe; Description: {cm:LaunchProgram,{#AppName}}; Flags: nowait postinstall skipifsilent; WorkingDir: {app}


; Include ISSI:
;#define ISSI_IncludePath "C:\ISSI"
;#include ISSI_IncludePath+"\_issi.isi"

[Components]
Name: AddIns; Description: AddIns; Types: full custom; Flags: disablenouninstallwarning
Name: AddIns/Language; Description: Languages bindings; Types: full custom; Flags: disablenouninstallwarning
Name: AddIns/Language/Anubis; Description: Anubis Language; Types: full custom compact; Flags: fixed disablenouninstallwarning
Name: AddIns/Display; Description: Editors; Types: full custom; Flags: disablenouninstallwarning
;Name: AddIns/Display/ResourceEditor; Description: AddIns/Display/Resource Editor; Types: full custom; Flags: disablenouninstallwarning
Name: AddIns/Display/XmlEditor; Description: AddIns/Display/Xml Editor; Types: full custom; Flags: disablenouninstallwarning
Name: AddIns/Misc; Description: Miscellaneous; Types: full custom; Flags: disablenouninstallwarning
Name: AddIns/Misc/AddInManager; Description: AddIns/Misc/AddIns Manager; Types: full custom compact; Flags: disablenouninstallwarning
;Name: AddIns/Misc/AddinScout; Description: AddIns/Misc/Addins Scout; Types: full custom; Flags: disablenouninstallwarning
Name: AddIns/Misc/FiletypeRegisterer; Description: AddIns/Misc/Filetype Registerer; Types: full custom compact; Flags: disablenouninstallwarning
Name: AddIns/Misc/HighlightingEditor; Description: AddIns/Misc/Highlighting Syntax Editor; Types: full custom; Flags: disablenouninstallwarning
Name: AddIns/Misc/RegExpTk; Description: AddIns/Misc/RegExpTk; Types: full custom; Flags: disablenouninstallwarning
Name: AddIns/Misc/StartPage; Description: AddIns/Misc/Start Page; Types: full custom; Flags: disablenouninstallwarning
Name: AddIns/Misc/Subversion; Description: AddIns/Misc/Subversion; Types: full custom; Flags: disablenouninstallwarning

[InstallDelete]
Name: {app}\AddIns\*.*; Type: filesandordirs
Name: {app}\bin\*.*; Type: filesandordirs
Name: {app}\doc\*.*; Type: filesandordirs
Name: {app}\data\*.*; Type: filesandordirs
Name: {userappdata}\.SoftArchi\PyramIDE; Type: filesandordirs; Components: ; Tasks: ResetSettings

[INI]
Filename: {app}\softarchi.url; Section: InternetShortcut; Key: URL; String: http://www.softarchi.com/

[UninstallDelete]
Type: files; Name: {app}\softarchi.url

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked
Name: ResetSettings; Description: {cm:ResetAllSettings}; GroupDescription: Settings:; Flags: unchecked


[Icons]
Name: {group}\{#AppName}; Filename: {app}\bin\PyramIDE.exe; WorkingDir: {app}; IconIndex: 0; Comment: {#AppName} {#AppVersion}
Name: {group}\{#AppName} on the Web; Filename: {app}\softarchi.url; WorkingDir: {app}
Name: {userdesktop}\{#AppName}; Filename: {app}\bin\PyramIDE.exe; Tasks: desktopicon; WorkingDir: {app}; IconIndex: 0; Comment: {#AppName} {#AppVersion}
Name: {userappdata}\Microsoft\Internet Explorer\Quick Launch\{#AppName}; Filename: {app}\bin\PyramIDE.exe; Tasks: quicklaunchicon; WorkingDir: {app}; IconIndex: 0; Comment: {#AppName} {#AppVersion}
Name: {group}\{cm:UninstallProgram, {#AppName}}; Filename: {uninstallexe}


[Languages]
;Name: en; MessagesFile: compiler:Default.isl,{#ISSI_IncludePath}\Languages\_issi_English.isl; LicenseFile: doc\Licence-en.rtf
;Name: fr; MessagesFile: compiler:\Languages\French.isl,{#ISSI_IncludePath}\Languages\_issi_French.isl; LicenseFile: doc\Licence-fr.rtf
Name: en; MessagesFile: compiler:Default.isl; LicenseFile: doc\Licence-en.rtf
Name: fr; MessagesFile: compiler:\Languages\French.isl; LicenseFile: doc\Licence-fr.rtf

[_ISTool]
UseAbsolutePaths=false
EnableISX=true

[CustomMessages]
en.Dependencies=Dependencies to install:
fr.Dependencies=Logiciels pr�-requis � installer :
en.Compiling=Compiling {#AppName} to native code to improve startup time...
fr.Compiling=Compilation de {#AppName} en code natif afin d'am�liorer les temps de lancement...
en.ResetAllSettings=Reset all user settings
fr.ResetAllSettings=R�initialiser toutes les pr�f�rences utilisateur
en.ExternalSetup=Installing %1... (This may take a few minutes)
fr.ExternalSetup=Installation de %1... (Cela peut prendre quelques minutes)
en.CheckingAnubisVersion=Checking for the last Anubis Language version...
fr.CheckingAnubisVersion=Recherche de la derni�re version du Langage Anubis...
[Code]
//#include "isxdl.iss"

var
  iePath, windowsInstaller31Path, dotnet20RedistPath, dotnet20SdkPath, vc8redistPath: string;
  anubisVersionPath, anubisPath, anubisSetupFile: string;
  downloadNeeded: boolean;
  hWnd: Integer;
  versionMS, versionLS: Cardinal;

  memoDependenciesNeeded: string;

const
	windowsInstaller31URL = 'http://www.softarchi.com/redist/WindowsInstaller-KB893803-v2-x86.exe';
	vc8redistURL = 'http://www.softarchi.com/redist/vcredist_x86.exe';
  dotnet20RedistURL = 'http://download.microsoft.com/download/5/6/7/567758a3-759e-473e-bf8f-52154438565a/dotnetfx.exe';
  dotnet20SdkURL = 'http://download.microsoft.com/download/c/4/b/c4b15d7d-6f37-4d5a-b9c6-8f07e7d46635/setup.exe';
  anubisURL = 'http://www.anubis-language.com/release/anubis_last_release_version.txt';
  anubisBaseURL = 'http://www.anubis-language.com/release/';

function InitializeSetup(): Boolean;
var
  sRet: string;
  nRet: integer;

begin
  Result := true;

  // Check for required Anubis installation (should be first)
//  if (not RegKeyExists(HKCR, 'Anubis module')) then begin
//    hWnd := StrToInt(ExpandConstant('{wizardhwnd}'));
//    anubisVersionPath := ExpandConstant('{tmp}\AnubisLastVersion.ini');
//    isxdl_SetOption('simple', ExpandConstant('{cm:CheckingAnubisVersion}'));
//    if isxdl_Download(hWnd, anubisURL, anubisVersionPath) = 0 then
//			isxdl_ClearFiles();
//    isxdl_SetOption('simple', '');
//		anubisSetupFile := GetIniString('x86-win32', 'file', 'Anubis_1_7_0_0_x86-win32.exe', anubisVersionPath);
//		memoDependenciesNeeded := memoDependenciesNeeded + '      Anubis Language' #13;
//		anubisPath := ExtractFileDrive(ExpandConstant('{src}')) + '\dependencies\' + anubisSetupFile;
//		if not FileExists(anubisPath) then begin
//			anubisPath := ExpandConstant('{tmp}\' + anubisSetupFile);
//			if not FileExists(anubisPath) then begin
//				isxdl_AddFile(anubisBaseURL + anubisSetupFile, anubisPath);
//				downloadNeeded := true;
//			end;
//		end;
//		SetIniString('install', 'anubis', anubisPath, ExpandConstant('{tmp}\dep.ini'));
//  end;

  // Check for required Windows Installer 3.1 installation
//  if (not GetVersionNumbers(ExpandConstant('{sys}') + '\MSI.DLL', versionMS, versionLS)) then
//		versionMS := 0;
//	if (versionMS < $30000) then begin
//    memoDependenciesNeeded := memoDependenciesNeeded + '      Windows Installer 3.1' #13;
//    windowsInstaller31Path := ExtractFileDrive(ExpandConstant('{src}')) + '\dependencies\WindowsInstaller-KB893803-v2-x86.exe';
//    if not FileExists(windowsInstaller31Path) then begin
//      windowsInstaller31Path := ExpandConstant('{tmp}\WindowsInstaller-KB893803-v2-x86.exe');
//      if not FileExists(windowsInstaller31Path) then begin
//        isxdl_AddFile(windowsInstaller31URL, windowsInstaller31Path);
//        downloadNeeded := true;
//      end;
//    end;
//    SetIniString('install', 'windowsInstaller31', windowsInstaller31Path, ExpandConstant('{tmp}\dep.ini'));
//  end;

//  // Check for required netfx 2.0 installation
//  if (not RegKeyExists(HKLM, 'Software\Microsoft\.NETFramework\policy\v2.0')) then begin
//    memoDependenciesNeeded := memoDependenciesNeeded + '      .NET Framework 2.0' #13;
//    dotnet20RedistPath := ExtractFileDrive(ExpandConstant('{src}')) + '\dependencies\dotnetfx.exe';
//    if not FileExists(dotnet20RedistPath) then begin
//      dotnet20RedistPath := ExpandConstant('{tmp}\dotnetfx.exe');
//      if not FileExists(dotnet20RedistPath) then begin
//        isxdl_AddFile(dotnet20RedistURL, dotnet20RedistPath);
//        downloadNeeded := true;
//      end;
//    end;
//    SetIniString('install', 'dotnet20Redist', dotnet20RedistPath, ExpandConstant('{tmp}\dep.ini'));
//  end;

//  // Check for required netfx SDK 2.0 installation
//  if (not RegValueExists(HKLM, 'Software\Microsoft\.NETFramework', 'sdkInstallRootv2.0')) then begin
//    memoDependenciesNeeded := memoDependenciesNeeded + '      .NET Framework 2.0 SDK' #13;
//    dotnet20SdkPath := ExtractFileDrive(ExpandConstant('{src}')) + '\dependencies\dotnetsdk.exe';
//    if not FileExists(dotnet20SdkPath) then begin
//      dotnet20SdkPath := ExpandConstant('{tmp}\dotnetsdk.exe');
//      if not FileExists(dotnet20SdkPath) then begin
//        isxdl_AddFile(dotnet20SdkURL, dotnet20SdkPath);
//        downloadNeeded := true;
//      end;
//    end;
//    SetIniString('install', 'dotnet20Sdk', dotnet20SdkPath, ExpandConstant('{tmp}\dep.ini'));
//  end;

  // Check for required Visual Studio 8 redistribuables
//  if (not FileExists(ExpandConstant('{win}') + '\WinSxS\x86_Microsoft.VC80.CRT_1fc8b3b9a1e18e3b_8.0.50727.762_none_10b2f55f9bffb8f8\msvcr80.dll')) then begin
//    memoDependenciesNeeded := memoDependenciesNeeded + '      Visual Studio 8 Redistribuables' #13;
//    vc8redistPath := ExtractFileDrive(ExpandConstant('{src}')) + '\dependencies\vcredist_x86.exe';
//    if not FileExists(vc8redistPath) then begin
//      vc8redistPath := ExpandConstant('{tmp}\vcredist_x86.exe');
//      if not FileExists(vc8redistPath) then begin
//        isxdl_AddFile(vc8redistURL, vc8redistPath);
//        downloadNeeded := true;
//      end;
//    end;
//    SetIniString('install', 'vc8redist', vc8redistPath, ExpandConstant('{tmp}\dep.ini'));
//  end;

//  sRet := '';
//  RegQueryStringValue(HKLM, 'Software\Microsoft\Internet Explorer', 'Version', sRet);
//  if downloadNeeded and (sRet < '3') then begin
//    // Downloads are needed and isxdl can't initialize to download them, abort
//    Result := false;
//    if FileExists(iePath) then begin
//      MsgBox('Internet Explorer 5 (or later) must be installed manually before Setup can continue.' #13#13 'Setup will now exit and begin the IE 6 installation.', mbInformation, MB_OK);
//      ShellExec('open', iePath, '', '', SW_SHOW, ewNoWait, nRet);
//    end else begin
//      if MsgBox('Internet Explorer 5 (or later) must be installed manually before Setup can continue.' #13#13 'Would you like to visit the download site now?', mbConfirmation, MB_OKCANCEL) = IDOK then
//        ShellExec('open', 'http://www.microsoft.com/ie', '', '', SW_SHOW, ewNoWait, nRet);
//    end;
//  end;
end;


function NextButtonClick(CurPage: Integer): Boolean;
var
  hWnd: Integer;

begin
  Result := true;

  if CurPage = wpReady then begin

    hWnd := StrToInt(ExpandConstant('{wizardhwnd}'));

    // don't try to init isxdl if it's not needed because it will error on < ie 3
//    if downloadNeeded then
//      if isxdl_DownloadFiles(hWnd) = 0 then Result := false;
  end;
end;

function UpdateReadyMemo(Space, NewLine, MemoUserInfoInfo, MemoDirInfo, MemoTypeInfo, MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: String): String;
var
  s: string;

begin
  if memoDependenciesNeeded <> '' then s := s + ExpandConstant('{cm:Dependencies}') + NewLine + memoDependenciesNeeded + NewLine;
  if MemoUserInfoInfo <> '' then s := s + MemoUserInfoInfo + NewLine + NewLine;
  if MemoDirInfo <> '' then s := s + MemoDirInfo + NewLine + NewLine;
  if MemoTypeInfo <> '' then s := s + MemoTypeInfo + NewLine + NewLine;
  if MemoComponentsInfo <> '' then s := s + MemoComponentsInfo + NewLine + NewLine;
  if MemoGroupInfo <> '' then s := s + MemoGroupInfo + NewLine + NewLine;
  if MemoTasksInfo <> '' then s := s + MemoTasksInfo + NewLine + NewLine;

  Result := s
end;

[InnoIDE_Settings]
LogFileOverwrite=false

