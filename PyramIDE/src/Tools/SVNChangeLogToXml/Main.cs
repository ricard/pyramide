using System;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.Reflection;
using System.Windows.Forms;
using System.IO;
using NSvn.Common;
using NSvn.Core;

class MainClass
{
	public static int Main(string[] args)
	{
		Console.WriteLine("Initializing changelog application...");
		try {
			if (!File.Exists("PyramIDE.sln"))
			{
				if (File.Exists(@"..\..\..\..\PyramIDE.sln"))
				{
					Directory.SetCurrentDirectory(@"..\..\..\..");
				}
				if (File.Exists("..\\src\\PyramIDE.sln"))
				{
					Directory.SetCurrentDirectory("..\\src");
				}
			}
			if (!File.Exists("PyramIDE.sln"))
			{
				Console.WriteLine("Working directory must be PyramIDE\\src or PyramIDE\\bin!");
				return 2;
			}
			int rev = 1, start = 2;
			for(int i = 0; i < args.Length; i++)
			{
				if(args[i] == "--REVISION") {
					rev = CreateRevisionFile();
				}
				else if(args[i] == "--START") {
					Int32.TryParse(args[i + 1], out start);
				}
			}
			ConvertChangeLog(rev, start);
			return 0;
		} catch (Exception ex) {
			Console.WriteLine(ex);
			return 1;
		}
	}
	
	static int CreateRevisionFile()
	{
		Console.Write("Writing revision to file: ");
		Client client = new Client();
		InitAuthentificationProviders(client);
		client.Update(".", Revision.Unspecified, Recurse.None);
		int rev = client.SingleStatus(".").Entry.Revision;
		Console.WriteLine(rev);
		using (StreamWriter writer = new StreamWriter("../REVISION")) {
			writer.Write(rev.ToString());
		}
		return rev;
	}
	
	static void ConvertChangeLog(int rev, int startRevision)
	{
		Console.WriteLine("Reading SVN changelog, this might take a while...");
		
		Client client = new Client();
		InitAuthentificationProviders(client);
		
		StringWriter writer = new StringWriter();
		XmlTextWriter xmlWriter = new XmlTextWriter(writer);
		xmlWriter.Formatting = Formatting.Indented;
		xmlWriter.WriteStartDocument();
		xmlWriter.WriteStartElement("log");
		client.Log(new string[] { ".." }, Revision.FromNumber(rev), Revision.FromNumber(startRevision), false, false,
							 delegate(LogMessage message)
							 {
								 if (message.Message.Length > 0)
								 {
									 xmlWriter.WriteStartElement("logentry");
									 xmlWriter.WriteAttributeString("revision", message.Revision.ToString(System.Globalization.CultureInfo.InvariantCulture));
									 xmlWriter.WriteElementString("author", message.Author);
									 xmlWriter.WriteElementString("date", message.Date.ToString("yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture));
									 xmlWriter.WriteElementString("msg", message.Message.Length > 0 ? message.Message : "no message");
									 xmlWriter.WriteEndElement();
								 }
							 });
		xmlWriter.WriteEndDocument();
		
		//Console.WriteLine(writer);
		
		{
		XmlTextReader input = new XmlTextReader(new StringReader(writer.ToString()));
		
		XslCompiledTransform xsl = new XslCompiledTransform();
		xsl.Load(@"..\data\ConversionStyleSheets\SVNChangelogToXml.xsl");
		
		StreamWriter tw = new StreamWriter(@"..\doc\ChangeLog.xml", false, Encoding.UTF8);
		xmlWriter = new XmlTextWriter(tw);
		xmlWriter.Formatting = Formatting.Indented;
		xsl.Transform(input, xmlWriter);
		xmlWriter.Close();
		tw.Close();
		}
		{
			XslCompiledTransform xsl = new XslCompiledTransform();
			xsl.Load(@"..\data\ConversionStyleSheets\ShowChangeLog.xsl");
			StreamWriter tw = new StreamWriter(@"..\doc\ChangeLog.html", false, Encoding.UTF8);
			xmlWriter = new XmlTextWriter(tw);
			xmlWriter.Formatting = Formatting.None;
			xsl.Transform(@"..\doc\ChangeLog.xml", xmlWriter);
			xmlWriter.Close();
			tw.Close();
			
//			changeLogHtml = writer.ToString().Replace("\n", "\n<br>");
		}
		Console.WriteLine("Finished");
	}

	private static void InitAuthentificationProviders(Client client)
	{
		client.AuthBaton.Add(AuthenticationProvider.GetWindowsSimpleProvider());
		client.AuthBaton.Add(AuthenticationProvider.GetUsernameProvider());
		client.AuthBaton.Add(AuthenticationProvider.GetSimpleProvider());
		//		client.AuthBaton.Add(AuthenticationProvider.GetSimplePromptProvider(PasswordPrompt, 3));
		//client.AuthBaton.Add(AuthenticationProvider.GetSslServerTrustFileProvider());
		//client.AuthBaton.Add(AuthenticationProvider.GetSslServerTrustPromptProvider(
		//    new SslServerTrustPromptDelegate(client.SslServerTrustPrompt)));
		//client.AuthBaton.Add(
		//    AuthenticationProvider.GetSslClientCertPasswordFileProvider());
		//client.AuthBaton.Add(
		//    AuthenticationProvider.GetSslClientCertPasswordPromptProvider(
		//    new SslClientCertPasswordPromptDelegate(
		//    client.ClientCertificatePasswordPrompt), 3));
		//client.AuthBaton.Add(
		//    AuthenticationProvider.GetSslClientCertFileProvider());
		//client.AuthBaton.Add(
		//    AuthenticationProvider.GetSslClientCertPromptProvider(
		//    new SslClientCertPromptDelegate(client.ClientCertificatePrompt), 3));
	}
	
	static SimpleCredential PasswordPrompt(string realm, string userName, bool maySave)
	{
		Console.WriteLine();
		Console.WriteLine("SUBVERSION: Authentication for realm: " + realm);
		Console.Write("Username: ");
		userName = Console.ReadLine();
		Console.Write("Password: ");
		string pwd = Console.ReadLine();
		return new SimpleCredential(userName, pwd, maySave);
	}
}
