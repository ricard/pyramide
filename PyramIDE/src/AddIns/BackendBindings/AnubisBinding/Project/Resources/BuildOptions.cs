// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Daniel Grunwald" email="daniel@danielgrunwald.de"/>
//     <version>$Revision: 1228 $</version>
// </file>

using System;
using System.Collections.Generic;
using System.Windows.Forms;

using ICSharpCode.SharpDevelop.Project;
using ICSharpCode.Core;
using ICSharpCode.SharpDevelop;
using ICSharpCode.SharpDevelop.Gui;
using ICSharpCode.SharpDevelop.Gui.XmlForms;
using ICSharpCode.SharpDevelop.Gui.OptionPanels;

using StringPair = System.Collections.Generic.KeyValuePair<string, string>;

namespace SoftArchitect.AnubisBinding.Ressources
{
	public class BuildOptions : AbstractBuildOptions
	{
		public override void LoadPanelContents()
		{
			SetupFromXmlResource("BuildOptions.xfrm");
			InitializeHelper();

			InitBaseIntermediateOutputPath();
			InitIntermediateOutputPath();
			InitOutputPath();
			InitXmlDoc();

			ConfigurationGuiBinding b;

			b = helper.BindString("commandLineTextBox", "CommandLine", TextBoxEditMode.EditRawProperty);
			b.DefaultLocation = PropertyStorageLocations.ConfigurationSpecific;
			b.CreateLocationButton("commandLineTextBox");

			b = helper.BindBoolean("makeUsedFileCheckBox", "MakeUsedFile", false);
			b.CreateLocationButton("makeUsedFileCheckBox");

			b = helper.BindBoolean("makeUnusedFileCheckBox", "MakeUnusedFile", false);
			b.CreateLocationButton("makeUnusedFileCheckBox");

			b = helper.BindBoolean("verboseCheckBox", "AnubiscVerbosity", false);
			b.CreateLocationButton("verboseCheckBox");

			ConnectBrowseFolder("browseAnubisFolderButton", "anubisFolderTextBox", TextBoxEditMode.EditRawProperty);
			ConnectBrowseFolder("browseMyAnubisFolderButton", "myAnubisFolderTextBox", TextBoxEditMode.EditRawProperty);

			b = helper.BindString("anubisFolderTextBox", "AnubisPath", TextBoxEditMode.EditRawProperty);
			b.CreateLocationButton("anubisFolderTextBox");

			b = helper.BindString("myAnubisFolderTextBox", "MyAnubisPath", TextBoxEditMode.EditRawProperty);
			b.CreateLocationButton("myAnubisFolderTextBox");


			helper.AddConfigurationSelector(this);
		}
	}
}
