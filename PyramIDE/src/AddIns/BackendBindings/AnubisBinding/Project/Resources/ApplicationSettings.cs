/*
 * Created by SharpDevelop.
 * User: ricard
 * Date: 20/06/2006
 * Time: 21:12
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ICSharpCode.SharpDevelop.Gui.OptionPanels;
using ICSharpCode.SharpDevelop.Internal.ExternalTool;
using ICSharpCode.Core;
using ICSharpCode.SharpDevelop.Gui;
using ICSharpCode.SharpDevelop.Project;
using System.IO;

namespace SoftArchitect.AnubisBinding.Ressources
{
	/// <summary>
	/// Summary of ApplicationSettings
	/// </summary>
	public class ApplicationSettings : AbstractProjectOptionPanel
	{
		public override void LoadPanelContents()
		{
			SetupFromXmlResource("ApplicationSettings.xfrm");

			InitializeHelper();

			ConfigurationGuiBinding b;

			Get<TextBox>("assemblyName").TextChanged += new EventHandler(RefreshOutputNameTextBox);
			b = helper.BindString("assemblyNameTextBox", "AssemblyName", TextBoxEditMode.EditRawProperty);
			b.CreateLocationButton("assemblyNameTextBox");

			ComboBox mainSourceFileComboBox = Get<ComboBox>("mainSourceFile");
			mainSourceFileComboBox.Items.Clear();
			foreach (ProjectItem item in project.Items)
			{
				if (item.ItemType == ItemType.Compile && Path.GetExtension(item.FileName.ToLower()) == ".anubis")
					mainSourceFileComboBox.Items.Add(item.Include);
			}
			b = helper.BindString("mainSourceFileComboBox", "StartupObject", TextBoxEditMode.EditRawProperty);
			b.CreateLocationButton("mainSourceFileComboBox");

			Get<TextBox>("projectFolder").Text = project.Directory;
			Get<TextBox>("projectFile").Text = Path.GetFileName(project.FileName);
		}

		void RefreshOutputNameTextBox(object sender, EventArgs e)
		{
			Get<TextBox>("outputName").Text = Get<TextBox>("assemblyName").Text + ".adm";
		}
	}
}
