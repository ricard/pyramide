using System;
using System.Collections.Generic;
using System.Text;
using ICSharpCode.Core;
using ICSharpCode.SharpDevelop.Gui;
using ICSharpCode.SharpDevelop.Gui.OptionPanels;

namespace SoftArchitect.AnubisBinding.Ressources
{
	public class IncludePaths : AbstractProjectOptionPanel
	{
		public override void LoadPanelContents()
		{
			InitializeHelper();

			StringListEditor editor = new StringListEditor();
			editor.BrowseForDirectory = true;
			editor.ListCaption = StringParser.Parse("&${res:Dialog_ProjectOptions_IncludePaths}:");
			editor.TitleText = StringParser.Parse("&${res:Dialog.ExportProjectToHtml.FolderLabel}");
			editor.AddButtonText = StringParser.Parse("${res:Dialog.ProjectOptions.ReferencePaths.AddPath}");
			editor.ListChanged += delegate { IsDirty = true; };
			ReferencePaths.SemicolonSeparatedStringListBinding b = new ReferencePaths.SemicolonSeparatedStringListBinding(editor);
			helper.AddBinding("IncludePaths", b);
			this.Controls.Add(editor);
			b.CreateLocationButton(editor);

			helper.AddConfigurationSelector(this);
		}

	}
}
