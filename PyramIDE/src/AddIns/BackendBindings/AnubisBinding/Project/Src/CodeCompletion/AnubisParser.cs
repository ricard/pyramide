// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Daniel Grunwald" email="daniel@danielgrunwald.de"/>
//     <version>$Revision: 1392 $</version>
// </file>

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ICSharpCode.Core;
using ICSharpCode.SharpDevelop.Dom;
using ICSharpCode.SharpDevelop.Project;
using SoftArchitect.AnubisBinding;
using SoftArchitect.AnubisParser;

namespace SoftArchitect.AnubisBinding.CodeCompletion
{
	public class AnubisParser : IParser
	{
		string[] lexerTags;
		
		public string[] LexerTags {
			get {
				return lexerTags;
			}
			set {
				lexerTags = value;
			}
		}
		
		public LanguageProperties Language {
			get {
				return AnubisLanguageProperties.Instance;
			}
		}
		
		public IExpressionFinder CreateExpressionFinder(string fileName)
		{
			return new ExpressionFinder(fileName);
		}
		
		public bool CanParse(string fileName)
		{
			return string.Equals(Path.GetExtension(fileName), ".boo", StringComparison.InvariantCultureIgnoreCase);
		}

		public bool CanParse(IProject project)
		{
			return project.Language == AnubisLanguageBinding.LanguageName;
		}

		public ICompilationUnit Parse(IProjectContent projectContent, string fileName)
		{
			string content;
			using (StreamReader reader = new StreamReader(fileName)) {
				content = reader.ReadToEnd();
			}
			return Parse(projectContent, fileName, content);
		}
		
		public ICompilationUnit Parse(IProjectContent projectContent, string fileName, string fileContent)
		{
			LoggingService.Debug("Parse " + fileName);
			ParserEngine parser = new ParserEngine(new StringReader(fileContent), fileName, projectContent);
			ICompilationUnit cu = parser.DoParse();
			AddCommentsAndRegions(cu, fileContent, fileName);
			return cu;
		}
		
		void AddCommentsAndRegions(ICompilationUnit cu, string fileContent, string fileName)
		{
			ExpressionFinder ef = new ExpressionFinder(fileName);
			ef.ResetStateMachine();
			int state = 0;
			StringBuilder commentBuilder = null;
			char commentStartChar = '\0';
			int commentStartColumn = 0;
			
			Stack<string> regionTitleStack  = new Stack<string>();
			Stack<int>    regionLineStack   = new Stack<int>();
			Stack<int>    regionColumnStack = new Stack<int>();
			
			int line = 1;
			int column = 0;
			for (int i = 0; i < fileContent.Length; i++) {
				column += 1;
				char c = fileContent[i];
				if (c == '\n') {
					column = 0;
					line += 1;
				}
				state = ef.FeedStateMachine(state, c);
				if (state == ExpressionFinder.LineCommentState) 
				{
					if (commentBuilder == null) {
						commentStartChar = c;
						commentStartColumn = column;
						commentBuilder = new StringBuilder();
					} else {
						if (commentBuilder.Length > 0) {
							commentBuilder.Append(c);
						} else if (!char.IsWhiteSpace(c)) {
							commentStartColumn = column - 1;
							commentBuilder.Append(c);
						}
					}
				} 
				else if (commentBuilder != null) 
				{
					string text = commentBuilder.ToString();
					commentBuilder = null;
					if (commentStartChar == '/' && text.StartsWith("region ")) 
					{
						regionTitleStack.Push(text.Substring(7));
						regionLineStack.Push(line);
						regionColumnStack.Push(commentStartColumn - 1);
					} 
					else if (commentStartChar == '/' && text.StartsWith("endregion") && regionTitleStack.Count > 0) 
					{
						// Add folding region
						cu.FoldingRegions.Add(new FoldingRegion(regionTitleStack.Pop(),
						                                        new DomRegion(regionLineStack.Pop(),
						                                                      regionColumnStack.Pop(),
						                                                      line, column)));
					}
					else 
					{
						foreach (string tag in lexerTags) {
							if (text.StartsWith(tag)) {
								TagComment tagComment = new TagComment(tag, new DomRegion(line, commentStartColumn));
								tagComment.CommentString = text.Substring(tag.Length);
								cu.TagComments.Add(tagComment);
								break;
							}
						}
					}
				}
			}
		}
				
		public IResolver CreateResolver()
		{
			return null;// new AnubisResolver();
		}
	}
}
