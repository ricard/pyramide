// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Daniel Grunwald" email="daniel@danielgrunwald.de"/>
//     <version>$Revision: 1389 $</version>
// </file>

using System;
using System.Collections;
using System.Collections.Generic;
using ICSharpCode.Core;
using ICSharpCode.SharpDevelop;
using ICSharpCode.SharpDevelop.Dom;
using NRResolver = ICSharpCode.SharpDevelop.Dom.NRefactoryResolver.NRefactoryResolver;

namespace SoftArchitect.AnubisBinding.CodeCompletion
{
	public class AnubisResolver : IResolver
	{
		#region Fields and properties
		ICompilationUnit _cu;
		IProjectContent _pc;
		int _caretLine;
		int _caretColumn;
		IClass _callingClass;
		IMethodOrProperty _callingMember;
		
		public IClass CallingClass {
			get {
				return _callingClass;
			}
		}
		
		public IMethodOrProperty CallingMember {
			get {
				return _callingMember;
			}
		}
		
		public int CaretLine {
			get {
				return _caretLine;
			}
		}
		
		public int CaretColumn {
			get {
				return _caretColumn;
			}
		}
		
		public IProjectContent ProjectContent {
			get {
				return _pc;
			}
		}
		
		public ICompilationUnit CompilationUnit {
			get {
				return _cu;
			}
		}
		
		#endregion
		
		#region Initialization
		bool Initialize(string fileName, int caretLine, int caretColumn)
		{
			ParseInformation parseInfo = ParserService.GetParseInformation(fileName);
			if (parseInfo == null) {
				return false;
			}
			this._cu = parseInfo.MostRecentCompilationUnit;
			if (_cu == null) {
				return false;
			}
			this._pc = _cu.ProjectContent;
			this._caretLine = caretLine;
			this._caretColumn = caretColumn;
			this._callingClass = GetCallingClass(_pc);
			_callingMember = ResolveCurrentMember(_callingClass);
			if (_callingMember == null) {
				if (_cu != parseInfo.BestCompilationUnit) {
					IClass olderClass = GetCallingClass(parseInfo.BestCompilationUnit.ProjectContent);
					if (olderClass != null && _callingClass == null) {
						this._callingClass = olderClass;
					}
					_callingMember = ResolveCurrentMember(olderClass);
				}
			}
			return true;
		}
		
		IClass GetCallingClass(IProjectContent pc)
		{
			IClass callingClass = _cu.GetInnermostClass(_caretLine, _caretColumn);
			if (callingClass == null) {
				if (_cu.Classes.Count == 0) return null;
				callingClass = _cu.Classes[_cu.Classes.Count - 1];
				if (!callingClass.Region.IsEmpty) {
					if (callingClass.Region.BeginLine > _caretLine)
						callingClass = null;
				}
			}
			return callingClass;
		}
		
		IMethodOrProperty ResolveCurrentMember(IClass callingClass)
		{
			//LoggingService.DebugFormatted("Getting current method... _caretLine = {0}, _caretColumn = {1}", _caretLine, _caretColumn);
			if (callingClass == null) return null;
			IMethodOrProperty best = null;
			int line = 0;
			foreach (IMethod m in callingClass.Methods) {
				if (m.Region.BeginLine <= _caretLine && m.Region.BeginLine > line) {
					line = m.Region.BeginLine;
					best = m;
				}
			}
			foreach (IProperty m in callingClass.Properties) {
				if (m.Region.BeginLine <= _caretLine && m.Region.BeginLine > line) {
					line = m.Region.BeginLine;
					best = m;
				}
			}
			if (callingClass.Region.IsEmpty) {
				// maybe we are in Main method?
				foreach (IMethod m in callingClass.Methods) {
					if (m.Region.IsEmpty && !m.IsSynthetic) {
						// the main method
						if (best == null || best.BodyRegion.EndLine < _caretLine)
							return m;
					}
				}
			}
			return best;
		}
		#endregion
		
		#region Resolve
		public ResolveResult Resolve(ExpressionResult expressionResult,
		                             int caretLineNumber, int caretColumn,
		                             string fileName, string fileContent)
		{
			if (!Initialize(fileName, caretLineNumber, caretColumn))
				return null;
			
			// TODO This function needs a full parsing to known exactly the expression point to.
			LoggingService.Debug("Resolve " + expressionResult.ToString());
			if (expressionResult.Expression == "__GlobalNamespace") { // used for "import" completion
				return new NamespaceResolveResult(_callingClass, _callingMember, "");
			}
			
			ResolveResult rr = NRResolver.GetResultFromDeclarationLine(_callingClass, _callingMember as IMethodOrProperty, _caretLine, caretColumn, expressionResult.Expression);
			if (rr != null) 
				return rr;

			foreach (IClass c in _cu.Classes)
			{
				DomRegion region = c.Region;
				if(caretLineNumber >= c.Region.BeginLine && caretLineNumber <= c.Region.EndLine)
				{
					foreach (IMethod method in c.Methods)
					{
						if(caretLineNumber >= method.Region.BeginLine && caretLineNumber <= method.Region.EndLine)
						{}
						else if(caretLineNumber >= method.BodyRegion.BeginLine && caretLineNumber <= method.BodyRegion.EndLine)
						{}
						
					}
				}
			}
			
			//if (expr is AST.IntegerLiteralExpression)
			//  return new IntegerLiteralResolveResult(_callingClass, _callingMember);

			//try
			//{
			//  string name = expressionResult.Expression;
			//  IReturnType rt = _pc.SearchType(new SearchTypeRequest(name, 0, _callingClass, _cu, _caretLine, caretColumn)).Result;
			//  if (rt != null && rt.GetUnderlyingClass() != null)
			//    return new TypeResolveResult(_callingClass, _callingMember, rt);
			//  rt = _pc.SearchType(new SearchTypeRequest(name, 0, _callingClass, _cu, _caretLine, caretColumn)).Result;
			//  if (rt != null && rt.GetUnderlyingClass() != null)
			//    return new TypeResolveResult(_callingClass, _callingMember, rt);
			//} else {
			//  if (expr.NodeType == AST.NodeType.ReferenceExpression) {
			//    // this could be a macro
			//    if (AnubisProject.AnubisCompilerPC != null)
			//    {
			//      string name = ((AST.ReferenceExpression)expr).Name;
			//      IClass c = AnubisProject.AnubisCompilerPC.GetClass("Anubis.Lang." + char.ToUpper(name[0]) + name.Substring(1) + "Macro");
			//      if (c != null)
			//        return new TypeResolveResult(_callingClass, _callingMember, c);
			//    }
			//  }
			//}
			
			ResolveVisitor visitor = new ResolveVisitor(this);
			visitor.Visit(expr);
			ResolveResult result = visitor.ResolveResult;
			if (expressionResult.Context == ExpressionContext.Type && result is MixedResolveResult)
				result = (result as MixedResolveResult).TypeResult;
			return result;
		}
				
		#endregion
		
		#region CtrlSpace
		static IClass GetPrimitiveClass(IProjectContent pc, string systemType, string newName)
		{
			IClass c = pc.GetClass(systemType);
			if (c == null) {
				LoggingService.Warn("Could not find " + systemType);
				return null;
			}
			DefaultClass c2 = new DefaultClass(c.CompilationUnit, newName);
			c2.ClassType = c.ClassType;
			c2.Modifiers = c.Modifiers;
			c2.Documentation = c.Documentation;
			c2.BaseTypes.AddRange(c.BaseTypes);
			c2.Methods.AddRange(c.Methods);
			c2.Fields.AddRange(c.Fields);
			c2.Properties.AddRange(c.Properties);
			c2.Events.AddRange(c.Events);
			return c2;
		}
		
		public ArrayList CtrlSpace(int caretLine, int caretColumn, string fileName, string fileContent, ExpressionContext context)
		{
			ArrayList result = new ArrayList();
			
			if (!Initialize(fileName, caretLine, caretColumn))
				return null;
			if (context == ExpressionContext.Importable) {
				_pc.AddNamespaceContents(result, "", _pc.Language, true);
				NRResolver.AddUsing(result, _pc.DefaultImports, _pc);
				return result;
			}
			
			NRResolver.AddContentsFromCalling(result, _callingClass, _callingMember);
			AddImportedNamespaceContents(result);

			if (AnubisProject.AnubisCompilerPC != null)
			{
				if (context == ExpressionFinder.AnubisAttributeContext.Instance)
				{
					foreach (object o in AnubisProject.AnubisCompilerPC.GetNamespaceContents("Anubis.Lang"))
					{
						IClass c = o as IClass;
						if (c != null && c.Name.EndsWith("Attribute") && !c.IsAbstract) {
							result.Add(GetPrimitiveClass(AnubisProject.AnubisCompilerPC, c.FullyQualifiedName, c.Name.Substring(0, c.Name.Length - 9).ToLowerInvariant()));
						}
					}
				} else {
					foreach (object o in AnubisProject.AnubisCompilerPC.GetNamespaceContents("Anubis.Lang"))
					{
						IClass c = o as IClass;
						if (c != null && c.Name.EndsWith("Macro") && !c.IsAbstract) {
							result.Add(GetPrimitiveClass(AnubisProject.AnubisCompilerPC, c.FullyQualifiedName, c.Name.Substring(0, c.Name.Length - 5).ToLowerInvariant()));
						}
					}
				}
			}
			
			List<string> knownVariableNames = new List<string>();
			foreach (object o in result) {
				IMember m = o as IMember;
				if (m != null) {
					knownVariableNames.Add(m.Name);
				}
			}
			VariableListLookupVisitor vllv = new VariableListLookupVisitor(knownVariableNames, this);
			vllv.Visit(GetCurrentAnubisMethod());
			foreach (KeyValuePair<string, IReturnType> entry in vllv.Results) {
				result.Add(new DefaultField.LocalVariableField(entry.Value, entry.Key, DomRegion.Empty, _callingClass));
			}
			
			return result;
		}
		
		// used by ctrl+space and resolve visitor (resolve identifier)
		public ArrayList GetImportedNamespaceContents()
		{
			ArrayList result = new ArrayList();
			AddImportedNamespaceContents(result);
			return result;
		}
		
		void AddImportedNamespaceContents(ArrayList list)
		{
			IClass c;
			foreach (KeyValuePair<string, string> pair in AnubisAmbience.ReverseTypeConversionTable)
			{
				c = GetPrimitiveClass(_pc, pair.Value, pair.Key);
				if (c != null) list.Add(c);
			}
			list.Add(new DuckClass(_cu));
			NRResolver.AddImportedNamespaceContents(list, _cu, _callingClass);
		}
		
		internal class DuckClass : DefaultClass
		{
			public DuckClass(ICompilationUnit cu) : base(cu, "duck")
			{
				Documentation = "Use late-binding to access members of this type.<br/>\n'If it walks like a duck and quacks like a duck, it must be a duck.'";
				Modifiers = ModifierEnum.Public;
			}
			
			protected override IReturnType CreateDefaultReturnType()
			{
				return new DuckReturnType(this);
			}
		}
		
		internal class DuckReturnType : AbstractReturnType
		{
			IClass c;
			public DuckReturnType(IClass c) {
				this.c = c;
				FullyQualifiedName = c.FullyQualifiedName;
			}
			public override IClass GetUnderlyingClass() {
				return c;
			}
			public override List<IMethod> GetMethods() {
				return new List<IMethod>();
			}
			public override List<IProperty> GetProperties() {
				return new List<IProperty>();
			}
			public override List<IField> GetFields() {
				return new List<IField>();
			}
			public override List<IEvent> GetEvents() {
				return new List<IEvent>();
			}
		}
		#endregion
	}
}
