#region Using directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using GoldParser;
using ICSharpCode.SharpDevelop.Dom;

#endregion

namespace SoftArchitect.AnubisBinding.Parser
{
	/// <summary>
	/// Summary description for SimpleModule.
	/// </summary>
	internal class Context
	{
		private GoldParser.Parser m_parser;
		ICompilationUnit _cu;
		Stack<DefaultClass> _currentClass = new Stack<DefaultClass>();

		private IClass OuterClass
		{
			get
			{
				if (_currentClass.Count > 0)
					return _currentClass.Peek();
				else
					return null;
			}
		}

		public Context(GoldParser.Parser parser, ICompilationUnit program)
		{
			m_parser = parser;
			_cu = program;
			DefaultClass c = new DefaultClass(_cu, ClassType.Module, ModifierEnum.Public, DomRegion.Empty, null);
			c.FullyQualifiedName = Path.GetFileNameWithoutExtension(_cu.FileName);
			_cu.Classes.Add(c);
			_currentClass.Push(c);
			
		}

		public void OnToken()
		{
			// We need text of the following tokens for synatx tree creation.
			switch ((SymbolConstants)m_parser.TokenSymbol.Index)
			{
				case SymbolConstants.SYMBOL_YY__READ:
				case SymbolConstants.SYMBOL_YY__REPLACED_BY:
					// Special case for instructions used as symbols into paragraphs
					if(m_parser.TokenLinePosition > 1)
						m_parser.TokenSymbol = new Symbol((int)SymbolConstants.SYMBOL_YY__SYMBOLL,
																							"yy_symbolL", 
																							SymbolType.Terminal);
					m_parser.TokenSyntaxNode = new SyntaxNode(m_parser.TokenSymbol.Index,
																										m_parser.TokenText,
																										new DomRegion(m_parser.TokenLineNumber,
																																	m_parser.TokenLinePosition,
																																	m_parser.LineNumber,
																																	m_parser.LinePosition));
					break;
				case SymbolConstants.SYMBOL_EOF:
				case SymbolConstants.SYMBOL_ERROR:
				case SymbolConstants.SYMBOL_WHITESPACE:
				case SymbolConstants.SYMBOL_COMMENTEND:
				case SymbolConstants.SYMBOL_COMMENTLINE:
				case SymbolConstants.SYMBOL_COMMENTSTART:
				case SymbolConstants.SYMBOL_YY__ALERT:
				case SymbolConstants.SYMBOL_YY__ALT_NUMBER:
				case SymbolConstants.SYMBOL_YY__AMPERSAND:
				case SymbolConstants.SYMBOL_YY__ANB_STRING:
				case SymbolConstants.SYMBOL_YY__ARROW:
				case SymbolConstants.SYMBOL_YY__AVM:
				case SymbolConstants.SYMBOL_YY__BIT_WIDTH:
				case SymbolConstants.SYMBOL_YY__CARRET:
				case SymbolConstants.SYMBOL_YY__CHAR:
				case SymbolConstants.SYMBOL_YY__CHECKING_EVERY:
				case SymbolConstants.SYMBOL_YY__COLON:
				case SymbolConstants.SYMBOL_YY__COMMA:
				case SymbolConstants.SYMBOL_YY__CONNECT_TO_FILE:
				case SymbolConstants.SYMBOL_YY__CONNECT_TO_IP:
				case SymbolConstants.SYMBOL_YY__DEBUG_AVM:
				case SymbolConstants.SYMBOL_YY__DELEGATE:
				case SymbolConstants.SYMBOL_YY__DESCRIPTION:
				case SymbolConstants.SYMBOL_YY__DOT:
				case SymbolConstants.SYMBOL_YY__DOTS:
				case SymbolConstants.SYMBOL_YY__ELSE:
				case SymbolConstants.SYMBOL_YY__EQUALS:
				case SymbolConstants.SYMBOL_YY__EXCHANGE:
				case SymbolConstants.SYMBOL_YY__EXISTS:
				case SymbolConstants.SYMBOL_YY__EXISTS_UNIQUE:
				case SymbolConstants.SYMBOL_YY__FLOAT:
				case SymbolConstants.SYMBOL_YY__FORALL:
				case SymbolConstants.SYMBOL_YY__G_OPERATION:
				case SymbolConstants.SYMBOL_YY__GADDR:
				case SymbolConstants.SYMBOL_YY__GREATER:
				case SymbolConstants.SYMBOL_YY__GREATEROREQ:
				case SymbolConstants.SYMBOL_YY__IF:
				case SymbolConstants.SYMBOL_YY__IMPLIES:
				case SymbolConstants.SYMBOL_YY__INDIRECT:
				case SymbolConstants.SYMBOL_YY__INTEGER:
				case SymbolConstants.SYMBOL_YY__IS:
				case SymbolConstants.SYMBOL_YY__LBRACE:
				case SymbolConstants.SYMBOL_YY__LBRACKET:
				case SymbolConstants.SYMBOL_YY__LEFT_SHIFT:
				case SymbolConstants.SYMBOL_YY__LESS:
				case SymbolConstants.SYMBOL_YY__LESSOREQ:
				case SymbolConstants.SYMBOL_YY__LOAD_MODULE:
				case SymbolConstants.SYMBOL_YY__LOCK:
				case SymbolConstants.SYMBOL_YY__LPAR:
				case SymbolConstants.SYMBOL_YY__MAPSTO:
				case SymbolConstants.SYMBOL_YY__MINUS:
				case SymbolConstants.SYMBOL_YY__MOD:
				case SymbolConstants.SYMBOL_YY__MVAR:
				case SymbolConstants.SYMBOL_YY__NON_EQUAL:
				case SymbolConstants.SYMBOL_YY__OPERATION:
				case SymbolConstants.SYMBOL_YY__P_OPERATION:
				case SymbolConstants.SYMBOL_YY__P_TYPE:
				case SymbolConstants.SYMBOL_YY__P_VARIABLE:
				case SymbolConstants.SYMBOL_YY__PERCENT:
				case SymbolConstants.SYMBOL_YY__PLUS:
				case SymbolConstants.SYMBOL_YY__PROTECT:
				case SymbolConstants.SYMBOL_YY__RADDR:
				case SymbolConstants.SYMBOL_YY__RBRACE:
				case SymbolConstants.SYMBOL_YY__RBRACKET:
				case SymbolConstants.SYMBOL_YY__READ_PATH:
				case SymbolConstants.SYMBOL_YY__RIGHT_SHIFT:
				case SymbolConstants.SYMBOL_YY__RPAR:
				case SymbolConstants.SYMBOL_YY__RPAR_ARROW:
				case SymbolConstants.SYMBOL_YY__RWADDR:
				case SymbolConstants.SYMBOL_YY__SEMICOLON:
				case SymbolConstants.SYMBOL_YY__SERIALIZE:
				case SymbolConstants.SYMBOL_YY__SLASH:
				case SymbolConstants.SYMBOL_YY__STAR:
				case SymbolConstants.SYMBOL_YY__STRUCTPTR:
				case SymbolConstants.SYMBOL_YY__SUCCEEDS:
				case SymbolConstants.SYMBOL_YY__SUCCEEDS_AS:
				case SymbolConstants.SYMBOL_YY__SYMBOLL:
				case SymbolConstants.SYMBOL_YY__SYMBOLU:
				case SymbolConstants.SYMBOL_YY__TERMINAL:
				case SymbolConstants.SYMBOL_YY__THEN:
				case SymbolConstants.SYMBOL_YY__TILDE:
				case SymbolConstants.SYMBOL_YY__TYPE:
				case SymbolConstants.SYMBOL_YY__TYPE_BYTEARRAY:
				case SymbolConstants.SYMBOL_YY__TYPE_FLOAT:
				case SymbolConstants.SYMBOL_YY__TYPE_INT32:
				case SymbolConstants.SYMBOL_YY__TYPE_LISTENER:
				case SymbolConstants.SYMBOL_YY__TYPE_STRING:
				case SymbolConstants.SYMBOL_YY__UNSERIALIZE:
				case SymbolConstants.SYMBOL_YY__UTVAR:
				case SymbolConstants.SYMBOL_YY__VAR:
				case SymbolConstants.SYMBOL_YY__VARIABLE:
				case SymbolConstants.SYMBOL_YY__VBAR:
				case SymbolConstants.SYMBOL_YY__VCOPY:
				case SymbolConstants.SYMBOL_YY__WADDR:
				case SymbolConstants.SYMBOL_YY__WAIT_FOR:
				case SymbolConstants.SYMBOL_YY__WITH:
				case SymbolConstants.SYMBOL_YY__WRITE:
				default:
					m_parser.TokenSyntaxNode = new SyntaxNode(m_parser.TokenSymbol.Index,
					                                          m_parser.TokenText,
					                                          new DomRegion(m_parser.TokenLineNumber,
					                                                        m_parser.TokenLinePosition,
					                                                        m_parser.LineNumber,
					                                                        m_parser.LinePosition));
					break;
			}
		}

		public void OnReduction()
		{
			switch ((RuleConstants)m_parser.ReductionRule.Index)
			{
				#region Read / Replaced by
				///////////////////////////////////////////////////////////
				/// Read / Replaced by
				///////////////////////////////////////////////////////////

				case RuleConstants.RULE_PARAGRAPH_YY__READ_YY__READ_PATH:
					//<Paragraph> ::= yy__read yy__read_path
				case RuleConstants.RULE_PARAGRAPH_YY__REPLACED_BY_YY__READ_PATH:
					//<Paragraph> ::= yy__replaced_by yy__read_path
					{
						DefaultUsing u = new DefaultUsing(_cu.ProjectContent, new DomRegion(m_parser.LineNumber, 0));
						u.Usings.Add(Token(1));
						_cu.Usings.Add(u);
						m_parser.TokenSyntaxNode = new SyntaxNode(m_parser.ReductionRule.Index,
																											GetRegion());
					}
					break;
				#endregion

				#region Type definition
					
				///////////////////////////////////////////////////////////
				/// Type definition
				///////////////////////////////////////////////////////////

				case RuleConstants.RULE_TYPEDEFINITION_YY__SYMBOLU_YY__EQUALS_YY__DOT:
					//<Type Definition> ::= <Type KW> yy__SymbolU yy__equals <Type> yy__dot
					{
						DefaultClass c = new DefaultClass(_cu, 
						                                  ClassType.Struct,
						                                  ExtractModifier(0),
						                                  GetRegion(),
						                                  OuterClass);
						c.FullyQualifiedName = Token(1);
						OuterClass.InnerClasses.Add(c);
						SyntaxNode node = MakeSyntaxNode();
						m_parser.TokenSyntaxNode = node;
						c.UserData = node;
					}
					break;

				case RuleConstants.RULE_TYPEDEFINITION_YY__SYMBOLU_YY__COLON:
					//<Type Definition> ::= <Type KW> yy__SymbolU yy__colon <Alternatives>
					{
						DefaultClass c = new DefaultClass(_cu,
																							ClassType.Struct,
																							ExtractModifier(0),
																							GetRegion(),
																							OuterClass);
						c.FullyQualifiedName = Token(1);
						OuterClass.InnerClasses.Add(c);
						SyntaxNode node = MakeSyntaxNode();
						m_parser.TokenSyntaxNode = node;
						c.UserData = node;
					}
					break;

				case RuleConstants.RULE_TYPEDEFINITION_YY__SYMBOLU_YY__LPAR_YY__RPAR_YY__COLON:
					//<Type Definition> ::= <Type KW> yy__SymbolU yy__lpar <Type Vars> yy__rpar yy__colon <Alternatives>
					{
						DefaultClass c = new DefaultClass(_cu,
																							ClassType.Struct,
																							ExtractModifier(0),
																							GetRegion(),
																							OuterClass);
						c.FullyQualifiedName = Token(1);
						OuterClass.InnerClasses.Add(c);
						SyntaxNode node = MakeSyntaxNode();
						m_parser.TokenSyntaxNode = node;
						c.UserData = node;
					}
					break;

				#endregion

				#region Variable declaration
					
				///////////////////////////////////////////////////////////
				/// Variable declaration
				///////////////////////////////////////////////////////////

				case RuleConstants.RULE_VARIABLEDECLARATION_YY__SYMBOLL_YY__EQUALS_YY__DOT:
					//<Variable Declaration> ::= <Var KW> <Type> yy__symbolL yy__equals <Term> yy__dot
					{
						DefaultField field = new DefaultField(ExtractReturnType(1),
						                                      Token(2), 
						                                      ExtractModifier(0),
						                                      GetRegion(),
						                                      OuterClass);
						OuterClass.Fields.Add(field);
						SyntaxNode node = MakeSyntaxNode();
						m_parser.TokenSyntaxNode = node;
						field.UserData = node;
					}
					break;

				#endregion

				#region Operation definition
					
				///////////////////////////////////////////////////////////
				/// Operation definition
				///////////////////////////////////////////////////////////

				case RuleConstants.RULE_OPERATIONDEFINITION_YY__SYMBOLL_YY__EQUALS_YY__DOT:
					// <Operation Definition> ::= <Op KW> <Type> yy__symbolL yy__equals <Term> yy__dot
					{
						DefaultMethod method = new DefaultMethod(Token(2), null, ExtractModifier(0),
						                                         new DomRegion(GetSyntaxNode(0).Region.BeginLine,
						                                                       GetSyntaxNode(0).Region.BeginColumn,
						                                                       GetSyntaxNode(2).Region.EndLine,
						                                                       GetSyntaxNode(2).Region.EndColumn),
						                                         GetRegion(), OuterClass);
						//ConvertTemplates(node, method);
						// return type must be assign AFTER ConvertTemplates
						method.ReturnType = ExtractReturnType(1);
						if (method.ReturnType == null)
							method.ReturnType = new DefaultReturnType(new DefaultClass(_cu, "One"));
						OuterClass.Methods.Add(method);

						SyntaxNode node = MakeSyntaxNode();
						m_parser.TokenSyntaxNode = node;
						method.UserData = node;
					}
					break;

				case RuleConstants.RULE_OPERATIONDEFINITION_YY__SYMBOLL_YY__LPAR_YY__RPAR_YY__EQUALS_YY__DOT:
					// <Operation Definition> ::= <Op KW> <Type> yy__symbolL yy__lpar <Op Args> yy__rpar yy__equals <Term> yy__dot
					{
						DefaultMethod method = new DefaultMethod(Token(2), null, ExtractModifier(0),
						                                         new DomRegion(GetSyntaxNode(0).Region.BeginLine,
						                                                       GetSyntaxNode(0).Region.BeginColumn,
						                                                       GetSyntaxNode(5).Region.EndLine,
						                                                       GetSyntaxNode(5).Region.EndColumn),
						                                         GetRegion(), OuterClass);
						//ConvertTemplates(node, method);
						// return type must be assign AFTER ConvertTemplates
						method.ReturnType = ExtractReturnType(1);
						if (method.ReturnType == null)
							method.ReturnType = new DefaultReturnType(new DefaultClass(_cu, "One"));
						ExtractParameters(4, method);
						OuterClass.Methods.Add(method);

						SyntaxNode node = MakeSyntaxNode();
						m_parser.TokenSyntaxNode = node;
						method.UserData = node;
					}
					break;

				case RuleConstants.RULE_OPERATIONDEFINITION_YY__EQUALS_YY__DOT:
					// <Operation Definition> ::= <Op KW> <Type> <Op Arg> <Simple Binary Op> <Op Arg> yy__equals <Term> yy__dot
					{
						DefaultMethod method = new DefaultMethod(ExtractSimpleOperatorName(3), null, ExtractModifier(0),
																										 new DomRegion(GetSyntaxNode(0).Region.BeginLine,
																																	 GetSyntaxNode(0).Region.BeginColumn,
																																	 GetSyntaxNode(4).Region.EndLine,
																																	 GetSyntaxNode(4).Region.EndColumn),
																										 GetRegion(), OuterClass);
						//ConvertTemplates(node, method);
						// return type must be assign AFTER ConvertTemplates
						method.ReturnType = ExtractReturnType(1);
						if (method.ReturnType == null)
							method.ReturnType = new DefaultReturnType(new DefaultClass(_cu, "One"));
						ExtractParameters(2, method);
						ExtractParameters(4, method);
						OuterClass.Methods.Add(method);

						SyntaxNode node = MakeSyntaxNode();
						m_parser.TokenSyntaxNode = node;
						method.UserData = node;
					}
					break;

				case RuleConstants.RULE_OPERATIONDEFINITION_YY__EQUALS_YY__DOT2:
					// <Operation Definition> ::= <Op KW> <Type> <Simple Unary Op> <Op Arg> yy__equals <Term> yy__dot
					{
						DefaultMethod method = new DefaultMethod(ExtractSimpleOperatorName(2), null, ExtractModifier(0),
																										 new DomRegion(GetSyntaxNode(0).Region.BeginLine,
																																	 GetSyntaxNode(0).Region.BeginColumn,
																																	 GetSyntaxNode(3).Region.EndLine,
																																	 GetSyntaxNode(3).Region.EndColumn),
																										 GetRegion(), OuterClass);
						//ConvertTemplates(node, method);
						// return type must be assign AFTER ConvertTemplates
						method.ReturnType = ExtractReturnType(1);
						if (method.ReturnType == null)
							method.ReturnType = new DefaultReturnType(new DefaultClass(_cu, "One"));
						ExtractParameters(3, method);
						OuterClass.Methods.Add(method);

						SyntaxNode node = MakeSyntaxNode();
						m_parser.TokenSyntaxNode = node;
						method.UserData = node;
					}
					break;

				case RuleConstants.RULE_OPERATIONDEFINITION_YY__MOD_YY__RPAR_YY__EQUALS_YY__DOT:
					//<Operation Definition> ::= <Op KW> <Type> <Op Arg> yy__mod <Op Arg> yy__rpar yy__equals <Term> yy__dot
					{
						DefaultMethod method = new DefaultMethod(Token(3), null, ExtractModifier(0),
																										 new DomRegion(GetSyntaxNode(0).Region.BeginLine,
																																	 GetSyntaxNode(0).Region.BeginColumn,
																																	 GetSyntaxNode(4).Region.EndLine,
																																	 GetSyntaxNode(4).Region.EndColumn),
																										 GetRegion(), OuterClass);
						//ConvertTemplates(node, method);
						// return type must be assign AFTER ConvertTemplates
						method.ReturnType = ExtractReturnType(1);
						if (method.ReturnType == null)
							method.ReturnType = new DefaultReturnType(new DefaultClass(_cu, "One"));
						ExtractParameters(2, method);
						ExtractParameters(4, method);
						OuterClass.Methods.Add(method);

						SyntaxNode node = MakeSyntaxNode();
						m_parser.TokenSyntaxNode = node;
						method.UserData = node;
					}
					break;

				#endregion
					
				#region Not used rules

				//case RuleConstants.RULE_TYPEVARS_YY__UTVAR_YY__COMMA:
				//  //<Type Vars> ::= yy__utvar yy__comma <Type Vars>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TYPEVARS_YY__UTVAR:
				//  //<Type Vars> ::= yy__utvar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVES_YY__DOT:
				//  //<Alternatives> ::= yy__dot
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVES:
				//  //<Alternatives> ::= <Alternatives1>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVES1_YY__COMMA:
				//  //<Alternatives1> ::= <Alternatives1> yy__comma <Alternative>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVES1_YY__DOT:
				//  //<Alternatives1> ::= <Alternative> yy__dot
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVES1_YY__DOTS:
				//  //<Alternatives1> ::= yy__dots
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__SYMBOLL:
				//  //<Alternative> ::= yy__symbolL
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__LBRACKET_YY__RBRACKET:
				//  //<Alternative> ::= yy__lbracket yy__rbracket
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__SYMBOLL_YY__LPAR_YY__RPAR:
				//  //<Alternative> ::= yy__symbolL yy__lpar <Alt Operands> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__LBRACKET_YY__DOT_YY__RBRACKET:
				//  //<Alternative> ::= yy__lbracket <Alt Operand> yy__dot <Alt Operand> yy__rbracket
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__PLUS:
				//  //<Alternative> ::= <Alt Operand> yy__plus <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__STAR:
				//  //<Alternative> ::= <Alt Operand> yy__star <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__PERCENT:
				//  //<Alternative> ::= <Alt Operand> yy__percent <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__CARRET:
				//  //<Alternative> ::= <Alt Operand> yy__carret <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__VBAR:
				//  //<Alternative> ::= <Alt Operand> yy__vbar <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__AMPERSAND:
				//  //<Alternative> ::= <Alt Operand> yy__ampersand <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__ARROW:
				//  //<Alternative> ::= <Alt Operand> yy__arrow <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__EQUALS:
				//  //<Alternative> ::= <Alt Operand> yy__equals <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__IMPLIES:
				//  //<Alternative> ::= <Alt Operand> yy__implies <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__LEFT_SHIFT:
				//  //<Alternative> ::= <Alt Operand> yy__left_shift <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__RIGHT_SHIFT:
				//  //<Alternative> ::= <Alt Operand> yy__right_shift <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__MINUS:
				//  //<Alternative> ::= <Alt Operand> yy__minus <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__SLASH:
				//  //<Alternative> ::= <Alt Operand> yy__slash <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__MOD_YY__RPAR:
				//  //<Alternative> ::= <Alt Operand> yy__mod <Alt Operand> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__LESS:
				//  //<Alternative> ::= <Alt Operand> yy__less <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__NON_EQUAL:
				//  //<Alternative> ::= <Alt Operand> yy__non_equal <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__LESSOREQ:
				//  //<Alternative> ::= <Alt Operand> yy__lessoreq <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__TILDE:
				//  //<Alternative> ::= yy__tilde <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTERNATIVE_YY__STAR2:
				//  //<Alternative> ::= yy__star <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTOPERANDS_YY__COMMA:
				//  //<Alt Operands> ::= <Alt Operands> yy__comma <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTOPERANDS:
				//  //<Alt Operands> ::= <Alt Operand>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTOPERAND:
				//  //<Alt Operand> ::= <Type>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_ALTOPERAND_YY__SYMBOLL:
				//  //<Alt Operand> ::= <Type> yy__symbolL
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_OPERATIONDECLARATION_YY__SYMBOLL_YY__LPAR_YY__RPAR_YY__DOT:
				//  //<Operation Declaration> ::= <Op KW> <Type> yy__symbolL yy__lpar <Op Args> yy__rpar yy__dot
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_OPERATIONDECLARATION_YY__SYMBOLL_YY__DOT:
				//  //<Operation Declaration> ::= <Op KW> <Type> yy__symbolL yy__dot
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_OPERATIONDECLARATION_YY__DOT:
				//  //<Operation Definition> ::= <Op KW> <Type> <Op Arg> <Simple Binary Op> <Op Arg> yy__dot
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_OPERATIONDECLARATION_YY__DOT2:
				//  //<Operation Definition> ::= <Op KW> <Type> <Simple Unary Op> <Op Arg> yy__dot
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_OPERATIONDECLARATION_YY__MOD_YY__RPAR_YY__DOT:
				//  //<Operation Definition> ::= <Op KW> <Type> <Op Arg> yy__mod <Op Arg> yy__rpar yy__dot
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_FARGS_YY__COMMA:
				//  //<FArgs> ::= <FArg> yy__comma <FArgs>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_FARGS:
				//  //<FArgs> ::= <FArg>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_FARG_YY__SYMBOLL:
				//  //<FArg> ::= <Type> yy__symbolL
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERMS:
				//  //<Terms> ::= <Terms2>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERMS2:
				//  //<Terms> ::= <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERMS2_YY__COMMA:
				//  //<Terms2> ::= <Terms> yy__comma <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__PROTECT:
				//  //<Term> ::= yy__protect <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__DEBUG_AVM:
				//  //<Term> ::= yy__debug_avm <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__TERMINAL:
				//  //<Term> ::= yy__terminal <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LOCK_YY__COMMA:
				//  //<Term> ::= yy__lock <Term> yy__comma <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__DELEGATE_YY__COMMA:
				//  //<Term> ::= yy__delegate <Term> yy__comma <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__CHECKING_EVERY_YY__COMMA_YY__WAIT_FOR_YY__THEN:
				//  //<Term> ::= yy__checking_every <Term> <yy  milliseconds> yy__comma yy__wait_for <Term> yy__then <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__SEMICOLON:
				//  //<Term> ::= <Term> yy__semicolon <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__SYMBOLL:
				//  //<Term> ::= yy__symbolL
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LPAR_YY__RPAR:
				//  //<Term> ::= yy__lpar yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__INTEGER:
				//  //<Term> ::= yy__integer
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__CHAR:
				//  //<Term> ::= yy__char
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__FLOAT:
				//  //<Term> ::= yy__float
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__FORALL_YY__SYMBOLL_YY__COLON_YY__COMMA:
				//  //<Term> ::= yy__forall yy__symbolL yy__colon <Type> yy__comma <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__FORALL_YY__SYMBOLL_YY__COLON_YY__COMMA2:
				//  //<Term> ::= yy__forall yy__symbolL yy__colon <Term> yy__comma <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__EXISTS_YY__SYMBOLL_YY__COLON_YY__COMMA:
				//  //<Term> ::= yy__exists yy__symbolL yy__colon <Type> yy__comma <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__EXISTS_UNIQUE_YY__SYMBOLL_YY__COLON_YY__COMMA:
				//  //<Term> ::= yy__exists_unique yy__symbolL yy__colon <Type> yy__comma <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__DESCRIPTION_YY__SYMBOLL_YY__COLON_YY__COMMA:
				//  //<Term> ::= yy__description yy__symbolL yy__colon <Type> yy__comma <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LPAR_YY__RPAR2:
				//  //<Term> ::= yy__lpar <Term> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LPAR_YY__RPAR3:
				//  //<Term> ::= yy__lpar <Terms2> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LPAR_YY__RPAR4:
				//  //<Term> ::= yy__lpar <Type> yy__rpar <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LPAR_YY__COLON_YY__RPAR:
				//  //<Term> ::= yy__lpar yy__colon <Term> yy__rpar <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LPAR_YY__COLON_YY__RPAR2:
				//  //<Term> ::= yy__lpar yy__colon <Type> yy__rpar <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__WRITE:
				//  //<Term> ::= <Term> yy__write <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__EXCHANGE:
				//  //<Term> ::= <Term> yy__exchange <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__STAR:
				//  //<Term> ::= yy__star <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LPAR_YY__RPAR_YY__CONNECT_TO_FILE:
				//  //<Term> ::= yy__lpar <Type> yy__rpar yy__connect_to_file <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LPAR_YY__RPAR_YY__CONNECT_TO_IP_YY__COLON:
				//  //<Term> ::= yy__lpar <Type> yy__rpar yy__connect_to_IP <Term> yy__colon <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LPAR_YY__RPAR_YY__MAPSTO:
				//  //<Term> ::= yy__lpar <FArgs> yy__rpar yy__mapsto <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__ALERT:
				//  //<Term> ::= yy__alert
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__ALT_NUMBER_YY__LPAR_YY__RPAR:
				//  //<Term> ::= yy__alt_number yy__lpar <Term> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__AVM_YY__LBRACE_YY__RBRACE:
				//  //<Term> ::= yy__avm yy__lbrace <AVM> yy__rbrace
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM:
				//  //<Term> ::= <App Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM2:
				//  //<Term> ::= <Conditional>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM3:
				//  //<Term> ::= <List>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LOAD_MODULE_YY__LPAR_YY__RPAR:
				//  //<Term> ::= yy__load_module yy__lpar <Term> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__SERIALIZE_YY__LPAR_YY__RPAR:
				//  //<Term> ::= yy__serialize yy__lpar <Term> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__UNSERIALIZE_YY__LPAR_YY__RPAR:
				//  //<Term> ::= yy__unserialize yy__lpar <Term> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__BIT_WIDTH_YY__LPAR_YY__RPAR:
				//  //<Term> ::= yy__bit_width yy__lpar <Type> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__INDIRECT_YY__LPAR_YY__RPAR:
				//  //<Term> ::= yy__indirect yy__lpar <Type> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__VCOPY_YY__LPAR_YY__COMMA_YY__RPAR:
				//  //<Term> ::= yy__vcopy yy__lpar <Term> yy__comma <Term> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__ANB_STRING:
				//  //<Term> ::= yy__anb_string
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__WITH_YY__SYMBOLL_YY__EQUALS_YY__COMMA:
				//  //<Term> ::= yy__with yy__symbolL yy__equals <Term> yy__comma <With Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_TERM_YY__LBRACE_YY__SYMBOLL_YY__COLON_YY__COMMA_YY__RBRACE:
				//  //<Term> ::= yy__lbrace yy__symbolL yy__colon <Type> yy__comma <Term> yy__rbrace
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_WITHTERM:
				//  //<With Term> ::= <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_WITHTERM_YY__SYMBOLL_YY__EQUALS_YY__COMMA:
				//  //<With Term> ::= yy__symbolL yy__equals <Term> yy__comma <With Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_LIST_YY__LBRACKET_YY__RBRACKET:
				//  //<List> ::= yy__lbracket <ListItems> yy__rbracket
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_LIST_YY__LBRACKET_YY__RBRACKET2:
				//  //<List> ::= yy__lbracket yy__rbracket
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_LIST_YY__LBRACKET_YY__DOT_YY__RBRACKET:
				//  //<List> ::= yy__lbracket <Term> yy__dot <Term> yy__rbracket
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_LISTITEMS_YY__COMMA:
				//  //<ListItems> ::= <ListItems> yy__comma <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_LISTITEMS:
				//  //<ListItems> ::= <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__MINUS:
				//  //<App Term> ::= yy__minus <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__LPAR_YY__RPAR:
				//  //<App Term> ::= <Term> yy__lpar <Terms> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM:
				//  //<App Term> ::= <Term> <List>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__PLUS:
				//  //<App Term> ::= <Term> yy__plus <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__STAR:
				//  //<App Term> ::= <Term> yy__star <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__PERCENT:
				//  //<App Term> ::= <Term> yy__percent <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__CARRET:
				//  //<App Term> ::= <Term> yy__carret <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__VBAR:
				//  //<App Term> ::= <Term> yy__vbar <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__AMPERSAND:
				//  //<App Term> ::= <Term> yy__ampersand <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__ARROW:
				//  //<App Term> ::= <Term> yy__arrow <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__EQUALS:
				//  //<App Term> ::= <Term> yy__equals <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__IMPLIES:
				//  //<App Term> ::= <Term> yy__implies <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__LEFT_SHIFT:
				//  //<App Term> ::= <Term> yy__left_shift <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__RIGHT_SHIFT:
				//  //<App Term> ::= <Term> yy__right_shift <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__MINUS2:
				//  //<App Term> ::= <Term> yy__minus <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__SLASH:
				//  //<App Term> ::= <Term> yy__slash <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__LESS:
				//  //<App Term> ::= <Term> yy__less <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__NON_EQUAL:
				//  //<App Term> ::= <Term> yy__non_equal <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__GREATER:
				//  //<App Term> ::= <Term> yy__greater <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__LESSOREQ:
				//  //<App Term> ::= <Term> yy__lessoreq <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__GREATEROREQ:
				//  //<App Term> ::= <Term> yy__greateroreq <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__MOD_YY__RPAR:
				//  //<App Term> ::= <Term> yy__mod <Term> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_APPTERM_YY__TILDE:
				//  //<App Term> ::= yy__tilde <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_CONDITIONAL_YY__IF_YY__IS_YY__LBRACE_YY__RBRACE:
				//  //<Conditional> ::= yy__if <Term> yy__is yy__lbrace <Clauses> yy__rbrace
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_CONDITIONAL_YY__IF_YY__SUCCEEDS_AS_YY__SYMBOLL_YY__THEN:
				//  //<Conditional> ::= yy__if <Term> yy__succeeds_as yy__symbolL yy__then <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_CONDITIONAL_YY__IF_YY__SUCCEEDS_YY__THEN:
				//  //<Conditional> ::= yy__if <Term> yy__succeeds yy__then <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_CONDITIONAL_YY__IF_YY__IS:
				//  //<Conditional> ::= yy__if <Term> yy__is <Clause>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_CONDITIONAL_YY__IF_YY__THEN_YY__ELSE:
				//  //<Conditional> ::= yy__if <Term> yy__then <Term> yy__else <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_CONDITIONAL_YY__IF_YY__IS_YY__ELSE:
				//  //<Conditional> ::= yy__if <Term> yy__is <Clause> yy__else <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_CLAUSES_YY__COMMA:
				//  //<Clauses> ::= <Clauses> yy__comma <Clause>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_CLAUSES:
				//  //<Clauses> ::= <Clause>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_CLAUSE_YY__THEN:
				//  //<Clause> ::= <Head> yy__then <Term>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__SYMBOLL:
				//  //<Head> ::= yy__symbolL
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__LBRACKET_YY__RBRACKET:
				//  //<Head> ::= yy__lbracket yy__rbracket
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__SYMBOLL_YY__LPAR_YY__RPAR:
				//  //<Head> ::= yy__symbolL yy__lpar yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__SYMBOLL_YY__LPAR_YY__RPAR2:
				//  //<Head> ::= yy__symbolL yy__lpar <Resur Syms> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__LBRACKET_YY__DOT_YY__RBRACKET:
				//  //<Head> ::= yy__lbracket <Resur Sym> yy__dot <Resur Sym> yy__rbracket
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__PLUS:
				//  //<Head> ::= <Resur Sym> yy__plus <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__STAR:
				//  //<Head> ::= <Resur Sym> yy__star <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__PERCENT:
				//  //<Head> ::= <Resur Sym> yy__percent <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__CARRET:
				//  //<Head> ::= <Resur Sym> yy__carret <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__VBAR:
				//  //<Head> ::= <Resur Sym> yy__vbar <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__AMPERSAND:
				//  //<Head> ::= <Resur Sym> yy__ampersand <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__ARROW:
				//  //<Head> ::= <Resur Sym> yy__arrow <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__EQUALS:
				//  //<Head> ::= <Resur Sym> yy__equals <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__IMPLIES:
				//  //<Head> ::= <Resur Sym> yy__implies <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__LEFT_SHIFT:
				//  //<Head> ::= <Resur Sym> yy__left_shift <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__RIGHT_SHIFT:
				//  //<Head> ::= <Resur Sym> yy__right_shift <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__MINUS:
				//  //<Head> ::= <Resur Sym> yy__minus <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__SLASH:
				//  //<Head> ::= <Resur Sym> yy__slash <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__MOD_YY__RPAR:
				//  //<Head> ::= <Resur Sym> yy__mod <Resur Sym> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__LESS:
				//  //<Head> ::= <Resur Sym> yy__less <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__NON_EQUAL:
				//  //<Head> ::= <Resur Sym> yy__non_equal <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__LESSOREQ:
				//  //<Head> ::= <Resur Sym> yy__lessoreq <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__TILDE:
				//  //<Head> ::= yy__tilde <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_HEAD_YY__LPAR_YY__RPAR:
				//  //<Head> ::= yy__lpar <Resur Syms1> yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_RESURSYM_YY__SYMBOLL:
				//  //<Resur Sym> ::= yy__symbolL
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_RESURSYM_YY__SYMBOLL2:
				//  //<Resur Sym> ::= <Type> yy__symbolL
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_RESURSYMS:
				//  //<Resur Syms> ::= <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_RESURSYMS2:
				//  //<Resur Syms> ::= <Resur Syms1>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_RESURSYMS1_YY__COMMA:
				//  //<Resur Syms1> ::= <Resur Syms> yy__comma <Resur Sym>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_AVM:
				//  //<AVM> ::= <AVM Instr> <AVM>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_AVMINSTR_YY__SYMBOLL:
				//  //<AVM Instr> ::= yy__symbolL
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_AVMINSTR_YY__LPAR_YY__SYMBOLL:
				//  //<AVM Instr> ::= yy__lpar yy__symbolL <Int MCons>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_INTMCONS_YY__RPAR:
				//  //<Int MCons> ::= yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_INTMCONS_YY__DOT_YY__INTEGER_YY__RPAR:
				//  //<Int MCons> ::= yy__dot yy__integer yy__rpar
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_INTMCONS_YY__INTEGER:
				//  //<Int MCons> ::= yy__integer <Int MCons>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_INTMCONS_YY__ANB_STRING:
				//  //<Int MCons> ::= yy__anb_string <Int MCons>
				//  //todo: Create a new object using the stored user objects.
				//  break;

				//case RuleConstants.RULE_YYMILLISECONDS_YY__SYMBOLL:
				//  //<yy  milliseconds> ::= yy__symbolL
				//  //todo: Create a new object using the stored user objects.
				//  break;
				#endregion
					
				default:
					{
						SyntaxNode node = new SyntaxNode(m_parser.ReductionRule.Index, GetRegion());
						for (int i = 0; i < m_parser.ReductionCount; i++)
						{
							node.Children.Add(GetSyntaxNode(i));
						}
						m_parser.TokenSyntaxNode = node;
					}
					//					throw new RuleException("Unknown rule");
					break;
			}
			// Default handler to be sure that there is always a valid SyntaxNode object.
			Debug.Assert(m_parser.TokenSyntaxNode != null, "m_parser.TokenSyntaxNode is null! SyntaxNode should be stored here...");
			if (m_parser.TokenSyntaxNode == null)
			{
				SyntaxNode tmpNode = new SyntaxNode(m_parser.ReductionRule.Index, GetRegion());
				for (int i = 0; i < m_parser.ReductionCount; i++)
				{
					tmpNode.Children.Add(GetSyntaxNode(i));
				}
				m_parser.TokenSyntaxNode = tmpNode;
			}
		}

		private string Token(int index)
		{
			return GetSyntaxNode(index).Content;
		}

		private SyntaxNode MakeSyntaxNode()
		{
			SyntaxNode node = new SyntaxNode(m_parser.ReductionRule.Index, GetRegion());
			for (int i = 0; i < m_parser.ReductionCount; i++)
			{
				node.Children.Add(GetSyntaxNode(i));
			}
			return node;
		}

		private SyntaxNode GetSyntaxNode(int index)
		{
			SyntaxNode node = (SyntaxNode) m_parser.GetReductionSyntaxNode(index);
			Debug.Assert(node != null, "SyntaxNode should never be null!");
			return node;
		}

		private IReturnType GetReturnType(string name)
		{
			IReturnType rt;
			if (AnubisAmbience.ReverseTypeConversionTable.ContainsKey(name))
				rt = new GetClassReturnType(_cu.ProjectContent, AnubisAmbience.ReverseTypeConversionTable[name], 0);
			else
				rt = new SearchClassReturnType(_cu.ProjectContent, OuterClass, m_parser.LineNumber, m_parser.LinePosition,
				                               name, 0);
			return rt;
		}

		#region Extract Grammar Elements

		private string ExtractSimpleOperatorName(int index)
		{
			return ExtractSimpleOperatorName(GetSyntaxNode(index));
		}

		private string ExtractSimpleOperatorName(SyntaxNode node)
		{
			if (node.Content != string.Empty) // In case of trim reduction, we should have directly the symbol
				return node.Content;
			switch ((RuleConstants)node.RuleId)
			{
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__PLUS:
					//<Simple Binary Op> ::= yy__plus
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__STAR:
					//<Simple Binary Op> ::= yy__star
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__PERCENT:
					//<Simple Binary Op> ::= yy__percent
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__CARRET:
					//<Simple Binary Op> ::= yy__carret
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__VBAR:
					//<Simple Binary Op> ::= yy__vbar
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__AMPERSAND:
					//<Simple Binary Op> ::= yy__ampersand
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__ARROW:
					//<Simple Binary Op> ::= yy__arrow
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__EQUALS:
					//<Simple Binary Op> ::= yy__equals
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__IMPLIES:
					//<Simple Binary Op> ::= yy__implies
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__LEFT_SHIFT:
					//<Simple Binary Op> ::= yy__left_shift
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__RIGHT_SHIFT:
					//<Simple Binary Op> ::= yy__right_shift
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__MINUS:
					//<Simple Binary Op> ::= yy__minus
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__SLASH:
					//<Simple Binary Op> ::= yy__slash
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__LESS:
					//<Simple Binary Op> ::= yy__less
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__NON_EQUAL:
					//<Simple Binary Op> ::= yy__non_equal
				case RuleConstants.RULE_SIMPLEBINARYOP_YY__LESSOREQ:
					//<Simple Binary Op> ::= yy__lessoreq
					return node[0].Content;

				case RuleConstants.RULE_SIMPLEUNARYOP_YY__MINUS:
					//<Simple Unary Op> ::= yy__minus
				case RuleConstants.RULE_SIMPLEUNARYOP_YY__TILDE:
					//<Simple Unary Op> ::= yy__tilde
					return node[0].Content;

				default:
					throw new Exception(string.Format("Unknown rule ({0}) for ExtractReturnType().", node.RuleId));
			}
		}


		private ModifierEnum ExtractModifier(int nodeIndex)
		{
			return ExtractModifier(GetSyntaxNode(nodeIndex));
		}
		
		private ModifierEnum ExtractModifier(SyntaxNode node)
		{
			switch((RuleConstants)node.RuleId)
			{
				case RuleConstants.RULE_OPKW_YY__OPERATION:
					//<Op KW> ::= yy__operation
					return ModifierEnum.Private;

				case RuleConstants.RULE_OPKW_YY__P_OPERATION:
					//<Op KW> ::= yy__p_operation
					return ModifierEnum.Public;

				case RuleConstants.RULE_OPKW_YY__G_OPERATION:
					//<Op KW> ::= yy__g_operation
					return ModifierEnum.Public;

				case RuleConstants.RULE_VARKW_YY__VARIABLE:
					//<Var KW> ::= yy__variable
					return ModifierEnum.Private;

				case RuleConstants.RULE_VARKW_YY__P_VARIABLE:
					//<Var KW> ::= yy__p_variable
					return ModifierEnum.Public;

				case RuleConstants.RULE_TYPEKW_YY__TYPE:
					//<Type KW> ::= yy__type
					return ModifierEnum.Private;

				case RuleConstants.RULE_TYPEKW_YY__P_TYPE:
					//<Type KW> ::= yy__p_type
					return ModifierEnum.Public;
				
				default:
					throw new Exception(string.Format("Unknown rule ({0}) for ExtractModifier().", node.RuleId));
			}
		}

		private IReturnType ExtractReturnType(int index)
		{
			return ExtractReturnType(GetSyntaxNode(index));
		}

		private IReturnType ExtractReturnType(SyntaxNode node)
		{
			switch((RuleConstants)node.RuleId)
			{
				case RuleConstants.RULE_TYPE_YY__SYMBOLU:				// <Type> ::= yy__SymbolU
				case RuleConstants.RULE_TYPE_YY__TYPE_STRING:		// <Type> ::= yy__type_String
				case RuleConstants.RULE_TYPE_YY__TYPE_BYTEARRAY:// <Type> ::= yy__type_ByteArray
				case RuleConstants.RULE_TYPE_YY__TYPE_INT32:		// <Type> ::= yy__type_Int32
				case RuleConstants.RULE_TYPE_YY__TYPE_FLOAT:		// <Type> ::= yy__type_Float
				case RuleConstants.RULE_TYPE_YY__TYPE_LISTENER:	// <Type> ::= yy__type_Listener
					return GetReturnType(node[0].Content);

				case RuleConstants.RULE_TYPE:
					// <Type> ::= <Func Type>
					return ExtractFunctionReturnType(node[0]);

				case RuleConstants.RULE_TYPE_YY__LPAR_YY__RPAR:
					// <Type> ::= yy__lpar <Type> yy__rpar
					return ExtractReturnType(node[1]);

				case RuleConstants.RULE_TYPE_YY__UTVAR:
					//<Type> ::= yy__utvar
					return GetReturnType(node[0].Content);

				case RuleConstants.RULE_TYPE_YY__RADDR_YY__LPAR_YY__RPAR:
					//<Type> ::= yy__RAddr yy__lpar <Type> yy__rpar
				case RuleConstants.RULE_TYPE_YY__WADDR_YY__LPAR_YY__RPAR:
					//<Type> ::= yy__WAddr yy__lpar <Type> yy__rpar
				case RuleConstants.RULE_TYPE_YY__RWADDR_YY__LPAR_YY__RPAR:
					//<Type> ::= yy__RWAddr yy__lpar <Type> yy__rpar
				case RuleConstants.RULE_TYPE_YY__GADDR_YY__LPAR_YY__RPAR:
					//<Type> ::= yy__GAddr yy__lpar <Type> yy__rpar
				case RuleConstants.RULE_TYPE_YY__VAR_YY__LPAR_YY__RPAR:
					// <Type> ::= yy__Var yy__lpar <Type> yy__rpar
				case RuleConstants.RULE_TYPE_YY__MVAR_YY__LPAR_YY__RPAR:
					// <Type> ::= yy__MVar yy__lpar <Type> yy__rpar
					return ExtractReturnType(node[2]);

				case RuleConstants.RULE_TYPE_YY__SYMBOLU_YY__LPAR_YY__RPAR:
					// <Type> ::= yy__SymbolU yy__lpar <Types> yy__rpar
					return new SearchClassReturnType(_cu.ProjectContent,
					                                 OuterClass,
					                                 m_parser.LineNumber,
					                                 m_parser.LinePosition,
					                                 node[0].Content,
					                                 0);

				case RuleConstants.RULE_TYPE_YY__STRUCTPTR_YY__LPAR_YY__SYMBOLU_YY__RPAR:
					//<Type> ::= yy__StructPtr yy__lpar yy__SymbolU yy__rpar
					return GetReturnType(node[2].Content);

				case RuleConstants.RULE_TYPE_YY__LPAR_YY__COMMA_YY__RPAR:
					//<Type> ::= yy__lpar <Type> yy__comma <Types> yy__rpar
					{
						CombinedReturnType combi = new CombinedReturnType(new List<IReturnType>(),
						                                                  "", "", "", "");
						combi.BaseTypes.Add(ExtractReturnType(node[1]));
						ExtractReturnTypes(node[3], combi);
						return combi;
					}

				case RuleConstants.RULE_TYPE_YY__LBRACE_YY__RBRACE:
					//<Type> ::= yy__lbrace <Type> yy__rbrace
					return ExtractReturnType(node[1]);

				case RuleConstants.RULE_FUNCTYPE_YY__ARROW:
				case RuleConstants.RULE_FUNCTYPE_YY__LPAR_YY__RPAR_ARROW:
					// Because of trim reduction, we can have this directly
					return ExtractFunctionReturnType(node);

				default:
					throw new Exception(string.Format("Unknown rule ({0}) for ExtractReturnType().", node.RuleId));
			}
		}

		private void ExtractReturnTypes(SyntaxNode node, CombinedReturnType combi)
		{
			switch((RuleConstants)node.RuleId)
			{
				case RuleConstants.RULE_TYPES_YY__COMMA:
					//<Types> ::= <Types> yy__comma <Type>
					ExtractReturnTypes(node[0], combi);
					combi.BaseTypes.Add(ExtractReturnType(node[2]));
					break;

				case RuleConstants.RULE_TYPES:
					//<Types> ::= <Type>
					combi.BaseTypes.Add(ExtractReturnType(node[0]));
					break;

				default:
					throw new Exception(string.Format("Unknown rule ({0}) for ExtractReturnTypes().", node.RuleId));
			}
		}
		
		private void ExtractParameters(int index, IMethodOrProperty method)
		{
			ExtractParameters(GetSyntaxNode(index), method);
		}

		private void ExtractParameters(SyntaxNode node, IMethodOrProperty method)
		{
			switch((RuleConstants)node.RuleId)
			{
				case RuleConstants.RULE_OPARGS_YY__COMMA:
					//<Op Args> ::= <Op Arg> yy__comma <Op Args>
					ExtractParameters(node[0], method);
					ExtractParameters(node[2], method);
					break;

				case RuleConstants.RULE_OPARGS_YY__COMMA2:
					//<Op Args> ::= <Op Arg> yy__comma
				case RuleConstants.RULE_OPARGS:
					//<Op Args> ::= <Op Arg>
					ExtractParameters(node[0], method);
					break;

				case RuleConstants.RULE_OPARG_YY__SYMBOLL:
					//<Op Arg> ::= <Type> yy__symbolL
					{
						DefaultParameter param = new DefaultParameter(node[1].Content,
						                                              ExtractReturnType(node[0]),
						                                              node.Region);
						method.Parameters.Add(param);
					}
					break;

				default:
					throw new Exception(string.Format("Unknown rule ({0}) for ExtractParameters().", node.RuleId));
			}
		}

		private IReturnType ExtractFunctionReturnType(SyntaxNode node)
		{
			AnonymousMethodReturnType lambda = new AnonymousMethodReturnType(_cu);
			switch((RuleConstants)node.RuleId)
			{
				case RuleConstants.RULE_TYPE:
				case RuleConstants.RULE_FUNCTYPE_YY__ARROW:
					//<Func Type> ::= <Type> yy__arrow <Type>
					lambda.MethodReturnType = ExtractReturnType(node[2]);
					lambda.MethodParameters.Add(new DefaultParameter("", ExtractReturnType(node[0]), node[0].Region));
					break;

				case RuleConstants.RULE_FUNCTYPE_YY__LPAR_YY__RPAR_ARROW:
					//<Func Type> ::= yy__lpar <Types Args> yy__rpar_arrow <Type>
					lambda.MethodReturnType = ExtractReturnType(node[3]);
					ExtractTypeArgs(node[1], lambda);
					break;

				default:
					throw new Exception(string.Format("Unknown rule ({0}) for ExtractFunctionReturnType().", node.RuleId));
			}
			return lambda;
		}

		private void ExtractTypeArgs(SyntaxNode node, AnonymousMethodReturnType lambda)
		{
			switch ((RuleConstants)node.RuleId)
			{
				case RuleConstants.RULE_TYPESARGS:
					//<Types Args> ::= <Type>
					{
						DefaultParameter param = new DefaultParameter("",
																													ExtractReturnType(node[0]),
																													node.Region);
						lambda.MethodParameters.Add(param);
					}
					break;

				case RuleConstants.RULE_TYPESARGS_YY__SYMBOLL:
					//<Types Args> ::= <Type> yy__symbolL
					{
						DefaultParameter param = new DefaultParameter(node[1].Content,
																													ExtractReturnType(node[0]),
																													node.Region);
						lambda.MethodParameters.Add(param);
					}
					break;

				case RuleConstants.RULE_TYPESARGS_YY__COMMA:
					//<Types Args> ::= <Type> yy__comma <Types Args>
					{
						DefaultParameter param = new DefaultParameter("",
																													ExtractReturnType(node[0]),
																													node.Region);
						lambda.MethodParameters.Add(param);
						ExtractTypeArgs(node[2], lambda);
					}
					break;

				case RuleConstants.RULE_TYPESARGS_YY__SYMBOLL_YY__COMMA:
					//<Types Args> ::= <Type> yy__symbolL yy__comma <Types Args>
					{
						DefaultParameter param = new DefaultParameter(node[1].Content,
																													ExtractReturnType(node[0]),
																													node.Region);
						lambda.MethodParameters.Add(param);
					}
					ExtractTypeArgs(node[3], lambda);
					break;

				default:
					throw new Exception(string.Format("Unknown rule ({0}) for ExtractTypeArgs().", node.RuleId));
			}
		}

		private DomRegion GetRegion()
		{
			if (m_parser.LinePosition < 0)
				return DomRegion.Empty;
			else if (m_parser.ReductionCount > 0)
			{
				DomRegion beg = GetSyntaxNode(0).Region;
				DomRegion end = GetSyntaxNode(m_parser.ReductionCount - 1).Region;
				return new DomRegion(beg.BeginLine, beg.BeginColumn,
				                     end.EndLine, end.EndColumn);
			}
			else
				return new DomRegion(m_parser.LineNumber, m_parser.LinePosition,
														 m_parser.LineNumber, m_parser.LinePosition);
		}

		#endregion

		public enum SymbolConstants : int
		{
			SYMBOL_EOF = 0, // (EOF)
			SYMBOL_ERROR = 1, // (Error)
			SYMBOL_WHITESPACE = 2, // (Whitespace)
			SYMBOL_COMMENTEND = 3, // (Comment End)
			SYMBOL_COMMENTLINE = 4, // (Comment Line)
			SYMBOL_COMMENTSTART = 5, // (Comment Start)
			SYMBOL_YY__ALERT = 6, // yy__alert
			SYMBOL_YY__ALT_NUMBER = 7, // yy__alt_number
			SYMBOL_YY__AMPERSAND = 8, // yy__ampersand
			SYMBOL_YY__ANB_STRING = 9, // yy__anb_string
			SYMBOL_YY__ARROW = 10, // yy__arrow
			SYMBOL_YY__AVM = 11, // yy__avm
			SYMBOL_YY__BIT_WIDTH = 12, // yy__bit_width
			SYMBOL_YY__CARRET = 13, // yy__carret
			SYMBOL_YY__CHAR = 14, // yy__char
			SYMBOL_YY__CHECKING_EVERY = 15, // yy__checking_every
			SYMBOL_YY__COLON = 16, // yy__colon
			SYMBOL_YY__COMMA = 17, // yy__comma
			SYMBOL_YY__CONNECT_TO_FILE = 18, // yy__connect_to_file
			SYMBOL_YY__CONNECT_TO_IP = 19, // yy__connect_to_IP
			SYMBOL_YY__DEBUG_AVM = 20, // yy__debug_avm
			SYMBOL_YY__DELEGATE = 21, // yy__delegate
			SYMBOL_YY__DESCRIPTION = 22, // yy__description
			SYMBOL_YY__DOT = 23, // yy__dot
			SYMBOL_YY__DOTS = 24, // yy__dots
			SYMBOL_YY__ELSE = 25, // yy__else
			SYMBOL_YY__EQUALS = 26, // yy__equals
			SYMBOL_YY__EXCHANGE = 27, // yy__exchange
			SYMBOL_YY__EXISTS = 28, // yy__exists
			SYMBOL_YY__EXISTS_UNIQUE = 29, // yy__exists_unique
			SYMBOL_YY__FLOAT = 30, // yy__float
			SYMBOL_YY__FORALL = 31, // yy__forall
			SYMBOL_YY__G_OPERATION = 32, // yy__g_operation
			SYMBOL_YY__GADDR = 33, // yy__GAddr
			SYMBOL_YY__GREATER = 34, // yy__greater
			SYMBOL_YY__GREATEROREQ = 35, // yy__greateroreq
			SYMBOL_YY__IF = 36, // yy__if
			SYMBOL_YY__IMPLIES = 37, // yy__implies
			SYMBOL_YY__INDIRECT = 38, // yy__indirect
			SYMBOL_YY__INTEGER = 39, // yy__integer
			SYMBOL_YY__IS = 40, // yy__is
			SYMBOL_YY__LBRACE = 41, // yy__lbrace
			SYMBOL_YY__LBRACKET = 42, // yy__lbracket
			SYMBOL_YY__LEFT_SHIFT = 43, // yy__left_shift
			SYMBOL_YY__LESS = 44, // yy__less
			SYMBOL_YY__LESSOREQ = 45, // yy__lessoreq
			SYMBOL_YY__LOAD_MODULE = 46, // yy__load_module
			SYMBOL_YY__LOCK = 47, // yy__lock
			SYMBOL_YY__LPAR = 48, // yy__lpar
			SYMBOL_YY__MAPSTO = 49, // yy__mapsto
			SYMBOL_YY__MINUS = 50, // yy__minus
			SYMBOL_YY__MOD = 51, // yy__mod
			SYMBOL_YY__MVAR = 52, // yy__MVar
			SYMBOL_YY__NON_EQUAL = 53, // yy__non_equal
			SYMBOL_YY__OPERATION = 54, // yy__operation
			SYMBOL_YY__P_OPERATION = 55, // yy__p_operation
			SYMBOL_YY__P_TYPE = 56, // yy__p_type
			SYMBOL_YY__P_VARIABLE = 57, // yy__p_variable
			SYMBOL_YY__PERCENT = 58, // yy__percent
			SYMBOL_YY__PLUS = 59, // yy__plus
			SYMBOL_YY__PROTECT = 60, // yy__protect
			SYMBOL_YY__RADDR = 61, // yy__RAddr
			SYMBOL_YY__RBRACE = 62, // yy__rbrace
			SYMBOL_YY__RBRACKET = 63, // yy__rbracket
			SYMBOL_YY__READ = 64, // yy__read
			SYMBOL_YY__READ_PATH = 65, // yy__read_path
			SYMBOL_YY__REC_MAPSTO = 66, // yy__rec_mapsto
			SYMBOL_YY__REPLACED_BY = 67, // yy__replaced_by
			SYMBOL_YY__RIGHT_SHIFT = 68, // yy__right_shift
			SYMBOL_YY__RPAR = 69, // yy__rpar
			SYMBOL_YY__RPAR_ARROW = 70, // yy__rpar_arrow
			SYMBOL_YY__RWADDR = 71, // yy__RWAddr
			SYMBOL_YY__SEMICOLON = 72, // yy__semicolon
			SYMBOL_YY__SERIALIZE = 73, // yy__serialize
			SYMBOL_YY__SLASH = 74, // yy__slash
			SYMBOL_YY__STAR = 75, // yy__star
			SYMBOL_YY__STRUCTPTR = 76, // yy__StructPtr
			SYMBOL_YY__SUCCEEDS = 77, // yy__succeeds
			SYMBOL_YY__SUCCEEDS_AS = 78, // yy__succeeds_as
			SYMBOL_YY__SYMBOLL = 79, // yy__symbolL
			SYMBOL_YY__SYMBOLU = 80, // yy__SymbolU
			SYMBOL_YY__TERMINAL = 81, // yy__terminal
			SYMBOL_YY__THEN = 82, // yy__then
			SYMBOL_YY__TILDE = 83, // yy__tilde
			SYMBOL_YY__TYPE = 84, // yy__type
			SYMBOL_YY__TYPE_BYTEARRAY = 85, // yy__type_ByteArray
			SYMBOL_YY__TYPE_FLOAT = 86, // yy__type_Float
			SYMBOL_YY__TYPE_INT32 = 87, // yy__type_Int32
			SYMBOL_YY__TYPE_LISTENER = 88, // yy__type_Listener
			SYMBOL_YY__TYPE_STRING = 89, // yy__type_String
			SYMBOL_YY__UNSERIALIZE = 90, // yy__unserialize
			SYMBOL_YY__UTVAR = 91, // yy__utvar
			SYMBOL_YY__VAR = 92, // yy__Var
			SYMBOL_YY__VARIABLE = 93, // yy__variable
			SYMBOL_YY__VBAR = 94, // yy__vbar
			SYMBOL_YY__VCOPY = 95, // yy__vcopy
			SYMBOL_YY__WADDR = 96, // yy__WAddr
			SYMBOL_YY__WAIT_FOR = 97, // yy__wait_for
			SYMBOL_YY__WITH = 98, // yy__with
			SYMBOL_YY__WRITE = 99, // yy__write
			SYMBOL_ALTOPERAND = 100, // <Alt Operand>
			SYMBOL_ALTOPERANDS = 101, // <Alt Operands>
			SYMBOL_ALTERNATIVE = 102, // <Alternative>
			SYMBOL_ALTERNATIVES = 103, // <Alternatives>
			SYMBOL_ALTERNATIVES1 = 104, // <Alternatives1>
			SYMBOL_APPTERM = 105, // <App Term>
			SYMBOL_AVM = 106, // <AVM>
			SYMBOL_AVMINSTR = 107, // <AVM Instr>
			SYMBOL_CLAUSE = 108, // <Clause>
			SYMBOL_CLAUSES = 109, // <Clauses>
			SYMBOL_CONDITIONAL = 110, // <Conditional>
			SYMBOL_FARG = 111, // <FArg>
			SYMBOL_FARGS = 112, // <FArgs>
			SYMBOL_FUNCTYPE = 113, // <Func Type>
			SYMBOL_HEAD = 114, // <Head>
			SYMBOL_INTMCONS = 115, // <Int MCons>
			SYMBOL_LIST = 116, // <List>
			SYMBOL_LISTITEMS = 117, // <ListItems>
			SYMBOL_OPARG = 118, // <Op Arg>
			SYMBOL_OPARGS = 119, // <Op Args>
			SYMBOL_OPKW = 120, // <Op KW>
			SYMBOL_OPERATIONDECLARATION = 121, // <Operation Declaration>
			SYMBOL_OPERATIONDEFINITION = 122, // <Operation Definition>
			SYMBOL_PARAGRAPH = 123, // <Paragraph>
			SYMBOL_PARAGRAPHS = 124, // <Paragraphs>
			SYMBOL_RESURSYM = 125, // <Resur Sym>
			SYMBOL_RESURSYMS = 126, // <Resur Syms>
			SYMBOL_RESURSYMS1 = 127, // <Resur Syms1>
			SYMBOL_SIMPLEBINARYOP = 128, // <Simple Binary Op>
			SYMBOL_SIMPLEUNARYOP = 129, // <Simple Unary Op>
			SYMBOL_START = 130, // <Start>
			SYMBOL_TERM = 131, // <Term>
			SYMBOL_TERMS = 132, // <Terms>
			SYMBOL_TERMS2 = 133, // <Terms2>
			SYMBOL_TYPE = 134, // <Type>
			SYMBOL_TYPEDEFINITION = 135, // <Type Definition>
			SYMBOL_TYPEKW = 136, // <Type KW>
			SYMBOL_TYPEVARS = 137, // <Type Vars>
			SYMBOL_TYPES = 138, // <Types>
			SYMBOL_TYPESARGS = 139, // <Types Args>
			SYMBOL_VARKW = 140, // <Var KW>
			SYMBOL_VARIABLEDECLARATION = 141, // <Variable Declaration>
			SYMBOL_WITHTERM = 142, // <With Term>
			SYMBOL_YYMILLISECONDS = 143  // <yy  milliseconds>
		};

		public enum RuleConstants : int
		{
			RULE_START = 0, // <Start> ::= <Paragraphs>
			RULE_PARAGRAPHS = 1, // <Paragraphs> ::= <Paragraphs> <Paragraph>
			RULE_PARAGRAPHS2 = 2, // <Paragraphs> ::= <Paragraph>
			RULE_PARAGRAPH = 3, // <Paragraph> ::= <Type Definition>
			RULE_PARAGRAPH2 = 4, // <Paragraph> ::= <Operation Definition>
			RULE_PARAGRAPH3 = 5, // <Paragraph> ::= <Operation Declaration>
			RULE_PARAGRAPH4 = 6, // <Paragraph> ::= <Variable Declaration>
			RULE_PARAGRAPH_YY__READ_YY__READ_PATH = 7, // <Paragraph> ::= yy__read yy__read_path
			RULE_PARAGRAPH_YY__REPLACED_BY_YY__READ_PATH = 8, // <Paragraph> ::= yy__replaced_by yy__read_path
			RULE_OPKW_YY__OPERATION = 9, // <Op KW> ::= yy__operation
			RULE_OPKW_YY__P_OPERATION = 10, // <Op KW> ::= yy__p_operation
			RULE_OPKW_YY__G_OPERATION = 11, // <Op KW> ::= yy__g_operation
			RULE_VARKW_YY__VARIABLE = 12, // <Var KW> ::= yy__variable
			RULE_VARKW_YY__P_VARIABLE = 13, // <Var KW> ::= yy__p_variable
			RULE_TYPEKW_YY__TYPE = 14, // <Type KW> ::= yy__type
			RULE_TYPEKW_YY__P_TYPE = 15, // <Type KW> ::= yy__p_type
			RULE_TYPEDEFINITION_YY__SYMBOLU_YY__EQUALS_YY__DOT = 16, // <Type Definition> ::= <Type KW> yy__SymbolU yy__equals <Type> yy__dot
			RULE_TYPEDEFINITION_YY__SYMBOLU_YY__COLON = 17, // <Type Definition> ::= <Type KW> yy__SymbolU yy__colon <Alternatives>
			RULE_TYPEDEFINITION_YY__SYMBOLU_YY__LPAR_YY__RPAR_YY__COLON = 18, // <Type Definition> ::= <Type KW> yy__SymbolU yy__lpar <Type Vars> yy__rpar yy__colon <Alternatives>
			RULE_TYPEVARS_YY__UTVAR_YY__COMMA = 19, // <Type Vars> ::= yy__utvar yy__comma <Type Vars>
			RULE_TYPEVARS_YY__UTVAR = 20, // <Type Vars> ::= yy__utvar
			RULE_ALTERNATIVES_YY__DOT = 21, // <Alternatives> ::= yy__dot
			RULE_ALTERNATIVES_YY__DOTS = 22, // <Alternatives> ::= yy__dots
			RULE_ALTERNATIVES = 23, // <Alternatives> ::= <Alternatives1>
			RULE_ALTERNATIVES1_YY__DOT = 24, // <Alternatives1> ::= <Alternative> yy__dot
			RULE_ALTERNATIVES1_YY__COMMA_YY__DOTS = 25, // <Alternatives1> ::= <Alternative> yy__comma yy__dots
			RULE_ALTERNATIVES1_YY__COMMA = 26, // <Alternatives1> ::= <Alternative> yy__comma <Alternatives1>
			RULE_ALTERNATIVE_YY__SYMBOLL = 27, // <Alternative> ::= yy__symbolL
			RULE_ALTERNATIVE_YY__LBRACKET_YY__RBRACKET = 28, // <Alternative> ::= yy__lbracket yy__rbracket
			RULE_ALTERNATIVE_YY__SYMBOLL_YY__LPAR_YY__RPAR = 29, // <Alternative> ::= yy__symbolL yy__lpar <Alt Operands> yy__rpar
			RULE_ALTERNATIVE_YY__LBRACKET_YY__DOT_YY__RBRACKET = 30, // <Alternative> ::= yy__lbracket <Alt Operand> yy__dot <Alt Operand> yy__rbracket
			RULE_ALTERNATIVE_YY__PLUS = 31, // <Alternative> ::= <Alt Operand> yy__plus <Alt Operand>
			RULE_ALTERNATIVE_YY__STAR = 32, // <Alternative> ::= <Alt Operand> yy__star <Alt Operand>
			RULE_ALTERNATIVE_YY__PERCENT = 33, // <Alternative> ::= <Alt Operand> yy__percent <Alt Operand>
			RULE_ALTERNATIVE_YY__CARRET = 34, // <Alternative> ::= <Alt Operand> yy__carret <Alt Operand>
			RULE_ALTERNATIVE_YY__VBAR = 35, // <Alternative> ::= <Alt Operand> yy__vbar <Alt Operand>
			RULE_ALTERNATIVE_YY__AMPERSAND = 36, // <Alternative> ::= <Alt Operand> yy__ampersand <Alt Operand>
			RULE_ALTERNATIVE_YY__ARROW = 37, // <Alternative> ::= <Alt Operand> yy__arrow <Alt Operand>
			RULE_ALTERNATIVE_YY__EQUALS = 38, // <Alternative> ::= <Alt Operand> yy__equals <Alt Operand>
			RULE_ALTERNATIVE_YY__IMPLIES = 39, // <Alternative> ::= <Alt Operand> yy__implies <Alt Operand>
			RULE_ALTERNATIVE_YY__LEFT_SHIFT = 40, // <Alternative> ::= <Alt Operand> yy__left_shift <Alt Operand>
			RULE_ALTERNATIVE_YY__RIGHT_SHIFT = 41, // <Alternative> ::= <Alt Operand> yy__right_shift <Alt Operand>
			RULE_ALTERNATIVE_YY__MINUS = 42, // <Alternative> ::= <Alt Operand> yy__minus <Alt Operand>
			RULE_ALTERNATIVE_YY__SLASH = 43, // <Alternative> ::= <Alt Operand> yy__slash <Alt Operand>
			RULE_ALTERNATIVE_YY__MOD_YY__RPAR = 44, // <Alternative> ::= <Alt Operand> yy__mod <Alt Operand> yy__rpar
			RULE_ALTERNATIVE_YY__LESS = 45, // <Alternative> ::= <Alt Operand> yy__less <Alt Operand>
			RULE_ALTERNATIVE_YY__NON_EQUAL = 46, // <Alternative> ::= <Alt Operand> yy__non_equal <Alt Operand>
			RULE_ALTERNATIVE_YY__LESSOREQ = 47, // <Alternative> ::= <Alt Operand> yy__lessoreq <Alt Operand>
			RULE_ALTERNATIVE_YY__TILDE = 48, // <Alternative> ::= yy__tilde <Alt Operand>
			RULE_ALTERNATIVE_YY__STAR2 = 49, // <Alternative> ::= yy__star <Alt Operand>
			RULE_ALTOPERANDS_YY__COMMA = 50, // <Alt Operands> ::= <Alt Operands> yy__comma <Alt Operand>
			RULE_ALTOPERANDS = 51, // <Alt Operands> ::= <Alt Operand>
			RULE_ALTOPERAND = 52, // <Alt Operand> ::= <Type>
			RULE_ALTOPERAND_YY__SYMBOLL = 53, // <Alt Operand> ::= <Type> yy__symbolL
			RULE_TYPES_YY__COMMA = 54, // <Types> ::= <Type> yy__comma <Types>
			RULE_TYPES = 55, // <Types> ::= <Type>
			RULE_TYPE_YY__SYMBOLU = 56, // <Type> ::= yy__SymbolU
			RULE_TYPE = 57, // <Type> ::= <Func Type>
			RULE_TYPE_YY__LPAR_YY__RPAR = 58, // <Type> ::= yy__lpar <Type> yy__rpar
			RULE_TYPE_YY__UTVAR = 59, // <Type> ::= yy__utvar
			RULE_TYPE_YY__TYPE_STRING = 60, // <Type> ::= yy__type_String
			RULE_TYPE_YY__TYPE_BYTEARRAY = 61, // <Type> ::= yy__type_ByteArray
			RULE_TYPE_YY__TYPE_INT32 = 62, // <Type> ::= yy__type_Int32
			RULE_TYPE_YY__TYPE_FLOAT = 63, // <Type> ::= yy__type_Float
			RULE_TYPE_YY__TYPE_LISTENER = 64, // <Type> ::= yy__type_Listener
			RULE_TYPE_YY__RADDR_YY__LPAR_YY__RPAR = 65, // <Type> ::= yy__RAddr yy__lpar <Type> yy__rpar
			RULE_TYPE_YY__WADDR_YY__LPAR_YY__RPAR = 66, // <Type> ::= yy__WAddr yy__lpar <Type> yy__rpar
			RULE_TYPE_YY__RWADDR_YY__LPAR_YY__RPAR = 67, // <Type> ::= yy__RWAddr yy__lpar <Type> yy__rpar
			RULE_TYPE_YY__GADDR_YY__LPAR_YY__RPAR = 68, // <Type> ::= yy__GAddr yy__lpar <Type> yy__rpar
			RULE_TYPE_YY__VAR_YY__LPAR_YY__RPAR = 69, // <Type> ::= yy__Var yy__lpar <Type> yy__rpar
			RULE_TYPE_YY__MVAR_YY__LPAR_YY__RPAR = 70, // <Type> ::= yy__MVar yy__lpar <Type> yy__rpar
			RULE_TYPE_YY__SYMBOLU_YY__LPAR_YY__RPAR = 71, // <Type> ::= yy__SymbolU yy__lpar <Types> yy__rpar
			RULE_TYPE_YY__STRUCTPTR_YY__LPAR_YY__SYMBOLU_YY__RPAR = 72, // <Type> ::= yy__StructPtr yy__lpar yy__SymbolU yy__rpar
			RULE_TYPE_YY__LPAR_YY__COMMA_YY__RPAR = 73, // <Type> ::= yy__lpar <Type> yy__comma <Types> yy__rpar
			RULE_TYPE_YY__LBRACE_YY__RBRACE = 74, // <Type> ::= yy__lbrace <Type> yy__rbrace
			RULE_FUNCTYPE_YY__ARROW = 75, // <Func Type> ::= <Type> yy__arrow <Type>
			RULE_FUNCTYPE_YY__LPAR_YY__RPAR_ARROW = 76, // <Func Type> ::= yy__lpar <Types Args> yy__rpar_arrow <Type>
			RULE_TYPESARGS = 77, // <Types Args> ::= <Type>
			RULE_TYPESARGS_YY__SYMBOLL = 78, // <Types Args> ::= <Type> yy__symbolL
			RULE_TYPESARGS_YY__COMMA = 79, // <Types Args> ::= <Type> yy__comma <Types Args>
			RULE_TYPESARGS_YY__SYMBOLL_YY__COMMA = 80, // <Types Args> ::= <Type> yy__symbolL yy__comma <Types Args>
			RULE_VARIABLEDECLARATION_YY__SYMBOLL_YY__EQUALS_YY__DOT = 81, // <Variable Declaration> ::= <Var KW> <Type> yy__symbolL yy__equals <Term> yy__dot
			RULE_OPERATIONDEFINITION_YY__SYMBOLL_YY__EQUALS_YY__DOT = 82, // <Operation Definition> ::= <Op KW> <Type> yy__symbolL yy__equals <Term> yy__dot
			RULE_OPERATIONDEFINITION_YY__SYMBOLL_YY__LPAR_YY__RPAR_YY__EQUALS_YY__DOT = 83, // <Operation Definition> ::= <Op KW> <Type> yy__symbolL yy__lpar <Op Args> yy__rpar yy__equals <Term> yy__dot
			RULE_OPERATIONDEFINITION_YY__EQUALS_YY__DOT = 84, // <Operation Definition> ::= <Op KW> <Type> <Op Arg> <Simple Binary Op> <Op Arg> yy__equals <Term> yy__dot
			RULE_OPERATIONDEFINITION_YY__EQUALS_YY__DOT2 = 85, // <Operation Definition> ::= <Op KW> <Type> <Simple Unary Op> <Op Arg> yy__equals <Term> yy__dot
			RULE_OPERATIONDEFINITION_YY__MOD_YY__RPAR_YY__EQUALS_YY__DOT = 86, // <Operation Definition> ::= <Op KW> <Type> <Op Arg> yy__mod <Op Arg> yy__rpar yy__equals <Term> yy__dot
			RULE_OPERATIONDECLARATION_YY__SYMBOLL_YY__LPAR_YY__RPAR_YY__DOT = 87, // <Operation Declaration> ::= <Op KW> <Type> yy__symbolL yy__lpar <Op Args> yy__rpar yy__dot
			RULE_OPERATIONDECLARATION_YY__SYMBOLL_YY__DOT = 88, // <Operation Declaration> ::= <Op KW> <Type> yy__symbolL yy__dot
			RULE_OPERATIONDECLARATION_YY__DOT = 89, // <Operation Declaration> ::= <Op KW> <Type> <Op Arg> <Simple Binary Op> <Op Arg> yy__dot
			RULE_OPERATIONDECLARATION_YY__DOT2 = 90, // <Operation Declaration> ::= <Op KW> <Type> <Simple Unary Op> <Op Arg> yy__dot
			RULE_OPERATIONDECLARATION_YY__MOD_YY__RPAR_YY__DOT = 91, // <Operation Declaration> ::= <Op KW> <Type> <Op Arg> yy__mod <Op Arg> yy__rpar yy__dot
			RULE_SIMPLEBINARYOP_YY__PLUS = 92, // <Simple Binary Op> ::= yy__plus
			RULE_SIMPLEBINARYOP_YY__STAR = 93, // <Simple Binary Op> ::= yy__star
			RULE_SIMPLEBINARYOP_YY__PERCENT = 94, // <Simple Binary Op> ::= yy__percent
			RULE_SIMPLEBINARYOP_YY__CARRET = 95, // <Simple Binary Op> ::= yy__carret
			RULE_SIMPLEBINARYOP_YY__VBAR = 96, // <Simple Binary Op> ::= yy__vbar
			RULE_SIMPLEBINARYOP_YY__AMPERSAND = 97, // <Simple Binary Op> ::= yy__ampersand
			RULE_SIMPLEBINARYOP_YY__ARROW = 98, // <Simple Binary Op> ::= yy__arrow
			RULE_SIMPLEBINARYOP_YY__EQUALS = 99, // <Simple Binary Op> ::= yy__equals
			RULE_SIMPLEBINARYOP_YY__IMPLIES = 100, // <Simple Binary Op> ::= yy__implies
			RULE_SIMPLEBINARYOP_YY__LEFT_SHIFT = 101, // <Simple Binary Op> ::= yy__left_shift
			RULE_SIMPLEBINARYOP_YY__RIGHT_SHIFT = 102, // <Simple Binary Op> ::= yy__right_shift
			RULE_SIMPLEBINARYOP_YY__MINUS = 103, // <Simple Binary Op> ::= yy__minus
			RULE_SIMPLEBINARYOP_YY__SLASH = 104, // <Simple Binary Op> ::= yy__slash
			RULE_SIMPLEBINARYOP_YY__LESS = 105, // <Simple Binary Op> ::= yy__less
			RULE_SIMPLEBINARYOP_YY__NON_EQUAL = 106, // <Simple Binary Op> ::= yy__non_equal
			RULE_SIMPLEBINARYOP_YY__LESSOREQ = 107, // <Simple Binary Op> ::= yy__lessoreq
			RULE_SIMPLEUNARYOP_YY__MINUS = 108, // <Simple Unary Op> ::= yy__minus
			RULE_SIMPLEUNARYOP_YY__TILDE = 109, // <Simple Unary Op> ::= yy__tilde
			RULE_OPARGS_YY__COMMA = 110, // <Op Args> ::= <Op Arg> yy__comma <Op Args>
			RULE_OPARGS_YY__COMMA2 = 111, // <Op Args> ::= <Op Arg> yy__comma
			RULE_OPARGS = 112, // <Op Args> ::= <Op Arg>
			RULE_OPARG_YY__SYMBOLL = 113, // <Op Arg> ::= <Type> yy__symbolL
			RULE_FARGS_YY__COMMA = 114, // <FArgs> ::= <FArg> yy__comma <FArgs>
			RULE_FARGS = 115, // <FArgs> ::= <FArg>
			RULE_FARG_YY__SYMBOLL = 116, // <FArg> ::= <Type> yy__symbolL
			RULE_TERMS = 117, // <Terms> ::= <Terms2>
			RULE_TERMS2 = 118, // <Terms> ::= <Term>
			RULE_TERMS2_YY__COMMA = 119, // <Terms2> ::= <Terms> yy__comma <Term>
			RULE_TERM_YY__PROTECT = 120, // <Term> ::= yy__protect <Term>
			RULE_TERM_YY__DEBUG_AVM = 121, // <Term> ::= yy__debug_avm <Term>
			RULE_TERM_YY__TERMINAL = 122, // <Term> ::= yy__terminal <Term>
			RULE_TERM_YY__LOCK_YY__COMMA = 123, // <Term> ::= yy__lock <Term> yy__comma <Term>
			RULE_TERM_YY__DELEGATE_YY__COMMA = 124, // <Term> ::= yy__delegate <Term> yy__comma <Term>
			RULE_TERM_YY__CHECKING_EVERY_YY__COMMA_YY__WAIT_FOR_YY__THEN = 125, // <Term> ::= yy__checking_every <Term> <yy  milliseconds> yy__comma yy__wait_for <Term> yy__then <Term>
			RULE_TERM_YY__SEMICOLON = 126, // <Term> ::= <Term> yy__semicolon <Term>
			RULE_TERM_YY__SYMBOLL = 127, // <Term> ::= yy__symbolL
			RULE_TERM_YY__LPAR_YY__RPAR = 128, // <Term> ::= yy__lpar yy__rpar
			RULE_TERM_YY__INTEGER = 129, // <Term> ::= yy__integer
			RULE_TERM_YY__CHAR = 130, // <Term> ::= yy__char
			RULE_TERM_YY__FLOAT = 131, // <Term> ::= yy__float
			RULE_TERM_YY__FORALL_YY__SYMBOLL_YY__COLON_YY__COMMA = 132, // <Term> ::= yy__forall yy__symbolL yy__colon <Type> yy__comma <Term>
			RULE_TERM_YY__FORALL_YY__SYMBOLL_YY__COLON_YY__COMMA2 = 133, // <Term> ::= yy__forall yy__symbolL yy__colon <Term> yy__comma <Term>
			RULE_TERM_YY__EXISTS_YY__SYMBOLL_YY__COLON_YY__COMMA = 134, // <Term> ::= yy__exists yy__symbolL yy__colon <Type> yy__comma <Term>
			RULE_TERM_YY__EXISTS_UNIQUE_YY__SYMBOLL_YY__COLON_YY__COMMA = 135, // <Term> ::= yy__exists_unique yy__symbolL yy__colon <Type> yy__comma <Term>
			RULE_TERM_YY__DESCRIPTION_YY__SYMBOLL_YY__COLON_YY__COMMA = 136, // <Term> ::= yy__description yy__symbolL yy__colon <Type> yy__comma <Term>
			RULE_TERM_YY__LPAR_YY__RPAR2 = 137, // <Term> ::= yy__lpar <Term> yy__rpar
			RULE_TERM_YY__LPAR_YY__RPAR3 = 138, // <Term> ::= yy__lpar <Terms2> yy__rpar
			RULE_TERM_YY__LPAR_YY__RPAR4 = 139, // <Term> ::= yy__lpar <Type> yy__rpar <Term>
			RULE_TERM_YY__LPAR_YY__COLON_YY__RPAR = 140, // <Term> ::= yy__lpar yy__colon <Term> yy__rpar <Term>
			RULE_TERM_YY__LPAR_YY__COLON_YY__RPAR2 = 141, // <Term> ::= yy__lpar yy__colon <Type> yy__rpar <Term>
			RULE_TERM_YY__WRITE = 142, // <Term> ::= <Term> yy__write <Term>
			RULE_TERM_YY__EXCHANGE = 143, // <Term> ::= <Term> yy__exchange <Term>
			RULE_TERM_YY__STAR = 144, // <Term> ::= yy__star <Term>
			RULE_TERM_YY__LPAR_YY__RPAR_YY__CONNECT_TO_FILE = 145, // <Term> ::= yy__lpar <Type> yy__rpar yy__connect_to_file <Term>
			RULE_TERM_YY__LPAR_YY__RPAR_YY__CONNECT_TO_IP_YY__COLON = 146, // <Term> ::= yy__lpar <Type> yy__rpar yy__connect_to_IP <Term> yy__colon <Term>
			RULE_TERM_YY__LPAR_YY__RPAR_YY__MAPSTO = 147, // <Term> ::= yy__lpar <FArgs> yy__rpar yy__mapsto <Term>
			RULE_TERM_YY__LPAR_YY__RPAR_YY__REC_MAPSTO = 148, // <Term> ::= yy__lpar <FArgs> yy__rpar yy__rec_mapsto <Term>
			RULE_TERM_YY__ALERT = 149, // <Term> ::= yy__alert
			RULE_TERM_YY__ALT_NUMBER_YY__LPAR_YY__RPAR = 150, // <Term> ::= yy__alt_number yy__lpar <Term> yy__rpar
			RULE_TERM_YY__AVM_YY__LBRACE_YY__RBRACE = 151, // <Term> ::= yy__avm yy__lbrace <AVM> yy__rbrace
			RULE_TERM = 152, // <Term> ::= <App Term>
			RULE_TERM2 = 153, // <Term> ::= <Conditional>
			RULE_TERM3 = 154, // <Term> ::= <List>
			RULE_TERM_YY__LOAD_MODULE_YY__LPAR_YY__RPAR = 155, // <Term> ::= yy__load_module yy__lpar <Term> yy__rpar
			RULE_TERM_YY__SERIALIZE_YY__LPAR_YY__RPAR = 156, // <Term> ::= yy__serialize yy__lpar <Term> yy__rpar
			RULE_TERM_YY__UNSERIALIZE_YY__LPAR_YY__RPAR = 157, // <Term> ::= yy__unserialize yy__lpar <Term> yy__rpar
			RULE_TERM_YY__BIT_WIDTH_YY__LPAR_YY__RPAR = 158, // <Term> ::= yy__bit_width yy__lpar <Type> yy__rpar
			RULE_TERM_YY__INDIRECT_YY__LPAR_YY__RPAR = 159, // <Term> ::= yy__indirect yy__lpar <Type> yy__rpar
			RULE_TERM_YY__VCOPY_YY__LPAR_YY__COMMA_YY__RPAR = 160, // <Term> ::= yy__vcopy yy__lpar <Term> yy__comma <Term> yy__rpar
			RULE_TERM_YY__ANB_STRING = 161, // <Term> ::= yy__anb_string
			RULE_TERM_YY__WITH_YY__SYMBOLL_YY__EQUALS_YY__COMMA = 162, // <Term> ::= yy__with yy__symbolL yy__equals <Term> yy__comma <With Term>
			RULE_TERM_YY__LBRACE_YY__SYMBOLL_YY__COLON_YY__COMMA_YY__RBRACE = 163, // <Term> ::= yy__lbrace yy__symbolL yy__colon <Type> yy__comma <Term> yy__rbrace
			RULE_WITHTERM = 164, // <With Term> ::= <Term>
			RULE_WITHTERM_YY__SYMBOLL_YY__EQUALS_YY__COMMA = 165, // <With Term> ::= yy__symbolL yy__equals <Term> yy__comma <With Term>
			RULE_LIST_YY__LBRACKET_YY__RBRACKET = 166, // <List> ::= yy__lbracket <ListItems> yy__rbracket
			RULE_LIST_YY__LBRACKET_YY__RBRACKET2 = 167, // <List> ::= yy__lbracket yy__rbracket
			RULE_LIST_YY__LBRACKET_YY__DOT_YY__RBRACKET = 168, // <List> ::= yy__lbracket <Term> yy__dot <Term> yy__rbracket
			RULE_LISTITEMS_YY__COMMA = 169, // <ListItems> ::= <ListItems> yy__comma <Term>
			RULE_LISTITEMS_YY__COMMA2 = 170, // <ListItems> ::= <ListItems> yy__comma
			RULE_LISTITEMS = 171, // <ListItems> ::= <Term>
			RULE_APPTERM_YY__MINUS = 172, // <App Term> ::= yy__minus <Term>
			RULE_APPTERM_YY__LPAR_YY__RPAR = 173, // <App Term> ::= <Term> yy__lpar <Terms> yy__rpar
			RULE_APPTERM = 174, // <App Term> ::= <Term> <List>
			RULE_APPTERM_YY__PLUS = 175, // <App Term> ::= <Term> yy__plus <Term>
			RULE_APPTERM_YY__STAR = 176, // <App Term> ::= <Term> yy__star <Term>
			RULE_APPTERM_YY__PERCENT = 177, // <App Term> ::= <Term> yy__percent <Term>
			RULE_APPTERM_YY__CARRET = 178, // <App Term> ::= <Term> yy__carret <Term>
			RULE_APPTERM_YY__VBAR = 179, // <App Term> ::= <Term> yy__vbar <Term>
			RULE_APPTERM_YY__AMPERSAND = 180, // <App Term> ::= <Term> yy__ampersand <Term>
			RULE_APPTERM_YY__ARROW = 181, // <App Term> ::= <Term> yy__arrow <Term>
			RULE_APPTERM_YY__EQUALS = 182, // <App Term> ::= <Term> yy__equals <Term>
			RULE_APPTERM_YY__IMPLIES = 183, // <App Term> ::= <Term> yy__implies <Term>
			RULE_APPTERM_YY__LEFT_SHIFT = 184, // <App Term> ::= <Term> yy__left_shift <Term>
			RULE_APPTERM_YY__RIGHT_SHIFT = 185, // <App Term> ::= <Term> yy__right_shift <Term>
			RULE_APPTERM_YY__MINUS2 = 186, // <App Term> ::= <Term> yy__minus <Term>
			RULE_APPTERM_YY__SLASH = 187, // <App Term> ::= <Term> yy__slash <Term>
			RULE_APPTERM_YY__LESS = 188, // <App Term> ::= <Term> yy__less <Term>
			RULE_APPTERM_YY__NON_EQUAL = 189, // <App Term> ::= <Term> yy__non_equal <Term>
			RULE_APPTERM_YY__GREATER = 190, // <App Term> ::= <Term> yy__greater <Term>
			RULE_APPTERM_YY__LESSOREQ = 191, // <App Term> ::= <Term> yy__lessoreq <Term>
			RULE_APPTERM_YY__GREATEROREQ = 192, // <App Term> ::= <Term> yy__greateroreq <Term>
			RULE_APPTERM_YY__MOD_YY__RPAR = 193, // <App Term> ::= <Term> yy__mod <Term> yy__rpar
			RULE_APPTERM_YY__TILDE = 194, // <App Term> ::= yy__tilde <Term>
			RULE_CONDITIONAL_YY__IF_YY__IS_YY__LBRACE_YY__RBRACE = 195, // <Conditional> ::= yy__if <Term> yy__is yy__lbrace <Clauses> yy__rbrace
			RULE_CONDITIONAL_YY__IF_YY__SUCCEEDS_AS_YY__SYMBOLL_YY__THEN = 196, // <Conditional> ::= yy__if <Term> yy__succeeds_as yy__symbolL yy__then <Term>
			RULE_CONDITIONAL_YY__IF_YY__SUCCEEDS_YY__THEN = 197, // <Conditional> ::= yy__if <Term> yy__succeeds yy__then <Term>
			RULE_CONDITIONAL_YY__IF_YY__IS = 198, // <Conditional> ::= yy__if <Term> yy__is <Clause>
			RULE_CONDITIONAL_YY__IF_YY__THEN_YY__ELSE = 199, // <Conditional> ::= yy__if <Term> yy__then <Term> yy__else <Term>
			RULE_CONDITIONAL_YY__IF_YY__IS_YY__ELSE = 200, // <Conditional> ::= yy__if <Term> yy__is <Clause> yy__else <Term>
			RULE_CLAUSES_YY__COMMA = 201, // <Clauses> ::= <Clauses> yy__comma <Clause>
			RULE_CLAUSES_YY__COMMA2 = 202, // <Clauses> ::= <Clauses> yy__comma
			RULE_CLAUSES = 203, // <Clauses> ::= <Clause>
			RULE_CLAUSES2 = 204, // <Clauses> ::= <Clauses> <Clause>
			RULE_CLAUSE_YY__THEN = 205, // <Clause> ::= <Head> yy__then <Term>
			RULE_HEAD_YY__SYMBOLL = 206, // <Head> ::= yy__symbolL
			RULE_HEAD_YY__LBRACKET_YY__RBRACKET = 207, // <Head> ::= yy__lbracket yy__rbracket
			RULE_HEAD_YY__SYMBOLL_YY__LPAR_YY__RPAR = 208, // <Head> ::= yy__symbolL yy__lpar yy__rpar
			RULE_HEAD_YY__SYMBOLL_YY__LPAR_YY__RPAR2 = 209, // <Head> ::= yy__symbolL yy__lpar <Resur Syms> yy__rpar
			RULE_HEAD_YY__LBRACKET_YY__DOT_YY__RBRACKET = 210, // <Head> ::= yy__lbracket <Resur Sym> yy__dot <Resur Sym> yy__rbracket
			RULE_HEAD_YY__PLUS = 211, // <Head> ::= <Resur Sym> yy__plus <Resur Sym>
			RULE_HEAD_YY__STAR = 212, // <Head> ::= <Resur Sym> yy__star <Resur Sym>
			RULE_HEAD_YY__PERCENT = 213, // <Head> ::= <Resur Sym> yy__percent <Resur Sym>
			RULE_HEAD_YY__CARRET = 214, // <Head> ::= <Resur Sym> yy__carret <Resur Sym>
			RULE_HEAD_YY__VBAR = 215, // <Head> ::= <Resur Sym> yy__vbar <Resur Sym>
			RULE_HEAD_YY__AMPERSAND = 216, // <Head> ::= <Resur Sym> yy__ampersand <Resur Sym>
			RULE_HEAD_YY__ARROW = 217, // <Head> ::= <Resur Sym> yy__arrow <Resur Sym>
			RULE_HEAD_YY__EQUALS = 218, // <Head> ::= <Resur Sym> yy__equals <Resur Sym>
			RULE_HEAD_YY__IMPLIES = 219, // <Head> ::= <Resur Sym> yy__implies <Resur Sym>
			RULE_HEAD_YY__LEFT_SHIFT = 220, // <Head> ::= <Resur Sym> yy__left_shift <Resur Sym>
			RULE_HEAD_YY__RIGHT_SHIFT = 221, // <Head> ::= <Resur Sym> yy__right_shift <Resur Sym>
			RULE_HEAD_YY__MINUS = 222, // <Head> ::= <Resur Sym> yy__minus <Resur Sym>
			RULE_HEAD_YY__SLASH = 223, // <Head> ::= <Resur Sym> yy__slash <Resur Sym>
			RULE_HEAD_YY__MOD_YY__RPAR = 224, // <Head> ::= <Resur Sym> yy__mod <Resur Sym> yy__rpar
			RULE_HEAD_YY__LESS = 225, // <Head> ::= <Resur Sym> yy__less <Resur Sym>
			RULE_HEAD_YY__NON_EQUAL = 226, // <Head> ::= <Resur Sym> yy__non_equal <Resur Sym>
			RULE_HEAD_YY__LESSOREQ = 227, // <Head> ::= <Resur Sym> yy__lessoreq <Resur Sym>
			RULE_HEAD_YY__TILDE = 228, // <Head> ::= yy__tilde <Resur Sym>
			RULE_HEAD_YY__LPAR_YY__RPAR = 229, // <Head> ::= yy__lpar <Resur Syms1> yy__rpar
			RULE_RESURSYM_YY__SYMBOLL = 230, // <Resur Sym> ::= yy__symbolL
			RULE_RESURSYM_YY__SYMBOLL2 = 231, // <Resur Sym> ::= <Type> yy__symbolL
			RULE_RESURSYMS = 232, // <Resur Syms> ::= <Resur Sym>
			RULE_RESURSYMS2 = 233, // <Resur Syms> ::= <Resur Syms1>
			RULE_RESURSYMS1_YY__COMMA = 234, // <Resur Syms1> ::= <Resur Syms> yy__comma <Resur Sym>
			RULE_AVM = 235, // <AVM> ::= <AVM Instr> <AVM>
			RULE_AVMINSTR_YY__SYMBOLL = 236, // <AVM Instr> ::= yy__symbolL
			RULE_AVMINSTR_YY__LPAR_YY__SYMBOLL = 237, // <AVM Instr> ::= yy__lpar yy__symbolL <Int MCons>
			RULE_INTMCONS_YY__RPAR = 238, // <Int MCons> ::= yy__rpar
			RULE_INTMCONS_YY__DOT_YY__INTEGER_YY__RPAR = 239, // <Int MCons> ::= yy__dot yy__integer yy__rpar
			RULE_INTMCONS_YY__INTEGER = 240, // <Int MCons> ::= yy__integer <Int MCons>
			RULE_INTMCONS_YY__ANB_STRING = 241, // <Int MCons> ::= yy__anb_string <Int MCons>
			RULE_YYMILLISECONDS_YY__SYMBOLL = 242  // <yy  milliseconds> ::= yy__symbolL
		};

	}

	[Serializable()]
	public class RuleException : System.Exception
	{

		public RuleException(string message)
			: base(message)
		{
		}

		public RuleException(string message,
												 Exception inner)
			: base(message, inner)
		{
		}

		protected RuleException(SerializationInfo info,
														StreamingContext context)
			: base(info, context)
		{
		}

	}
}