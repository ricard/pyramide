#region Using directives

using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Text;

using GoldParser;
using ICSharpCode.Core;
using ICSharpCode.SharpDevelop.Dom;

#endregion

namespace SoftArchitect.AnubisBinding.Parser
{
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class ParserEngine
	{
		private Grammar _grammar;
		private TextReader _reader;
		private string _filename;
		IProjectContent _projectContent;
		private Context _context;

		public ParserEngine(TextReader reader, string filename, IProjectContent projectContent)
		{
			_reader = reader;
			_filename = filename;
			_projectContent = projectContent;
			LoadGrammar();
		}

		private void LoadGrammar()
		{
			string fileName = Path.GetDirectoryName(System.Reflection.Assembly.GetCallingAssembly().Location);
			fileName = Path.Combine(fileName, "Resources");
			fileName = Path.Combine(fileName, "anubis.cgt");
			using (Stream stream = File.OpenRead(fileName))
			{
				BinaryReader reader = new BinaryReader(stream);
				_grammar = new Grammar(reader);
			}
		}

		private void PushCommentEndToken(GoldParser.Parser parser)
		{
			parser.PushInputToken(new Symbol((int)Context.SymbolConstants.SYMBOL_COMMENTEND, "Comment End", SymbolType.CommentEnd),
															"*/",
															null);
		}

		private void PushCommentStartToken(GoldParser.Parser parser)
		{
			parser.PushInputToken(new Symbol((int)Context.SymbolConstants.SYMBOL_COMMENTSTART, "Comment Start", SymbolType.CommentStart),
														"/*",
														null);
		}

		private void SourceLineReadCallback(GoldParser.Parser parser,
		                                   int lineStart,
		                                   int lineLength)
		{
			string line = parser.LineText;
			if (lineLength > 0 && !line.StartsWith(" ") && !line.StartsWith("\t")
				&& !line.StartsWith("//") && !line.StartsWith("/*") && !line.StartsWith("*/")
			  && !line.StartsWith("."))
			{
				PushCommentEndToken(parser);
			}
		}

		public ICompilationUnit DoParse()
		{
			//This procedure starts the GOLD Parser Engine and handles each of the
			//messages it returns. Each time a reduction is made, a new instance of a
			//"Simple" object is created and stored in the parse tree. The resulting tree
			//will be a pure representation of the Simple language and will be ready to
			//implement.
			GoldParser.Parser parser = new GoldParser.Parser(_reader, _grammar);
			PushCommentStartToken(parser);
			parser.SourceLineReadCallback = SourceLineReadCallback;
			parser.TrimReductions = false;
			ICompilationUnit _program = new DefaultCompilationUnit(_projectContent);
			_program.FileName = _filename;
			_context = new Context(parser, _program);
			while (true)
			{
				try
				{
					switch (parser.Parse())
					{
						case ParseMessage.LexicalError:
							LoggingService.Error("LEXICAL ERROR. Line " + parser.LineNumber + ". Cannot recognize token: " + parser.TokenText);
							_program.ErrorsDuringCompile = true;
							return _program;

						case ParseMessage.SyntaxError:
							if (parser.TokenSymbol.Index != (int)Context.SymbolConstants.SYMBOL_EOF)
							{
								parser.PushInputToken(new Symbol((int)Context.SymbolConstants.SYMBOL_EOF, null, SymbolType.End), null, null);
								break;
							}
							else
							{
								if (parser.InputTokenCount > 0)
									parser.PopInputToken(); // Discard the fake EOF token

								if (parser.InputTokenCount > 0 && parser.TokenSymbol.SymbolType == SymbolType.CommentEnd)
								{
									parser.PopInputToken(); // Discard the wrongly inserted CommentEnd token
																					// This can occure when there is something into the
																					// first column inside a paragraph
									break;
								}
								
								StringBuilder text = new StringBuilder();
								foreach (Symbol tokenSymbol in parser.GetExpectedTokens())
								{
									text.Append('\n');
									text.Append(tokenSymbol.ToString());
								}
								string msg = "SYNTAX ERROR. Line " + parser.LineNumber + ". Found '" + parser.TokenSymbol + "' Expecting:" + text.ToString();
								LoggingService.Error(msg);
								_program.ErrorsDuringCompile = true;
								//TaskService.Add(new Task(new CompilerError(_filename, parser.LineNumber, parser.LinePosition, "E0", msg)));
								return _program;
							}

						case ParseMessage.Reduction:
							//== Create a new customized object and replace the
							//== CurrentReduction with it. This saves memory and allows
							//== easier interpretation
							_context.OnReduction();
							if (parser.ReductionRule.Index == (int)Context.RuleConstants.RULE_PARAGRAPH
									|| parser.ReductionRule.Index == (int)Context.RuleConstants.RULE_PARAGRAPH2
									|| parser.ReductionRule.Index == (int)Context.RuleConstants.RULE_PARAGRAPH3
									|| parser.ReductionRule.Index == (int)Context.RuleConstants.RULE_PARAGRAPH4
									|| parser.ReductionRule.Index == (int)Context.RuleConstants.RULE_PARAGRAPH_YY__READ_YY__READ_PATH
									|| parser.ReductionRule.Index == (int)Context.RuleConstants.RULE_PARAGRAPH_YY__REPLACED_BY_YY__READ_PATH
								 )
							{
								if (parser.InputTokenCount > 0 && parser.TokenSymbol.SymbolType == SymbolType.End)
								{
									// this can only occure when we insert manually a EOF token in syntax error case
									// so we should remove it here
									parser.PopInputToken();
								}
								if (parser.TokenSymbol != null && parser.TokenSymbol.SymbolType != SymbolType.End)
									PushCommentStartToken(parser);
							}

							break;

						case ParseMessage.Accept:
							//=== Success!
							//m_program = (SimpleStatement) parser.TokenSyntaxNode;                
							LoggingService.Debug("-- Program Accepted --");
							return _program;

						case ParseMessage.TokenRead:
							//=== Make sure that we store token string for needed tokens.
							_context.OnToken();
							break;

						case ParseMessage.InternalError:
							LoggingService.Error("INTERNAL ERROR! Something is horribly wrong");
							_program.ErrorsDuringCompile = true;
							return _program;

						case ParseMessage.NotLoadedError:
							//=== Due to the if-statement above, this case statement should never be true
							LoggingService.Error("NOT LOADED ERROR! Compiled Grammar Table not loaded");
							_program.ErrorsDuringCompile = true;
							return _program;

						case ParseMessage.CommentError:
							LoggingService.Error("COMMENT ERROR! Unexpected end of file");
							_program.ErrorsDuringCompile = true;
							return _program;

						case ParseMessage.CommentBlockRead:
							//=== Do nothing
							break;

						case ParseMessage.CommentLineRead:
							//=== Do nothing
							break;
					}
				}
				catch(Exception ex)
				{
					LoggingService.Error(string.Format("PARSER EXCEPTION [{0}({1})]! {2}",
					                                   Path.GetFileName(_filename),
					                                   parser.LineNumber,
					                                   ex.Message));
					_program.ErrorsDuringCompile = true;
					return _program;
				}
			}
		}
		
	}
}
