using System;
using System.Collections.Generic;
using System.Text;
using ICSharpCode.SharpDevelop.Dom;

namespace SoftArchitect.AnubisBinding.Parser
{
	internal class SyntaxNode
	{
		private int _ruleId = -1;
		private string _content;
		private DomRegion _region;
		private List<SyntaxNode> _children = new List<SyntaxNode>();

		#region Constructors

		/// <summary>
		/// Constructor used by terminal token
		/// </summary>
		/// <param name="rule">Token index</param>
		/// <param name="content">Token value</param>
		/// <param name="region">Region encapsuling the token</param>
		public SyntaxNode(int rule, string content, DomRegion region)
		{
			_ruleId = rule;
			_content = content;
			_region = region;
		}

		/// <summary>
		/// Constructor used by non-terminal tokens. Warning, children list have to 
		/// be filled by hand.
		/// </summary>
		/// <param name="rule"></param>
		/// <param name="region"></param>
		public SyntaxNode(int rule, DomRegion region)
		: this(rule, "", region)
		{
		}
		
		/// <summary>
		/// Constructor used by non-terminal tokens
		/// </summary>
		/// <param name="rule">Rule index</param>
		/// <param name="children"></param>
		/// <param name="region"></param>
		public SyntaxNode(int rule, SyntaxNode[] children, DomRegion region)
		: this(rule, "", region)
		{
			_children.AddRange(children);
		}

		public SyntaxNode(int rule, SyntaxNode[] children, DomRegion startRegion, DomRegion endRegion)
			: this(rule, children, new DomRegion(startRegion.BeginLine, startRegion.BeginColumn,
																								 endRegion.EndLine, endRegion.EndColumn))
		{
		}

		#endregion

		public string Content
		{
			get { return _content; }
			set { _content = value; }
		}

		public DomRegion Region
		{
			get { return _region; }
			set { _region = value; }
		}

		public List<SyntaxNode> Children
		{
			get { return _children; }
			set { _children = value; }
		}

		public int RuleId
		{
			get { return _ruleId; }
			set { _ruleId = value; }
		}
		
		public SyntaxNode this[int index]
		{
			get { return _children[index]; }
		}
	}
}
