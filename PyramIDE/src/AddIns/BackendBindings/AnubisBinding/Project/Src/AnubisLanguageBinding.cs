// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Daniel Grunwald" email="daniel@danielgrunwald.de"/>
//     <version>$Revision: 915 $</version>
// </file>

using System;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;
using System.Xml;
using System.CodeDom.Compiler;
using System.Threading;
using ICSharpCode.SharpDevelop.Project;
using ICSharpCode.SharpDevelop.Internal.Templates;
using ICSharpCode.SharpDevelop.Gui;
using ICSharpCode.Core;

namespace SoftArchitect.AnubisBinding
{
	public class AnubisLanguageBinding : ILanguageBinding
	{
		public const string LanguageName = "Anubis";
		
		public string Language {
			get {
				return LanguageName;
			}
		}
		
		#region routines for single file compilation
		public bool CanCompile(string fileName)
		{
			string ext = Path.GetExtension(fileName);
			if (ext == null)
				return false;
			return string.Equals(ext, ".ANUBIS", StringComparison.InvariantCultureIgnoreCase);
		}
		
		public string GetCompiledOutputName(string fileName)
		{
			return Path.ChangeExtension(fileName, ".adm");
		}
		
		public CompilerResults CompileFile(string fileName)
		{
			throw new NotImplementedException();
		}
		
		public void Execute(string fileName, bool debug)
		{
			throw new NotImplementedException(); // only needed for single-file compilation
		}
		#endregion

		public IProject LoadProject(IMSBuildEngineProvider engineProvider, string fileName, string projectName)
		{
			return new AnubisProject(engineProvider, fileName, projectName);
		}
		
		public IProject CreateProject(ProjectCreateInformation info)
		{
			return new AnubisProject(info);
		}
	}
}
