// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Cedric RICARD" email="ricard@softarchi.com"/>
//     <version>$Revision: 915 $</version>
// </file>

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using ICSharpCode.SharpDevelop.Debugging;
using ICSharpCode.SharpDevelop.Dom;
using ICSharpCode.SharpDevelop.Internal.Templates;
using ICSharpCode.SharpDevelop.Project;
using ICSharpCode.Core;

namespace SoftArchitect.AnubisBinding
{
	public class AnubisProject : CompilableProject
	{
		static bool initialized = false;
		public static readonly string AnubisBinPath = Path.GetDirectoryName(typeof(AnubisProject).Assembly.Location);
		
		void Init()
		{
			if (!initialized) {
				initialized = true;
				MSBuildEngine.CompileTaskNames.Add("anubisc");
				MSBuildEngine.MSBuildProperties.Add("AnubisBinPath", AnubisBinPath);
			}
		}

		public AnubisProject(IMSBuildEngineProvider engineProvider, string fileName, string projectName)
			: base(engineProvider)
		{
			Name = projectName;
			Init();
			LoadProject(fileName);
		}

		public AnubisProject(ProjectCreateInformation info)
			: base(info.Solution)
		{
			Init();
			Create(info);
			AddImport("$(AnubisBinPath)\\Anubis.Build.targets", null);
		}

		//public override bool CanCompile(string fileName)
		//{
		//  return new AnubisLanguageBinding().CanCompile(fileName);
		//}
		
// 		internal static IProjectContent AnubisCompilerPC;
// 		internal static IProjectContent AnubisUsefulPC;

		//public override void Build(MSBuildEngineCallback callback, IDictionary<string, string> additionalProperties)
		//{
		//  AnubisBuild("Build", callback, additionalProperties);
		//}

		//public override void Rebuild(MSBuildEngineCallback callback, IDictionary<string, string> additionalProperties)
		//{
		//  AnubisBuild("Rebuild", callback, additionalProperties);
		//}

		//private void AnubisBuild(string target, MSBuildEngineCallback callback, IDictionary<string, string> additionalProperties)
		//{
		//  string startupFile = GetProperty("StartupObject");
		//  if (startupFile == "")
		//  {
		//    MessageBox.Show(ResourceService.GetString("AnubisProject_StartupObjectNotDefined"),
		//                    ResourceService.GetString("AnubisProject_StartupObjectNotDefined_Title"),
		//                    MessageBoxButtons.OK,
		//                    MessageBoxIcon.Error);
		//  }
		//  else
		//    RunMSBuild(target, callback, additionalProperties);
		//}

		#region Start / Run

		void AnubisStart(string adm, bool withDebugging)
		{
			ProcessStartInfo psi = new ProcessStartInfo();
			psi.FileName = "anbexec.exe"; //Path.Combine(Directory, program);
			string workingDir = StringParser.Parse(StartWorkingDirectory);
			if (workingDir.Length == 0)
			{
				psi.WorkingDirectory = Path.Combine(Directory, GetEvaluatedProperty("OutputPath") ?? "");
			}
			else
			{
				psi.WorkingDirectory = Path.Combine(Directory, workingDir);
			}
			psi.Arguments = "\"" + adm + "\" " + StringParser.Parse(StartArguments);

			//if (!File.Exists(psi.FileName))
			//{
			//  MessageService.ShowError(psi.FileName + " does not exist and cannot be started.");
			//  return;
			//}
			if (!System.IO.Directory.Exists(psi.WorkingDirectory))
			{
				MessageService.ShowError("Working directory " + psi.WorkingDirectory + " does not exist; the process cannot be started. You can specify the working directory in the project options.");
				return;
			}

			if (withDebugging)
			{
				DebuggerService.CurrentDebugger.Start(psi);
			}
			else
			{
				DebuggerService.CurrentDebugger.StartWithoutDebugging(psi);
			}
		}

		public override void Start(bool withDebugging)
		{
			switch (StartAction)
			{
				case StartAction.Project:
					AnubisStart(OutputAssemblyFullPath, withDebugging);
					break;
				default:
					base.Start(withDebugging);
					break;
			}
		}

		#endregion

		[Browsable(false)]
		public override IAmbience Ambience {
			get {
				return AnubisAmbience.Instance;
			}
		}

		[Browsable(false)]
		public override string OutputAssemblyFullPath
		{
			get
			{
				string outputPath = GetEvaluatedProperty("OutputPath") ?? "";
				return Path.Combine(Path.Combine(Directory, outputPath), AssemblyName + ".adm");
			}
		}

		public override ItemType GetDefaultItemType(string fileName)
		{
			if (string.Equals(Path.GetExtension(fileName), ".anubis", StringComparison.OrdinalIgnoreCase))
				return ItemType.Compile;
			else
				return base.GetDefaultItemType(fileName);
		}

		public override string Language
		{
			get { return "Anubis"; }
		}

		public override LanguageProperties LanguageProperties
		{
			get { return AnubisLanguageProperties.Instance; }
		}
	}
}
