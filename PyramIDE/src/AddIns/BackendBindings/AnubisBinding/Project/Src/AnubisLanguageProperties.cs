// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Daniel Grunwald" email="daniel@danielgrunwald.de"/>
//     <version>$Revision: 915 $</version>
// </file>

using System;
using ICSharpCode.SharpDevelop.Dom;
using ICSharpCode.SharpDevelop.Dom.Refactoring;

namespace SoftArchitect.AnubisBinding
{
	public class AnubisLanguageProperties : LanguageProperties
	{
		public readonly static AnubisLanguageProperties Instance = new AnubisLanguageProperties();

		public AnubisLanguageProperties() : base(StringComparer.InvariantCulture) { }

		public override string ToString()
		{
			return "[LanguageProperties: Anubis]";
		}
	
		public override CodeGenerator CodeGenerator
		{
			get
			{
				return AnubisCodeGenerator.Instance;
			}
		}

	}
}
