// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Matthew Ward" email="mrward@users.sourceforge.net"/>
//     <version>$Revision: 915 $</version>
// </file>

using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;

namespace Anubis.Build.Tasks
{
	public class AnubisCompilerResultsParser
	{
		private string lastFilename = "";

		public const string NormalErrorPattern = @"(?<file>.*) \(line (?<line>\d+), column (?<column>\d+)\)\s+(?<error>\w+)\s+(?<number>[EW][\d\w]+):\s+(?<message>.*)";
		public const string RemarkErrorPattern = @"\s+(?<remark>Remark):\s+(?<message>.*)";
		public const string GeneralErrorPattern = @"(?<error>.+?)\s+(?<number>[\d\w]+?):\s+(?<message>.*)";

		Regex normalError = new Regex(NormalErrorPattern, RegexOptions.Compiled);
		Regex remark = new Regex(RemarkErrorPattern, RegexOptions.Compiled);
		Regex generalError = new Regex(GeneralErrorPattern, RegexOptions.Compiled);

		public CompilerError ParseLine(string line)
		{
			// try to match standard mono errors
			Match match = normalError.Match(line);
			if (match.Success)
			{
				CompilerError error = new CompilerError();
				error.Column = Int32.Parse(match.Result("${column}"));
				error.Line = Int32.Parse(match.Result("${line}"));
				error.FileName = Path.GetFullPath(match.Result("${file}"));
				lastFilename = error.FileName;
				error.IsWarning = match.Result("${error}") != "Error";
				error.ErrorNumber = match.Result("${number}");
				error.ErrorText = match.Result("${message}");
				return error;
			}
			else
			{
				match = remark.Match(line);
				if (match.Success)
				{
					CompilerError error = new CompilerError();
					error.FileName = lastFilename;
					error.IsWarning = true;
					error.ErrorNumber = "";
					error.ErrorText = match.Result("${message}");
					return error;
				}
			}
			return null;
		}
	}
}
