﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AnubisBuildTask")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
//[assembly: AssemblyCompany("SoftArchi")]
//[assembly: AssemblyProduct("AnubisBuildTask")]
//[assembly: AssemblyCopyright("Copyright © SoftArchi 2006")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("64b94110-a2f2-48e5-a5b4-7cdc5487c016")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
//[assembly: AssemblyVersion("1.0.0.0")]
//[assembly: AssemblyFileVersion("1.0.0.0")]
