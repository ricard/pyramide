// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Matthew Ward" email="mrward@users.sourceforge.net"/>
//     <version>$Revision: 915 $</version>
// </file>

using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.CodeDom.Compiler;
using Microsoft.VisualBasic.FileIO;

namespace Anubis.Build.Tasks
{
	/// <summary>
	/// MSBuild task for Anubis Compiler.
	/// </summary>
	public class Anubisc : ToolTask
	{
		#region Compiler flags
		string _assemblyName;
		bool _alert;
		bool _stats;
		bool _english;
		bool _ignoreReads;
		string _anubisPath;
		string _myAnubisPath;
		private string _includePaths;
		bool _htmlIndex;
		string _maxTempPeer;
		bool _noPreemption;
		bool _used;
		bool _unused;
		bool _reads;
		string _tabs;
		bool _typeVerificationOnly;
		bool _verbose;
		ITaskItem[] _sources;
		private string _outputPath;
		string _tmpModulePath;
		private string _additionnalCommandLine;

		public string AssemblyName
		{
			get { return _assemblyName; }
			set { _assemblyName = value; }
		}
		
		public bool Alert
		{
			get { return _alert; }
			set { _alert = value; }
		}

		public bool Stats
		{
			get { return _stats; }
			set { _stats = value; }
		}

		public bool English
		{
			get { return _english; }
			set { _english = value; }
		}

		public bool IgnoreReads
		{
			get { return _ignoreReads; }
			set { _ignoreReads = value; }
		}

		public string AnubisPath
		{
			get { return _anubisPath; }
			set { _anubisPath = value; }
		}

		public string MyAnubisPath
		{
			get { return _myAnubisPath; }
			set { _myAnubisPath = value; }
		}

		public string IncludePaths
		{
			get { return _includePaths; }
			set { _includePaths = value; }
		}

		public bool HtmlIndex
		{
			get { return _htmlIndex; }
			set { _htmlIndex = value; }
		}

		public string MaxTemporaryPeer
		{
			get { return _maxTempPeer; }
			set { _maxTempPeer = value; }
		}

		public bool NoPreemption
		{
			get { return _noPreemption; }
			set { _noPreemption = value; }
		}

		public bool Used
		{
			get { return _used; }
			set { _used = value; }
		}

		public bool Unused
		{
			get { return _unused; }
			set { _unused = value; }
		}

		public bool Reads
		{
			get { return _reads; }
			set { _reads = value; }
		}

		public string Tabs
		{
			get { return _tabs; }
			set { _tabs = value; }
		}

		public bool TypeVerificationOnly
		{
			get { return _typeVerificationOnly; }
			set { _typeVerificationOnly = value; }
		}

		public bool Verbose
		{
			get { return _verbose; }
			set { _verbose = value; }
		}

		public ITaskItem[] Sources
		{
			get
			{
				return _sources;
			}
			set
			{
				_sources = value;
			}
		}
				
		public string OutputPath
		{
			get { return _outputPath; }
			set { _outputPath = value; }
		}

		public string CommandLine
		{
			get { return _additionnalCommandLine; }
			set { _additionnalCommandLine = value; }
		}

		#endregion

		AnubisCompilerResultsParser parser = new AnubisCompilerResultsParser();

		protected override string ToolName
		{
			get
			{
				return "anubis.exe";
			}
		}

		protected override string GenerateFullPathToTool()
		{
			// TODO look for the anubis compiler
			string path, filename = "anubis.exe"; 
			if (AnubisPath != null && AnubisPath.Length > 0)
			{
				path = Path.Combine(AnubisPath, filename);
				if(File.Exists(path))
					return path;
			}
			path = filename; // HACK because anubis.exe is on Path
			if (path == null)
			{
				Log.LogErrorWithCodeFromResources("General.AnubisCompilerNotFound");
			}
			return path; 
		}

		public override bool Execute()
		{
			_tmpModulePath = "";
			int errorCount = 0;

			// Run the anubis compiler
			bool result = base.Execute();
			
			// Now, we output all errors
			foreach(AnubisError anubisError in _errors)
			{
				CompilerError error = anubisError.Error;
				if (error != null)
				{
					string errorText = error.ErrorText;
					foreach (string line in anubisError.Details)
					{
						errorText += " \n" + line;
					}
					if (error.IsWarning)
					{
						Log.LogWarning("Warning", error.ErrorNumber, null, error.FileName, error.Line, error.Column, error.Line, error.Column, errorText);
					}
					else
					{
						Log.LogError("Error", error.ErrorNumber, null, error.FileName, error.Line, error.Column, error.Line, error.Column, errorText);
						errorCount++;
					}
				}
				//else
				//  foreach (string line in anubisError.Details)
				//  {
				//    Log.LogMessage(MessageImportance.High, line);
				//  }
			}
			
			// Then we copy and rename the output
			if (errorCount == 0 && result && _tmpModulePath != "")
			{
				FileSystem.CopyFile(_tmpModulePath, Path.Combine(OutputPath, AssemblyName + ".adm"), true);
			}
			return result;
		}

		protected override bool HandleTaskExecutionErrors()
		{
			return false;
		}

		protected override string GenerateCommandLineCommands()
		{
			CompilerCommandLineArguments args = new CompilerCommandLineArguments();
			args.AppendSwitchIfTrue("-alert", Alert);	// send a message for each 'alert' encountered.
			args.AppendSwitchIfTrue("-board", Stats); // the compiler produces statistics.
			args.AppendSwitchIfTrue("-en", English);
			args.AppendSwitchIfTrue("-c", IgnoreReads);	// the compiler reads but does not compile files called by 'read'. 
																									// (faster but produces no module)
			args.AppendFileNameIfNotNull("-I", IncludePaths);	// (where 'paths' is a list of directories separated by ';')
																												// additional directories within which 'read' will search for.
			args.AppendFileNameIfNotNull("-cdir:", AnubisPath);	// (where * is a directory path) sets the 'anubis' directory.
			args.AppendFileNameIfNotNull("-pdir:", MyAnubisPath);		// (where * is a directory path) sets the 'my_anubis' directory.
			args.AppendSwitchIfTrue("-index", HtmlIndex);	//the compiler produces an index in HTML (add mask(s) for files)
			args.AppendSwitchIfNotNull("-mpl", MaxTemporaryPeer);	//(where * is an integer) do not use more than * temporary pairs.
			args.AppendSwitchIfTrue("-nopre", NoPreemption);
			args.AppendSwitchIfTrue("-used", Used);			// makes a file '.used' showing operations which are used.
			args.AppendSwitchIfTrue("-unused", Unused);	// makes a file '.unused' showing operations which are not used.
			args.AppendSwitchIfTrue("-reads", Reads);		// ???
			args.AppendSwitchIfNotNull("-tab", Tabs);	//(where * is an integer < 21) sets the width of tabulators.
			args.AppendSwitchIfTrue("-to", TypeVerificationOnly);
			args.AppendSwitchIfTrue("-v", Verbose);		// (verbose) the compiler explains its actions.
			//args.AppendSwitchIfNotNull("-output", AssemblyName);	//(where * is an integer) do not use more than * temporary pairs.
			args.AppendTextUnquoted(" " + CommandLine);
			args.AppendFileNamesIfNotNull(Sources, " ");
			return args.ToString();
		}

		public const string BuildFinishedPattern = @"Constructing module '(?<module>.*)' \.\.\. Done\.";
		Regex buildFinished = new Regex(BuildFinishedPattern, RegexOptions.Compiled);

		
		private class AnubisError
		{
			private CompilerError _error;
			private List<string> _details = new List<string>();
			
			public AnubisError(CompilerError error)
			{
				_error = error;
			}

			public AnubisError(string line)
			{
				_error = null;
				AddLine(line);
			}

			public CompilerError Error
			{
				get { return _error; }
			}

			public List<string> Details
			{
				get { return _details; }
			}

			public void AddLine(string line)
			{
				Details.Add(line);
			}
		}
		
		private List<AnubisError> _errors = new List<AnubisError>();
		private AnubisError _currentError = null;
		
		/// <summary>
		/// Each line of output, from either standard output or standard error
		/// is parsed and any compiler errors are logged.  If the line cannot
		/// be parsed it is logged using the specified message importance.
		/// </summary>
		protected override void LogEventsFromTextOutput(string singleLine, MessageImportance messageImportance)
		{
			CompilerError error = parser.ParseLine(singleLine);
			if (error != null)
			{
				_currentError = new AnubisError(error);
				_errors.Add(_currentError);
			}
			else
			{
				// try to match module creation line
				Match match = buildFinished.Match(singleLine);
				if (match.Success)
				{
					_currentError = null;
					_errors.Add(new AnubisError(singleLine));
					//Log.LogMessage(MessageImportance.High, singleLine);
					_tmpModulePath = match.Result("${module}");
					FileSystem.CopyFile(_tmpModulePath, Path.Combine(OutputPath, Path.GetFileName(_tmpModulePath)), true);
				}
				else if (_currentError != null)/* && (singleLine.StartsWith(" ") 
																					|| singleLine.StartsWith("\t") 
																					|| singleLine.Length == 0) )*/
				{
					_currentError.AddLine(singleLine);
				}
				else
				{
					_currentError = null;
					_errors.Add(new AnubisError(singleLine));
					//Log.LogMessage(MessageImportance.High, singleLine);
				}
			}
		}
	}
}
