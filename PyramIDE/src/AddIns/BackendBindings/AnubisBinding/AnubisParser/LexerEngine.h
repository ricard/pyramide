#ifndef __LEXER_ENGINE_H__
#define __LEXER_ENGINE_H__

using namespace System;
using namespace System::IO;
using namespace System::Text;

#include "FlexLexer.h"
#include "Anubis.tab.hpp"

#include <vcclr.h>

typedef struct
{
	gcroot<Object^> object;
} symbol_t;


class LexerEngine :
	public yyFlexLexer
{
public:
	LexerEngine(TextReader^ reader);
	virtual ~LexerEngine(void);

	int yylex(symbol_t* yylval, YYLTYPE* yylloc);

	//int ReadInput([InAttribute] [OutAttribute] array<wchar_t>^ buffer, int count);
	int ReadInput(char * buf, int max_size);

	static const char * GetUnmanagedString(String ^ s);
	static void FreeUnmanagedString(const char * lstr);

	void begin_INITIAL(void);


private:
	// This function is  used for storing one  character into the buffer.
	void begin_str();
	void store_str_char(char);
	void end_str();
	void update_lc(void);
	void store_external_comment(char c);
	String^ rec_name(char *s, int del);
	void lexical_kwread_error();
	void lexical_error(); 

private:
	gcroot<TextReader^> _reader;
	int current_par_line; // line number of first line of paragraph currently read 
	YYLTYPE _loc;
	//int lineno;						// current line number 
	//int colno;						// current column number 
	int comlevel;					// counting the level of nested comments
	int tab_seen;					// becomes 1 when at least one tabulator has been seen
	int tab_width;				// default (guessed) width of tabulators

	//Managing character  strings. We  use the  buffer 'str_buf' for  reading the  content of
  // character strings.
	gcroot<StringBuilder ^> str_buf; 
	YYLTYPE _strLoc;

	/*** Managing external (off paragraph) comments (used when making an index). ***/ 
	gcroot<StringBuilder ^> ext_com; 
	YYLTYPE _comLoc;
};

#endif	//ndef __LEXER_ENGINE_H__
