/* lexer.y *******************************************************************************

                                             Anubis 1. 
                                    The lexer of the compiler. 

*****************************************************************************************/

/*
    This LEX/FLEX file is the lexer for the Anubis compiler. 
*/ 


%{
#include "Anubis.tab.hpp"
#include "LexerEngine.h"

#define YY_INPUT(buf, result, max_size) \
			result = ((LexerEngine*)this)->ReadInput(buf, max_size)

#define YY_NO_UNPUT
#define YY_USER_ACTION           update_lc(); 

#define YYSTYPE symbol_t   
   
#define YY_DECL int LexerEngine::yylex(YYSTYPE* yylval, YYLTYPE* yylloc)

#define return_token(t) return_token2(t, gcnew String(yytext))
#define return_token2(t, val) return_token3(t, val, &_loc)
#define return_token3(t, val, loc) \
		{	\
			if(loc != yylloc) \
				*yylloc = *loc; \
			yylval->object = val; \
			return t; \
		}


%}



%option c++
%option yyclass="LexerEngine"
%option yylineno
%option noyywrap



/* states ------------------------------------------------------------------------------*/
%x COM
%x PAR
%x INCL
%x STR
%x STRTL
   
/* state STRTL is used when a string is too long. */ 
/* states CONF and CONFCOM are used to read 'compiler.conf'.*/ 

/* definition of white characters */
W   [\ \t\r\n]   
   
   
/* the lexer ----------------------------------------------------------------------------*/
%%
<PAR>"/*"                         { comlevel = 1; BEGIN COM; }
<COM>"/*"                         { comlevel++; }
<COM>"*/"                         { comlevel--; if (!comlevel) { BEGIN PAR; } }
<COM>\n                           { }
<COM>.                            { }
<PAR>\/\/.*$                      { /* dbl_slash_comment(yytext+2); */ }
<PAR>\/\/.*												{ }
<PAR>\"                           { BEGIN STR; begin_str(); }
<STR>\"                           { BEGIN PAR; end_str();
                                    return_token3(yy__anb_string, str_buf->ToString(), &_strLoc); }
<STR>\\\"                         { store_str_char('\"'); }
<STR>\\n                          { store_str_char('\n'); }
<STR>\\r                          { store_str_char('\r'); }
<STR>\\t                          { store_str_char('\t'); }
<STR>\\\\                         { store_str_char('\\'); }
<STR>\n                           { store_str_char('\n'); }
<STR>.                            { store_str_char(yytext[0]); }
<STRTL>\"                         { BEGIN PAR; }
<STRTL>\n                         { }
<STRTL>.                          { }
<PAR>__LINE__                     { return_token( yy__macro_integer ); }
<PAR>__FILE__                     { return_token( yy__anb_string ); }
<PAR>__TIME__                     { return_token( yy__macro_integer ); }      
<PAR>\'[^\'\n\t\r\"]\'            { return_token2(yy__char, gcnew Char(yytext[1])); }
<PAR>\'\\n\'                      { return_token2(yy__char, gcnew Char('\n')); }
<PAR>\'\\r\'                      { return_token2(yy__char, gcnew Char('\r')); }
<PAR>\'\\t\'                      { return_token2(yy__char, gcnew Char('\t')); }
<PAR>\'\\\"\'                     { return_token2(yy__char, gcnew Char('\"')); }
<PAR>\'\\\\\'                     { return_token2(yy__char, gcnew Char('\\')); }
<PAR>\'\\\'\'                     { return_token2(yy__char, gcnew Char('\'')); }
<PAR>"("                               { return_token(yy__lpar); }
<PAR>")"                               { return_token(yy__rpar); }
<PAR>\,{W}*\)                          { return_token(yy__rpar); }
<PAR>"{"                               { return_token(yy__lbrace); }
<PAR>"}"                               { return_token(yy__rbrace); }
<PAR>\,{W}*\}                          { return_token(yy__rbrace); }
<PAR>"["                               { return_token(yy__lbracket); }
<PAR>"]"                               { return_token(yy__rbracket); }
<PAR>\,{W}*\]                          { return_token(yy__rbracket); }
<PAR>":"                               { return_token(yy__colon); }
<PAR>";"                               { return_token(yy__semicolon); }
<PAR>"-"                               { return_token(yy__minus); }
<PAR>"|-|"                             { return_token(yy__abs_minus); }
<PAR>"."                               { return_token(yy__dot); }
<PAR>\.$                               { return_token(yy__enddot); }
<PAR>\.{W}                             { return_token(yy__enddot); }
<PAR>".."                              { return_token(yy__dotdot); }
<PAR>".>"                              { return_token(yy__dotsup); }
<PAR>\%                                { return_token(yy__percent); }
<PAR>","                               { return_token(yy__comma); }
<PAR>"~"                               { return_token(yy__tilde); }
<PAR>"="                               { return_token(yy__equals); }
<PAR>"/="                              { return_token(yy__non_equal); }
<PAR>"!="                              { return_token(yy__exclam_equal); }
<PAR>"+"                               { return_token(yy__plus); }
<PAR>"*"                               { return_token(yy__star); }
<PAR>"^"                               { return_token(yy__carret); }
<PAR>"\&"                              { return_token(yy__ampersand); }
<PAR>"|"                               { return_token(yy__vbar); }
<PAR>">>"                              { return_token(yy__right_shift); }
<PAR>"<<"                              { return_token(yy__left_shift); }
<PAR>"=>"                              { return_token(yy__implies); }
<PAR>"?"                               { return_token(yy__forall); }
<PAR>"!"                               { return_token(yy__exists); }
<PAR>"!!"                              { return_token(yy__exists_unique); }
<PAR>"#!"                              { return_token(yy__description); }
<PAR>"/"                               { return_token(yy__slash); }
<PAR>"\\"                              { return_token(yy__backslash); }
<PAR>\({W}*mod{W}                      { return_token(yy__mod); }
<PAR>"<"                               { return_token(yy__less); }
<PAR>">"                               { return_token(yy__greater); }
<PAR>"=<"                              { return_token(yy__lessoreq); }
<PAR>">="                              { return_token(yy__greateroreq); }
<PAR>"-<"                              { return_token(yy__sless); }
<PAR>">-"                              { return_token(yy__sgreater); }
<PAR>"-=<"                             { return_token(yy__slessoreq); }
<PAR>">=-"                             { return_token(yy__sgreateroreq); }
<PAR>"+<"                              { return_token(yy__uless); }
<PAR>">+"                              { return_token(yy__ugreater); }
<PAR>"+=<"                             { return_token(yy__ulessoreq); }
<PAR>">=+"                             { return_token(yy__ugreateroreq); }
<PAR>"<-"                              { return_token(yy__write); }
<PAR>"<->"                             { return_token(yy__exchange); }
<PAR>"->"                              { return_token(yy__arrow); }
<PAR>"->>"                             { return_token(yy__arroww); }
<PAR>"|->"                             { return_token(yy__mapsto); }
<PAR>"|->>"                            { return_token(yy__mapstoo); }
<PAR>"..."                             { return_token(yy__dots); }
<PAR>\){W}*\-\>                        { return_token(yy__rpar_arrow); }
<PAR>String                            { return_token(yy__type_String); }
<PAR>ByteArray                         { return_token(yy__type_ByteArray); }
<PAR>Int                               { return_token(yy__type_Int); }
<PAR>Omega                             { return_token(yy__type_Omega); }
<PAR>bit_width                         { return_token(yy__bit_width); }
<PAR>is_indirect_type                  { return_token(yy__indirect); }
<PAR>\�vcopy                           { return_token(yy__vcopy); }
<PAR>\�load_module                     { return_token(yy__load_module); }
<PAR>\�serialize                       { return_token(yy__serialize); }
<PAR>\�unserialize                     { return_token(yy__unserialize); }
<PAR>Float                             { return_token(yy__type_Float); }
<PAR>\�Listener                        { return_token(yy__type_Listener); }
<PAR>\�StructPtr                       { return_token(yy__StructPtr); }
<PAR>\�avm                             { return_token(yy__avm); }
<PAR>if                                { return_token(yy__if); }
<PAR>^[Pp]roof                         { return_token(yy__proof); }
<PAR>since                             { return_token(yy__since); }
<PAR>is                                { return_token(yy__is); }
<PAR>then                              { return_token(yy__then); }
<PAR>alert                             { return_token(yy__alert); }
<PAR>protect                           { return_token(yy__protect); }
<PAR>lock                              { return_token(yy__lock); }
<PAR>succeeds                          { return_token(yy__succeeds); }
<PAR>succeeds{W}+as                    { return_token(yy__succeeds_as); }
<PAR>wait{W}+for                       { return_token(yy__wait_for); }
<PAR>checking{W}+every                 { return_token(yy__checking_every); }
<PAR>delegate                          { return_token(yy__delegate); }
<PAR>else                              { return_token(yy__else); }
<PAR>with                              { return_token(yy__with); }
<PAR>alternative_number                { return_token(yy__alt_number); }
<PAR>RStream                           { return_token(yy__RStream); }
<PAR>WStream                           { return_token(yy__WStream); }
<PAR>RWStream                          { return_token(yy__RWStream); }
<PAR>GAddr                             { return_token(yy__GAddr); }
<PAR>Var                               { return_token(yy__Var); }
<PAR>MVar                              { return_token(yy__MVar); }
<PAR>connect{W}+to{W}+file             { return_token(yy__connect_to_file); }
<PAR>connect{W}+to{W}+network          { return_token(yy__connect_to_IP); }
<PAR>debug                             { return_token(yy__debug_avm); }
<PAR>terminal                          { return_token(yy__terminal); }
<PAR>[Ww]e{W}+have                     { return_token(yy__we_have); }
<PAR>[Ii]t{W}+is{W}+enough{W}+to{W}+prove({W}+that)? { return_token(yy__enough); } 
<PAR>[Ll]et                            { return_token(yy__let); } 
<PAR>[Aa]ssume({W}+that)?              { return_token(yy__assume); }
<PAR>[Hh]ence                          { return_token(yy__hence); } 
<PAR>[Ii]ndeed                         { return_token(yy__indeed); } 
^[Ee]xecute[\ \t]+(.+)$        { /*return_token(yy__execute); */}
^to[\ ]*do\:.*\n               { printf(yytext); fflush(stdout); }
^[Tt]ype                       { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__type); }
^[Pp]ublic{W}+type             { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__p_type); }
^[Aa]lias                      { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__type_alias); }
^[Pp]ublic{W}+alias            { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__p_type_alias); }
^[Vv]ariable                   { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__variable); }
^[Pp]ublic{W}+variable         { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__p_variable); }
^[Tt]heorem                    { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__theorem); }
^[Pp]ublic{W}+theorem          { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__p_theorem); }
^[Oo]peration                  { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__operation); }
^[Gg]lobal{W}+operation        { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__g_operation); }
^[Dd]efine                     { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__operation); }
^[Dd]efine{W}+inline           { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__operation_inline); }
^[Gg]lobal{W}+define           { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__g_operation); }
^[Pp]ublic{W}+define           { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__p_operation); }
^[Pp]ublic{W}+define{W}+inline { BEGIN PAR; current_par_line = _loc.first_line; 
                                 return_token(yy__p_operation_inline); }
^[Rr]ead{W}+                   { BEGIN INCL; return_token(yy__read); }
^[Rr]eplaced[\ \t]+by[\ \t]+   { BEGIN INCL; return_token(yy__replaced_by); }
^C[\ \t]+constructors[\ \t]+for { BEGIN PAR; current_par_line = _loc.first_line; 
                                  return_token(yy__C_constr_for); }
<INCL>[^\ \n\t\r]+              { BEGIN INITIAL; return_token(yy__anb_string); }
<INCL>\n                { lexical_kwread_error(); }
<INCL>.                 { lexical_kwread_error(); }
<<EOF>>                 { yyterminate(); }
<PAR>[0-9]+                            { return_token(yy__integer); }
<PAR>0[Xx][0-9A-Fa-f]+                 { return_token(yy__integer); } 
<PAR>[0-9]+\.[0-9]+                    { return_token(yy__float); }
<PAR>\$[A-Z][A-Za-z0-9\_]*             { return_token2(yy__utvar, gcnew String(yytext+1)); }
<PAR>[A-Z][A-Za-z0-9\_]*               { return_token(yy__Symbol); }
<PAR>[a-z\_][A-Za-z0-9\_]*             { return_token(yy__symbol); }
<PAR>\|\-[a-z\_][A-Za-z0-9\_]*\-\>     { return_token2(yy__rec_mapsto, rec_name(yytext, 4)); }
<PAR>\|\-[a-z\_][A-Za-z0-9\_]*\-\>\>   { return_token2(yy__rec_mapstoo, rec_name(yytext, 5)); }
<PAR>\�[A-Z][A-Za-z0-9\_]*             { return_token(yy__Symbol); }
<PAR>\�[a-z\_][A-Za-z0-9\_]*           { return_token(yy__symbol); }
<PAR>\�                                { return_token(yy__symbol); }
<PAR>[ \t\r]                           { }
<PAR>\n                                { }
<PAR>.                                 { lexical_error(); }
\n                                     { store_external_comment('\n'); }
.                                      { store_external_comment(yytext[0]); }
%%


/* going to state INITIAL */    
void LexerEngine::begin_INITIAL(void)
{
  BEGIN INITIAL; 
}

   
/* end of lexer -----------------------------------------------------------------------*/
   
