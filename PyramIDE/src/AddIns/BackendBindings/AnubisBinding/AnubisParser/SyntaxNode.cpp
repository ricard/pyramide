#include "StdAfx.h"
#include "SyntaxNode.h"

using namespace SoftArchitect::AnubisParser;


SyntaxNode::SyntaxNode(int rule, String^ content, DomRegion^ region)
{
	Init(rule, content, region);
}

SyntaxNode::SyntaxNode(int rule, DomRegion^ region)
{
	Init(rule, "", region);
}

//SyntaxNode::SyntaxNode(int rule, array<SyntaxNode^>^ children, DomRegion^ region)
//{
//	Init(rule, "", region);
//	_children->AddRange(children);
//}
//
//SyntaxNode::SyntaxNode(int rule, array<SyntaxNode^>^ children, DomRegion^ startRegion, DomRegion^ endRegion)
//{
//	Init(rule, "", gcnew DomRegion(startRegion->BeginLine, startRegion->BeginColumn,
//																 endRegion->EndLine, endRegion->EndColumn));
//	_children->AddRange(children);
//}

void SyntaxNode::Init(int rule, String^ content, DomRegion^ region)
{
	_ruleId = rule;
	_content = content;
	_region = region;
	_children = gcnew List<SyntaxNode^>();
}
