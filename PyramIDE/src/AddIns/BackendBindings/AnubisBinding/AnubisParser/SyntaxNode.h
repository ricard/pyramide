#pragma once

using namespace System;
using namespace System::Collections::Generic;
using namespace ICSharpCode::SharpDevelop::Dom;

namespace SoftArchitect { 
	namespace AnubisParser {

		public ref class SyntaxNode
		{
		private:
			int _ruleId;
			String^ _content;
			DomRegion^ _region;
			List<SyntaxNode^>^ _children;

			void Init(int rule, String^ content, DomRegion^ region);

		public:
			/// <summary>
			/// Constructor used by terminal token
			/// </summary>
			/// <param name="rule">Token index</param>
			/// <param name="content">Token value</param>
			/// <param name="region">Region encapsuling the token</param>
			SyntaxNode(int rule, String^ content, DomRegion^ region);


			/// <summary>
			/// Constructor used by non-terminal tokens. Warning, children list have to 
			/// be filled by hand.
			/// </summary>
			/// <param name="rule"></param>
			/// <param name="region"></param>
			SyntaxNode(int rule, DomRegion^ region);


			///// <summary>
			///// Constructor used by non-terminal tokens
			///// </summary>
			///// <param name="rule">Rule index</param>
			///// <param name="children"></param>
			///// <param name="region"></param>
			//SyntaxNode(int rule, array<SyntaxNode^>^ children, DomRegion^ region);

			//SyntaxNode(int rule, array<SyntaxNode^>^ children, DomRegion^ startRegion, DomRegion^ endRegion);

			property String^ Content
			{
				String^ get()									{ return _content; }
				void set(String^ value)				{ _content = value; }
			}

			property DomRegion ^ Region
			{
				DomRegion ^ get()							{ return _region; }
				void set(DomRegion^ region)		{ _region = region; }
			}

			property List<SyntaxNode^>^ Children
			{
				List<SyntaxNode^>^ get()			{ return _children; }
				void set(List<SyntaxNode^>^ children) { _children = children; }
			}

			property int RuleId
			{
				int get()											{ return _ruleId; }
				void set(int ruleId)					{ _ruleId = ruleId; }
			}
			
			property SyntaxNode^ default[int]
			{
				SyntaxNode^ get(int index)		{ return _children[index]; }
			}
		};

	}
}