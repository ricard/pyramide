// AnubisParser.h
#ifndef __PARSER_ENGINE_H__
#define __PARSER_ENGINE_H__

#pragma once

using namespace System;
using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;
using namespace ICSharpCode::Core;
using namespace ICSharpCode::SharpDevelop::Dom;

#include "LexerEngine.h"


   

namespace SoftArchitect { 
	namespace AnubisParser {

		public ref class ParserEngine
		{
		private:
			TextReader^ _reader;
			String^ _filename;
			IProjectContent^ _projectContent;
			ICompilationUnit^ _cu;
			Stack<DefaultClass^>^ _currentClass;

			LexerEngine*	_lexer;

		public:
			ParserEngine(TextReader^ reader, String^ filename, IProjectContent^ projectContent);

			ICompilationUnit^ DoParse();

			void AddUsing(String ^ file, const YYLTYPE & loc);
			IClass^ AddType(ModifierEnum modifier, String ^ name, const YYLTYPE & loc);
			IField^ AddVar(ModifierEnum modifier, IReturnType^ type, String ^ name, const YYLTYPE & loc);
			IMethod^ AddFunction(
				ModifierEnum modifier, 
				IReturnType^ type, 
				String ^ name, 
				const YYLTYPE & locSep, // location of the separator ('=')
				const YYLTYPE & loc
				);
			IReturnType^ GetReturnType(String^ name, const YYLTYPE & loc);

			static DomRegion CreateDomRegion(const YYLTYPE & loc)
			{
				return *gcnew DomRegion(loc.first_line, loc.first_column, loc.last_line, loc.last_column);
			}

		public:
			property ICompilationUnit^ CU
			{
				ICompilationUnit^ get()			
				{ 
					return _cu;
				}
			}

			property IClass^ OuterClass
			{
				IClass^ get()			
				{ 
					if(_currentClass->Count > 0)
						return _currentClass->Peek();
					else
						return nullptr;
				}
			}

			property TextReader^ Reader
			{
				TextReader^ get()			
				{ 
					return _reader;
				}
			}

			property LexerEngine& Lexer
			{
				LexerEngine& get()			
				{ 
					return *_lexer;
				}
			}

			property String^ Filename
			{
				String^ get()	
				{ 
					return _filename;
				}
			}
		};


	}
}


#endif	//ndef __PARSER_ENGINE_H__
