// This is the main DLL file.

#include "stdafx.h"

#include "ParserEngine.h"

using namespace System;
using namespace System::Diagnostics;
using namespace System::IO;
using namespace SoftArchitect::AnubisParser;
using System::Runtime::InteropServices::Marshal;

int yyparse (ParserEngine^ parser);

ParserEngine::ParserEngine(TextReader^ reader, String^ filename, IProjectContent^ projectContent)
{
	_reader = reader;
	_filename = filename;
	_projectContent = projectContent;
	_lexer = new LexerEngine(reader);
}

ICompilationUnit^ ParserEngine::DoParse()
{
	_cu = gcnew DefaultCompilationUnit(_projectContent);
	_cu->FileName = _filename;
	DefaultClass^ c = gcnew DefaultClass(_cu, ClassType::Module, ModifierEnum::Public | ModifierEnum::Sealed, DomRegion::Empty, nullptr);
	c->FullyQualifiedName = Path::GetFileNameWithoutExtension(_cu->FileName);
	_cu->Classes->Add(c);
	_currentClass = gcnew Stack<DefaultClass^>();
	_currentClass->Push(c);

	int ret = yyparse(this);
	if(ret == 1)
		LoggingService::Error(String::Format("Parsing error for file {0}", _filename));
	else if(ret == 2)
		LoggingService::Error(String::Format("Out of memory while parsing file {0}", _filename));

	return _cu;
}

void ParserEngine::AddUsing(String ^ file, const YYLTYPE & loc)
{
	DefaultUsing^ u = gcnew DefaultUsing(_cu->ProjectContent, *gcnew DomRegion(loc.first_line, 0));
	u->Usings->Add(file);
	_cu->Usings->Add(u);
}

IClass^ ParserEngine::AddType(ModifierEnum modifier, String ^ name, const YYLTYPE & loc)
{
	DefaultClass^ c = gcnew DefaultClass(_cu,
																			 ClassType::Struct,
																			 modifier | ModifierEnum::Sealed,
																			 *gcnew DomRegion(loc.first_line, loc.first_column, loc.last_line, loc.last_column),
																			 OuterClass);
	c->FullyQualifiedName = name;
	OuterClass->InnerClasses->Add(c);
	return c;
}

IField^ ParserEngine::AddVar(ModifierEnum modifier, IReturnType^ type, String ^ name, const YYLTYPE & loc)
{
	DefaultField^ field = gcnew DefaultField(type,
														               name, 
															             modifier,
																					 *gcnew DomRegion(loc.first_line, loc.first_column, loc.last_line, loc.last_column),
																	         OuterClass);
	OuterClass->Fields->Add(field);
	return field;
}

IMethod^ ParserEngine::AddFunction(ModifierEnum modifier, 
																	 IReturnType^ type, 
																	 String ^ name, 
																	 const YYLTYPE & locSep, 
																	 const YYLTYPE & loc)
{
	DefaultMethod^ method = gcnew DefaultMethod(name, nullptr, modifier,
																						  *gcnew DomRegion(loc.first_line, loc.first_column, locSep.last_line, locSep.last_column),
																						  *gcnew DomRegion(locSep.last_line, locSep.last_column, loc.last_line, loc.last_column),
																              OuterClass);
	//ConvertTemplates(node, method);
	// return type must be assign AFTER ConvertTemplates
	method->ReturnType = type;
	if (method->ReturnType == nullptr)
		method->ReturnType = gcnew DefaultReturnType(gcnew DefaultClass(_cu, "One"));
	OuterClass->Methods->Add(method);
	return method;
}

IReturnType^ ParserEngine::GetReturnType(String^ name, const YYLTYPE & loc)
{
	return gcnew SearchClassReturnType(_cu->ProjectContent, 
																		 OuterClass, 
																		 loc.first_line, 
																		 loc.first_column,
																		 name, 
																		 0);
}


