/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     yy__if = 258,
     yy__is = 259,
     yy__then = 260,
     yy__type = 261,
     yy__operation = 262,
     yy__variable = 263,
     yy__utvar = 264,
     yy__float = 265,
     yy__alert = 266,
     yy__theorem = 267,
     yy__p_theorem = 268,
     yy__proof = 269,
     yy__read = 270,
     yy__replaced_by = 271,
     yy__exchange = 272,
     yy__symbol = 273,
     yy__Symbol = 274,
     yy__lpar = 275,
     yy__lbracket = 276,
     yy__dot = 277,
     yy__rbracket = 278,
     yy__comma = 279,
     yy__bit_width = 280,
     yy__semicolon = 281,
     yy__plus = 282,
     yy__minus = 283,
     yy__star = 284,
     yy__carret = 285,
     yy__ampersand = 286,
     yy__vbar = 287,
     yy__equals = 288,
     yy__write = 289,
     yy__percent = 290,
     yy__abs_minus = 291,
     yy__rbrace = 292,
     yy__lbrace = 293,
     yy__rpar = 294,
     yy__colon = 295,
     yy__implies = 296,
     yy__forall = 297,
     yy__exists = 298,
     yy__exists_unique = 299,
     yy__decimal_digit = 300,
     yy__anb_string = 301,
     yy__type_String = 302,
     yy__type_ByteArray = 303,
     yy__description = 304,
     yy__arrow = 305,
     yy__ndarrow = 306,
     yy__with = 307,
     yy__type_Float = 308,
     yy__type_Listener = 309,
     yy__indirect = 310,
     yy__serialize = 311,
     yy__unserialize = 312,
     yy__vcopy = 313,
     yy__non_equal = 314,
     yy__load_module = 315,
     yy__type_Int = 316,
     yy__wait_for = 317,
     yy__delegate = 318,
     yy__tilde = 319,
     yy__type_Omega = 320,
     yy__type_alias = 321,
     yy__p_type_alias = 322,
     yy__checking_every = 323,
     yy__dotdot = 324,
     yy__dotsup = 325,
     yy__exclam_equal = 326,
     yy__rpar_arrow = 327,
     yy__rpar_ndarrow = 328,
     yy__dots = 329,
     yy__g_operation = 330,
     yy__less = 331,
     yy__greater = 332,
     yy__lessoreq = 333,
     yy__greateroreq = 334,
     yy__sless = 335,
     yy__sgreater = 336,
     yy__slessoreq = 337,
     yy__sgreateroreq = 338,
     yy__uless = 339,
     yy__ugreater = 340,
     yy__ulessoreq = 341,
     yy__ugreateroreq = 342,
     yy__mod = 343,
     yy__slash = 344,
     yy__backslash = 345,
     yy__else = 346,
     yy__operation_inline = 347,
     yy__p_operation_inline = 348,
     yy__succeeds = 349,
     yy__succeeds_as = 350,
     yy__connect_to_file = 351,
     yy__RStream = 352,
     yy__WStream = 353,
     yy__RWStream = 354,
     yy__C_constr_for = 355,
     yy__connect_to_IP = 356,
     yy__GAddr = 357,
     yy__Var = 358,
     yy__MVar = 359,
     yy__StructPtr = 360,
     yy__debug_avm = 361,
     yy__terminal = 362,
     yy__avm = 363,
     yy__left_shift = 364,
     yy__right_shift = 365,
     yy__since = 366,
     yy__p_operation = 367,
     yy__p_type = 368,
     yy__p_variable = 369,
     yy__protect = 370,
     yy__lock = 371,
     yy__alt_number = 372,
     yy__config_file = 373,
     yy__verbose = 374,
     yy__stop_after = 375,
     yy__mapsto = 376,
     yy__rec_mapsto = 377,
     yy__language = 378,
     yy__mapstoo = 379,
     yy__rec_mapstoo = 380,
     yy__arroww = 381,
     yy__conf_int = 382,
     yy__conf_string = 383,
     yy__conf_symbol = 384,
     yy__we_have = 385,
     yy__enough = 386,
     yy__let = 387,
     yy__assume = 388,
     yy__indeed = 389,
     yy__hence = 390,
     yy__enddot = 391,
     yy__eof = 392,
     yy__integer = 393,
     yy__macro_integer = 394,
     yy__dummy = 395,
     yy__char = 396,
     prec_symbol = 397,
     prec_par_term = 398,
     prec_of_type = 399,
     prec_sym_type = 400,
     unaryminus = 401,
     yy__high = 402
   };
#endif
/* Tokens.  */
#define yy__if 258
#define yy__is 259
#define yy__then 260
#define yy__type 261
#define yy__operation 262
#define yy__variable 263
#define yy__utvar 264
#define yy__float 265
#define yy__alert 266
#define yy__theorem 267
#define yy__p_theorem 268
#define yy__proof 269
#define yy__read 270
#define yy__replaced_by 271
#define yy__exchange 272
#define yy__symbol 273
#define yy__Symbol 274
#define yy__lpar 275
#define yy__lbracket 276
#define yy__dot 277
#define yy__rbracket 278
#define yy__comma 279
#define yy__bit_width 280
#define yy__semicolon 281
#define yy__plus 282
#define yy__minus 283
#define yy__star 284
#define yy__carret 285
#define yy__ampersand 286
#define yy__vbar 287
#define yy__equals 288
#define yy__write 289
#define yy__percent 290
#define yy__abs_minus 291
#define yy__rbrace 292
#define yy__lbrace 293
#define yy__rpar 294
#define yy__colon 295
#define yy__implies 296
#define yy__forall 297
#define yy__exists 298
#define yy__exists_unique 299
#define yy__decimal_digit 300
#define yy__anb_string 301
#define yy__type_String 302
#define yy__type_ByteArray 303
#define yy__description 304
#define yy__arrow 305
#define yy__ndarrow 306
#define yy__with 307
#define yy__type_Float 308
#define yy__type_Listener 309
#define yy__indirect 310
#define yy__serialize 311
#define yy__unserialize 312
#define yy__vcopy 313
#define yy__non_equal 314
#define yy__load_module 315
#define yy__type_Int 316
#define yy__wait_for 317
#define yy__delegate 318
#define yy__tilde 319
#define yy__type_Omega 320
#define yy__type_alias 321
#define yy__p_type_alias 322
#define yy__checking_every 323
#define yy__dotdot 324
#define yy__dotsup 325
#define yy__exclam_equal 326
#define yy__rpar_arrow 327
#define yy__rpar_ndarrow 328
#define yy__dots 329
#define yy__g_operation 330
#define yy__less 331
#define yy__greater 332
#define yy__lessoreq 333
#define yy__greateroreq 334
#define yy__sless 335
#define yy__sgreater 336
#define yy__slessoreq 337
#define yy__sgreateroreq 338
#define yy__uless 339
#define yy__ugreater 340
#define yy__ulessoreq 341
#define yy__ugreateroreq 342
#define yy__mod 343
#define yy__slash 344
#define yy__backslash 345
#define yy__else 346
#define yy__operation_inline 347
#define yy__p_operation_inline 348
#define yy__succeeds 349
#define yy__succeeds_as 350
#define yy__connect_to_file 351
#define yy__RStream 352
#define yy__WStream 353
#define yy__RWStream 354
#define yy__C_constr_for 355
#define yy__connect_to_IP 356
#define yy__GAddr 357
#define yy__Var 358
#define yy__MVar 359
#define yy__StructPtr 360
#define yy__debug_avm 361
#define yy__terminal 362
#define yy__avm 363
#define yy__left_shift 364
#define yy__right_shift 365
#define yy__since 366
#define yy__p_operation 367
#define yy__p_type 368
#define yy__p_variable 369
#define yy__protect 370
#define yy__lock 371
#define yy__alt_number 372
#define yy__config_file 373
#define yy__verbose 374
#define yy__stop_after 375
#define yy__mapsto 376
#define yy__rec_mapsto 377
#define yy__language 378
#define yy__mapstoo 379
#define yy__rec_mapstoo 380
#define yy__arroww 381
#define yy__conf_int 382
#define yy__conf_string 383
#define yy__conf_symbol 384
#define yy__we_have 385
#define yy__enough 386
#define yy__let 387
#define yy__assume 388
#define yy__indeed 389
#define yy__hence 390
#define yy__enddot 391
#define yy__eof 392
#define yy__integer 393
#define yy__macro_integer 394
#define yy__dummy 395
#define yy__char 396
#define prec_symbol 397
#define prec_par_term 398
#define prec_of_type 399
#define prec_sym_type 400
#define unaryminus 401
#define yy__high 402




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
typedef int YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif




