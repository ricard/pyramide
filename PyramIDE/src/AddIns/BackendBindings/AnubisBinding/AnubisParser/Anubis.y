/* grammar.y *****************************************************************

																		Anubis
														 The language grammar. 

*****************************************************************************/ 
 
/* 
		This YACC/BISON file contains the grammar for the Anubis compiler. 

*/ 


//%skeleton "lalr1.cc"                          /*  -*- C++ -*- */
//%define "parser_class_name" "AnubisParser"
//%define "filename_type" "char"

%{
#include <stdlib.h>
#define YYINCLUDED_STDLIB_H			// To avoid some unwanted warnings 
#define YYSTYPE symbol_t   

#ifdef DEBUG
//#define YYDEBUG 1
#endif

#include "ParserEngine.h"
#include "Anubis.tab.hpp"  // to have a YYLTYPE definition

using namespace SoftArchitect::AnubisParser;
using namespace ICSharpCode::Core;

static int yylex (YYSTYPE* yylval, YYLTYPE* yylloc, ParserEngine^ parser);



static void yyerror(YYLTYPE* yylloc, ParserEngine^ parser, char *);
//static char * synterrmsg = "";    

static void print_token_value (FILE *, int, YYSTYPE);
#define YYPRINT(file, type, value) print_token_value (file, type, value)


%}

%pure-parser
%defines
%locations
%verbose
%debug

%parse-param { ParserEngine^ parser }
%lex-param   { ParserEngine^ parser }

%initial-action
{
  yydebug = 0;
};



 





/* all (non one char) tokens begin by yy__ -----------------------------------------------*/ 
%token     yy__if yy__is yy__then yy__type yy__operation yy__variable yy__utvar yy__float yy__alert
%token     yy__theorem yy__p_theorem yy__proof yy__read yy__replaced_by yy__exchange
%token     yy__symbol yy__Symbol yy__lpar yy__lbracket yy__dot yy__rbracket yy__comma yy__bit_width
%token     yy__semicolon yy__plus yy__minus yy__star yy__carret yy__ampersand yy__vbar 
%token     yy__equals yy__write yy__percent yy__abs_minus
%token     yy__rbrace yy__lbrace yy__rpar yy__colon yy__implies yy__forall yy__exists yy__exists_unique
%token     yy__decimal_digit yy__anb_string yy__type_String yy__type_ByteArray yy__description
%token     yy__arrow yy__ndarrow yy__with yy__type_Float yy__type_Listener yy__indirect
%token     yy__serialize yy__unserialize yy__vcopy yy__non_equal yy__load_module yy__type_Int
%token     yy__wait_for yy__delegate yy__tilde yy__type_Omega yy__type_alias yy__p_type_alias
%token     yy__checking_every yy__dotdot yy__dotsup yy__exclam_equal
%token     yy__rpar_arrow yy__rpar_ndarrow yy__dots yy__g_operation
%token     yy__less yy__greater yy__lessoreq yy__greateroreq 
%token     yy__sless yy__sgreater yy__slessoreq yy__sgreateroreq 
%token     yy__uless yy__ugreater yy__ulessoreq yy__ugreateroreq 
%token     yy__mod yy__slash yy__backslash yy__else yy__operation_inline yy__p_operation_inline
%token     yy__succeeds yy__succeeds_as yy__connect_to_file 
%token     yy__RStream yy__WStream yy__RWStream
%token     yy__C_constr_for yy__connect_to_IP yy__GAddr yy__Var yy__MVar yy__StructPtr
%token     yy__debug_avm yy__terminal yy__avm
%token     yy__left_shift yy__right_shift yy__since
%token     yy__p_operation yy__p_type yy__p_variable yy__protect yy__lock yy__alt_number
%token     yy__config_file yy__verbose yy__stop_after yy__mapsto yy__rec_mapsto yy__language
%token     yy__mapstoo yy__rec_mapstoo yy__arroww   
%token     yy__conf_int yy__conf_string yy__conf_symbol
%token     yy__we_have yy__enough yy__let yy__assume yy__indeed yy__hence yy__enddot yy__eof
%token     yy__integer yy__macro_integer yy__dummy
   
%token     yy__char



/* valued non terminals ----------------------------------------------------------------*/
/*
%type      Alternatives1 Alternatives Alternative AltOperands1 AltOperand 
%type      FArgs1 FArg OpArgs OpArgs1 OpArg Term AppTerm Terms Terms1 Terms2
%type      Conditional Clauses Clause Head ResurSym1 ResurSym2 ResurSym Type
%type      TypeVars1 Types1 TypesArgs1 Types2 List SymbolOrDecimalDigit 
%type      WithTerm SimpleBinaryOp SimpleUnaryOp ListHead
%type      AVM AVM_Instr IntMCons 
%type      Justif Dot EndDot
	 
%type   OpKW TypeKW VarKW ThKW TypeAliasKW
*/


/* precedence and associations rules ---------------------------------------------------*/

%right yy__if yy__float yy__alert yy__bit_width yy__forall yy__exists yy__exists_unique yy__anb_string
       yy__description yy__indirect yy__serialize yy__unserialize yy__vcopy yy__load_module
       yy__delegate yy__checking_every yy__avm yy__since yy__alt_number yy__integer
       yy__char error yy__utvar yy__Symbol yy__decimal_digit yy__type_String yy__type_ByteArray
       yy__type_Float yy__type_Listener
       yy__GAddr yy__Var yy__MVar yy__StructPtr yy__type_Int yy__macro_integer
   
%right yy__protect yy__lock
%right yy__debug_avm yy__terminal
%right yy__assume yy__let yy__enough yy__we_have yy__hence
%right yy__comma
%right yy__mapsto yy__rec_mapsto  
%right yy__colon
%right yy__is yy__with
%right yy__then yy__else
%right yy__semicolon
%right prec_symbol yy__symbol
%right yy__rpar
%right prec_par_term
%right prec_of_type
%right prec_sym_type
%right yy__vbar
%right yy__ampersand
%right yy__implies yy__left_shift yy__right_shift
%right yy__tilde   
%right yy__less yy__greater yy__lessoreq yy__greateroreq yy__sless yy__sgreater yy__slessoreq yy__sgreateroreq yy__uless yy__ugreater yy__ulessoreq yy__ugreateroreq yy__equals yy__write yy__exclam_equal yy__non_equal yy__exchange
%right yy__connect_to_file yy__connect_to_IP 
%right yy__mod
%right yy__plus
%left  yy__minus yy__abs_minus
%right yy__star
%right yy__percent   
%left  yy__slash yy__backslash yy__dot
%nonassoc yy__RStream yy__WStream yy__RWStream
%right yy__carret
%right unaryminus
%right yy__arrow yy__ndarrow yy__rpar_arrow yy__rpar_ndarrow
%right yy__lpar yy__lbrace yy__lbracket yy__rbracket
%left  yy__dotdot yy__dotsup
%right yy__high   






%% /* begining of grammar */ 





/* Texts are sequences of paragraphs -------------------------------------------------*/ 
Start:						Text																	{ }
;

Text:							Paragraphs														{ }
; 

Paragraphs:				/* Empty */														{ }
|									Paragraphs Paragraph									{ }
;


Dot:     yy__dot         { }
|        yy__enddot      { }
;
   
EndDot:  yy__enddot      { }
|        yy__dot yy__eof { }
; 
   

/* after each paragraph, some work is to be done ---------------------------------------*/ 
Paragraph:				Par			{ parser->Lexer.begin_INITIAL(); }
|									error		{ parser->Lexer.begin_INITIAL(); }
;





/* sorts of paragraphs -----------------------------------------------------*/ 
Par:		TypeDefinition												{ }
|				OperationDefinition										{ }
|				OperationDeclaration									{ }
|				VariableDeclaration										{ }
|				C_constr															{ }
|				yy__read yy__anb_string								{ parser->AddUsing((String^)(Object^)$2.object, @$); }
|				yy__replaced_by yy__anb_string				{ parser->AddUsing((String^)(Object^)$2.object, @$); }
|				error EndDot													{ yyerrok; }
;


C_constr:		yy__C_constr_for yy__Symbol yy__equals Type EndDot		{ }
;

OpKW:
		yy__operation				{ $$.object = ModifierEnum::Private; }
|		yy__p_operation			{ $$.object = ModifierEnum::Public; }
|   yy__operation_inline  { $$.object = ModifierEnum::Public; }
|   yy__p_operation_inline { $$.object = ModifierEnum::Public; }
|		yy__g_operation			{ $$.object = ModifierEnum::Public; }
; 
	 
VarKW:     
		yy__variable        { $$.object = ModifierEnum::Private; }
|   yy__p_variable      { $$.object = ModifierEnum::Public; }
;    
	 
TypeKW:    yy__type     { $$.object = ModifierEnum::Private; }
|          yy__p_type   { $$.object = ModifierEnum::Public; }
;
	 
TypeAliasKW:    yy__type_alias                      { $$.object = ModifierEnum::Private;}
|               yy__p_type_alias                    { $$.object = ModifierEnum::Public;}
;
   
/* type (and type scheme) definitions -----------------------------------------*/ 
TypeDefinition: 
  TypeAliasKW yy__Symbol yy__equals Type EndDot     { parser->AddType((ModifierEnum)(Object^)$1.object, (String^)(Object^)$2.object, @4); }
| TypeAliasKW yy__Symbol yy__lpar TypeVars1 yy__rpar yy__equals Type EndDot { parser->AddType((ModifierEnum)(Object^)$1.object, (String^)(Object^)$2.object, @7); }
/*|	TypeKW yy__Symbol yy__equals Type EndDot					{ parser->AddType((ModifierEnum)(Object^)$1.object, (String^)(Object^)$2.object, @4); }*/
|	TypeKW yy__Symbol yy__colon Alternatives					{ parser->AddType((ModifierEnum)(Object^)$1.object, (String^)(Object^)$2.object, @4); }
| TypeKW yy__Symbol yy__lpar TypeVars1 yy__rpar yy__colon Alternatives
																										{ parser->AddType((ModifierEnum)(Object^)$1.object, (String^)(Object^)$2.object, @7); }
;

TypeVars1:       yy__utvar                                { }
|                yy__utvar yy__comma TypeVars1              { }
; 






/* alternatives --------------------------------------------------------------*/
Alternatives:   EndDot                                   { }
|               yy__dots                                 { }
|               Alternatives1                            { }
;   
	 
Alternatives1:  
		Alternative EndDot                                   { }
|   Alternative yy__comma yy__dots                       { }
|   Alternative yy__comma Alternatives1                  { }
;

Alternative:  
		yy__symbol                                               { }
|   yy__decimal_digit                                        { }
|   yy__lbracket yy__rbracket                                { }
|   yy__symbol yy__lpar AltOperands1 yy__rpar                { }
|   yy__lbracket AltOperand Dot AltOperand yy__rbracket  { }
|   AltOperand yy__plus AltOperand                       { }
|   AltOperand yy__dot  AltOperand                       { }
|   AltOperand yy__dotdot AltOperand                     { }
|   AltOperand yy__dotsup AltOperand                     { }
|   AltOperand yy__star AltOperand                       { }
|   AltOperand yy__percent AltOperand                    { }
|   AltOperand yy__carret AltOperand                     { }
|   AltOperand yy__vbar AltOperand                       { }
|   AltOperand yy__ampersand AltOperand                  { }
|   AltOperand yy__colon AltOperand                      { }
|   AltOperand yy__arrow AltOperand                      { }
|   AltOperand yy__equals AltOperand                     { }
|   AltOperand yy__implies AltOperand                    { }
|   AltOperand yy__left_shift AltOperand                 { }
|   AltOperand yy__right_shift AltOperand                { }
|   AltOperand yy__minus AltOperand                      { }
|   AltOperand yy__abs_minus AltOperand                  { }
|   AltOperand yy__slash AltOperand                      { }
|   AltOperand yy__backslash AltOperand                  { }
|   AltOperand yy__mod AltOperand yy__rpar               { }
|   AltOperand yy__less AltOperand                       { }
|   AltOperand yy__sless AltOperand                      { }
|   AltOperand yy__uless AltOperand                      { }
|   AltOperand yy__non_equal AltOperand                  { }
|   AltOperand yy__exclam_equal AltOperand               { }
|   AltOperand yy__lessoreq AltOperand                   { }
|   AltOperand yy__slessoreq AltOperand                  { }
|   AltOperand yy__ulessoreq AltOperand                  { }
|   yy__tilde AltOperand                                 { }
|   yy__star  AltOperand                                 { }
;

AltOperands1:     
		AltOperand                                           { }
|   AltOperand yy__comma AltOperands1                    { }
; 

AltOperand:   
		Type yy__symbol                                      { }
|   Type              %prec yy__comma                    { } 
; 







/* types ---------------------------------------------------------------*/
Type:             
		yy__lpar Type yy__rpar                              { $$ = $2; }
|   yy__Symbol    %prec prec_sym_type                   { $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
|   yy__utvar                                           { $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
|   yy__type_String                                     { $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
|   yy__type_ByteArray                                  { $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
|   yy__type_Int                                        { $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
|   yy__type_Float                                      { $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
|   yy__type_Listener                                   { $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
|   yy__forall yy__utvar Type                           { $$ = $3; }
	 
|   yy__RStream                    											{ $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
|   yy__WStream                  											  { $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
|   yy__RWStream               											    { $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
	 
|   yy__GAddr yy__lpar Type yy__rpar                    { $$ = $3; }
|   yy__Var yy__lpar Type yy__rpar                      { $$ = $3; }
|   yy__MVar yy__lpar Type yy__rpar                     { $$ = $3; }
|   yy__Symbol yy__lpar Types1 yy__rpar                 { $$.object = parser->GetReturnType((String^)(Object^)$1.object, @1); }
|   yy__StructPtr yy__lpar yy__Symbol yy__rpar          { $$.object = parser->GetReturnType((String^)(Object^)$3.object, @3); }
|   Type yy__arrow Type                                 { AnonymousMethodReturnType^ lambda = gcnew AnonymousMethodReturnType(parser->CU);
																													lambda->MethodReturnType = (IReturnType^)(Object^)$3.object;
																													lambda->MethodParameters->Add(gcnew DefaultParameter("", (IReturnType^)(Object^)$1.object, parser->CreateDomRegion(@1)));
																													$$.object = lambda; }
|   yy__Symbol yy__lpar Types1 yy__rpar_arrow Type			{ AnonymousMethodReturnType^ lambda = gcnew AnonymousMethodReturnType(parser->CU);
																													lambda->MethodReturnType = (IReturnType^)(Object^)$5.object;
																													// Todo : parameters
																													$$.object = lambda; }
|   yy__lpar TypesArgs1 yy__rpar_arrow Type             { AnonymousMethodReturnType^ lambda = gcnew AnonymousMethodReturnType(parser->CU);
																													lambda->MethodReturnType = (IReturnType^)(Object^)$4.object;
																													lambda->MethodParameters = (IList<IParameter^>^)(Object^)$2.object;
																													$$.object = lambda; }
|   yy__lpar Types2 yy__rpar                            { $$ = $2; }
|   yy__lbrace Type yy__rbrace                          { $$ = $2; }
//|   yy__lpar TypesArgs1 yy__rpar {}
|   error EndDot																				{ $$.object = gcnew DefaultReturnType(gcnew DefaultClass(parser->CU, "__error__")); }
; 

Types1:      
		Type																								{ }
|   Type yy__comma Types1                               { }
;  

TypesArgs1:      
		Type																								{ IList<IParameter^>^ params = gcnew List<IParameter^>(); 
																													params->Add(gcnew DefaultParameter("", (IReturnType^)(Object^)$1.object, parser->CreateDomRegion(@1)));
																													$$.object = params; }
|   Type yy__symbol																			{ IList<IParameter^>^ params = gcnew List<IParameter^>(); 
																													params->Add(gcnew DefaultParameter((String^)(Object^)$2.object, (IReturnType^)(Object^)$1.object, parser->CreateDomRegion(@1)));
																													$$.object = params; }
|   Type yy__comma TypesArgs1														{ IList<IParameter^>^ params = (List<IParameter^>^)(Object^)$3.object;
																													params->Add(gcnew DefaultParameter("", (IReturnType^)(Object^)$1.object, parser->CreateDomRegion(@1)));
																													$$.object = params; }
|   Type yy__symbol yy__comma TypesArgs1								{ IList<IParameter^>^ params = (List<IParameter^>^)(Object^)$4.object;
																													params->Add(gcnew DefaultParameter((String^)(Object^)$2.object, (IReturnType^)(Object^)$1.object, parser->CreateDomRegion(@1)));
																													$$.object = params; }
;  

Types2:        
		Type yy__comma Type                                { CombinedReturnType^ combi = gcnew CombinedReturnType(gcnew List<IReturnType^>(), "", "", "", ""); 
						                                             combi->BaseTypes->Add((IReturnType^)(Object^)$1.object);
						                                             combi->BaseTypes->Add((IReturnType^)(Object^)$3.object);
						                                             $$.object = combi; }
|   Type yy__comma Types2                              { CombinedReturnType^ combi = (CombinedReturnType^)(Object^)$3.object;
																												 combi->BaseTypes->Insert(0, (IReturnType^)(Object^)$1.object);
																												 $$.object = combi; }
;
 






/* declaration of global variable -------------------------------------------------*/ 
VariableDeclaration: VarKW Type yy__symbol yy__equals Term EndDot 
		{ parser->AddVar((ModifierEnum)(Object^)$1.object, nullptr, (String^)(Object^)$3.object, @$); }
; 





/* definition of operation ------------------------------------------------------*/
SimpleBinaryOp:     yy__plus						{ }
|                   yy__dot							{ }
|                   yy__dotdot					{ }
|                   yy__dotsup					{ }
|                   yy__star						{ }
|                   yy__percent					{ }
|                   yy__carret					{ }
|                   yy__vbar						{ }
|                   yy__ampersand				{ }
|                   yy__colon						{ }
|                   yy__arrow						{ }
|                   yy__equals					{ }
|                   yy__implies					{ }
|                   yy__left_shift			{ }
|                   yy__right_shift			{ }
|                   yy__minus						{ }
|                   yy__abs_minus				{ }
|                   yy__slash						{ }
|                   yy__backslash				{ }
|                   yy__less						{ }
|                   yy__sless 					{ }
|                   yy__uless 					{ }
|                   yy__non_equal				{ }
|                   yy__exclam_equal		{ }
|                   yy__lessoreq				{ }
|                   yy__slessoreq 			{ }
|                   yy__ulessoreq 			{ }
; 

SimpleUnaryOp:      yy__plus  { }
|                   yy__minus { }
|                   yy__tilde { }
; 

OperationDefinition: 
	OpKW Type yy__symbol yy__equals Term EndDot
		{ parser->AddFunction((ModifierEnum)(Object^)$1.object, (IReturnType^)(Object^)$2.object, (String^)(Object^)$3.object, @4, @$); }
		
|	OpKW Type yy__symbol yy__lpar OpArgs yy__rpar yy__equals Term EndDot
		{ DefaultMethod^ method = (DefaultMethod^)parser->AddFunction((ModifierEnum)(Object^)$1.object, (IReturnType^)(Object^)$2.object, (String^)(Object^)$3.object, @7, @$);
			method->Parameters = (IList<IParameter^>^)(Object^)$5.object; }
		
/* define T X x + Y y = term.*/ 
|	OpKW Type  OpArg SimpleBinaryOp OpArg  yy__equals Term EndDot
		{ IMethod^ method = parser->AddFunction((ModifierEnum)(Object^)$1.object, (IReturnType^)(Object^)$2.object, (String^)(Object^)$4.object, @6, @$); 
			method->Parameters->Add((IParameter^)(Object^)$3.object);
			method->Parameters->Add((IParameter^)(Object^)$5.object); }

/* define T X x + Y y.*/
|	OpKW Type  OpArg SimpleBinaryOp OpArg  EndDot

/* define T [X x . Y y] = term.*/
|	OpKW Type  yy__lbracket OpArg Dot OpArg yy__rbracket yy__equals Term EndDot
		{ IMethod^ method = parser->AddFunction((ModifierEnum)(Object^)$1.object, (IReturnType^)(Object^)$2.object, (String^)(Object^)$5.object, @8, @$); 
			method->Parameters->Add((IParameter^)(Object^)$4.object);
			method->Parameters->Add((IParameter^)(Object^)$6.object); }

/* define T [X x . Y y]. */ 
|	OpKW Type  yy__lbracket OpArg Dot OpArg yy__rbracket EndDot

/* define T ~ X x = term. */ 
|	OpKW Type SimpleUnaryOp OpArg  yy__equals Term EndDot
		{ IMethod^ method = parser->AddFunction((ModifierEnum)(Object^)$1.object, (IReturnType^)(Object^)$2.object, (String^)(Object^)$3.object, @5, @$);
			method->Parameters->Add((IParameter^)(Object^)$4.object); }

/* define T ~ X x. */ 
|	OpKW Type  SimpleUnaryOp OpArg  EndDot

/* define T X x (mod Y y) = term. */    
|	OpKW Type  OpArg yy__mod OpArg yy__rpar yy__equals Term EndDot
		{ IMethod^ method = parser->AddFunction((ModifierEnum)(Object^)$1.object, (IReturnType^)(Object^)$2.object, (String^)(Object^)$4.object, @7, @$); 
			method->Parameters->Add((IParameter^)(Object^)$3.object);
			method->Parameters->Add((IParameter^)(Object^)$5.object); }
		
/* define T X x (mod Y y). */    
|	OpKW Type  OpArg yy__mod OpArg yy__rpar EndDot

;


OperationDeclaration: 
	OpKW Type yy__symbol yy__lpar OpArgs yy__rpar EndDot
| OpKW Type yy__symbol EndDot
;








/* arguments of operations ----------------------------------------------------*/
OpArgs:   /* empty */												{ $$.object = nullptr; }
|           OpArgs1													{ $$ = $1; }
; 

OpArgs1:    OpArg														{ IList<IParameter^>^ params = gcnew List<IParameter^>(); 
																							params->Add((IParameter^)(Object^)$1.object);
																							$$.object = params;}
|           OpArg yy__comma OpArgs1					{ IList<IParameter^>^ params = (List<IParameter^>^)(Object^)$3.object;
																							params->Insert(0, (IParameter^)(Object^)$1.object);
																							$$.object = params;
																							}
; 

OpArg:      Type yy__symbol									{ $$.object = gcnew DefaultParameter((String^)(Object^)$2.object, (IReturnType^)(Object^)$1.object, parser->CreateDomRegion(@1))}
; 



/* arguments of (non top level) functions */ 
/*
FArgs:                                     { }
|          FArgs1                          { }
;
*/
	 
FArgs1:    FArg                            { }
|          FArg yy__comma FArgs1           { }
;
	 
FArg:      Type yy__symbol                 { }
//|          yy__symbol  %prec prec_symbol	{ }
;    


/* terms ---------------------------------------------------------------------------*/
Term: yy__alert																				{ }
|     yy__alt_number yy__lpar Term yy__rpar           { }
|     yy__protect Term                                { }
|     yy__lock Term yy__comma Term                    { }
|     yy__debug_avm Term                              { }
|     yy__avm yy__lbrace AVM yy__rbrace               { }
|     yy__terminal Term                               { }
|     yy__symbol                %prec prec_symbol     { }
//|     yy__Symbol                %prec prec_symbol     {}
|     yy__lpar yy__rpar         %prec prec_symbol     {}
|     yy__integer               %prec prec_symbol     { }
|     yy__macro_integer         %prec prec_symbol     { }
|     yy__char                  %prec prec_symbol     { }
|     yy__float                 %prec prec_symbol     { }
|     yy__forall yy__utvar Term                       { }
|     yy__lpar Term yy__rpar            %prec prec_par_term   { }
|     yy__lpar Terms2 yy__rpar          %prec prec_par_term   { }
|     yy__lpar Type yy__rpar Term           %prec prec_of_type  { }
|     yy__lpar yy__colon Term yy__rpar Term %prec prec_of_type  { }
|     yy__lpar yy__colon Type yy__rpar Term %prec prec_of_type  { }
|     AppTerm                                       { }
|     Conditional                                   { }
//|     Term					                              { }
|     yy__lbracket List                               { }
|     yy__star Term                                   { }
//|     yy__star Type                                 { }
|     Term yy__write Term                             { }
|     Term yy__exchange Term                          { }
|     Term yy__semicolon Term													{ }
|     yy__load_module yy__lpar Term yy__rpar              { }
|     yy__serialize yy__lpar Term yy__rpar                { }
|     yy__unserialize yy__lpar Term yy__rpar              { }
|     yy__bit_width yy__lpar Type yy__rpar                { }
|     yy__indirect yy__lpar Type yy__rpar                 { }
|     yy__vcopy yy__lpar Term yy__comma Term yy__rpar       { }
|     yy__lpar Type yy__rpar yy__connect_to_file Term       { }
|     yy__lpar Type yy__rpar yy__connect_to_IP Term yy__colon Term    { }
|     yy__anb_string																				{ }  
|     yy__with yy__symbol yy__equals Term yy__comma WithTerm         { }
|     yy__checking_every Term yy__milliseconds yy__comma yy__wait_for Term yy__then Term  
																												{ }
|     yy__delegate Term yy__comma Term                      { }
|     yy__lbrace yy__symbol yy__colon Type yy__comma Term yy__rbrace { }
|     yy__lpar FArgs1 yy__rpar yy__mapsto Term                   { }
|     yy__lpar FArgs1 yy__rpar yy__rec_mapsto Term               { }
|     yy__forall yy__symbol yy__colon Type yy__comma Term { } 
|     yy__forall yy__symbol yy__colon Term yy__comma Term { } 
|     yy__exists yy__symbol yy__colon Type yy__comma Term { } 
|     yy__exists_unique yy__symbol yy__colon Type yy__comma Term { } 
|     yy__description yy__symbol yy__colon Type yy__comma Term { } 
//|     yy__djed { YYERROR; }
; 

Terms2:         Term yy__comma Term               { }
|               Term yy__comma Terms2             { }
; 



/* 'with' terms ------------------------------------------------------------------*/ 
WithTerm:       Term   %prec yy__comma                    { }
|               yy__symbol yy__equals Term yy__comma WithTerm      { }
|               SimpleBinaryOp yy__equals Term yy__comma WithTerm      { }
|               yy__tilde yy__equals Term yy__comma WithTerm       { }
; 

	 
	 
/* lists -----------------------------------------------------------------------------*/
List:      yy__rbracket                       { }
|          Term yy__rbracket                  { }
|          Term yy__comma List                { }
|          Term Dot Term yy__rbracket					{ }
;




/* applicative terms ------------------------------------------------------------------*/
AppTerm:    Term yy__lpar Terms yy__rpar        { }
|           Term yy__lbracket List { }
|					Term yy__plus        Term { }
|         Term yy__dot Term   %prec yy__high { }
|         Term yy__dotdot      Term { }
|         Term yy__dotsup      Term { }
|         Term yy__star        Term { }
|         Term yy__percent     Term { }
|         Term yy__carret      Term { }
|         Term yy__vbar        Term { }
|         Term yy__ampersand   Term { }
|         Term yy__colon       Term { }
|         Term yy__arrow       Term { }
|         Term yy__equals      Term { }
|         Term yy__implies     Term { }
|         Term yy__left_shift  Term { }
|         Term yy__right_shift Term { }
|         Term yy__minus       Term { }
|         Term yy__abs_minus   Term { }
|         Term yy__slash       Term { }
|         Term yy__backslash   Term { }
|         Term yy__less        Term { }
|         Term yy__sless       Term { }
|         Term yy__uless       Term { }
|         Term yy__non_equal   Term { }
|         Term yy__exclam_equal  Term { }
|         Term yy__greater     Term { }
|         Term yy__sgreater     Term { }
|         Term yy__ugreater     Term { }
|         Term yy__lessoreq    Term { }
|         Term yy__slessoreq    Term { }
|         Term yy__ulessoreq    Term { }
|         Term yy__greateroreq Term { }
|         Term yy__sgreateroreq Term { }
|         Term yy__ugreateroreq Term { }
|         Term yy__mod Term yy__rpar { }
|         yy__plus  Term %prec unaryminus { }
|         yy__minus Term %prec unaryminus { }
|         yy__tilde Term  { }
; 

Terms:      /* empty */                    { }
|           Terms1                         { }
; 

Terms1:     Term                           { }
|           Term yy__comma Terms1                { }
; 




/* conditional terms -------------------------------------------------------------------*/
Conditional:   yy__if Term yy__is yy__lbrace Clauses yy__rbrace 
| yy__if Term yy__succeeds_as yy__symbol yy__then Term
| yy__if Term yy__succeeds yy__then Term
| yy__if Term yy__is Clause 
| yy__since Term yy__is Head yy__comma Term 
| yy__if Term yy__then Term yy__else Term
| yy__if Term yy__is Clause yy__else Term  
| yy__if Term yy__then Term
; 



/* clauses in conditionals ------------------------------------------------*/
/*   
Clauses1:      Clause                       { }
|              Clause yy__comma Clauses1    { }
|              Clause Clauses1              { }
;
*/ 
	 
Clauses:       /* empty */                  { }
|              Clause Clauses               { }
|              Clause yy__comma Clauses     { }
;
	 
Clause:       Head yy__then Term            { }
; 

Head: yy__symbol                             { }
|     yy__decimal_digit                      { }
|     yy__lbracket yy__rbracket              { }

|     yy__symbol yy__lpar yy__rpar           { }
|     yy__symbol yy__lpar ResurSym1 yy__rpar { }
//|     yy__lbracket ResurSym yy__dot ResurSym yy__rbracket { }
|			yy__lbracket ListHead									 { }
|     ResurSym yy__plus ResurSym            { }
|     ResurSym yy__dot ResurSym                           { }
|     ResurSym yy__dotdot ResurSym                        { }
|     ResurSym yy__dotsup ResurSym                        { }
|     ResurSym yy__star ResurSym            { }
|     ResurSym yy__percent ResurSym         { }
|     ResurSym yy__carret ResurSym          { }
|     ResurSym yy__vbar ResurSym            { }
|     ResurSym yy__ampersand ResurSym       { }
|     ResurSym yy__colon ResurSym                         { }
|     ResurSym yy__arrow ResurSym           { }
|     ResurSym yy__equals ResurSym          { }
|     ResurSym yy__implies ResurSym         { }
|     ResurSym yy__left_shift ResurSym      { }
|     ResurSym yy__right_shift ResurSym     { }
|     ResurSym yy__minus ResurSym           { }
|     ResurSym yy__abs_minus ResurSym                     { }
|     ResurSym yy__slash ResurSym           { }
|     ResurSym yy__backslash ResurSym                     { }
|     ResurSym yy__mod ResurSym yy__rpar    { }
|     ResurSym yy__less ResurSym            { }
|     ResurSym yy__sless ResurSym                         { }
|     ResurSym yy__uless ResurSym                         { }
|     ResurSym yy__non_equal ResurSym       { }
|     ResurSym yy__exclam_equal ResurSym                  { }
|     ResurSym yy__lessoreq ResurSym        { }
|     ResurSym yy__slessoreq ResurSym                     { }
|     ResurSym yy__ulessoreq ResurSym                     { }
|     yy__tilde ResurSym                    { }
|     yy__lpar ResurSym2 yy__rpar           { }
;

/* Nested heads in the form of lists: [h . t], [h], [h,i], ... [h,i,j . t] */ 
ListHead:   ResurSym yy__rbracket                   { }
|           ResurSym yy__comma ListHead             { }
|           ResurSym Dot ResurSym yy__rbracket			{ }
;
   
/* How it was before the introduction of nested heads 
ResurSym:      yy__symbol                     { }
|              Type yy__symbol                { }
;
*/

/* How it is after the introduction of nested heads */ 
ResurSym:      Head                           { }
|              Type yy__symbol                { }
;
/* A ResurSym is returned in one of the forms:
     
   <string>
   (<type> . <string>)
   
*/ 

ResurSym1:     ResurSym                     { }
|              ResurSym yy__comma ResurSym1       { }
; 

ResurSym2:     ResurSym yy__comma ResurSym      { }
|              ResurSym yy__comma ResurSym2     { }
;

	 
/* virtual machine instructions ---------------------------------------------------------*/    
AVM:                 { }
|      AVM_Instr AVM { }
; 

AVM_Instr:     yy__symbol { }
|              yy__lpar yy__symbol IntMCons { } 
;

IntMCons:      yy__rpar                   { }
|              Dot yy__integer yy__rpar   { }
|              yy__integer IntMCons       { }
|              yy__anb_string IntMCons    { }
;

	 
/* pseudo keywords */ 
	 
yy__milliseconds:    yy__symbol  { }
; 

	 
%% /* end of grammar */ 

int yylex (YYSTYPE* yylval, YYLTYPE* yylloc, ParserEngine^ parser)
{
	return parser->Lexer.yylex(yylval, yylloc);
}

void yyerror(YYLTYPE* yylloc, ParserEngine^ parser, char * msg)
{
	// TODO yyerror()
	LoggingService::Debug(String::Format("PARSER ERROR: {0} on {1}({2},{3})", 
																			 gcnew String(msg), parser->Filename, yylloc->first_line, yylloc->first_column));
}


static void
print_token_value (FILE *file, int type, YYSTYPE value)
{
	Object^ obj = value.object;
	if(obj != nullptr)
	{
		const char * str = LexerEngine::GetUnmanagedString(obj->ToString());
    fprintf (file, "%s", str);
    LexerEngine::FreeUnmanagedString(str);
  }
}
