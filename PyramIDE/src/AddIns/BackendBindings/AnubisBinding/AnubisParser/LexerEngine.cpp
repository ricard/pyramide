#include "StdAfx.h"
#include "LexerEngine.h"

using namespace ICSharpCode::Core;
using System::Runtime::InteropServices::Marshal;

LexerEngine::LexerEngine(TextReader^ reader)
:	_reader(reader),
	current_par_line(0),	// line number of first line of paragraph currently read
	comlevel(0),					// counting the level of nested comments
	tab_seen(0),					// becomes 1 when at least one tabulator has been seen
	tab_width(4),					// default (guessed) width of tabulators
	ext_com(gcnew StringBuilder()) 
{
	_loc.first_line = 1;
	_loc.first_column = 1;
	_loc.last_line = 1;
	_loc.last_column = 1;
}

LexerEngine::~LexerEngine(void)
{
}


// updating line and column counters ---------------------------------------------
void LexerEngine::update_lc(void)
{
	_loc.first_line = _loc.last_line;
	_loc.first_column = _loc.last_column;
	int i = 0;
	while (yytext[i] != 0)
	{
		if (yytext[i] == '\t') { tab_seen = 1;      _loc.last_column += tab_width; }
		if (yytext[i] == '\n') { _loc.last_line++;  _loc.last_column = 1; }
		else                   _loc.last_column++; 
		i++; 
	} 
}

void LexerEngine::begin_str()
{
	str_buf = gcnew StringBuilder();
	_strLoc.first_column = _loc.last_column; // before the "
	_strLoc.first_line = _loc.first_line;
}

/* storing a character into the string buffer 'str_buf'. */ 
void LexerEngine::store_str_char(char c)
{
	str_buf->Append(c); 
}

void LexerEngine::end_str()
{
	_strLoc.last_column = _loc.first_column; // before the "
	_strLoc.last_line = _loc.first_line;
}


/* The function 'store_external_comment' is used while making the global index. 
*/
void LexerEngine::store_external_comment(char c)
{
	ext_com->Append(c); 
}

/* extract 'name' from '|-name->' */ 
String^ LexerEngine::rec_name(char *s, int del)
{
	return gcnew String(s, 2, strlen(s) - del);
}

void LexerEngine::lexical_kwread_error()
{
	LoggingService::Error(String::Format("LEXICAL ERROR. Line {0}. Cannot recognize token: {1}", (Int32)_loc.first_line, gcnew String(yytext, 0, strlen(yytext))));
}

void LexerEngine::lexical_error()
{
	LoggingService::Error(String::Format("LEXICAL ERROR. Line {0}. Cannot recognize token: {1}", (Int32)_loc.first_line, gcnew String(yytext, 0, strlen(yytext))));
}

int LexerEngine::ReadInput(char * buf, int max_size)
{
	array<wchar_t>^ buffer = gcnew array<wchar_t>(max_size+1);
	int real_size = _reader->Read(buffer, 0, max_size);
	buffer[real_size] = L'\0';
	String ^tmpStr = gcnew String(buffer);
	const char * str = GetUnmanagedString(tmpStr);
	strncpy_s(buf, max_size+1, str, real_size);
	FreeUnmanagedString(str);
	return real_size;
}

const char * LexerEngine::GetUnmanagedString(String ^ s)
{
	const char * lstr = 0;

	try
	{
		// copies the contents of s into Windows global heap,
		// converting to ANSI format on-the-fly if required.
		// It also allocates the required native heap memory.
		lstr = static_cast<const char *>(const_cast<void*>(static_cast<const void*>(Marshal::StringToHGlobalAnsi(s))));
	}
	// s is 0
	catch (ArgumentException ^ /*e*/)
	{
		// exception handling code

	}
	// could not allocate enough memory on native heap
	catch (OutOfMemoryException ^ /*e*/)
	{
		// exception handling code
	}
	return lstr;
}

void LexerEngine::FreeUnmanagedString(const char * lstr)
{
	Marshal::FreeHGlobal(static_cast<IntPtr>(const_cast<void*>(static_cast<const void*>(lstr))));
}