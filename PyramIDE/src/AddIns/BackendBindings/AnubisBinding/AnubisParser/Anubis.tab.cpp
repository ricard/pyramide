/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Using locations.  */
#define YYLSP_NEEDED 1



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     yy__if = 258,
     yy__is = 259,
     yy__then = 260,
     yy__type = 261,
     yy__operation = 262,
     yy__variable = 263,
     yy__utvar = 264,
     yy__float = 265,
     yy__alert = 266,
     yy__theorem = 267,
     yy__p_theorem = 268,
     yy__proof = 269,
     yy__read = 270,
     yy__replaced_by = 271,
     yy__exchange = 272,
     yy__symbol = 273,
     yy__Symbol = 274,
     yy__lpar = 275,
     yy__lbracket = 276,
     yy__dot = 277,
     yy__rbracket = 278,
     yy__comma = 279,
     yy__bit_width = 280,
     yy__semicolon = 281,
     yy__plus = 282,
     yy__minus = 283,
     yy__star = 284,
     yy__carret = 285,
     yy__ampersand = 286,
     yy__vbar = 287,
     yy__equals = 288,
     yy__write = 289,
     yy__percent = 290,
     yy__abs_minus = 291,
     yy__rbrace = 292,
     yy__lbrace = 293,
     yy__rpar = 294,
     yy__colon = 295,
     yy__implies = 296,
     yy__forall = 297,
     yy__exists = 298,
     yy__exists_unique = 299,
     yy__decimal_digit = 300,
     yy__anb_string = 301,
     yy__type_String = 302,
     yy__type_ByteArray = 303,
     yy__description = 304,
     yy__arrow = 305,
     yy__ndarrow = 306,
     yy__with = 307,
     yy__type_Float = 308,
     yy__type_Listener = 309,
     yy__indirect = 310,
     yy__serialize = 311,
     yy__unserialize = 312,
     yy__vcopy = 313,
     yy__non_equal = 314,
     yy__load_module = 315,
     yy__type_Int = 316,
     yy__wait_for = 317,
     yy__delegate = 318,
     yy__tilde = 319,
     yy__type_Omega = 320,
     yy__type_alias = 321,
     yy__p_type_alias = 322,
     yy__checking_every = 323,
     yy__dotdot = 324,
     yy__dotsup = 325,
     yy__exclam_equal = 326,
     yy__rpar_arrow = 327,
     yy__rpar_ndarrow = 328,
     yy__dots = 329,
     yy__g_operation = 330,
     yy__less = 331,
     yy__greater = 332,
     yy__lessoreq = 333,
     yy__greateroreq = 334,
     yy__sless = 335,
     yy__sgreater = 336,
     yy__slessoreq = 337,
     yy__sgreateroreq = 338,
     yy__uless = 339,
     yy__ugreater = 340,
     yy__ulessoreq = 341,
     yy__ugreateroreq = 342,
     yy__mod = 343,
     yy__slash = 344,
     yy__backslash = 345,
     yy__else = 346,
     yy__operation_inline = 347,
     yy__p_operation_inline = 348,
     yy__succeeds = 349,
     yy__succeeds_as = 350,
     yy__connect_to_file = 351,
     yy__RStream = 352,
     yy__WStream = 353,
     yy__RWStream = 354,
     yy__C_constr_for = 355,
     yy__connect_to_IP = 356,
     yy__GAddr = 357,
     yy__Var = 358,
     yy__MVar = 359,
     yy__StructPtr = 360,
     yy__debug_avm = 361,
     yy__terminal = 362,
     yy__avm = 363,
     yy__left_shift = 364,
     yy__right_shift = 365,
     yy__since = 366,
     yy__p_operation = 367,
     yy__p_type = 368,
     yy__p_variable = 369,
     yy__protect = 370,
     yy__lock = 371,
     yy__alt_number = 372,
     yy__config_file = 373,
     yy__verbose = 374,
     yy__stop_after = 375,
     yy__mapsto = 376,
     yy__rec_mapsto = 377,
     yy__language = 378,
     yy__mapstoo = 379,
     yy__rec_mapstoo = 380,
     yy__arroww = 381,
     yy__conf_int = 382,
     yy__conf_string = 383,
     yy__conf_symbol = 384,
     yy__we_have = 385,
     yy__enough = 386,
     yy__let = 387,
     yy__assume = 388,
     yy__indeed = 389,
     yy__hence = 390,
     yy__enddot = 391,
     yy__eof = 392,
     yy__integer = 393,
     yy__macro_integer = 394,
     yy__dummy = 395,
     yy__char = 396,
     prec_symbol = 397,
     prec_par_term = 398,
     prec_of_type = 399,
     prec_sym_type = 400,
     unaryminus = 401,
     yy__high = 402
   };
#endif
/* Tokens.  */
#define yy__if 258
#define yy__is 259
#define yy__then 260
#define yy__type 261
#define yy__operation 262
#define yy__variable 263
#define yy__utvar 264
#define yy__float 265
#define yy__alert 266
#define yy__theorem 267
#define yy__p_theorem 268
#define yy__proof 269
#define yy__read 270
#define yy__replaced_by 271
#define yy__exchange 272
#define yy__symbol 273
#define yy__Symbol 274
#define yy__lpar 275
#define yy__lbracket 276
#define yy__dot 277
#define yy__rbracket 278
#define yy__comma 279
#define yy__bit_width 280
#define yy__semicolon 281
#define yy__plus 282
#define yy__minus 283
#define yy__star 284
#define yy__carret 285
#define yy__ampersand 286
#define yy__vbar 287
#define yy__equals 288
#define yy__write 289
#define yy__percent 290
#define yy__abs_minus 291
#define yy__rbrace 292
#define yy__lbrace 293
#define yy__rpar 294
#define yy__colon 295
#define yy__implies 296
#define yy__forall 297
#define yy__exists 298
#define yy__exists_unique 299
#define yy__decimal_digit 300
#define yy__anb_string 301
#define yy__type_String 302
#define yy__type_ByteArray 303
#define yy__description 304
#define yy__arrow 305
#define yy__ndarrow 306
#define yy__with 307
#define yy__type_Float 308
#define yy__type_Listener 309
#define yy__indirect 310
#define yy__serialize 311
#define yy__unserialize 312
#define yy__vcopy 313
#define yy__non_equal 314
#define yy__load_module 315
#define yy__type_Int 316
#define yy__wait_for 317
#define yy__delegate 318
#define yy__tilde 319
#define yy__type_Omega 320
#define yy__type_alias 321
#define yy__p_type_alias 322
#define yy__checking_every 323
#define yy__dotdot 324
#define yy__dotsup 325
#define yy__exclam_equal 326
#define yy__rpar_arrow 327
#define yy__rpar_ndarrow 328
#define yy__dots 329
#define yy__g_operation 330
#define yy__less 331
#define yy__greater 332
#define yy__lessoreq 333
#define yy__greateroreq 334
#define yy__sless 335
#define yy__sgreater 336
#define yy__slessoreq 337
#define yy__sgreateroreq 338
#define yy__uless 339
#define yy__ugreater 340
#define yy__ulessoreq 341
#define yy__ugreateroreq 342
#define yy__mod 343
#define yy__slash 344
#define yy__backslash 345
#define yy__else 346
#define yy__operation_inline 347
#define yy__p_operation_inline 348
#define yy__succeeds 349
#define yy__succeeds_as 350
#define yy__connect_to_file 351
#define yy__RStream 352
#define yy__WStream 353
#define yy__RWStream 354
#define yy__C_constr_for 355
#define yy__connect_to_IP 356
#define yy__GAddr 357
#define yy__Var 358
#define yy__MVar 359
#define yy__StructPtr 360
#define yy__debug_avm 361
#define yy__terminal 362
#define yy__avm 363
#define yy__left_shift 364
#define yy__right_shift 365
#define yy__since 366
#define yy__p_operation 367
#define yy__p_type 368
#define yy__p_variable 369
#define yy__protect 370
#define yy__lock 371
#define yy__alt_number 372
#define yy__config_file 373
#define yy__verbose 374
#define yy__stop_after 375
#define yy__mapsto 376
#define yy__rec_mapsto 377
#define yy__language 378
#define yy__mapstoo 379
#define yy__rec_mapstoo 380
#define yy__arroww 381
#define yy__conf_int 382
#define yy__conf_string 383
#define yy__conf_symbol 384
#define yy__we_have 385
#define yy__enough 386
#define yy__let 387
#define yy__assume 388
#define yy__indeed 389
#define yy__hence 390
#define yy__enddot 391
#define yy__eof 392
#define yy__integer 393
#define yy__macro_integer 394
#define yy__dummy 395
#define yy__char 396
#define prec_symbol 397
#define prec_par_term 398
#define prec_of_type 399
#define prec_sym_type 400
#define unaryminus 401
#define yy__high 402




/* Copy the first part of user declarations.  */
#line 18 "Anubis.y"

#include <stdlib.h>
#define YYINCLUDED_STDLIB_H			// To avoid some unwanted warnings 
#define YYSTYPE symbol_t   

#ifdef DEBUG
//#define YYDEBUG 1
#endif

#include "ParserEngine.h"
#include "Anubis.tab.hpp"  // to have a YYLTYPE definition

using namespace SoftArchitect::AnubisParser;
using namespace ICSharpCode::Core;

static int yylex (YYSTYPE* yylval, YYLTYPE* yylloc, ParserEngine^ parser);



static void yyerror(YYLTYPE* yylloc, ParserEngine^ parser, char *);
//static char * synterrmsg = "";    

static void print_token_value (FILE *, int, YYSTYPE);
#define YYPRINT(file, type, value) print_token_value (file, type, value)




/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
typedef int YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


/* Copy the second part of user declarations.  */


/* Line 219 of yacc.c.  */
#line 430 "Anubis.tab.cpp"

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T) && (defined (__STDC__) || defined (__cplusplus))
# include <stddef.h> /* INFRINGES ON USER NAME SPACE */
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if defined (__STDC__) || defined (__cplusplus)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     define YYINCLUDED_STDLIB_H
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2005 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM ((YYSIZE_T) -1)
#  endif
#  ifdef __cplusplus
extern "C" {
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if (! defined (malloc) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if (! defined (free) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifdef __cplusplus
}
#  endif
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYLTYPE_IS_TRIVIAL) && YYLTYPE_IS_TRIVIAL \
             && defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
    YYLTYPE yyls;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   6266

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  148
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  53
/* YYNRULES -- Number of rules. */
#define YYNRULES  329
/* YYNRULES -- Number of states. */
#define YYNSTATES  708

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   402

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short int yyprhs[] =
{
       0,     0,     3,     5,     7,     8,    11,    13,    15,    17,
      20,    22,    24,    26,    28,    30,    32,    34,    37,    40,
      43,    49,    51,    53,    55,    57,    59,    61,    63,    65,
      67,    69,    71,    77,    86,    91,    99,   101,   105,   107,
     109,   111,   114,   118,   122,   124,   126,   129,   134,   140,
     144,   148,   152,   156,   160,   164,   168,   172,   176,   180,
     184,   188,   192,   196,   200,   204,   208,   212,   216,   221,
     225,   229,   233,   237,   241,   245,   249,   253,   256,   259,
     261,   265,   268,   270,   274,   276,   278,   280,   282,   284,
     286,   288,   292,   294,   296,   298,   303,   308,   313,   318,
     323,   327,   333,   338,   342,   346,   349,   351,   355,   357,
     360,   364,   369,   373,   377,   384,   386,   388,   390,   392,
     394,   396,   398,   400,   402,   404,   406,   408,   410,   412,
     414,   416,   418,   420,   422,   424,   426,   428,   430,   432,
     434,   436,   438,   440,   442,   444,   451,   461,   470,   477,
     488,   497,   505,   511,   521,   529,   537,   542,   543,   545,
     547,   551,   554,   556,   560,   563,   565,   570,   573,   578,
     581,   586,   589,   591,   594,   596,   598,   600,   602,   606,
     610,   614,   619,   625,   631,   633,   635,   638,   641,   645,
     649,   653,   658,   663,   668,   673,   678,   685,   691,   699,
     701,   708,   717,   722,   730,   736,   742,   749,   756,   763,
     770,   777,   781,   785,   787,   793,   799,   805,   807,   810,
     814,   819,   824,   828,   832,   836,   840,   844,   848,   852,
     856,   860,   864,   868,   872,   876,   880,   884,   888,   892,
     896,   900,   904,   908,   912,   916,   920,   924,   928,   932,
     936,   940,   944,   948,   952,   956,   960,   965,   968,   971,
     974,   975,   977,   979,   983,   990,   997,  1003,  1008,  1015,
    1022,  1029,  1034,  1035,  1038,  1042,  1046,  1048,  1050,  1053,
    1057,  1062,  1065,  1069,  1073,  1077,  1081,  1085,  1089,  1093,
    1097,  1101,  1105,  1109,  1113,  1117,  1121,  1125,  1129,  1133,
    1137,  1141,  1146,  1150,  1154,  1158,  1162,  1166,  1170,  1174,
    1178,  1181,  1185,  1188,  1192,  1197,  1199,  1202,  1204,  1208,
    1212,  1216,  1217,  1220,  1222,  1226,  1228,  1232,  1235,  1238
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short int yyrhs[] =
{
     149,     0,    -1,   150,    -1,   151,    -1,    -1,   151,   154,
      -1,    22,    -1,   136,    -1,   136,    -1,    22,   137,    -1,
     155,    -1,     1,    -1,   161,    -1,   175,    -1,   176,    -1,
     172,    -1,   156,    -1,    15,    46,    -1,    16,    46,    -1,
       1,   153,    -1,   100,    19,    33,   168,   153,    -1,     7,
      -1,   112,    -1,    92,    -1,    93,    -1,    75,    -1,     8,
      -1,   114,    -1,     6,    -1,   113,    -1,    66,    -1,    67,
      -1,   160,    19,    33,   168,   153,    -1,   160,    19,    20,
     162,    39,    33,   168,   153,    -1,   159,    19,    40,   163,
      -1,   159,    19,    20,   162,    39,    40,   163,    -1,     9,
      -1,     9,    24,   162,    -1,   153,    -1,    74,    -1,   164,
      -1,   165,   153,    -1,   165,    24,    74,    -1,   165,    24,
     164,    -1,    18,    -1,    45,    -1,    21,    23,    -1,    18,
      20,   166,    39,    -1,    21,   167,   152,   167,    23,    -1,
     167,    27,   167,    -1,   167,    22,   167,    -1,   167,    69,
     167,    -1,   167,    70,   167,    -1,   167,    29,   167,    -1,
     167,    35,   167,    -1,   167,    30,   167,    -1,   167,    32,
     167,    -1,   167,    31,   167,    -1,   167,    40,   167,    -1,
     167,    50,   167,    -1,   167,    33,   167,    -1,   167,    41,
     167,    -1,   167,   109,   167,    -1,   167,   110,   167,    -1,
     167,    28,   167,    -1,   167,    36,   167,    -1,   167,    89,
     167,    -1,   167,    90,   167,    -1,   167,    88,   167,    39,
      -1,   167,    76,   167,    -1,   167,    80,   167,    -1,   167,
      84,   167,    -1,   167,    59,   167,    -1,   167,    71,   167,
      -1,   167,    78,   167,    -1,   167,    82,   167,    -1,   167,
      86,   167,    -1,    64,   167,    -1,    29,   167,    -1,   167,
      -1,   167,    24,   166,    -1,   168,    18,    -1,   168,    -1,
      20,   168,    39,    -1,    19,    -1,     9,    -1,    47,    -1,
      48,    -1,    61,    -1,    53,    -1,    54,    -1,    42,     9,
     168,    -1,    97,    -1,    98,    -1,    99,    -1,   102,    20,
     168,    39,    -1,   103,    20,   168,    39,    -1,   104,    20,
     168,    39,    -1,    19,    20,   169,    39,    -1,   105,    20,
      19,    39,    -1,   168,    50,   168,    -1,    19,    20,   169,
      72,   168,    -1,    20,   170,    72,   168,    -1,    20,   171,
      39,    -1,    38,   168,    37,    -1,     1,   153,    -1,   168,
      -1,   168,    24,   169,    -1,   168,    -1,   168,    18,    -1,
     168,    24,   170,    -1,   168,    18,    24,   170,    -1,   168,
      24,   168,    -1,   168,    24,   171,    -1,   158,   168,    18,
      33,   182,   153,    -1,    27,    -1,    22,    -1,    69,    -1,
      70,    -1,    29,    -1,    35,    -1,    30,    -1,    32,    -1,
      31,    -1,    40,    -1,    50,    -1,    33,    -1,    41,    -1,
     109,    -1,   110,    -1,    28,    -1,    36,    -1,    89,    -1,
      90,    -1,    76,    -1,    80,    -1,    84,    -1,    59,    -1,
      71,    -1,    78,    -1,    82,    -1,    86,    -1,    27,    -1,
      28,    -1,    64,    -1,   157,   168,    18,    33,   182,   153,
      -1,   157,   168,    18,    20,   177,    39,    33,   182,   153,
      -1,   157,   168,   179,   173,   179,    33,   182,   153,    -1,
     157,   168,   179,   173,   179,   153,    -1,   157,   168,    21,
     179,   152,   179,    23,    33,   182,   153,    -1,   157,   168,
      21,   179,   152,   179,    23,   153,    -1,   157,   168,   174,
     179,    33,   182,   153,    -1,   157,   168,   174,   179,   153,
      -1,   157,   168,   179,    88,   179,    39,    33,   182,   153,
      -1,   157,   168,   179,    88,   179,    39,   153,    -1,   157,
     168,    18,    20,   177,    39,   153,    -1,   157,   168,    18,
     153,    -1,    -1,   178,    -1,   179,    -1,   179,    24,   178,
      -1,   168,    18,    -1,   181,    -1,   181,    24,   180,    -1,
     168,    18,    -1,    11,    -1,   117,    20,   182,    39,    -1,
     115,   182,    -1,   116,   182,    24,   182,    -1,   106,   182,
      -1,   108,    38,   197,    37,    -1,   107,   182,    -1,    18,
      -1,    20,    39,    -1,   138,    -1,   139,    -1,   141,    -1,
      10,    -1,    42,     9,   182,    -1,    20,   182,    39,    -1,
      20,   183,    39,    -1,    20,   168,    39,   182,    -1,    20,
      40,   182,    39,   182,    -1,    20,    40,   168,    39,   182,
      -1,   186,    -1,   189,    -1,    21,   185,    -1,    29,   182,
      -1,   182,    34,   182,    -1,   182,    17,   182,    -1,   182,
      26,   182,    -1,    60,    20,   182,    39,    -1,    56,    20,
     182,    39,    -1,    57,    20,   182,    39,    -1,    25,    20,
     168,    39,    -1,    55,    20,   168,    39,    -1,    58,    20,
     182,    24,   182,    39,    -1,    20,   168,    39,    96,   182,
      -1,    20,   168,    39,   101,   182,    40,   182,    -1,    46,
      -1,    52,    18,    33,   182,    24,   184,    -1,    68,   182,
     200,    24,    62,   182,     5,   182,    -1,    63,   182,    24,
     182,    -1,    38,    18,    40,   168,    24,   182,    37,    -1,
      20,   180,    39,   121,   182,    -1,    20,   180,    39,   122,
     182,    -1,    42,    18,    40,   168,    24,   182,    -1,    42,
      18,    40,   182,    24,   182,    -1,    43,    18,    40,   168,
      24,   182,    -1,    44,    18,    40,   168,    24,   182,    -1,
      49,    18,    40,   168,    24,   182,    -1,   182,    24,   182,
      -1,   182,    24,   183,    -1,   182,    -1,    18,    33,   182,
      24,   184,    -1,   173,    33,   182,    24,   184,    -1,    64,
      33,   182,    24,   184,    -1,    23,    -1,   182,    23,    -1,
     182,    24,   185,    -1,   182,   152,   182,    23,    -1,   182,
      20,   187,    39,    -1,   182,    21,   185,    -1,   182,    27,
     182,    -1,   182,    22,   182,    -1,   182,    69,   182,    -1,
     182,    70,   182,    -1,   182,    29,   182,    -1,   182,    35,
     182,    -1,   182,    30,   182,    -1,   182,    32,   182,    -1,
     182,    31,   182,    -1,   182,    40,   182,    -1,   182,    50,
     182,    -1,   182,    33,   182,    -1,   182,    41,   182,    -1,
     182,   109,   182,    -1,   182,   110,   182,    -1,   182,    28,
     182,    -1,   182,    36,   182,    -1,   182,    89,   182,    -1,
     182,    90,   182,    -1,   182,    76,   182,    -1,   182,    80,
     182,    -1,   182,    84,   182,    -1,   182,    59,   182,    -1,
     182,    71,   182,    -1,   182,    77,   182,    -1,   182,    81,
     182,    -1,   182,    85,   182,    -1,   182,    78,   182,    -1,
     182,    82,   182,    -1,   182,    86,   182,    -1,   182,    79,
     182,    -1,   182,    83,   182,    -1,   182,    87,   182,    -1,
     182,    88,   182,    39,    -1,    27,   182,    -1,    28,   182,
      -1,    64,   182,    -1,    -1,   188,    -1,   182,    -1,   182,
      24,   188,    -1,     3,   182,     4,    38,   190,    37,    -1,
       3,   182,    95,    18,     5,   182,    -1,     3,   182,    94,
       5,   182,    -1,     3,   182,     4,   191,    -1,   111,   182,
       4,   192,    24,   182,    -1,     3,   182,     5,   182,    91,
     182,    -1,     3,   182,     4,   191,    91,   182,    -1,     3,
     182,     5,   182,    -1,    -1,   191,   190,    -1,   191,    24,
     190,    -1,   192,     5,   182,    -1,    18,    -1,    45,    -1,
      21,    23,    -1,    18,    20,    39,    -1,    18,    20,   195,
      39,    -1,    21,   193,    -1,   194,    27,   194,    -1,   194,
      22,   194,    -1,   194,    69,   194,    -1,   194,    70,   194,
      -1,   194,    29,   194,    -1,   194,    35,   194,    -1,   194,
      30,   194,    -1,   194,    32,   194,    -1,   194,    31,   194,
      -1,   194,    40,   194,    -1,   194,    50,   194,    -1,   194,
      33,   194,    -1,   194,    41,   194,    -1,   194,   109,   194,
      -1,   194,   110,   194,    -1,   194,    28,   194,    -1,   194,
      36,   194,    -1,   194,    89,   194,    -1,   194,    90,   194,
      -1,   194,    88,   194,    39,    -1,   194,    76,   194,    -1,
     194,    80,   194,    -1,   194,    84,   194,    -1,   194,    59,
     194,    -1,   194,    71,   194,    -1,   194,    78,   194,    -1,
     194,    82,   194,    -1,   194,    86,   194,    -1,    64,   194,
      -1,    20,   196,    39,    -1,   194,    23,    -1,   194,    24,
     193,    -1,   194,   152,   194,    23,    -1,   192,    -1,   168,
      18,    -1,   194,    -1,   194,    24,   195,    -1,   194,    24,
     194,    -1,   194,    24,   196,    -1,    -1,   198,   197,    -1,
      18,    -1,    20,    18,   199,    -1,    39,    -1,   152,   138,
      39,    -1,   138,   199,    -1,    46,   199,    -1,    18,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,   170,   170,   173,   176,   177,   181,   182,   185,   186,
     191,   192,   200,   201,   202,   203,   204,   205,   206,   207,
     211,   215,   216,   217,   218,   219,   223,   224,   227,   228,
     231,   232,   237,   238,   240,   241,   245,   246,   255,   256,
     257,   261,   262,   263,   267,   268,   269,   270,   271,   272,
     273,   274,   275,   276,   277,   278,   279,   280,   281,   282,
     283,   284,   285,   286,   287,   288,   289,   290,   291,   292,
     293,   294,   295,   296,   297,   298,   299,   300,   301,   305,
     306,   310,   311,   322,   323,   324,   325,   326,   327,   328,
     329,   330,   332,   333,   334,   336,   337,   338,   339,   340,
     341,   345,   349,   353,   354,   356,   360,   361,   365,   368,
     371,   374,   380,   384,   396,   405,   406,   407,   408,   409,
     410,   411,   412,   413,   414,   415,   416,   417,   418,   419,
     420,   421,   422,   423,   424,   425,   426,   427,   428,   429,
     430,   431,   434,   435,   436,   440,   443,   448,   454,   457,
     463,   466,   471,   474,   480,   486,   487,   498,   499,   502,
     505,   511,   523,   524,   527,   533,   534,   535,   536,   537,
     538,   539,   540,   542,   543,   544,   545,   546,   547,   548,
     549,   550,   551,   552,   553,   554,   556,   557,   559,   560,
     561,   562,   563,   564,   565,   566,   567,   568,   569,   570,
     571,   572,   574,   575,   576,   577,   578,   579,   580,   581,
     582,   586,   587,   593,   594,   595,   596,   602,   603,   604,
     605,   612,   613,   614,   615,   616,   617,   618,   619,   620,
     621,   622,   623,   624,   625,   626,   627,   628,   629,   630,
     631,   632,   633,   634,   635,   636,   637,   638,   639,   640,
     641,   642,   643,   644,   645,   646,   647,   648,   649,   650,
     653,   654,   657,   658,   665,   666,   667,   668,   669,   670,
     671,   672,   685,   686,   687,   690,   693,   694,   695,   697,
     698,   700,   701,   702,   703,   704,   705,   706,   707,   708,
     709,   710,   711,   712,   713,   714,   715,   716,   717,   718,
     719,   720,   721,   722,   723,   724,   725,   726,   727,   728,
     729,   730,   734,   735,   736,   746,   747,   756,   757,   760,
     761,   766,   767,   770,   771,   774,   775,   776,   777,   783
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "yy__if", "yy__is", "yy__then",
  "yy__type", "yy__operation", "yy__variable", "yy__utvar", "yy__float",
  "yy__alert", "yy__theorem", "yy__p_theorem", "yy__proof", "yy__read",
  "yy__replaced_by", "yy__exchange", "yy__symbol", "yy__Symbol",
  "yy__lpar", "yy__lbracket", "yy__dot", "yy__rbracket", "yy__comma",
  "yy__bit_width", "yy__semicolon", "yy__plus", "yy__minus", "yy__star",
  "yy__carret", "yy__ampersand", "yy__vbar", "yy__equals", "yy__write",
  "yy__percent", "yy__abs_minus", "yy__rbrace", "yy__lbrace", "yy__rpar",
  "yy__colon", "yy__implies", "yy__forall", "yy__exists",
  "yy__exists_unique", "yy__decimal_digit", "yy__anb_string",
  "yy__type_String", "yy__type_ByteArray", "yy__description", "yy__arrow",
  "yy__ndarrow", "yy__with", "yy__type_Float", "yy__type_Listener",
  "yy__indirect", "yy__serialize", "yy__unserialize", "yy__vcopy",
  "yy__non_equal", "yy__load_module", "yy__type_Int", "yy__wait_for",
  "yy__delegate", "yy__tilde", "yy__type_Omega", "yy__type_alias",
  "yy__p_type_alias", "yy__checking_every", "yy__dotdot", "yy__dotsup",
  "yy__exclam_equal", "yy__rpar_arrow", "yy__rpar_ndarrow", "yy__dots",
  "yy__g_operation", "yy__less", "yy__greater", "yy__lessoreq",
  "yy__greateroreq", "yy__sless", "yy__sgreater", "yy__slessoreq",
  "yy__sgreateroreq", "yy__uless", "yy__ugreater", "yy__ulessoreq",
  "yy__ugreateroreq", "yy__mod", "yy__slash", "yy__backslash", "yy__else",
  "yy__operation_inline", "yy__p_operation_inline", "yy__succeeds",
  "yy__succeeds_as", "yy__connect_to_file", "yy__RStream", "yy__WStream",
  "yy__RWStream", "yy__C_constr_for", "yy__connect_to_IP", "yy__GAddr",
  "yy__Var", "yy__MVar", "yy__StructPtr", "yy__debug_avm", "yy__terminal",
  "yy__avm", "yy__left_shift", "yy__right_shift", "yy__since",
  "yy__p_operation", "yy__p_type", "yy__p_variable", "yy__protect",
  "yy__lock", "yy__alt_number", "yy__config_file", "yy__verbose",
  "yy__stop_after", "yy__mapsto", "yy__rec_mapsto", "yy__language",
  "yy__mapstoo", "yy__rec_mapstoo", "yy__arroww", "yy__conf_int",
  "yy__conf_string", "yy__conf_symbol", "yy__we_have", "yy__enough",
  "yy__let", "yy__assume", "yy__indeed", "yy__hence", "yy__enddot",
  "yy__eof", "yy__integer", "yy__macro_integer", "yy__dummy", "yy__char",
  "prec_symbol", "prec_par_term", "prec_of_type", "prec_sym_type",
  "unaryminus", "yy__high", "$accept", "Start", "Text", "Paragraphs",
  "Dot", "EndDot", "Paragraph", "Par", "C_constr", "OpKW", "VarKW",
  "TypeKW", "TypeAliasKW", "TypeDefinition", "TypeVars1", "Alternatives",
  "Alternatives1", "Alternative", "AltOperands1", "AltOperand", "Type",
  "Types1", "TypesArgs1", "Types2", "VariableDeclaration",
  "SimpleBinaryOp", "SimpleUnaryOp", "OperationDefinition",
  "OperationDeclaration", "OpArgs", "OpArgs1", "OpArg", "FArgs1", "FArg",
  "Term", "Terms2", "WithTerm", "List", "AppTerm", "Terms", "Terms1",
  "Conditional", "Clauses", "Clause", "Head", "ListHead", "ResurSym",
  "ResurSym1", "ResurSym2", "AVM", "AVM_Instr", "IntMCons",
  "yy__milliseconds", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,   148,   149,   150,   151,   151,   152,   152,   153,   153,
     154,   154,   155,   155,   155,   155,   155,   155,   155,   155,
     156,   157,   157,   157,   157,   157,   158,   158,   159,   159,
     160,   160,   161,   161,   161,   161,   162,   162,   163,   163,
     163,   164,   164,   164,   165,   165,   165,   165,   165,   165,
     165,   165,   165,   165,   165,   165,   165,   165,   165,   165,
     165,   165,   165,   165,   165,   165,   165,   165,   165,   165,
     165,   165,   165,   165,   165,   165,   165,   165,   165,   166,
     166,   167,   167,   168,   168,   168,   168,   168,   168,   168,
     168,   168,   168,   168,   168,   168,   168,   168,   168,   168,
     168,   168,   168,   168,   168,   168,   169,   169,   170,   170,
     170,   170,   171,   171,   172,   173,   173,   173,   173,   173,
     173,   173,   173,   173,   173,   173,   173,   173,   173,   173,
     173,   173,   173,   173,   173,   173,   173,   173,   173,   173,
     173,   173,   174,   174,   174,   175,   175,   175,   175,   175,
     175,   175,   175,   175,   175,   176,   176,   177,   177,   178,
     178,   179,   180,   180,   181,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
     182,   183,   183,   184,   184,   184,   184,   185,   185,   185,
     185,   186,   186,   186,   186,   186,   186,   186,   186,   186,
     186,   186,   186,   186,   186,   186,   186,   186,   186,   186,
     186,   186,   186,   186,   186,   186,   186,   186,   186,   186,
     186,   186,   186,   186,   186,   186,   186,   186,   186,   186,
     187,   187,   188,   188,   189,   189,   189,   189,   189,   189,
     189,   189,   190,   190,   190,   191,   192,   192,   192,   192,
     192,   192,   192,   192,   192,   192,   192,   192,   192,   192,
     192,   192,   192,   192,   192,   192,   192,   192,   192,   192,
     192,   192,   192,   192,   192,   192,   192,   192,   192,   192,
     192,   192,   193,   193,   193,   194,   194,   195,   195,   196,
     196,   197,   197,   198,   198,   199,   199,   199,   199,   200
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     1,     0,     2,     1,     1,     1,     2,
       1,     1,     1,     1,     1,     1,     1,     2,     2,     2,
       5,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     5,     8,     4,     7,     1,     3,     1,     1,
       1,     2,     3,     3,     1,     1,     2,     4,     5,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     4,     3,
       3,     3,     3,     3,     3,     3,     3,     2,     2,     1,
       3,     2,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     3,     1,     1,     1,     4,     4,     4,     4,     4,
       3,     5,     4,     3,     3,     2,     1,     3,     1,     2,
       3,     4,     3,     3,     6,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     6,     9,     8,     6,    10,
       8,     7,     5,     9,     7,     7,     4,     0,     1,     1,
       3,     2,     1,     3,     2,     1,     4,     2,     4,     2,
       4,     2,     1,     2,     1,     1,     1,     1,     3,     3,
       3,     4,     5,     5,     1,     1,     2,     2,     3,     3,
       3,     4,     4,     4,     4,     4,     6,     5,     7,     1,
       6,     8,     4,     7,     5,     5,     6,     6,     6,     6,
       6,     3,     3,     1,     5,     5,     5,     1,     2,     3,
       4,     4,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     4,     2,     2,     2,
       0,     1,     1,     3,     6,     6,     5,     4,     6,     6,
       6,     4,     0,     2,     3,     3,     1,     1,     2,     3,
       4,     2,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     4,     3,     3,     3,     3,     3,     3,     3,     3,
       2,     3,     2,     3,     4,     1,     2,     1,     3,     3,
       3,     0,     2,     1,     3,     1,     3,     2,     2,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned short int yydefact[] =
{
       4,     0,     2,     0,     1,    11,    28,    21,    26,     0,
       0,    30,    31,    25,    23,    24,     0,    22,    29,    27,
       5,    10,    16,     0,     0,     0,     0,    12,    15,    13,
      14,     0,     8,    19,    17,    18,     0,     0,    85,    84,
       0,     0,     0,    86,    87,    89,    90,    88,    92,    93,
      94,     0,     0,     0,     0,     0,     0,     0,     0,     9,
       0,   105,     0,   108,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   142,   143,     0,   144,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   106,     0,   109,
       0,    83,     0,   103,   104,    91,     0,     0,     0,     0,
       0,     0,   156,     0,   100,   161,     0,   116,   115,   130,
     119,   121,   123,   122,   126,   120,   131,   124,   127,   125,
     137,   117,   118,   138,   134,   139,   135,   140,   136,   141,
       0,   132,   133,   128,   129,     0,     0,    36,     0,    44,
       0,     0,    45,     0,    39,    38,    34,    40,     0,     0,
      82,     0,     0,    20,     0,    98,     0,     0,   108,   110,
     113,   102,    95,    96,    97,    99,     0,   158,   159,     0,
     177,   165,   172,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   199,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   174,   175,   176,     0,   184,   185,     6,     7,     0,
       0,   152,     0,     0,     0,     0,     0,     0,    46,     0,
      78,    77,     0,    41,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    81,     0,    32,   107,   101,   108,   111,     0,
       0,     0,     0,     0,   173,     0,     0,     0,     0,   162,
       0,     0,   217,     0,   186,     0,   257,   258,   187,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   259,     0,   169,   171,   321,     0,   167,     0,
       0,     0,   260,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     145,     0,     0,     0,     0,   148,   114,    37,     0,     0,
      79,     0,    42,    43,    50,    49,    64,    53,    55,    57,
      56,    60,    54,    65,    58,    61,    59,    72,    51,    52,
      73,    69,    74,    70,    75,    71,    76,     0,    66,    67,
      62,    63,     0,     0,     0,   155,   160,     0,     0,     0,
       0,     0,   108,     0,     0,     0,   164,     0,     0,     0,
       0,   179,   180,     6,   218,     0,     0,     0,     0,   178,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   329,     0,   323,     0,     0,   321,     0,     0,     0,
     189,   262,     0,   261,   222,   224,   190,   223,   238,   227,
     229,   231,   230,   234,   188,   228,   239,   232,   235,   233,
     245,   225,   226,   246,   242,   247,   250,   253,   243,   248,
     251,   254,   244,   249,   252,   255,     0,   240,   241,   236,
     237,     0,   151,     0,   154,     0,    35,    47,     0,     0,
      68,     0,     0,   276,     0,     0,     0,   277,     0,     0,
     267,   315,     0,   271,     0,     0,   164,    83,     0,     0,
       0,     0,   181,     0,     0,     0,   163,   211,   212,   219,
       0,   194,     0,     0,     0,     0,     0,     0,     0,   195,
     192,   193,     0,   191,   202,     0,     0,   170,   322,   315,
     168,   166,     0,   221,   256,     0,   150,     0,   147,    80,
      48,    33,   146,     0,   108,   315,     0,     0,   278,   281,
       0,     0,     0,     0,   310,   316,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   266,     0,   183,
     182,   197,     0,   204,   205,   220,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   325,     0,     0,     0,   324,
       0,   263,     0,   153,   279,   317,     0,   316,     0,   311,
       6,   312,     0,     0,   264,     0,   273,   270,   275,   283,
     282,   297,   286,   288,   290,   289,   293,   287,   298,   291,
     294,   292,   305,   284,   285,   306,   302,   307,   303,   308,
     304,   309,     0,   299,   300,   295,   296,   269,   265,     0,
       0,   206,   207,   208,   209,   210,   172,   115,   130,   119,
       0,     0,   213,   200,   196,     0,   328,   327,     0,   268,
     149,     0,   280,   319,   320,   313,     0,   274,   301,   198,
     203,     0,     0,     0,     0,   326,   318,   314,     0,     0,
       0,   201,     0,     0,     0,   214,   216,   215
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short int yydefgoto[] =
{
      -1,     1,     2,     3,   608,   145,    20,    21,    22,    23,
      24,    25,    26,    27,   138,   146,   147,   148,   349,   149,
     489,    88,    64,    65,    28,   671,    79,    29,    30,   166,
     167,   168,   268,   269,   672,   271,   673,   274,   205,   432,
     433,   206,   552,   553,   545,   549,   492,   616,   547,   425,
     426,   609,   422
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -538
static const short int yypact[] =
{
    -538,    17,  -538,    91,  -538,    -7,  -538,  -538,  -538,   -15,
      32,  -538,  -538,  -538,  -538,  -538,    47,  -538,  -538,  -538,
    -538,  -538,  -538,  3047,  3047,    86,    93,  -538,  -538,  -538,
    -538,   -98,  -538,  -538,  -538,  -538,    82,    -7,  -538,   119,
    3047,  3047,   153,  -538,  -538,  -538,  -538,  -538,  -538,  -538,
    -538,   158,   182,   194,   196,  2478,    10,    42,   140,  -538,
    3047,  -538,  3047,    33,   146,   197,   174,  3047,  3047,  3047,
    3047,   193,   -13,  3047,  -538,  -538,  3047,  -538,    19,  3047,
    5786,   210,   316,  1869,   316,  3047,   -14,    29,    -9,   296,
    3047,  -538,  3047,  -538,  -538,   276,   129,   178,   192,   288,
    2908,  1793,  -538,    -4,   276,  -538,   -12,  -538,  -538,  -538,
    -538,  -538,  -538,  -538,  -538,  -538,  -538,  -538,  -538,  -538,
    -538,  -538,  -538,  -538,  -538,  -538,  -538,  -538,  -538,  -538,
    3047,  -538,  -538,  -538,  -538,  3047,  1793,   304,   291,   312,
    2976,  3047,  -538,  3047,  -538,  -538,  -538,  -538,   -11,  5851,
      34,   292,   -14,  -538,  3047,  -538,  3047,  3047,   103,  -538,
    -538,   276,  -538,  -538,  -538,  -538,   294,  -538,   310,  1793,
    -538,  -538,  -538,   942,  1385,   320,  1793,  1793,  1793,   324,
     102,   325,   326,  -538,   329,   330,   331,   333,   344,   345,
     346,  1793,  1793,  1793,  1793,  1793,   332,  1793,  1793,  1793,
     348,  -538,  -538,  -538,  2043,  -538,  -538,  -538,  -538,  3047,
    1793,  -538,   334,    -8,  2043,   316,   309,  3047,  -538,    -4,
    -538,  -538,  2549,  -538,  3047,  3047,  3047,  3047,  3047,  3047,
    3047,  3047,  3047,  3047,  3047,  3047,  3047,  3047,  3047,  3047,
    3047,  3047,  3047,  3047,  3047,  3047,  3047,  3047,  3047,  3047,
    3047,  3047,  -538,   336,  -538,  -538,   276,    95,  -538,     0,
    3047,  2205,   942,  3007,  -538,  1066,   230,    26,   335,   356,
    3097,   343,  -538,  1958,  -538,  3047,    25,    25,   289,   347,
    1793,   377,   378,   379,   380,   339,  3047,  1793,  1793,  1793,
    1793,  3188,   640,  3273,  4942,  4942,    41,  2299,  4942,  3364,
    1793,  1793,  1793,  1385,  1487,  1793,  1793,  1793,  1793,  1793,
    1793,  1793,  1793,  1793,  1793,  1793,  1793,  1793,  1793,  1793,
    1793,  1793,  1793,  1793,  1793,  1793,  1793,  1793,  1793,  1793,
    1793,  1793,  1793,  1793,  1793,  1793,  1793,  1793,  1793,  1793,
    -538,   360,  2043,     2,  1793,  -538,  -538,  -538,  1869,   382,
     362,  3047,  -538,  -538,  -538,  -538,  -538,  -538,  -538,  -538,
    -538,  -538,  -538,  -538,  -538,  -538,  -538,  -538,  -538,  -538,
    -538,  -538,  -538,  -538,  -538,  -538,  -538,   383,  -538,  -538,
    -538,  -538,  3047,  3047,  1793,  -538,  -538,  2816,  1793,  1793,
     418,   366,   122,   201,  3449,  1066,  -538,  1283,   134,  3047,
    1793,  -538,  -538,   200,  -538,  1385,  1793,   267,  3047,  4942,
    1066,  3047,  3047,  3047,  1793,   273,  3534,  3619,  3710,  3795,
    1793,  -538,   401,  -538,   408,   390,    41,  2884,  1793,  3880,
     640,  3971,   389,  -538,  -538,  -538,  5197,   160,   289,   289,
     156,  5365,  5281,   640,   640,   412,   289,  4942,  5443,    25,
     640,  -538,  -538,   640,   640,   640,   640,   640,   640,   640,
     640,   640,   640,   640,   640,   640,  4056,   156,   156,  5443,
    5443,     5,  -538,  1793,  -538,  2043,  -538,  -538,  3047,   413,
    -538,   -14,  2043,   411,  2884,  2686,  2717,  -538,  2884,    46,
     349,   430,  5916,  5027,  1793,   432,   367,  1589,  1793,  1793,
    1793,  1793,  5281,  1793,  1793,    50,  -538,  4147,  -538,  -538,
    4232,  -538,    53,   120,  4323,   150,   173,   177,  4414,  -538,
    -538,  -538,  1793,  -538,  4942,   376,   -20,  -538,  -538,   417,
    4942,  -538,  1793,  -538,  -538,  1793,  -538,  2043,  -538,  -538,
    -538,  -538,  -538,  2785,   311,  -538,  5514,   404,  -538,  -538,
    2114,   157,   409,  2618,  6176,  -538,  1793,  1793,  2884,  2884,
    2884,  2884,  2884,  2884,  2884,  2884,  2884,  2884,  2884,  2884,
    2884,  2884,  2884,  2884,  2884,  2884,  2884,  2884,  2884,  2884,
    2884,  2884,  2884,  2884,  2884,  2884,  1793,  5197,  1793,  5281,
    5281,    20,  5112,  4942,  4942,  -538,  1793,  1793,  1793,  1793,
    1793,  1793,  1181,  4499,  1793,  -538,   -20,   -20,   307,  -538,
    1793,  -538,  2043,  -538,  -538,  5585,   410,   367,  2884,  -538,
      66,  -538,  2884,  2884,  -538,  2717,  -538,  5197,  5197,     4,
     286,   163,   163,     4,  6046,  5981,  6176,   394,   163,  5916,
    6111,   165,  6176,  -538,  -538,  6176,  6176,  6176,  6176,  6176,
    6176,  6176,  5650,     4,     4,  6111,  6111,  5197,  5197,  1793,
    4584,  4942,  4942,  4942,  4942,  4942,   419,  1793,  1793,  1793,
    1691,   420,  4942,  -538,  -538,  2385,  -538,  -538,   426,  4942,
    -538,  2884,  -538,  5514,  -538,  -538,  5721,  -538,  -538,  4942,
    -538,  1793,  1793,  1793,  1793,  -538,  -538,  -538,  4675,  4766,
    4857,  5197,  1181,  1181,  1181,  -538,  -538,  -538
};

/* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
    -538,  -538,  -538,  -538,  -102,    -5,  -538,  -538,  -538,  -538,
    -538,  -538,  -538,  -538,   -78,   111,   228,  -538,   -10,   164,
     523,   313,   -86,   381,  -538,   392,  -538,  -538,  -538,  -538,
     209,   -50,    74,  -538,   -43,    75,  -404,  -291,  -538,  -538,
     -58,  -538,  -537,    98,  -384,  -144,   258,  -201,  -132,    61,
    -538,  -304,  -538
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -273
static const short int yytable[] =
{
      33,   209,   207,   491,   159,    80,   151,   100,    31,    31,
      31,    31,   434,   222,    31,    31,   626,     4,   207,   605,
     101,   210,    31,   103,    31,   344,   606,    31,    81,   106,
     155,    34,    61,   384,   562,   473,    76,   105,   535,    59,
     302,   303,   389,   529,   396,   302,   303,   306,   307,   308,
     309,    89,   252,   154,   570,   314,   315,    90,   204,   423,
      76,   424,    82,   156,   555,   397,    36,   102,   396,    76,
     318,   258,    91,   572,   573,   318,    76,   596,    35,    76,
     212,   153,    83,    76,    76,   213,   484,   485,   687,   320,
     321,    -3,     5,   214,   320,   321,    76,     6,     7,     8,
      76,   211,   491,    76,    41,    57,     9,    10,   335,   336,
     337,   280,    58,    89,   509,    60,   208,   351,   607,   383,
     281,    89,    32,    32,    32,    32,   261,    90,    32,    32,
     270,   273,   208,   276,   277,   278,    32,   347,    32,    62,
     496,    32,  -112,   223,   597,    76,    90,   254,   291,   292,
     293,   294,   295,    76,   297,   298,   299,    11,    12,   341,
      84,   497,    67,    48,    49,    50,    13,   342,   162,   491,
      76,   406,    76,    85,   599,   555,   302,   303,    68,    76,
     302,   303,   389,    14,    15,   558,   309,   306,   307,   308,
     309,    16,   561,   562,    94,   314,   315,   600,   566,   340,
      76,   601,    69,    17,    18,    19,   318,    76,   345,   346,
     318,    94,    99,   570,    70,   570,    71,   163,    92,   270,
     173,   174,   394,    76,    76,   320,   321,    76,    76,   320,
     321,   164,   572,   573,   572,   573,    93,   409,   179,   395,
     498,   491,    76,   136,   416,   417,   418,   419,   281,   336,
     337,    76,   582,   583,   385,   503,   504,   429,   430,   431,
     273,   435,   436,   437,   438,   439,   440,   441,   442,   443,
     444,   445,   446,   447,   448,   449,   450,   451,   452,   453,
     454,   455,   456,   457,   458,   459,   460,   461,   462,   463,
     464,   465,   466,   467,   468,   469,   470,   159,   705,   706,
     707,   475,   676,   677,   219,   220,   511,   221,   558,   302,
     303,   389,   519,   559,   560,   561,   562,    76,   308,   309,
     157,   566,   567,    76,   314,   137,    76,   165,   215,   617,
     216,   253,   217,   259,   260,    90,   570,   472,   474,   318,
     275,   482,   279,   282,   283,   493,   435,   284,   285,   348,
      91,   286,   409,   287,   502,   572,   573,   507,   320,   321,
     435,    76,   273,   510,   288,   289,   290,   514,   300,   382,
     296,   518,   414,   343,   398,   582,   583,   524,   336,   337,
     399,   350,   402,   471,   495,   530,   478,   408,   354,   355,
     356,   357,   358,   359,   360,   361,   362,   363,   364,   365,
     366,   367,   368,   369,   370,   371,   372,   373,   374,   375,
     376,   377,   378,   379,   380,   381,   558,   410,   411,   412,
     413,   477,   480,   494,   562,   525,   526,   527,   533,   566,
     537,   543,   302,   303,   389,   557,   540,   588,   604,  -109,
     556,   610,   309,   619,   570,   678,   624,   314,   623,   682,
     353,   587,   691,   693,   502,   589,   590,   591,   592,   476,
     593,   594,   318,   572,   573,   695,   536,   255,   539,   386,
     538,   160,   135,   506,   611,   508,   541,   542,   685,   603,
     696,   320,   321,   582,   583,   490,   684,   528,     0,   431,
       0,     0,   612,     0,     0,     0,     0,     0,     0,     0,
       0,   336,   337,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   627,   628,   479,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   613,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   657,     0,   658,    55,    56,     0,     0,
       0,     0,     0,   660,   661,   662,   663,   664,   665,     0,
       0,   675,     0,    63,    66,     0,     0,   679,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    78,     0,
       0,     0,     0,    86,     0,    87,     0,     0,     0,     0,
      95,    96,    97,    98,     0,     0,    78,     0,     0,   104,
       0,     0,    78,     0,     0,     0,   150,   680,   152,     0,
       0,     0,     0,   158,     0,   161,   689,     0,     0,     0,
       0,     0,     0,    78,   276,   277,   278,   292,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   350,     0,     0,     0,     0,     0,   698,   699,
     700,   701,     0,    78,     0,     0,     0,   301,    78,     0,
     302,   303,   389,   150,   150,     0,   150,   306,   307,   308,
     309,     0,     0,   312,   313,   314,   315,    87,     0,   256,
     257,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     318,     0,     0,     0,     0,     0,   267,     0,     0,   319,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   320,
     321,   322,     0,     0,     0,     0,   323,   324,   325,   326,
     327,   328,   329,   330,   331,   332,   333,   334,   335,   336,
     337,     0,    78,     0,     0,     0,     0,     0,     0,     0,
     150,     0,   546,   550,     0,   150,   554,   150,   150,   150,
     150,   150,   150,   150,   150,   150,   150,   150,   150,   150,
     150,   150,   150,   150,   150,   150,   150,   150,   150,   150,
     150,   150,   150,   150,   150,     0,     0,     0,     0,     0,
       0,     0,     0,    78,     0,   392,    66,     0,   393,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   407,     0,
       0,   615,     0,     0,     0,     0,     0,     0,     0,   415,
       0,     0,     0,     0,     0,     0,   629,   630,   631,   632,
     633,   634,   635,   636,   637,   638,   639,   640,   641,   642,
     643,   644,   645,   646,   647,   648,   649,   650,   651,   652,
     653,   654,   655,   656,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   150,     0,     0,   150,     0,   683,     0,   629,     0,
     550,   686,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   481,   257,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    95,     0,
       0,     0,   505,     0,     0,     0,     0,     0,     0,     0,
       0,   512,     0,   513,   515,   516,   517,     0,     0,   615,
       0,     0,     0,    37,     0,   169,     0,     0,     0,     0,
       0,    38,   170,   171,     0,     0,     0,     0,     0,     0,
     172,    39,   262,   174,     0,     0,     0,   175,     0,   176,
     177,   178,     0,     0,     0,     0,     0,     0,     0,     0,
     263,   264,   265,     0,   266,   181,   182,     0,   183,    43,
      44,   184,     0,     0,   185,    45,    46,   186,   187,   188,
     189,   150,   190,    47,     0,   191,   192,   544,     0,   551,
     193,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    48,
      49,    50,     0,     0,    51,    52,    53,    54,   194,   195,
     196,     0,     0,   197,     0,     0,     0,   198,   199,   200,
       0,     0,     0,     0,     0,     0,     0,    37,     0,   169,
       0,     0,     0,     0,     0,    38,   170,   171,     0,     0,
     201,   202,     0,   203,   172,    39,   262,   174,     0,     0,
       0,   175,     0,   176,   177,   178,     0,     0,     0,     0,
       0,     0,     0,     0,   263,     0,     0,     0,   266,   181,
     182,     0,   183,    43,    44,   184,     0,     0,   185,    45,
      46,   186,   187,   188,   189,     0,   190,    47,     0,   191,
     192,     0,     0,     0,   193,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    48,    49,    50,     0,     0,    51,    52,
      53,    54,   194,   195,   196,     0,     0,   197,     0,     0,
       0,   198,   199,   200,   169,     0,     0,     0,     0,     0,
       0,   170,   171,     0,     0,     0,     0,     0,     0,   666,
       0,   173,   174,   107,   201,   202,   175,   203,   667,   668,
     669,   111,   112,   113,   114,     0,   115,   116,     0,   179,
       0,   117,   118,   180,   181,   182,     0,   183,     0,     0,
     184,   119,     0,   185,     0,     0,   186,   187,   188,   189,
     120,   190,     0,     0,   191,   670,     0,     0,     0,   193,
     121,   122,   123,     0,     0,     0,     0,   124,     0,   125,
       0,   126,     0,   127,     0,   128,     0,   129,     0,     0,
     131,   132,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   169,   194,   195,   196,
     133,   134,   197,   170,   171,     0,   198,   199,   200,     0,
       0,   172,     0,   173,   174,     0,     0,     0,   175,     0,
     176,   177,   178,     0,     0,     0,     0,     0,     0,   201,
     202,   179,   203,     0,     0,   180,   181,   182,     0,   183,
       0,     0,   184,     0,     0,   185,     0,     0,   186,   187,
     188,   189,     0,   190,     0,     0,   191,   192,     0,     0,
       0,   193,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   500,
       0,     0,     0,     0,   501,     0,     0,     0,   169,   194,
     195,   196,     0,     0,   197,   170,   171,     0,   198,   199,
     200,     0,     0,   172,     0,   173,   174,     0,   272,     0,
     175,     0,   176,   177,   178,     0,     0,     0,     0,     0,
       0,   201,   202,   179,   203,     0,     0,   180,   181,   182,
       0,   183,     0,     0,   184,     0,     0,   185,     0,     0,
     186,   187,   188,   189,     0,   190,     0,     0,   191,   192,
       0,     0,     0,   193,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     169,   194,   195,   196,     0,     0,   197,   170,   171,     0,
     198,   199,   200,     0,     0,   172,     0,   173,   174,     0,
       0,     0,   175,     0,   176,   177,   178,     0,     0,     0,
       0,     0,     0,   201,   202,   179,   203,     0,     0,   180,
     181,   182,     0,   183,     0,     0,   184,     0,     0,   185,
       0,     0,   186,   187,   188,   189,     0,   190,     0,     0,
     191,   192,     0,     0,     0,   193,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   169,   194,   195,   196,     0,     0,   197,   170,
     171,     0,   198,   199,   200,     0,     0,     0,     0,   173,
     174,     0,     0,     0,   175,     0,   176,   177,   178,     0,
       0,     0,     0,     0,    59,   201,   202,   179,   203,     0,
       0,   180,   181,   182,     0,   183,     0,     0,   184,     0,
       0,   185,     0,     0,   186,   187,   188,   189,     0,   190,
       0,     0,   191,   192,     0,     0,     0,   193,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   500,     0,     0,     0,     0,
     501,     0,     0,     0,   169,   194,   195,   196,     0,     0,
     197,   170,   171,     0,   198,   199,   200,     0,     0,   172,
       0,   173,   174,     0,     0,     0,   175,     0,   176,   177,
     178,     0,     0,     0,   692,     0,     0,   201,   202,   179,
     203,     0,     0,   180,   181,   182,     0,   183,     0,     0,
     184,     0,     0,   185,     0,     0,   186,   187,   188,   189,
       0,   190,     0,     0,   191,   192,     0,     0,     0,   193,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   169,   194,   195,   196,
       0,     0,   197,   170,   171,     0,   198,   199,   200,     0,
       0,   172,     0,   173,   174,     0,     0,     0,   175,     0,
     176,   177,   178,     0,     0,     0,     0,     0,     0,   201,
     202,   179,   203,     0,     0,   180,   181,   182,     0,   183,
       0,     0,   184,     0,     0,   185,     0,     0,   186,   187,
     188,   189,     0,   190,     0,     0,   191,   192,     0,     0,
       0,   193,     0,     0,     0,     0,     0,     0,     0,     0,
      37,     0,     0,     0,     0,     0,     0,     0,    38,     0,
       0,     0,     0,     0,     0,     0,     0,   139,    39,    40,
     140,    31,     0,     0,     0,     0,     0,     0,   141,   194,
     195,   196,     0,     0,   197,     0,     0,    41,   198,   199,
     200,    42,     0,     0,   142,     0,    43,    44,     0,     0,
       0,     0,    45,    46,     0,     0,     0,     0,     0,     0,
      47,   201,   202,   143,   203,     0,     0,     0,     0,     0,
       0,     0,     0,   144,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    48,    49,    50,     0,
       0,    51,    52,    53,    54,   301,     0,     0,   302,   303,
     403,   404,   405,     0,   305,   306,   307,   308,   309,   310,
     311,   312,   313,   314,   315,     0,     0,     0,   316,   317,
       0,     0,     0,     0,     0,    32,     0,     0,   318,     0,
       0,     0,     0,     0,     0,     0,     0,   319,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   320,   321,   322,
       0,     0,     0,     0,   323,   324,   325,   326,   327,   328,
     329,   330,   331,   332,   333,   334,   335,   336,   337,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     301,     0,     0,   302,   303,   304,     0,   338,   339,   305,
     306,   307,   308,   309,   310,   311,   312,   313,   314,   315,
       0,     0,     0,   316,   317,     0,     0,     0,     0,     0,
       0,     0,     0,   318,   208,     0,     0,     0,     0,     0,
       0,     0,   319,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   320,   321,   322,     0,     0,     0,     0,   323,
     324,   325,   326,   327,   328,   329,   330,   331,   332,   333,
     334,   335,   336,   337,     0,     0,   620,   621,   622,     0,
       0,   559,   560,   561,   562,   563,   564,   565,     0,   566,
     567,     0,   338,   339,   568,   569,     0,     0,     0,     0,
       0,     0,     0,     0,   570,     0,     0,     0,     0,     0,
       0,     0,     0,   571,     0,     0,     0,     0,     0,    32,
       0,     0,     0,   572,   573,   574,     0,     0,     0,     0,
     575,     0,   576,     0,   577,     0,   578,     0,   579,     0,
     580,     0,   581,   582,   583,     0,     0,     0,     0,   387,
     388,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   301,   584,   585,   302,   303,   389,     0,     0,
       0,   305,   306,   307,   308,   309,   310,   311,   312,   313,
     314,   315,     0,     0,     0,   316,   317,     0,     0,     0,
     208,     0,     0,     0,     0,   318,     0,     0,     0,     0,
       0,     0,     0,     0,   319,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   320,   321,   322,     0,     0,     0,
       0,   323,   324,   325,   326,   327,   328,   329,   330,   331,
     332,   333,   334,   335,   336,   337,     0,     0,     0,   390,
     391,     0,     0,   427,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   338,   339,   301,     0,     0,   302,
     303,   389,     0,     0,     0,   305,   306,   307,   308,   309,
     310,   311,   312,   313,   314,   315,     0,     0,     0,   316,
     317,     0,     0,     0,     0,     0,     0,     0,     0,   318,
       0,     0,     0,     0,     0,     0,     0,     0,   319,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   320,   321,
     322,     0,     0,     0,     0,   323,   324,   325,   326,   327,
     328,   329,   330,   331,   332,   333,   334,   335,   336,   337,
     694,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   301,     0,     0,   302,   303,   389,   338,   339,
       0,   305,   306,   307,   308,   309,   310,   311,   312,   313,
     314,   315,     0,     0,     0,   316,   317,     0,     0,     0,
       0,     0,     0,     0,     0,   318,     0,     0,     0,     0,
       0,     0,     0,     0,   319,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   320,   321,   322,     0,     0,     0,
       0,   323,   324,   325,   326,   327,   328,   329,   330,   331,
     332,   333,   334,   335,   336,   337,     0,     0,     0,    37,
       0,     0,     0,     0,     0,     0,     0,    38,     0,     0,
       0,     0,     0,     0,   338,   339,    72,    39,    40,    73,
       0,     0,     0,     0,     0,    74,    75,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    41,     0,     0,     0,
      42,     0,     0,     0,     0,    43,    44,     0,    76,     0,
       0,    45,    46,     0,     0,     0,     0,     0,     0,    47,
       0,     0,    77,     0,     0,     0,     0,     0,     0,     0,
      37,     0,     0,     0,     0,     0,     0,     0,    38,     0,
       0,     0,     0,     0,     0,     0,     0,   139,    39,    40,
     140,     0,     0,     0,     0,    48,    49,    50,   141,     0,
      51,    52,    53,    54,     0,     0,     0,    41,     0,     0,
       0,    42,     0,     0,   142,     0,    43,    44,     0,     0,
       0,     0,    45,    46,     0,     0,     0,     0,     0,     0,
      47,     0,     0,   143,     0,     0,     0,     0,     0,    37,
       0,     0,     0,   352,     0,     0,     0,    38,     0,     0,
       0,     0,     0,     0,     0,     0,   483,    39,   484,   485,
       0,     0,   625,     0,     0,     0,    48,    49,    50,     0,
       0,    51,    52,    53,    54,  -272,    41,     0,     0,     0,
      42,     0,     0,   487,     0,    43,    44,     0,     0,     0,
       0,    45,    46,     0,     0,     0,     0,     0,     0,    47,
       0,     0,   488,     0,     0,     0,     0,    37,     0,     0,
       0,     0,     0,     0,     0,    38,     0,     0,     0,     0,
       0,     0,     0,     0,   483,    39,   484,   485,     0,   548,
       0,     0,     0,     0,     0,    48,    49,    50,    37,     0,
      51,    52,    53,    54,    41,     0,    38,     0,    42,     0,
       0,   487,     0,    43,    44,   483,    39,   484,   485,    45,
      46,     0,     0,     0,     0,     0,     0,    47,     0,     0,
     488,     0,     0,     0,  -272,    41,     0,     0,     0,    42,
       0,     0,   487,     0,    43,    44,     0,     0,     0,     0,
      45,    46,     0,     0,     0,     0,     0,     0,    47,     0,
       0,   488,     0,    48,    49,    50,    37,     0,    51,    52,
      53,    54,     0,     0,    38,     0,     0,     0,     0,     0,
       0,     0,     0,   483,    39,   484,   485,     0,     0,     0,
       0,     0,     0,     0,    48,    49,    50,    37,     0,    51,
      52,    53,    54,    41,   614,    38,     0,    42,     0,     0,
     487,     0,    43,    44,   483,    39,   484,   485,    45,    46,
       0,     0,     0,     0,     0,     0,    47,     0,     0,   488,
       0,     0,     0,     0,   486,     0,     0,     0,    42,     0,
       0,   487,     0,    43,    44,     0,     0,     0,     0,    45,
      46,     0,     0,     0,     0,     0,     0,    47,     0,     0,
     488,     0,    48,    49,    50,    37,     0,    51,    52,    53,
      54,     0,     0,    38,     0,     0,     0,     0,     0,     0,
       0,     0,   483,    39,   484,   485,     0,     0,     0,    37,
       0,     0,     0,    48,    49,    50,     0,    38,    51,    52,
      53,    54,    41,     0,     0,     0,    42,    39,    40,   487,
       0,    43,    44,     0,     0,     0,     0,    45,    46,     0,
       0,     0,     0,     0,     0,    47,    41,  -157,   488,     0,
      42,     0,     0,     0,     0,    43,    44,     0,     0,     0,
       0,    45,    46,     0,     0,     0,     0,     0,     0,    47,
       0,     0,     0,     0,     0,     0,     0,    37,     0,     0,
       0,    48,    49,    50,     0,    38,    51,    52,    53,    54,
       0,     0,     0,     0,     0,    39,    40,     0,     0,   218,
       0,     0,     0,     0,     0,    48,    49,    50,    37,     0,
      51,    52,    53,    54,    41,     0,    38,     0,    42,     0,
       0,     0,     0,    43,    44,   279,    39,    40,     0,    45,
      46,     0,     0,     0,     0,     0,     0,    47,     0,     0,
       0,     0,     0,     0,     0,    41,     0,     0,    37,    42,
       0,     0,     0,     0,    43,    44,    38,     0,     0,     0,
      45,    46,     0,     0,     0,     0,    39,    40,    47,     0,
       0,     0,     0,    48,    49,    50,     0,     0,    51,    52,
      53,    54,     0,     0,     0,    41,     0,     0,     0,    42,
       0,     0,     0,     0,    43,    44,     0,     0,     0,     0,
      45,    46,     0,     0,    48,    49,    50,     0,    47,    51,
      52,    53,    54,     0,   301,     0,     0,   302,   303,   389,
       0,   400,     0,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,     0,     0,   401,   316,   317,     0,
       0,     0,     0,     0,    48,    49,    50,   318,     0,    51,
      52,    53,    54,     0,     0,     0,   319,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   320,   321,   322,     0,
       0,     0,     0,   323,   324,   325,   326,   327,   328,   329,
     330,   331,   332,   333,   334,   335,   336,   337,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   301,   338,   339,   302,   303,
     389,     0,   420,     0,   305,   306,   307,   308,   309,   310,
     311,   312,   313,   314,   315,     0,     0,     0,   316,   317,
       0,     0,     0,     0,     0,     0,     0,     0,   318,     0,
       0,     0,     0,     0,     0,     0,     0,   319,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   320,   321,   322,
       0,     0,     0,     0,   323,   324,   325,   326,   327,   328,
     329,   330,   331,   332,   333,   334,   335,   336,   337,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     301,   421,     0,   302,   303,   389,     0,   338,   339,   305,
     306,   307,   308,   309,   310,   311,   312,   313,   314,   315,
       0,     0,     0,   316,   317,     0,     0,     0,     0,     0,
       0,     0,     0,   318,     0,     0,     0,     0,     0,     0,
       0,     0,   319,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   320,   321,   322,     0,     0,     0,     0,   323,
     324,   325,   326,   327,   328,   329,   330,   331,   332,   333,
     334,   335,   336,   337,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   301,   338,   339,   302,   303,   389,     0,   428,     0,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,     0,     0,     0,   316,   317,     0,     0,     0,     0,
       0,     0,     0,     0,   318,     0,     0,     0,     0,     0,
       0,     0,     0,   319,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   320,   321,   322,     0,     0,     0,     0,
     323,   324,   325,   326,   327,   328,   329,   330,   331,   332,
     333,   334,   335,   336,   337,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   301,     0,     0,   302,
     303,   389,     0,   338,   339,   305,   306,   307,   308,   309,
     310,   311,   312,   313,   314,   315,     0,     0,   499,   316,
     317,     0,     0,     0,     0,     0,     0,     0,     0,   318,
       0,     0,     0,     0,     0,     0,     0,     0,   319,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   320,   321,
     322,     0,     0,     0,     0,   323,   324,   325,   326,   327,
     328,   329,   330,   331,   332,   333,   334,   335,   336,   337,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   301,     0,     0,   302,   303,   389,     0,   338,   339,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,     0,     0,   520,   316,   317,     0,     0,     0,     0,
       0,     0,     0,     0,   318,     0,     0,     0,     0,     0,
       0,     0,     0,   319,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   320,   321,   322,     0,     0,     0,     0,
     323,   324,   325,   326,   327,   328,   329,   330,   331,   332,
     333,   334,   335,   336,   337,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   301,     0,     0,   302,
     303,   389,     0,   338,   339,   305,   306,   307,   308,   309,
     310,   311,   312,   313,   314,   315,     0,     0,   521,   316,
     317,     0,     0,     0,     0,     0,     0,     0,     0,   318,
       0,     0,     0,     0,     0,     0,     0,     0,   319,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   320,   321,
     322,     0,     0,     0,     0,   323,   324,   325,   326,   327,
     328,   329,   330,   331,   332,   333,   334,   335,   336,   337,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   301,   338,   339,
     302,   303,   389,     0,   522,     0,   305,   306,   307,   308,
     309,   310,   311,   312,   313,   314,   315,     0,     0,     0,
     316,   317,     0,     0,     0,     0,     0,     0,     0,     0,
     318,     0,     0,     0,     0,     0,     0,     0,     0,   319,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   320,
     321,   322,     0,     0,     0,     0,   323,   324,   325,   326,
     327,   328,   329,   330,   331,   332,   333,   334,   335,   336,
     337,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   301,     0,     0,   302,   303,   389,     0,   338,
     339,   305,   306,   307,   308,   309,   310,   311,   312,   313,
     314,   315,     0,     0,   523,   316,   317,     0,     0,     0,
       0,     0,     0,     0,     0,   318,     0,     0,     0,     0,
       0,     0,     0,     0,   319,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   320,   321,   322,     0,     0,     0,
       0,   323,   324,   325,   326,   327,   328,   329,   330,   331,
     332,   333,   334,   335,   336,   337,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   301,     0,     0,
     302,   303,   389,     0,   338,   339,   305,   306,   307,   308,
     309,   310,   311,   312,   313,   314,   315,     0,     0,   531,
     316,   317,     0,     0,     0,     0,     0,     0,     0,     0,
     318,     0,     0,     0,     0,     0,     0,     0,     0,   319,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   320,
     321,   322,     0,     0,     0,     0,   323,   324,   325,   326,
     327,   328,   329,   330,   331,   332,   333,   334,   335,   336,
     337,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   301,   338,
     339,   302,   303,   389,     0,   532,     0,   305,   306,   307,
     308,   309,   310,   311,   312,   313,   314,   315,     0,     0,
       0,   316,   317,     0,     0,     0,     0,     0,     0,     0,
       0,   318,     0,     0,     0,     0,     0,     0,     0,     0,
     319,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     320,   321,   322,     0,     0,     0,     0,   323,   324,   325,
     326,   327,   328,   329,   330,   331,   332,   333,   334,   335,
     336,   337,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   301,     0,     0,   302,   303,   389,     0,
     338,   339,   305,   306,   307,   308,   309,   310,   311,   312,
     313,   314,   315,     0,     0,   534,   316,   317,     0,     0,
       0,     0,     0,     0,     0,     0,   318,     0,     0,     0,
       0,     0,     0,     0,     0,   319,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   320,   321,   322,     0,     0,
       0,     0,   323,   324,   325,   326,   327,   328,   329,   330,
     331,   332,   333,   334,   335,   336,   337,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   301,   338,   339,   302,   303,   389,
       0,   400,     0,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,     0,     0,     0,   316,   317,     0,
       0,     0,     0,     0,     0,     0,     0,   318,     0,     0,
       0,     0,     0,     0,     0,     0,   319,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   320,   321,   322,     0,
       0,     0,     0,   323,   324,   325,   326,   327,   328,   329,
     330,   331,   332,   333,   334,   335,   336,   337,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   301,
       0,     0,   302,   303,   389,   595,   338,   339,   305,   306,
     307,   308,   309,   310,   311,   312,   313,   314,   315,     0,
       0,     0,   316,   317,     0,     0,     0,     0,     0,     0,
       0,     0,   318,     0,     0,     0,     0,     0,     0,     0,
       0,   319,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   320,   321,   322,     0,     0,     0,     0,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     301,   338,   339,   302,   303,   389,     0,   598,     0,   305,
     306,   307,   308,   309,   310,   311,   312,   313,   314,   315,
       0,     0,     0,   316,   317,     0,     0,     0,     0,     0,
       0,     0,     0,   318,     0,     0,     0,     0,     0,     0,
       0,     0,   319,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   320,   321,   322,     0,     0,     0,     0,   323,
     324,   325,   326,   327,   328,   329,   330,   331,   332,   333,
     334,   335,   336,   337,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   301,   338,   339,   302,   303,   389,     0,   602,     0,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,     0,     0,     0,   316,   317,     0,     0,     0,     0,
       0,     0,     0,     0,   318,     0,     0,     0,     0,     0,
       0,     0,     0,   319,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   320,   321,   322,     0,     0,     0,     0,
     323,   324,   325,   326,   327,   328,   329,   330,   331,   332,
     333,   334,   335,   336,   337,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   301,     0,     0,   302,
     303,   389,     0,   338,   339,   305,   306,   307,   308,   309,
     310,   311,   312,   313,   314,   315,     0,     0,   674,   316,
     317,     0,     0,     0,     0,     0,     0,     0,     0,   318,
       0,     0,     0,     0,     0,     0,     0,     0,   319,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   320,   321,
     322,     0,     0,     0,     0,   323,   324,   325,   326,   327,
     328,   329,   330,   331,   332,   333,   334,   335,   336,   337,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   301,     0,     0,   302,   303,   389,     0,   338,   339,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   690,     0,     0,   316,   317,     0,     0,     0,     0,
       0,     0,     0,     0,   318,     0,     0,     0,     0,     0,
       0,     0,     0,   319,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   320,   321,   322,     0,     0,     0,     0,
     323,   324,   325,   326,   327,   328,   329,   330,   331,   332,
     333,   334,   335,   336,   337,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   301,   338,   339,   302,   303,   389,     0,   702,
       0,   305,   306,   307,   308,   309,   310,   311,   312,   313,
     314,   315,     0,     0,     0,   316,   317,     0,     0,     0,
       0,     0,     0,     0,     0,   318,     0,     0,     0,     0,
       0,     0,     0,     0,   319,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   320,   321,   322,     0,     0,     0,
       0,   323,   324,   325,   326,   327,   328,   329,   330,   331,
     332,   333,   334,   335,   336,   337,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   301,   338,   339,   302,   303,   389,     0,
     703,     0,   305,   306,   307,   308,   309,   310,   311,   312,
     313,   314,   315,     0,     0,     0,   316,   317,     0,     0,
       0,     0,     0,     0,     0,     0,   318,     0,     0,     0,
       0,     0,     0,     0,     0,   319,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   320,   321,   322,     0,     0,
       0,     0,   323,   324,   325,   326,   327,   328,   329,   330,
     331,   332,   333,   334,   335,   336,   337,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   301,   338,   339,   302,   303,   389,
       0,   704,     0,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,     0,     0,     0,   316,   317,     0,
       0,     0,     0,     0,     0,     0,     0,   318,     0,     0,
       0,     0,     0,     0,     0,     0,   319,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   320,   321,   322,     0,
       0,     0,     0,   323,   324,   325,   326,   327,   328,   329,
     330,   331,   332,   333,   334,   335,   336,   337,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   301,
       0,     0,   302,   303,   389,     0,   338,   339,   305,   306,
     307,   308,   309,   310,   311,   312,   313,   314,   315,     0,
       0,     0,   316,   317,     0,     0,     0,     0,     0,     0,
       0,     0,   318,     0,     0,     0,     0,     0,     0,     0,
       0,   319,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   320,   321,   322,     0,     0,     0,     0,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   301,     0,     0,   302,   303,   389,
       0,   338,   339,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,     0,     0,     0,     0,   317,     0,
       0,     0,     0,     0,     0,     0,     0,   318,     0,     0,
       0,     0,     0,     0,     0,     0,   319,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   320,   321,   322,     0,
       0,     0,     0,   323,   324,   325,   326,   327,   328,   329,
     330,   331,   332,   333,   334,   335,   336,   337,   586,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   301,
       0,     0,   302,   303,   389,     0,   338,   339,   305,   306,
     307,   308,   309,   310,   311,   312,   313,   314,   315,     0,
       0,     0,   659,   317,     0,     0,     0,     0,     0,     0,
       0,     0,   318,     0,     0,     0,     0,     0,     0,     0,
       0,   319,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   320,   321,   322,     0,     0,     0,     0,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   301,     0,     0,   302,   303,   389,
       0,   338,   339,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,     0,     0,     0,     0,   317,     0,
       0,     0,     0,     0,     0,     0,     0,   318,     0,     0,
       0,     0,     0,     0,     0,     0,   319,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   320,   321,   322,     0,
       0,     0,     0,   323,   324,   325,   326,   327,   328,   329,
     330,   331,   332,   333,   334,   335,   336,   337,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   301,     0,
       0,   302,   303,   389,     0,     0,   338,   339,   306,   307,
     308,   309,   310,   311,   312,   313,   314,   315,     0,     0,
       0,     0,   317,     0,     0,     0,     0,     0,     0,     0,
       0,   318,     0,     0,     0,     0,     0,     0,     0,     0,
     319,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     320,   321,   322,     0,     0,     0,     0,   323,   324,   325,
     326,   327,   328,   329,   330,   331,   332,   333,   334,   335,
     336,   337,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   301,     0,     0,   302,   303,   389,     0,     0,
     338,   339,   306,   307,   308,   309,   310,     0,   312,   313,
     314,   315,     0,     0,     0,     0,   317,     0,     0,     0,
       0,     0,     0,     0,     0,   318,     0,     0,     0,     0,
       0,     0,     0,     0,   319,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   320,   321,   322,     0,     0,     0,
       0,   323,   324,   325,   326,   327,   328,   329,   330,   331,
     332,   333,   334,   335,   336,   337,     0,     0,     0,     0,
     301,     0,     0,   302,   303,   389,     0,     0,     0,     0,
     306,   307,   308,   309,   338,   339,   312,   313,   314,   315,
       0,     0,     0,     0,   317,     0,     0,     0,     0,     0,
       0,     0,     0,   318,     0,     0,     0,     0,     0,     0,
       0,     0,   319,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   320,   321,   322,     0,     0,     0,     0,   323,
     324,   325,   326,   327,   328,   329,   330,   331,   332,   333,
     334,   335,   336,   337,     0,     0,   558,     0,   618,     0,
       0,   559,   560,   561,   562,   563,   564,   565,     0,   566,
     567,     0,   338,   339,   568,   569,     0,     0,     0,     0,
       0,     0,     0,     0,   570,     0,     0,     0,     0,     0,
       0,     0,     0,   571,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   572,   573,   574,     0,     0,     0,     0,
     575,     0,   576,     0,   577,     0,   578,     0,   579,     0,
     580,     0,   581,   582,   583,     0,     0,   558,     0,   681,
       0,     0,   559,   560,   561,   562,   563,   564,   565,     0,
     566,   567,     0,   584,   585,   568,   569,     0,     0,     0,
       0,     0,     0,     0,     0,   570,     0,     0,     0,     0,
       0,     0,     0,     0,   571,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   572,   573,   574,     0,     0,     0,
       0,   575,     0,   576,     0,   577,     0,   578,     0,   579,
       0,   580,   558,   581,   582,   583,     0,   559,   560,   561,
     562,   563,   564,   565,     0,   566,   567,     0,     0,   688,
     568,   569,     0,     0,   584,   585,     0,     0,     0,     0,
     570,     0,     0,     0,     0,     0,     0,     0,     0,   571,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   572,
     573,   574,     0,     0,     0,     0,   575,     0,   576,     0,
     577,     0,   578,     0,   579,     0,   580,     0,   581,   582,
     583,     0,     0,   558,   697,     0,     0,     0,   559,   560,
     561,   562,   563,   564,   565,     0,   566,   567,     0,   584,
     585,   568,   569,     0,     0,     0,     0,     0,     0,     0,
       0,   570,     0,     0,     0,     0,     0,     0,     0,     0,
     571,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     572,   573,   574,     0,     0,     0,     0,   575,     0,   576,
       0,   577,     0,   578,     0,   579,     0,   580,   107,   581,
     582,   583,     0,   108,   109,   110,   111,   112,   113,   114,
       0,   115,   116,     0,     0,     0,   117,   118,     0,     0,
     584,   585,     0,     0,     0,     0,   119,     0,     0,     0,
       0,     0,     0,     0,     0,   120,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   121,   122,   123,     0,     0,
       0,     0,   124,     0,   125,     0,   126,     0,   127,     0,
     128,     0,   129,   224,   130,   131,   132,     0,   225,   226,
     227,   228,   229,   230,   231,     0,   232,   233,     0,     0,
       0,   234,   235,     0,     0,   133,   134,     0,     0,     0,
       0,   236,     0,     0,     0,     0,     0,     0,     0,     0,
     237,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     238,   239,   240,     0,     0,     0,     0,   241,     0,   242,
       0,   243,     0,   244,     0,   245,     0,   246,   558,   247,
     248,   249,     0,   559,   560,   561,   562,   563,   564,   565,
       0,   566,   567,     0,     0,     0,   568,   569,     0,     0,
     250,   251,     0,     0,     0,     0,   570,     0,     0,     0,
       0,     0,     0,     0,     0,   571,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   572,   573,   574,     0,     0,
       0,     0,   575,     0,   576,     0,   577,     0,   578,     0,
     579,     0,   580,   558,   581,   582,   583,     0,   559,   560,
     561,   562,   563,   564,   565,     0,   566,   567,     0,     0,
       0,     0,   569,     0,     0,   584,   585,     0,     0,     0,
       0,   570,     0,     0,     0,     0,     0,     0,     0,     0,
     571,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     572,   573,   574,     0,     0,     0,     0,   575,     0,   576,
       0,   577,     0,   578,     0,   579,     0,   580,   558,   581,
     582,   583,     0,   559,   560,   561,   562,   563,     0,   565,
       0,   566,   567,     0,     0,     0,     0,   569,     0,     0,
     584,   585,     0,     0,     0,     0,   570,     0,     0,     0,
       0,     0,     0,     0,     0,   571,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   572,   573,   574,     0,     0,
       0,     0,   575,     0,   576,     0,   577,     0,   578,     0,
     579,     0,   580,   558,   581,   582,   583,     0,   559,   560,
     561,   562,     0,     0,   565,     0,   566,   567,     0,     0,
       0,     0,   569,     0,     0,   584,   585,     0,     0,     0,
       0,   570,     0,     0,     0,     0,     0,     0,     0,     0,
     571,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     572,   573,   574,     0,     0,     0,     0,   575,     0,   576,
       0,   577,     0,   578,     0,   579,     0,   580,   558,   581,
     582,   583,     0,   559,   560,   561,   562,     0,     0,   565,
       0,   566,   567,     0,     0,     0,     0,     0,     0,     0,
     584,   585,     0,     0,     0,     0,   570,     0,     0,     0,
       0,     0,     0,     0,     0,   571,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   572,   573,   574,     0,     0,
       0,     0,   575,     0,   576,     0,   577,     0,   578,     0,
     579,     0,   580,     0,   581,   582,   583
};

static const short int yycheck[] =
{
       5,   103,    22,   387,    90,    55,    84,    20,    22,    22,
      22,    22,   303,    24,    22,    22,   553,     0,    22,    39,
      33,    33,    22,    73,    22,    33,    46,    22,    18,    79,
      39,    46,    37,    33,    30,    33,    50,    18,    33,   137,
      20,    21,    22,   427,    18,    20,    21,    27,    28,    29,
      30,    18,    18,    24,    50,    35,    36,    24,   101,    18,
      50,    20,    20,    72,    18,    39,    19,    72,    18,    50,
      50,   157,    39,    69,    70,    50,    50,    24,    46,    50,
     130,    86,    40,    50,    50,   135,    20,    21,   625,    69,
      70,     0,     1,   136,    69,    70,    50,     6,     7,     8,
      50,   106,   486,    50,    38,    19,    15,    16,    88,    89,
      90,     9,    19,    18,   405,    33,   136,   219,   138,    24,
      18,    18,   136,   136,   136,   136,   169,    24,   136,   136,
     173,   174,   136,   176,   177,   178,   136,   215,   136,    20,
      18,   136,    39,   148,    24,    50,    24,   152,   191,   192,
     193,   194,   195,    50,   197,   198,   199,    66,    67,   209,
      20,    39,     9,    97,    98,    99,    75,   210,    39,   553,
      50,   273,    50,    33,    24,    18,    20,    21,    20,    50,
      20,    21,    22,    92,    93,    22,    30,    27,    28,    29,
      30,   100,    29,    30,    37,    35,    36,    24,    35,   204,
      50,    24,    20,   112,   113,   114,    50,    50,   213,   214,
      50,    37,    19,    50,    20,    50,    20,    39,    72,   262,
      20,    21,   265,    50,    50,    69,    70,    50,    50,    69,
      70,    39,    69,    70,    69,    70,    39,   280,    38,     9,
      39,   625,    50,    33,   287,   288,   289,   290,    18,    89,
      90,    50,    89,    90,   259,   121,   122,   300,   301,   302,
     303,   304,   305,   306,   307,   308,   309,   310,   311,   312,
     313,   314,   315,   316,   317,   318,   319,   320,   321,   322,
     323,   324,   325,   326,   327,   328,   329,   330,   331,   332,
     333,   334,   335,   336,   337,   338,   339,   383,   702,   703,
     704,   344,   606,   607,   140,   141,    39,   143,    22,    20,
      21,    22,    39,    27,    28,    29,    30,    50,    29,    30,
      24,    35,    36,    50,    35,     9,    50,    39,    24,    18,
      39,    39,    20,    39,    24,    24,    50,   342,   343,    50,
      20,   384,    18,    18,    18,   388,   389,    18,    18,    40,
      39,    20,   395,    20,   397,    69,    70,   400,    69,    70,
     403,    50,   405,   406,    20,    20,    20,   410,    20,    33,
      38,   414,    33,    39,    39,    89,    90,   420,    89,    90,
      24,   217,    39,    23,    18,   428,    24,    40,   224,   225,
     226,   227,   228,   229,   230,   231,   232,   233,   234,   235,
     236,   237,   238,   239,   240,   241,   242,   243,   244,   245,
     246,   247,   248,   249,   250,   251,    22,    40,    40,    40,
      40,    39,    39,     5,    30,    24,    18,    37,    39,    35,
     473,    20,    20,    21,    22,     5,    23,     5,    62,    72,
      91,    24,    30,    39,    50,   138,    37,    35,   550,    39,
     222,   494,    33,    33,   497,   498,   499,   500,   501,   348,
     503,   504,    50,    69,    70,    39,   471,   154,   478,   260,
     475,    90,    80,   399,   532,   400,   481,   482,   622,   522,
     681,    69,    70,    89,    90,   387,   618,   426,    -1,   532,
      -1,    -1,   535,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    89,    90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   556,   557,   351,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   537,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   586,    -1,   588,    23,    24,    -1,    -1,
      -1,    -1,    -1,   596,   597,   598,   599,   600,   601,    -1,
      -1,   604,    -1,    40,    41,    -1,    -1,   610,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    55,    -1,
      -1,    -1,    -1,    60,    -1,    62,    -1,    -1,    -1,    -1,
      67,    68,    69,    70,    -1,    -1,    73,    -1,    -1,    76,
      -1,    -1,    79,    -1,    -1,    -1,    83,   612,    85,    -1,
      -1,    -1,    -1,    90,    -1,    92,   659,    -1,    -1,    -1,
      -1,    -1,    -1,   100,   667,   668,   669,   670,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   478,    -1,    -1,    -1,    -1,    -1,   691,   692,
     693,   694,    -1,   130,    -1,    -1,    -1,    17,   135,    -1,
      20,    21,    22,   140,   141,    -1,   143,    27,    28,    29,
      30,    -1,    -1,    33,    34,    35,    36,   154,    -1,   156,
     157,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      50,    -1,    -1,    -1,    -1,    -1,   173,    -1,    -1,    59,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    -1,   209,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     217,    -1,   484,   485,    -1,   222,   488,   224,   225,   226,
     227,   228,   229,   230,   231,   232,   233,   234,   235,   236,
     237,   238,   239,   240,   241,   242,   243,   244,   245,   246,
     247,   248,   249,   250,   251,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   260,    -1,   262,   263,    -1,   265,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   275,    -1,
      -1,   543,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   286,
      -1,    -1,    -1,    -1,    -1,    -1,   558,   559,   560,   561,
     562,   563,   564,   565,   566,   567,   568,   569,   570,   571,
     572,   573,   574,   575,   576,   577,   578,   579,   580,   581,
     582,   583,   584,   585,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   348,    -1,    -1,   351,    -1,   618,    -1,   620,    -1,
     622,   623,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   382,   383,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   395,    -1,
      -1,    -1,   399,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   408,    -1,   410,   411,   412,   413,    -1,    -1,   681,
      -1,    -1,    -1,     1,    -1,     3,    -1,    -1,    -1,    -1,
      -1,     9,    10,    11,    -1,    -1,    -1,    -1,    -1,    -1,
      18,    19,    20,    21,    -1,    -1,    -1,    25,    -1,    27,
      28,    29,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    39,    40,    -1,    42,    43,    44,    -1,    46,    47,
      48,    49,    -1,    -1,    52,    53,    54,    55,    56,    57,
      58,   478,    60,    61,    -1,    63,    64,   484,    -1,   486,
      68,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    97,
      98,    99,    -1,    -1,   102,   103,   104,   105,   106,   107,
     108,    -1,    -1,   111,    -1,    -1,    -1,   115,   116,   117,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,    -1,     3,
      -1,    -1,    -1,    -1,    -1,     9,    10,    11,    -1,    -1,
     138,   139,    -1,   141,    18,    19,    20,    21,    -1,    -1,
      -1,    25,    -1,    27,    28,    29,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    -1,    42,    43,
      44,    -1,    46,    47,    48,    49,    -1,    -1,    52,    53,
      54,    55,    56,    57,    58,    -1,    60,    61,    -1,    63,
      64,    -1,    -1,    -1,    68,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    97,    98,    99,    -1,    -1,   102,   103,
     104,   105,   106,   107,   108,    -1,    -1,   111,    -1,    -1,
      -1,   115,   116,   117,     3,    -1,    -1,    -1,    -1,    -1,
      -1,    10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    18,
      -1,    20,    21,    22,   138,   139,    25,   141,    27,    28,
      29,    30,    31,    32,    33,    -1,    35,    36,    -1,    38,
      -1,    40,    41,    42,    43,    44,    -1,    46,    -1,    -1,
      49,    50,    -1,    52,    -1,    -1,    55,    56,    57,    58,
      59,    60,    -1,    -1,    63,    64,    -1,    -1,    -1,    68,
      69,    70,    71,    -1,    -1,    -1,    -1,    76,    -1,    78,
      -1,    80,    -1,    82,    -1,    84,    -1,    86,    -1,    -1,
      89,    90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     3,   106,   107,   108,
     109,   110,   111,    10,    11,    -1,   115,   116,   117,    -1,
      -1,    18,    -1,    20,    21,    -1,    -1,    -1,    25,    -1,
      27,    28,    29,    -1,    -1,    -1,    -1,    -1,    -1,   138,
     139,    38,   141,    -1,    -1,    42,    43,    44,    -1,    46,
      -1,    -1,    49,    -1,    -1,    52,    -1,    -1,    55,    56,
      57,    58,    -1,    60,    -1,    -1,    63,    64,    -1,    -1,
      -1,    68,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    96,
      -1,    -1,    -1,    -1,   101,    -1,    -1,    -1,     3,   106,
     107,   108,    -1,    -1,   111,    10,    11,    -1,   115,   116,
     117,    -1,    -1,    18,    -1,    20,    21,    -1,    23,    -1,
      25,    -1,    27,    28,    29,    -1,    -1,    -1,    -1,    -1,
      -1,   138,   139,    38,   141,    -1,    -1,    42,    43,    44,
      -1,    46,    -1,    -1,    49,    -1,    -1,    52,    -1,    -1,
      55,    56,    57,    58,    -1,    60,    -1,    -1,    63,    64,
      -1,    -1,    -1,    68,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       3,   106,   107,   108,    -1,    -1,   111,    10,    11,    -1,
     115,   116,   117,    -1,    -1,    18,    -1,    20,    21,    -1,
      -1,    -1,    25,    -1,    27,    28,    29,    -1,    -1,    -1,
      -1,    -1,    -1,   138,   139,    38,   141,    -1,    -1,    42,
      43,    44,    -1,    46,    -1,    -1,    49,    -1,    -1,    52,
      -1,    -1,    55,    56,    57,    58,    -1,    60,    -1,    -1,
      63,    64,    -1,    -1,    -1,    68,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     3,   106,   107,   108,    -1,    -1,   111,    10,
      11,    -1,   115,   116,   117,    -1,    -1,    -1,    -1,    20,
      21,    -1,    -1,    -1,    25,    -1,    27,    28,    29,    -1,
      -1,    -1,    -1,    -1,   137,   138,   139,    38,   141,    -1,
      -1,    42,    43,    44,    -1,    46,    -1,    -1,    49,    -1,
      -1,    52,    -1,    -1,    55,    56,    57,    58,    -1,    60,
      -1,    -1,    63,    64,    -1,    -1,    -1,    68,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    96,    -1,    -1,    -1,    -1,
     101,    -1,    -1,    -1,     3,   106,   107,   108,    -1,    -1,
     111,    10,    11,    -1,   115,   116,   117,    -1,    -1,    18,
      -1,    20,    21,    -1,    -1,    -1,    25,    -1,    27,    28,
      29,    -1,    -1,    -1,    33,    -1,    -1,   138,   139,    38,
     141,    -1,    -1,    42,    43,    44,    -1,    46,    -1,    -1,
      49,    -1,    -1,    52,    -1,    -1,    55,    56,    57,    58,
      -1,    60,    -1,    -1,    63,    64,    -1,    -1,    -1,    68,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     3,   106,   107,   108,
      -1,    -1,   111,    10,    11,    -1,   115,   116,   117,    -1,
      -1,    18,    -1,    20,    21,    -1,    -1,    -1,    25,    -1,
      27,    28,    29,    -1,    -1,    -1,    -1,    -1,    -1,   138,
     139,    38,   141,    -1,    -1,    42,    43,    44,    -1,    46,
      -1,    -1,    49,    -1,    -1,    52,    -1,    -1,    55,    56,
      57,    58,    -1,    60,    -1,    -1,    63,    64,    -1,    -1,
      -1,    68,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     9,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    18,    19,    20,
      21,    22,    -1,    -1,    -1,    -1,    -1,    -1,    29,   106,
     107,   108,    -1,    -1,   111,    -1,    -1,    38,   115,   116,
     117,    42,    -1,    -1,    45,    -1,    47,    48,    -1,    -1,
      -1,    -1,    53,    54,    -1,    -1,    -1,    -1,    -1,    -1,
      61,   138,   139,    64,   141,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    74,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    97,    98,    99,    -1,
      -1,   102,   103,   104,   105,    17,    -1,    -1,    20,    21,
      22,    23,    24,    -1,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    -1,    -1,    -1,    40,    41,
      -1,    -1,    -1,    -1,    -1,   136,    -1,    -1,    50,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,
      -1,    -1,    -1,    -1,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      17,    -1,    -1,    20,    21,    22,    -1,   109,   110,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      -1,    -1,    -1,    40,    41,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    50,   136,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    70,    71,    -1,    -1,    -1,    -1,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    -1,    -1,    22,    23,    24,    -1,
      -1,    27,    28,    29,    30,    31,    32,    33,    -1,    35,
      36,    -1,   109,   110,    40,    41,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,   136,
      -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,    -1,
      76,    -1,    78,    -1,    80,    -1,    82,    -1,    84,    -1,
      86,    -1,    88,    89,    90,    -1,    -1,    -1,    -1,     4,
       5,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    17,   109,   110,    20,    21,    22,    -1,    -1,
      -1,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    -1,    -1,    -1,    40,    41,    -1,    -1,    -1,
     136,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,
      -1,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    -1,    -1,    -1,    94,
      95,    -1,    -1,     4,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   109,   110,    17,    -1,    -1,    20,
      21,    22,    -1,    -1,    -1,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    -1,    -1,    -1,    40,
      41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,    70,
      71,    -1,    -1,    -1,    -1,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
       5,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    17,    -1,    -1,    20,    21,    22,   109,   110,
      -1,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    -1,    -1,    -1,    40,    41,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,
      -1,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    -1,    -1,    -1,     1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     9,    -1,    -1,
      -1,    -1,    -1,    -1,   109,   110,    18,    19,    20,    21,
      -1,    -1,    -1,    -1,    -1,    27,    28,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    -1,
      42,    -1,    -1,    -1,    -1,    47,    48,    -1,    50,    -1,
      -1,    53,    54,    -1,    -1,    -1,    -1,    -1,    -1,    61,
      -1,    -1,    64,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     9,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    18,    19,    20,
      21,    -1,    -1,    -1,    -1,    97,    98,    99,    29,    -1,
     102,   103,   104,   105,    -1,    -1,    -1,    38,    -1,    -1,
      -1,    42,    -1,    -1,    45,    -1,    47,    48,    -1,    -1,
      -1,    -1,    53,    54,    -1,    -1,    -1,    -1,    -1,    -1,
      61,    -1,    -1,    64,    -1,    -1,    -1,    -1,    -1,     1,
      -1,    -1,    -1,    74,    -1,    -1,    -1,     9,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    18,    19,    20,    21,
      -1,    -1,    24,    -1,    -1,    -1,    97,    98,    99,    -1,
      -1,   102,   103,   104,   105,    37,    38,    -1,    -1,    -1,
      42,    -1,    -1,    45,    -1,    47,    48,    -1,    -1,    -1,
      -1,    53,    54,    -1,    -1,    -1,    -1,    -1,    -1,    61,
      -1,    -1,    64,    -1,    -1,    -1,    -1,     1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     9,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    18,    19,    20,    21,    -1,    23,
      -1,    -1,    -1,    -1,    -1,    97,    98,    99,     1,    -1,
     102,   103,   104,   105,    38,    -1,     9,    -1,    42,    -1,
      -1,    45,    -1,    47,    48,    18,    19,    20,    21,    53,
      54,    -1,    -1,    -1,    -1,    -1,    -1,    61,    -1,    -1,
      64,    -1,    -1,    -1,    37,    38,    -1,    -1,    -1,    42,
      -1,    -1,    45,    -1,    47,    48,    -1,    -1,    -1,    -1,
      53,    54,    -1,    -1,    -1,    -1,    -1,    -1,    61,    -1,
      -1,    64,    -1,    97,    98,    99,     1,    -1,   102,   103,
     104,   105,    -1,    -1,     9,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    18,    19,    20,    21,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    97,    98,    99,     1,    -1,   102,
     103,   104,   105,    38,    39,     9,    -1,    42,    -1,    -1,
      45,    -1,    47,    48,    18,    19,    20,    21,    53,    54,
      -1,    -1,    -1,    -1,    -1,    -1,    61,    -1,    -1,    64,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    -1,    42,    -1,
      -1,    45,    -1,    47,    48,    -1,    -1,    -1,    -1,    53,
      54,    -1,    -1,    -1,    -1,    -1,    -1,    61,    -1,    -1,
      64,    -1,    97,    98,    99,     1,    -1,   102,   103,   104,
     105,    -1,    -1,     9,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    18,    19,    20,    21,    -1,    -1,    -1,     1,
      -1,    -1,    -1,    97,    98,    99,    -1,     9,   102,   103,
     104,   105,    38,    -1,    -1,    -1,    42,    19,    20,    45,
      -1,    47,    48,    -1,    -1,    -1,    -1,    53,    54,    -1,
      -1,    -1,    -1,    -1,    -1,    61,    38,    39,    64,    -1,
      42,    -1,    -1,    -1,    -1,    47,    48,    -1,    -1,    -1,
      -1,    53,    54,    -1,    -1,    -1,    -1,    -1,    -1,    61,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,    -1,    -1,
      -1,    97,    98,    99,    -1,     9,   102,   103,   104,   105,
      -1,    -1,    -1,    -1,    -1,    19,    20,    -1,    -1,    23,
      -1,    -1,    -1,    -1,    -1,    97,    98,    99,     1,    -1,
     102,   103,   104,   105,    38,    -1,     9,    -1,    42,    -1,
      -1,    -1,    -1,    47,    48,    18,    19,    20,    -1,    53,
      54,    -1,    -1,    -1,    -1,    -1,    -1,    61,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,     1,    42,
      -1,    -1,    -1,    -1,    47,    48,     9,    -1,    -1,    -1,
      53,    54,    -1,    -1,    -1,    -1,    19,    20,    61,    -1,
      -1,    -1,    -1,    97,    98,    99,    -1,    -1,   102,   103,
     104,   105,    -1,    -1,    -1,    38,    -1,    -1,    -1,    42,
      -1,    -1,    -1,    -1,    47,    48,    -1,    -1,    -1,    -1,
      53,    54,    -1,    -1,    97,    98,    99,    -1,    61,   102,
     103,   104,   105,    -1,    17,    -1,    -1,    20,    21,    22,
      -1,    24,    -1,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    -1,    -1,    39,    40,    41,    -1,
      -1,    -1,    -1,    -1,    97,    98,    99,    50,    -1,   102,
     103,   104,   105,    -1,    -1,    -1,    59,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,
      -1,    -1,    -1,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    17,   109,   110,    20,    21,
      22,    -1,    24,    -1,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    -1,    -1,    -1,    40,    41,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,
      -1,    -1,    -1,    -1,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      17,    18,    -1,    20,    21,    22,    -1,   109,   110,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      -1,    -1,    -1,    40,    41,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    70,    71,    -1,    -1,    -1,    -1,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    17,   109,   110,    20,    21,    22,    -1,    24,    -1,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    -1,    -1,    -1,    40,    41,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,    -1,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    17,    -1,    -1,    20,
      21,    22,    -1,   109,   110,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    -1,    -1,    39,    40,
      41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,    70,
      71,    -1,    -1,    -1,    -1,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    17,    -1,    -1,    20,    21,    22,    -1,   109,   110,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    -1,    -1,    39,    40,    41,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,    -1,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    17,    -1,    -1,    20,
      21,    22,    -1,   109,   110,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    -1,    -1,    39,    40,
      41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,    70,
      71,    -1,    -1,    -1,    -1,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    17,   109,   110,
      20,    21,    22,    -1,    24,    -1,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    -1,    -1,    -1,
      40,    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    17,    -1,    -1,    20,    21,    22,    -1,   109,
     110,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    -1,    -1,    39,    40,    41,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,
      -1,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    17,    -1,    -1,
      20,    21,    22,    -1,   109,   110,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    -1,    -1,    39,
      40,    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    17,   109,
     110,    20,    21,    22,    -1,    24,    -1,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    -1,    -1,
      -1,    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    70,    71,    -1,    -1,    -1,    -1,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    17,    -1,    -1,    20,    21,    22,    -1,
     109,   110,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    -1,    -1,    39,    40,    41,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,
      -1,    -1,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    17,   109,   110,    20,    21,    22,
      -1,    24,    -1,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    -1,    -1,    -1,    40,    41,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,
      -1,    -1,    -1,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    17,
      -1,    -1,    20,    21,    22,    23,   109,   110,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    -1,
      -1,    -1,    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    70,    71,    -1,    -1,    -1,    -1,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      17,   109,   110,    20,    21,    22,    -1,    24,    -1,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      -1,    -1,    -1,    40,    41,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    70,    71,    -1,    -1,    -1,    -1,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    17,   109,   110,    20,    21,    22,    -1,    24,    -1,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    -1,    -1,    -1,    40,    41,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,    -1,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    17,    -1,    -1,    20,
      21,    22,    -1,   109,   110,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    -1,    -1,    39,    40,
      41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,    70,
      71,    -1,    -1,    -1,    -1,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    17,    -1,    -1,    20,    21,    22,    -1,   109,   110,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    -1,    -1,    40,    41,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,    -1,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    17,   109,   110,    20,    21,    22,    -1,    24,
      -1,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    -1,    -1,    -1,    40,    41,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,
      -1,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    17,   109,   110,    20,    21,    22,    -1,
      24,    -1,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    -1,    -1,    -1,    40,    41,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,
      -1,    -1,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    17,   109,   110,    20,    21,    22,
      -1,    24,    -1,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    -1,    -1,    -1,    40,    41,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,
      -1,    -1,    -1,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    17,
      -1,    -1,    20,    21,    22,    -1,   109,   110,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    -1,
      -1,    -1,    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    70,    71,    -1,    -1,    -1,    -1,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    17,    -1,    -1,    20,    21,    22,
      -1,   109,   110,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    -1,    -1,    -1,    -1,    41,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,
      -1,    -1,    -1,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    17,
      -1,    -1,    20,    21,    22,    -1,   109,   110,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    -1,
      -1,    -1,    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    69,    70,    71,    -1,    -1,    -1,    -1,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    17,    -1,    -1,    20,    21,    22,
      -1,   109,   110,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    -1,    -1,    -1,    -1,    41,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,
      -1,    -1,    -1,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    17,    -1,
      -1,    20,    21,    22,    -1,    -1,   109,   110,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    -1,    -1,
      -1,    -1,    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    70,    71,    -1,    -1,    -1,    -1,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    17,    -1,    -1,    20,    21,    22,    -1,    -1,
     109,   110,    27,    28,    29,    30,    31,    -1,    33,    34,
      35,    36,    -1,    -1,    -1,    -1,    41,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,
      -1,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    -1,    -1,    -1,    -1,
      17,    -1,    -1,    20,    21,    22,    -1,    -1,    -1,    -1,
      27,    28,    29,    30,   109,   110,    33,    34,    35,    36,
      -1,    -1,    -1,    -1,    41,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    69,    70,    71,    -1,    -1,    -1,    -1,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    -1,    -1,    22,    -1,    24,    -1,
      -1,    27,    28,    29,    30,    31,    32,    33,    -1,    35,
      36,    -1,   109,   110,    40,    41,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,    -1,
      76,    -1,    78,    -1,    80,    -1,    82,    -1,    84,    -1,
      86,    -1,    88,    89,    90,    -1,    -1,    22,    -1,    24,
      -1,    -1,    27,    28,    29,    30,    31,    32,    33,    -1,
      35,    36,    -1,   109,   110,    40,    41,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,    -1,
      -1,    76,    -1,    78,    -1,    80,    -1,    82,    -1,    84,
      -1,    86,    22,    88,    89,    90,    -1,    27,    28,    29,
      30,    31,    32,    33,    -1,    35,    36,    -1,    -1,    39,
      40,    41,    -1,    -1,   109,   110,    -1,    -1,    -1,    -1,
      50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,
      70,    71,    -1,    -1,    -1,    -1,    76,    -1,    78,    -1,
      80,    -1,    82,    -1,    84,    -1,    86,    -1,    88,    89,
      90,    -1,    -1,    22,    23,    -1,    -1,    -1,    27,    28,
      29,    30,    31,    32,    33,    -1,    35,    36,    -1,   109,
     110,    40,    41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    70,    71,    -1,    -1,    -1,    -1,    76,    -1,    78,
      -1,    80,    -1,    82,    -1,    84,    -1,    86,    22,    88,
      89,    90,    -1,    27,    28,    29,    30,    31,    32,    33,
      -1,    35,    36,    -1,    -1,    -1,    40,    41,    -1,    -1,
     109,   110,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,
      -1,    -1,    76,    -1,    78,    -1,    80,    -1,    82,    -1,
      84,    -1,    86,    22,    88,    89,    90,    -1,    27,    28,
      29,    30,    31,    32,    33,    -1,    35,    36,    -1,    -1,
      -1,    40,    41,    -1,    -1,   109,   110,    -1,    -1,    -1,
      -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    70,    71,    -1,    -1,    -1,    -1,    76,    -1,    78,
      -1,    80,    -1,    82,    -1,    84,    -1,    86,    22,    88,
      89,    90,    -1,    27,    28,    29,    30,    31,    32,    33,
      -1,    35,    36,    -1,    -1,    -1,    40,    41,    -1,    -1,
     109,   110,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,
      -1,    -1,    76,    -1,    78,    -1,    80,    -1,    82,    -1,
      84,    -1,    86,    22,    88,    89,    90,    -1,    27,    28,
      29,    30,    31,    32,    33,    -1,    35,    36,    -1,    -1,
      -1,    -1,    41,    -1,    -1,   109,   110,    -1,    -1,    -1,
      -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    70,    71,    -1,    -1,    -1,    -1,    76,    -1,    78,
      -1,    80,    -1,    82,    -1,    84,    -1,    86,    22,    88,
      89,    90,    -1,    27,    28,    29,    30,    31,    -1,    33,
      -1,    35,    36,    -1,    -1,    -1,    -1,    41,    -1,    -1,
     109,   110,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,
      -1,    -1,    76,    -1,    78,    -1,    80,    -1,    82,    -1,
      84,    -1,    86,    22,    88,    89,    90,    -1,    27,    28,
      29,    30,    -1,    -1,    33,    -1,    35,    36,    -1,    -1,
      -1,    -1,    41,    -1,    -1,   109,   110,    -1,    -1,    -1,
      -1,    50,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      59,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      69,    70,    71,    -1,    -1,    -1,    -1,    76,    -1,    78,
      -1,    80,    -1,    82,    -1,    84,    -1,    86,    22,    88,
      89,    90,    -1,    27,    28,    29,    30,    -1,    -1,    33,
      -1,    35,    36,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     109,   110,    -1,    -1,    -1,    -1,    50,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    59,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    69,    70,    71,    -1,    -1,
      -1,    -1,    76,    -1,    78,    -1,    80,    -1,    82,    -1,
      84,    -1,    86,    -1,    88,    89,    90
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,   149,   150,   151,     0,     1,     6,     7,     8,    15,
      16,    66,    67,    75,    92,    93,   100,   112,   113,   114,
     154,   155,   156,   157,   158,   159,   160,   161,   172,   175,
     176,    22,   136,   153,    46,    46,    19,     1,     9,    19,
      20,    38,    42,    47,    48,    53,    54,    61,    97,    98,
      99,   102,   103,   104,   105,   168,   168,    19,    19,   137,
      33,   153,    20,   168,   170,   171,   168,     9,    20,    20,
      20,    20,    18,    21,    27,    28,    50,    64,   168,   174,
     179,    18,    20,    40,    20,    33,   168,   168,   169,    18,
      24,    39,    72,    39,    37,   168,   168,   168,   168,    19,
      20,    33,   153,   179,   168,    18,   179,    22,    27,    28,
      29,    30,    31,    32,    33,    35,    36,    40,    41,    50,
      59,    69,    70,    71,    76,    78,    80,    82,    84,    86,
      88,    89,    90,   109,   110,   173,    33,     9,   162,    18,
      21,    29,    45,    64,    74,   153,   163,   164,   165,   167,
     168,   162,   168,   153,    24,    39,    72,    24,   168,   170,
     171,   168,    39,    39,    39,    39,   177,   178,   179,     3,
      10,    11,    18,    20,    21,    25,    27,    28,    29,    38,
      42,    43,    44,    46,    49,    52,    55,    56,    57,    58,
      60,    63,    64,    68,   106,   107,   108,   111,   115,   116,
     117,   138,   139,   141,   182,   186,   189,    22,   136,   152,
      33,   153,   179,   179,   182,    24,    39,    20,    23,   167,
     167,   167,    24,   153,    22,    27,    28,    29,    30,    31,
      32,    33,    35,    36,    40,    41,    50,    59,    69,    70,
      71,    76,    78,    80,    82,    84,    86,    88,    89,    90,
     109,   110,    18,    39,   153,   169,   168,   168,   170,    39,
      24,   182,    20,    38,    39,    40,    42,   168,   180,   181,
     182,   183,    23,   182,   185,    20,   182,   182,   182,    18,
       9,    18,    18,    18,    18,    18,    20,    20,    20,    20,
      20,   182,   182,   182,   182,   182,    38,   182,   182,   182,
      20,    17,    20,    21,    22,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    40,    41,    50,    59,
      69,    70,    71,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,   109,   110,
     153,   179,   182,    39,    33,   153,   153,   162,    40,   166,
     167,   152,    74,   164,   167,   167,   167,   167,   167,   167,
     167,   167,   167,   167,   167,   167,   167,   167,   167,   167,
     167,   167,   167,   167,   167,   167,   167,   167,   167,   167,
     167,   167,    33,    24,    33,   153,   178,     4,     5,    22,
      94,    95,   168,   168,   182,     9,    18,    39,    39,    24,
      24,    39,    39,    22,    23,    24,   152,   168,    40,   182,
      40,    40,    40,    40,    33,   168,   182,   182,   182,   182,
      24,    18,   200,    18,    20,   197,   198,     4,    24,   182,
     182,   182,   187,   188,   185,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
     182,   182,   182,   182,   182,   182,   182,   182,   182,   182,
     182,    23,   153,    33,   153,   182,   163,    39,    24,   167,
      39,   168,   182,    18,    20,    21,    38,    45,    64,   168,
     191,   192,   194,   182,     5,    18,    18,    39,    39,    39,
      96,   101,   182,   121,   122,   168,   180,   182,   183,   185,
     182,    39,   168,   168,   182,   168,   168,   168,   182,    39,
      39,    39,    24,    39,   182,    24,    18,    37,   197,   192,
     182,    39,    24,    39,    39,    33,   153,   182,   153,   166,
      23,   153,   153,    20,   168,   192,   194,   196,    23,   193,
     194,   168,   190,   191,   194,    18,    91,     5,    22,    27,
      28,    29,    30,    31,    32,    33,    35,    36,    40,    41,
      50,    59,    69,    70,    71,    76,    78,    80,    82,    84,
      86,    88,    89,    90,   109,   110,    91,   182,     5,   182,
     182,   182,   182,   182,   182,    23,    24,    24,    24,    24,
      24,    24,    24,   182,    62,    39,    46,   138,   152,   199,
      24,   188,   182,   153,    39,   194,   195,    18,    24,    39,
      22,    23,    24,   152,    37,    24,   190,   182,   182,   194,
     194,   194,   194,   194,   194,   194,   194,   194,   194,   194,
     194,   194,   194,   194,   194,   194,   194,   194,   194,   194,
     194,   194,   194,   194,   194,   194,   194,   182,   182,    40,
     182,   182,   182,   182,   182,   182,    18,    27,    28,    29,
      64,   173,   182,   184,    39,   182,   199,   199,   138,   182,
     153,    24,    39,   194,   196,   193,   194,   190,    39,   182,
      37,    33,    33,    33,     5,    39,   195,    23,   182,   182,
     182,   182,    24,    24,    24,   184,   184,   184
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (&yylloc, parser, YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (0)


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (N)								\
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (0)
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
              (Loc).first_line, (Loc).first_column,	\
              (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, &yylloc, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval, &yylloc, parser)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr,					\
                  Type, Value, Location);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname[yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;
  (void) yylocationp;

  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");

# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, yylocationp)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;
  (void) yylocationp;

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (ParserEngine^ parser);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */






/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (ParserEngine^ parser)
#else
int
yyparse (parser)
    ParserEngine^ parser;
#endif
#endif
{
  /* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;
/* Location data for the look-ahead symbol.  */
YYLTYPE yylloc;

  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;

  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
  /* The locations where the error started and ended. */
  YYLTYPE yyerror_range[2];

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
  yylsp = yyls;
#if YYLTYPE_IS_TRIVIAL
  /* Initialize the default location before parsing starts.  */
  yylloc.first_line   = yylloc.last_line   = 1;
  yylloc.first_column = yylloc.last_column = 0;
#endif


  /* User initialization code. */
#line 56 "Anubis.y"
{
  yydebug = 0;
}
/* Line 930 of yacc.c.  */
#line 3036 "Anubis.tab.cpp"
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;
	YYLTYPE *yyls1 = yyls;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
	YYSTACK_RELOCATE (yyls);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a look-ahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to look-ahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
  *++yylsp = yylloc;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, yylsp - yylen, yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 170 "Anubis.y"
    { ;}
    break;

  case 3:
#line 173 "Anubis.y"
    { ;}
    break;

  case 4:
#line 176 "Anubis.y"
    { ;}
    break;

  case 5:
#line 177 "Anubis.y"
    { ;}
    break;

  case 6:
#line 181 "Anubis.y"
    { ;}
    break;

  case 7:
#line 182 "Anubis.y"
    { ;}
    break;

  case 8:
#line 185 "Anubis.y"
    { ;}
    break;

  case 9:
#line 186 "Anubis.y"
    { ;}
    break;

  case 10:
#line 191 "Anubis.y"
    { parser->Lexer.begin_INITIAL(); ;}
    break;

  case 11:
#line 192 "Anubis.y"
    { parser->Lexer.begin_INITIAL(); ;}
    break;

  case 12:
#line 200 "Anubis.y"
    { ;}
    break;

  case 13:
#line 201 "Anubis.y"
    { ;}
    break;

  case 14:
#line 202 "Anubis.y"
    { ;}
    break;

  case 15:
#line 203 "Anubis.y"
    { ;}
    break;

  case 16:
#line 204 "Anubis.y"
    { ;}
    break;

  case 17:
#line 205 "Anubis.y"
    { parser->AddUsing((String^)(Object^)(yyvsp[0]).object, (yyloc)); ;}
    break;

  case 18:
#line 206 "Anubis.y"
    { parser->AddUsing((String^)(Object^)(yyvsp[0]).object, (yyloc)); ;}
    break;

  case 19:
#line 207 "Anubis.y"
    { yyerrok; ;}
    break;

  case 20:
#line 211 "Anubis.y"
    { ;}
    break;

  case 21:
#line 215 "Anubis.y"
    { (yyval).object = ModifierEnum::Private; ;}
    break;

  case 22:
#line 216 "Anubis.y"
    { (yyval).object = ModifierEnum::Public; ;}
    break;

  case 23:
#line 217 "Anubis.y"
    { (yyval).object = ModifierEnum::Public; ;}
    break;

  case 24:
#line 218 "Anubis.y"
    { (yyval).object = ModifierEnum::Public; ;}
    break;

  case 25:
#line 219 "Anubis.y"
    { (yyval).object = ModifierEnum::Public; ;}
    break;

  case 26:
#line 223 "Anubis.y"
    { (yyval).object = ModifierEnum::Private; ;}
    break;

  case 27:
#line 224 "Anubis.y"
    { (yyval).object = ModifierEnum::Public; ;}
    break;

  case 28:
#line 227 "Anubis.y"
    { (yyval).object = ModifierEnum::Private; ;}
    break;

  case 29:
#line 228 "Anubis.y"
    { (yyval).object = ModifierEnum::Public; ;}
    break;

  case 30:
#line 231 "Anubis.y"
    { (yyval).object = ModifierEnum::Private;;}
    break;

  case 31:
#line 232 "Anubis.y"
    { (yyval).object = ModifierEnum::Public;;}
    break;

  case 32:
#line 237 "Anubis.y"
    { parser->AddType((ModifierEnum)(Object^)(yyvsp[-4]).object, (String^)(Object^)(yyvsp[-3]).object, (yylsp[-1])); ;}
    break;

  case 33:
#line 238 "Anubis.y"
    { parser->AddType((ModifierEnum)(Object^)(yyvsp[-7]).object, (String^)(Object^)(yyvsp[-6]).object, (yylsp[-1])); ;}
    break;

  case 34:
#line 240 "Anubis.y"
    { parser->AddType((ModifierEnum)(Object^)(yyvsp[-3]).object, (String^)(Object^)(yyvsp[-2]).object, (yylsp[0])); ;}
    break;

  case 35:
#line 242 "Anubis.y"
    { parser->AddType((ModifierEnum)(Object^)(yyvsp[-6]).object, (String^)(Object^)(yyvsp[-5]).object, (yylsp[0])); ;}
    break;

  case 36:
#line 245 "Anubis.y"
    { ;}
    break;

  case 37:
#line 246 "Anubis.y"
    { ;}
    break;

  case 38:
#line 255 "Anubis.y"
    { ;}
    break;

  case 39:
#line 256 "Anubis.y"
    { ;}
    break;

  case 40:
#line 257 "Anubis.y"
    { ;}
    break;

  case 41:
#line 261 "Anubis.y"
    { ;}
    break;

  case 42:
#line 262 "Anubis.y"
    { ;}
    break;

  case 43:
#line 263 "Anubis.y"
    { ;}
    break;

  case 44:
#line 267 "Anubis.y"
    { ;}
    break;

  case 45:
#line 268 "Anubis.y"
    { ;}
    break;

  case 46:
#line 269 "Anubis.y"
    { ;}
    break;

  case 47:
#line 270 "Anubis.y"
    { ;}
    break;

  case 48:
#line 271 "Anubis.y"
    { ;}
    break;

  case 49:
#line 272 "Anubis.y"
    { ;}
    break;

  case 50:
#line 273 "Anubis.y"
    { ;}
    break;

  case 51:
#line 274 "Anubis.y"
    { ;}
    break;

  case 52:
#line 275 "Anubis.y"
    { ;}
    break;

  case 53:
#line 276 "Anubis.y"
    { ;}
    break;

  case 54:
#line 277 "Anubis.y"
    { ;}
    break;

  case 55:
#line 278 "Anubis.y"
    { ;}
    break;

  case 56:
#line 279 "Anubis.y"
    { ;}
    break;

  case 57:
#line 280 "Anubis.y"
    { ;}
    break;

  case 58:
#line 281 "Anubis.y"
    { ;}
    break;

  case 59:
#line 282 "Anubis.y"
    { ;}
    break;

  case 60:
#line 283 "Anubis.y"
    { ;}
    break;

  case 61:
#line 284 "Anubis.y"
    { ;}
    break;

  case 62:
#line 285 "Anubis.y"
    { ;}
    break;

  case 63:
#line 286 "Anubis.y"
    { ;}
    break;

  case 64:
#line 287 "Anubis.y"
    { ;}
    break;

  case 65:
#line 288 "Anubis.y"
    { ;}
    break;

  case 66:
#line 289 "Anubis.y"
    { ;}
    break;

  case 67:
#line 290 "Anubis.y"
    { ;}
    break;

  case 68:
#line 291 "Anubis.y"
    { ;}
    break;

  case 69:
#line 292 "Anubis.y"
    { ;}
    break;

  case 70:
#line 293 "Anubis.y"
    { ;}
    break;

  case 71:
#line 294 "Anubis.y"
    { ;}
    break;

  case 72:
#line 295 "Anubis.y"
    { ;}
    break;

  case 73:
#line 296 "Anubis.y"
    { ;}
    break;

  case 74:
#line 297 "Anubis.y"
    { ;}
    break;

  case 75:
#line 298 "Anubis.y"
    { ;}
    break;

  case 76:
#line 299 "Anubis.y"
    { ;}
    break;

  case 77:
#line 300 "Anubis.y"
    { ;}
    break;

  case 78:
#line 301 "Anubis.y"
    { ;}
    break;

  case 79:
#line 305 "Anubis.y"
    { ;}
    break;

  case 80:
#line 306 "Anubis.y"
    { ;}
    break;

  case 81:
#line 310 "Anubis.y"
    { ;}
    break;

  case 82:
#line 311 "Anubis.y"
    { ;}
    break;

  case 83:
#line 322 "Anubis.y"
    { (yyval) = (yyvsp[-1]); ;}
    break;

  case 84:
#line 323 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[0]).object, (yylsp[0])); ;}
    break;

  case 85:
#line 324 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[0]).object, (yylsp[0])); ;}
    break;

  case 86:
#line 325 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[0]).object, (yylsp[0])); ;}
    break;

  case 87:
#line 326 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[0]).object, (yylsp[0])); ;}
    break;

  case 88:
#line 327 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[0]).object, (yylsp[0])); ;}
    break;

  case 89:
#line 328 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[0]).object, (yylsp[0])); ;}
    break;

  case 90:
#line 329 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[0]).object, (yylsp[0])); ;}
    break;

  case 91:
#line 330 "Anubis.y"
    { (yyval) = (yyvsp[0]); ;}
    break;

  case 92:
#line 332 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[0]).object, (yylsp[0])); ;}
    break;

  case 93:
#line 333 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[0]).object, (yylsp[0])); ;}
    break;

  case 94:
#line 334 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[0]).object, (yylsp[0])); ;}
    break;

  case 95:
#line 336 "Anubis.y"
    { (yyval) = (yyvsp[-1]); ;}
    break;

  case 96:
#line 337 "Anubis.y"
    { (yyval) = (yyvsp[-1]); ;}
    break;

  case 97:
#line 338 "Anubis.y"
    { (yyval) = (yyvsp[-1]); ;}
    break;

  case 98:
#line 339 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[-3]).object, (yylsp[-3])); ;}
    break;

  case 99:
#line 340 "Anubis.y"
    { (yyval).object = parser->GetReturnType((String^)(Object^)(yyvsp[-1]).object, (yylsp[-1])); ;}
    break;

  case 100:
#line 341 "Anubis.y"
    { AnonymousMethodReturnType^ lambda = gcnew AnonymousMethodReturnType(parser->CU);
																													lambda->MethodReturnType = (IReturnType^)(Object^)(yyvsp[0]).object;
																													lambda->MethodParameters->Add(gcnew DefaultParameter("", (IReturnType^)(Object^)(yyvsp[-2]).object, parser->CreateDomRegion((yylsp[-2]))));
																													(yyval).object = lambda; ;}
    break;

  case 101:
#line 345 "Anubis.y"
    { AnonymousMethodReturnType^ lambda = gcnew AnonymousMethodReturnType(parser->CU);
																													lambda->MethodReturnType = (IReturnType^)(Object^)(yyvsp[0]).object;
																													// Todo : parameters
																													(yyval).object = lambda; ;}
    break;

  case 102:
#line 349 "Anubis.y"
    { AnonymousMethodReturnType^ lambda = gcnew AnonymousMethodReturnType(parser->CU);
																													lambda->MethodReturnType = (IReturnType^)(Object^)(yyvsp[0]).object;
																													lambda->MethodParameters = (IList<IParameter^>^)(Object^)(yyvsp[-2]).object;
																													(yyval).object = lambda; ;}
    break;

  case 103:
#line 353 "Anubis.y"
    { (yyval) = (yyvsp[-1]); ;}
    break;

  case 104:
#line 354 "Anubis.y"
    { (yyval) = (yyvsp[-1]); ;}
    break;

  case 105:
#line 356 "Anubis.y"
    { (yyval).object = gcnew DefaultReturnType(gcnew DefaultClass(parser->CU, "__error__")); ;}
    break;

  case 106:
#line 360 "Anubis.y"
    { ;}
    break;

  case 107:
#line 361 "Anubis.y"
    { ;}
    break;

  case 108:
#line 365 "Anubis.y"
    { IList<IParameter^>^ params = gcnew List<IParameter^>(); 
																													params->Add(gcnew DefaultParameter("", (IReturnType^)(Object^)(yyvsp[0]).object, parser->CreateDomRegion((yylsp[0]))));
																													(yyval).object = params; ;}
    break;

  case 109:
#line 368 "Anubis.y"
    { IList<IParameter^>^ params = gcnew List<IParameter^>(); 
																													params->Add(gcnew DefaultParameter((String^)(Object^)(yyvsp[0]).object, (IReturnType^)(Object^)(yyvsp[-1]).object, parser->CreateDomRegion((yylsp[-1]))));
																													(yyval).object = params; ;}
    break;

  case 110:
#line 371 "Anubis.y"
    { IList<IParameter^>^ params = (List<IParameter^>^)(Object^)(yyvsp[0]).object;
																													params->Add(gcnew DefaultParameter("", (IReturnType^)(Object^)(yyvsp[-2]).object, parser->CreateDomRegion((yylsp[-2]))));
																													(yyval).object = params; ;}
    break;

  case 111:
#line 374 "Anubis.y"
    { IList<IParameter^>^ params = (List<IParameter^>^)(Object^)(yyvsp[0]).object;
																													params->Add(gcnew DefaultParameter((String^)(Object^)(yyvsp[-2]).object, (IReturnType^)(Object^)(yyvsp[-3]).object, parser->CreateDomRegion((yylsp[-3]))));
																													(yyval).object = params; ;}
    break;

  case 112:
#line 380 "Anubis.y"
    { CombinedReturnType^ combi = gcnew CombinedReturnType(gcnew List<IReturnType^>(), "", "", "", ""); 
						                                             combi->BaseTypes->Add((IReturnType^)(Object^)(yyvsp[-2]).object);
						                                             combi->BaseTypes->Add((IReturnType^)(Object^)(yyvsp[0]).object);
						                                             (yyval).object = combi; ;}
    break;

  case 113:
#line 384 "Anubis.y"
    { CombinedReturnType^ combi = (CombinedReturnType^)(Object^)(yyvsp[0]).object;
																												 combi->BaseTypes->Insert(0, (IReturnType^)(Object^)(yyvsp[-2]).object);
																												 (yyval).object = combi; ;}
    break;

  case 114:
#line 397 "Anubis.y"
    { parser->AddVar((ModifierEnum)(Object^)(yyvsp[-5]).object, nullptr, (String^)(Object^)(yyvsp[-3]).object, (yyloc)); ;}
    break;

  case 115:
#line 405 "Anubis.y"
    { ;}
    break;

  case 116:
#line 406 "Anubis.y"
    { ;}
    break;

  case 117:
#line 407 "Anubis.y"
    { ;}
    break;

  case 118:
#line 408 "Anubis.y"
    { ;}
    break;

  case 119:
#line 409 "Anubis.y"
    { ;}
    break;

  case 120:
#line 410 "Anubis.y"
    { ;}
    break;

  case 121:
#line 411 "Anubis.y"
    { ;}
    break;

  case 122:
#line 412 "Anubis.y"
    { ;}
    break;

  case 123:
#line 413 "Anubis.y"
    { ;}
    break;

  case 124:
#line 414 "Anubis.y"
    { ;}
    break;

  case 125:
#line 415 "Anubis.y"
    { ;}
    break;

  case 126:
#line 416 "Anubis.y"
    { ;}
    break;

  case 127:
#line 417 "Anubis.y"
    { ;}
    break;

  case 128:
#line 418 "Anubis.y"
    { ;}
    break;

  case 129:
#line 419 "Anubis.y"
    { ;}
    break;

  case 130:
#line 420 "Anubis.y"
    { ;}
    break;

  case 131:
#line 421 "Anubis.y"
    { ;}
    break;

  case 132:
#line 422 "Anubis.y"
    { ;}
    break;

  case 133:
#line 423 "Anubis.y"
    { ;}
    break;

  case 134:
#line 424 "Anubis.y"
    { ;}
    break;

  case 135:
#line 425 "Anubis.y"
    { ;}
    break;

  case 136:
#line 426 "Anubis.y"
    { ;}
    break;

  case 137:
#line 427 "Anubis.y"
    { ;}
    break;

  case 138:
#line 428 "Anubis.y"
    { ;}
    break;

  case 139:
#line 429 "Anubis.y"
    { ;}
    break;

  case 140:
#line 430 "Anubis.y"
    { ;}
    break;

  case 141:
#line 431 "Anubis.y"
    { ;}
    break;

  case 142:
#line 434 "Anubis.y"
    { ;}
    break;

  case 143:
#line 435 "Anubis.y"
    { ;}
    break;

  case 144:
#line 436 "Anubis.y"
    { ;}
    break;

  case 145:
#line 441 "Anubis.y"
    { parser->AddFunction((ModifierEnum)(Object^)(yyvsp[-5]).object, (IReturnType^)(Object^)(yyvsp[-4]).object, (String^)(Object^)(yyvsp[-3]).object, (yylsp[-2]), (yyloc)); ;}
    break;

  case 146:
#line 444 "Anubis.y"
    { DefaultMethod^ method = (DefaultMethod^)parser->AddFunction((ModifierEnum)(Object^)(yyvsp[-8]).object, (IReturnType^)(Object^)(yyvsp[-7]).object, (String^)(Object^)(yyvsp[-6]).object, (yylsp[-2]), (yyloc));
			method->Parameters = (IList<IParameter^>^)(Object^)(yyvsp[-4]).object; ;}
    break;

  case 147:
#line 449 "Anubis.y"
    { IMethod^ method = parser->AddFunction((ModifierEnum)(Object^)(yyvsp[-7]).object, (IReturnType^)(Object^)(yyvsp[-6]).object, (String^)(Object^)(yyvsp[-4]).object, (yylsp[-2]), (yyloc)); 
			method->Parameters->Add((IParameter^)(Object^)(yyvsp[-5]).object);
			method->Parameters->Add((IParameter^)(Object^)(yyvsp[-3]).object); ;}
    break;

  case 149:
#line 458 "Anubis.y"
    { IMethod^ method = parser->AddFunction((ModifierEnum)(Object^)(yyvsp[-9]).object, (IReturnType^)(Object^)(yyvsp[-8]).object, (String^)(Object^)(yyvsp[-5]).object, (yylsp[-2]), (yyloc)); 
			method->Parameters->Add((IParameter^)(Object^)(yyvsp[-6]).object);
			method->Parameters->Add((IParameter^)(Object^)(yyvsp[-4]).object); ;}
    break;

  case 151:
#line 467 "Anubis.y"
    { IMethod^ method = parser->AddFunction((ModifierEnum)(Object^)(yyvsp[-6]).object, (IReturnType^)(Object^)(yyvsp[-5]).object, (String^)(Object^)(yyvsp[-4]).object, (yylsp[-2]), (yyloc));
			method->Parameters->Add((IParameter^)(Object^)(yyvsp[-3]).object); ;}
    break;

  case 153:
#line 475 "Anubis.y"
    { IMethod^ method = parser->AddFunction((ModifierEnum)(Object^)(yyvsp[-8]).object, (IReturnType^)(Object^)(yyvsp[-7]).object, (String^)(Object^)(yyvsp[-5]).object, (yylsp[-2]), (yyloc)); 
			method->Parameters->Add((IParameter^)(Object^)(yyvsp[-6]).object);
			method->Parameters->Add((IParameter^)(Object^)(yyvsp[-4]).object); ;}
    break;

  case 157:
#line 498 "Anubis.y"
    { (yyval).object = nullptr; ;}
    break;

  case 158:
#line 499 "Anubis.y"
    { (yyval) = (yyvsp[0]); ;}
    break;

  case 159:
#line 502 "Anubis.y"
    { IList<IParameter^>^ params = gcnew List<IParameter^>(); 
																							params->Add((IParameter^)(Object^)(yyvsp[0]).object);
																							(yyval).object = params;;}
    break;

  case 160:
#line 505 "Anubis.y"
    { IList<IParameter^>^ params = (List<IParameter^>^)(Object^)(yyvsp[0]).object;
																							params->Insert(0, (IParameter^)(Object^)(yyvsp[-2]).object);
																							(yyval).object = params;
																							;}
    break;

  case 161:
#line 511 "Anubis.y"
    { (yyval).object = gcnew DefaultParameter((String^)(Object^)(yyvsp[0]).object, (IReturnType^)(Object^)(yyvsp[-1]).object, parser->CreateDomRegion((yylsp[-1])));}
    break;

  case 162:
#line 523 "Anubis.y"
    { ;}
    break;

  case 163:
#line 524 "Anubis.y"
    { ;}
    break;

  case 164:
#line 527 "Anubis.y"
    { ;}
    break;

  case 165:
#line 533 "Anubis.y"
    { ;}
    break;

  case 166:
#line 534 "Anubis.y"
    { ;}
    break;

  case 167:
#line 535 "Anubis.y"
    { ;}
    break;

  case 168:
#line 536 "Anubis.y"
    { ;}
    break;

  case 169:
#line 537 "Anubis.y"
    { ;}
    break;

  case 170:
#line 538 "Anubis.y"
    { ;}
    break;

  case 171:
#line 539 "Anubis.y"
    { ;}
    break;

  case 172:
#line 540 "Anubis.y"
    { ;}
    break;

  case 173:
#line 542 "Anubis.y"
    {;}
    break;

  case 174:
#line 543 "Anubis.y"
    { ;}
    break;

  case 175:
#line 544 "Anubis.y"
    { ;}
    break;

  case 176:
#line 545 "Anubis.y"
    { ;}
    break;

  case 177:
#line 546 "Anubis.y"
    { ;}
    break;

  case 178:
#line 547 "Anubis.y"
    { ;}
    break;

  case 179:
#line 548 "Anubis.y"
    { ;}
    break;

  case 180:
#line 549 "Anubis.y"
    { ;}
    break;

  case 181:
#line 550 "Anubis.y"
    { ;}
    break;

  case 182:
#line 551 "Anubis.y"
    { ;}
    break;

  case 183:
#line 552 "Anubis.y"
    { ;}
    break;

  case 184:
#line 553 "Anubis.y"
    { ;}
    break;

  case 185:
#line 554 "Anubis.y"
    { ;}
    break;

  case 186:
#line 556 "Anubis.y"
    { ;}
    break;

  case 187:
#line 557 "Anubis.y"
    { ;}
    break;

  case 188:
#line 559 "Anubis.y"
    { ;}
    break;

  case 189:
#line 560 "Anubis.y"
    { ;}
    break;

  case 190:
#line 561 "Anubis.y"
    { ;}
    break;

  case 191:
#line 562 "Anubis.y"
    { ;}
    break;

  case 192:
#line 563 "Anubis.y"
    { ;}
    break;

  case 193:
#line 564 "Anubis.y"
    { ;}
    break;

  case 194:
#line 565 "Anubis.y"
    { ;}
    break;

  case 195:
#line 566 "Anubis.y"
    { ;}
    break;

  case 196:
#line 567 "Anubis.y"
    { ;}
    break;

  case 197:
#line 568 "Anubis.y"
    { ;}
    break;

  case 198:
#line 569 "Anubis.y"
    { ;}
    break;

  case 199:
#line 570 "Anubis.y"
    { ;}
    break;

  case 200:
#line 571 "Anubis.y"
    { ;}
    break;

  case 201:
#line 573 "Anubis.y"
    { ;}
    break;

  case 202:
#line 574 "Anubis.y"
    { ;}
    break;

  case 203:
#line 575 "Anubis.y"
    { ;}
    break;

  case 204:
#line 576 "Anubis.y"
    { ;}
    break;

  case 205:
#line 577 "Anubis.y"
    { ;}
    break;

  case 206:
#line 578 "Anubis.y"
    { ;}
    break;

  case 207:
#line 579 "Anubis.y"
    { ;}
    break;

  case 208:
#line 580 "Anubis.y"
    { ;}
    break;

  case 209:
#line 581 "Anubis.y"
    { ;}
    break;

  case 210:
#line 582 "Anubis.y"
    { ;}
    break;

  case 211:
#line 586 "Anubis.y"
    { ;}
    break;

  case 212:
#line 587 "Anubis.y"
    { ;}
    break;

  case 213:
#line 593 "Anubis.y"
    { ;}
    break;

  case 214:
#line 594 "Anubis.y"
    { ;}
    break;

  case 215:
#line 595 "Anubis.y"
    { ;}
    break;

  case 216:
#line 596 "Anubis.y"
    { ;}
    break;

  case 217:
#line 602 "Anubis.y"
    { ;}
    break;

  case 218:
#line 603 "Anubis.y"
    { ;}
    break;

  case 219:
#line 604 "Anubis.y"
    { ;}
    break;

  case 220:
#line 605 "Anubis.y"
    { ;}
    break;

  case 221:
#line 612 "Anubis.y"
    { ;}
    break;

  case 222:
#line 613 "Anubis.y"
    { ;}
    break;

  case 223:
#line 614 "Anubis.y"
    { ;}
    break;

  case 224:
#line 615 "Anubis.y"
    { ;}
    break;

  case 225:
#line 616 "Anubis.y"
    { ;}
    break;

  case 226:
#line 617 "Anubis.y"
    { ;}
    break;

  case 227:
#line 618 "Anubis.y"
    { ;}
    break;

  case 228:
#line 619 "Anubis.y"
    { ;}
    break;

  case 229:
#line 620 "Anubis.y"
    { ;}
    break;

  case 230:
#line 621 "Anubis.y"
    { ;}
    break;

  case 231:
#line 622 "Anubis.y"
    { ;}
    break;

  case 232:
#line 623 "Anubis.y"
    { ;}
    break;

  case 233:
#line 624 "Anubis.y"
    { ;}
    break;

  case 234:
#line 625 "Anubis.y"
    { ;}
    break;

  case 235:
#line 626 "Anubis.y"
    { ;}
    break;

  case 236:
#line 627 "Anubis.y"
    { ;}
    break;

  case 237:
#line 628 "Anubis.y"
    { ;}
    break;

  case 238:
#line 629 "Anubis.y"
    { ;}
    break;

  case 239:
#line 630 "Anubis.y"
    { ;}
    break;

  case 240:
#line 631 "Anubis.y"
    { ;}
    break;

  case 241:
#line 632 "Anubis.y"
    { ;}
    break;

  case 242:
#line 633 "Anubis.y"
    { ;}
    break;

  case 243:
#line 634 "Anubis.y"
    { ;}
    break;

  case 244:
#line 635 "Anubis.y"
    { ;}
    break;

  case 245:
#line 636 "Anubis.y"
    { ;}
    break;

  case 246:
#line 637 "Anubis.y"
    { ;}
    break;

  case 247:
#line 638 "Anubis.y"
    { ;}
    break;

  case 248:
#line 639 "Anubis.y"
    { ;}
    break;

  case 249:
#line 640 "Anubis.y"
    { ;}
    break;

  case 250:
#line 641 "Anubis.y"
    { ;}
    break;

  case 251:
#line 642 "Anubis.y"
    { ;}
    break;

  case 252:
#line 643 "Anubis.y"
    { ;}
    break;

  case 253:
#line 644 "Anubis.y"
    { ;}
    break;

  case 254:
#line 645 "Anubis.y"
    { ;}
    break;

  case 255:
#line 646 "Anubis.y"
    { ;}
    break;

  case 256:
#line 647 "Anubis.y"
    { ;}
    break;

  case 257:
#line 648 "Anubis.y"
    { ;}
    break;

  case 258:
#line 649 "Anubis.y"
    { ;}
    break;

  case 259:
#line 650 "Anubis.y"
    { ;}
    break;

  case 260:
#line 653 "Anubis.y"
    { ;}
    break;

  case 261:
#line 654 "Anubis.y"
    { ;}
    break;

  case 262:
#line 657 "Anubis.y"
    { ;}
    break;

  case 263:
#line 658 "Anubis.y"
    { ;}
    break;

  case 272:
#line 685 "Anubis.y"
    { ;}
    break;

  case 273:
#line 686 "Anubis.y"
    { ;}
    break;

  case 274:
#line 687 "Anubis.y"
    { ;}
    break;

  case 275:
#line 690 "Anubis.y"
    { ;}
    break;

  case 276:
#line 693 "Anubis.y"
    { ;}
    break;

  case 277:
#line 694 "Anubis.y"
    { ;}
    break;

  case 278:
#line 695 "Anubis.y"
    { ;}
    break;

  case 279:
#line 697 "Anubis.y"
    { ;}
    break;

  case 280:
#line 698 "Anubis.y"
    { ;}
    break;

  case 281:
#line 700 "Anubis.y"
    { ;}
    break;

  case 282:
#line 701 "Anubis.y"
    { ;}
    break;

  case 283:
#line 702 "Anubis.y"
    { ;}
    break;

  case 284:
#line 703 "Anubis.y"
    { ;}
    break;

  case 285:
#line 704 "Anubis.y"
    { ;}
    break;

  case 286:
#line 705 "Anubis.y"
    { ;}
    break;

  case 287:
#line 706 "Anubis.y"
    { ;}
    break;

  case 288:
#line 707 "Anubis.y"
    { ;}
    break;

  case 289:
#line 708 "Anubis.y"
    { ;}
    break;

  case 290:
#line 709 "Anubis.y"
    { ;}
    break;

  case 291:
#line 710 "Anubis.y"
    { ;}
    break;

  case 292:
#line 711 "Anubis.y"
    { ;}
    break;

  case 293:
#line 712 "Anubis.y"
    { ;}
    break;

  case 294:
#line 713 "Anubis.y"
    { ;}
    break;

  case 295:
#line 714 "Anubis.y"
    { ;}
    break;

  case 296:
#line 715 "Anubis.y"
    { ;}
    break;

  case 297:
#line 716 "Anubis.y"
    { ;}
    break;

  case 298:
#line 717 "Anubis.y"
    { ;}
    break;

  case 299:
#line 718 "Anubis.y"
    { ;}
    break;

  case 300:
#line 719 "Anubis.y"
    { ;}
    break;

  case 301:
#line 720 "Anubis.y"
    { ;}
    break;

  case 302:
#line 721 "Anubis.y"
    { ;}
    break;

  case 303:
#line 722 "Anubis.y"
    { ;}
    break;

  case 304:
#line 723 "Anubis.y"
    { ;}
    break;

  case 305:
#line 724 "Anubis.y"
    { ;}
    break;

  case 306:
#line 725 "Anubis.y"
    { ;}
    break;

  case 307:
#line 726 "Anubis.y"
    { ;}
    break;

  case 308:
#line 727 "Anubis.y"
    { ;}
    break;

  case 309:
#line 728 "Anubis.y"
    { ;}
    break;

  case 310:
#line 729 "Anubis.y"
    { ;}
    break;

  case 311:
#line 730 "Anubis.y"
    { ;}
    break;

  case 312:
#line 734 "Anubis.y"
    { ;}
    break;

  case 313:
#line 735 "Anubis.y"
    { ;}
    break;

  case 314:
#line 736 "Anubis.y"
    { ;}
    break;

  case 315:
#line 746 "Anubis.y"
    { ;}
    break;

  case 316:
#line 747 "Anubis.y"
    { ;}
    break;

  case 317:
#line 756 "Anubis.y"
    { ;}
    break;

  case 318:
#line 757 "Anubis.y"
    { ;}
    break;

  case 319:
#line 760 "Anubis.y"
    { ;}
    break;

  case 320:
#line 761 "Anubis.y"
    { ;}
    break;

  case 321:
#line 766 "Anubis.y"
    { ;}
    break;

  case 322:
#line 767 "Anubis.y"
    { ;}
    break;

  case 323:
#line 770 "Anubis.y"
    { ;}
    break;

  case 324:
#line 771 "Anubis.y"
    { ;}
    break;

  case 325:
#line 774 "Anubis.y"
    { ;}
    break;

  case 326:
#line 775 "Anubis.y"
    { ;}
    break;

  case 327:
#line 776 "Anubis.y"
    { ;}
    break;

  case 328:
#line 777 "Anubis.y"
    { ;}
    break;

  case 329:
#line 783 "Anubis.y"
    { ;}
    break;


      default: break;
    }

/* Line 1126 of yacc.c.  */
#line 4833 "Anubis.tab.cpp"

  yyvsp -= yylen;
  yyssp -= yylen;
  yylsp -= yylen;

  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  int yytype = YYTRANSLATE (yychar);
	  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
	  YYSIZE_T yysize = yysize0;
	  YYSIZE_T yysize1;
	  int yysize_overflow = 0;
	  char *yymsg = 0;
#	  define YYERROR_VERBOSE_ARGS_MAXIMUM 5
	  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
	  int yyx;

#if 0
	  /* This is so xgettext sees the translatable formats that are
	     constructed on the fly.  */
	  YY_("syntax error, unexpected %s");
	  YY_("syntax error, unexpected %s, expecting %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
#endif
	  char *yyfmt;
	  char const *yyf;
	  static char const yyunexpected[] = "syntax error, unexpected %s";
	  static char const yyexpecting[] = ", expecting %s";
	  static char const yyor[] = " or %s";
	  char yyformat[sizeof yyunexpected
			+ sizeof yyexpecting - 1
			+ ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
			   * (sizeof yyor - 1))];
	  char const *yyprefix = yyexpecting;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 1;

	  yyarg[0] = yytname[yytype];
	  yyfmt = yystpcpy (yyformat, yyunexpected);

	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
		  {
		    yycount = 1;
		    yysize = yysize0;
		    yyformat[sizeof yyunexpected - 1] = '\0';
		    break;
		  }
		yyarg[yycount++] = yytname[yyx];
		yysize1 = yysize + yytnamerr (0, yytname[yyx]);
		yysize_overflow |= yysize1 < yysize;
		yysize = yysize1;
		yyfmt = yystpcpy (yyfmt, yyprefix);
		yyprefix = yyor;
	      }

	  yyf = YY_(yyformat);
	  yysize1 = yysize + yystrlen (yyf);
	  yysize_overflow |= yysize1 < yysize;
	  yysize = yysize1;

	  if (!yysize_overflow && yysize <= YYSTACK_ALLOC_MAXIMUM)
	    yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg)
	    {
	      /* Avoid sprintf, as that infringes on the user's name space.
		 Don't have undefined behavior even if the translation
		 produced a string with the wrong number of "%s"s.  */
	      char *yyp = yymsg;
	      int yyi = 0;
	      while ((*yyp = *yyf))
		{
		  if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		    {
		      yyp += yytnamerr (yyp, yyarg[yyi++]);
		      yyf += 2;
		    }
		  else
		    {
		      yyp++;
		      yyf++;
		    }
		}
	      yyerror (&yylloc, parser, yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    {
	      yyerror (&yylloc, parser, YY_("syntax error"));
	      goto yyexhaustedlab;
	    }
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror (&yylloc, parser, YY_("syntax error"));
    }

  yyerror_range[0] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
        }
      else
	{
	  yydestruct ("Error: discarding", yytoken, &yylval, &yylloc);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (0)
     goto yyerrorlab;

  yyerror_range[0] = yylsp[1-yylen];
  yylsp -= yylen;
  yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      yyerror_range[0] = *yylsp;
      yydestruct ("Error: popping", yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;

  yyerror_range[1] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the look-ahead.  YYLOC is available though. */
  YYLLOC_DEFAULT (yyloc, yyerror_range - 1, 2);
  *++yylsp = yyloc;

  /* Shift the error token. */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, parser, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval, &yylloc);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK;
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 787 "Anubis.y"
 /* end of grammar */ 

int yylex (YYSTYPE* yylval, YYLTYPE* yylloc, ParserEngine^ parser)
{
	return parser->Lexer.yylex(yylval, yylloc);
}

void yyerror(YYLTYPE* yylloc, ParserEngine^ parser, char * msg)
{
	// TODO yyerror()
	LoggingService::Debug(String::Format("PARSER ERROR: {0} on {1}({2},{3})", 
																			 gcnew String(msg), parser->Filename, yylloc->first_line, yylloc->first_column));
}


static void
print_token_value (FILE *file, int type, YYSTYPE value)
{
	Object^ obj = value.object;
	if(obj != nullptr)
	{
		const char * str = LexerEngine::GetUnmanagedString(obj->ToString());
    fprintf (file, "%s", str);
    LexerEngine::FreeUnmanagedString(str);
  }
}

