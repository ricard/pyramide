// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Matthew Ward" email="mrward@users.sourceforge.net"/>
//     <version>$Revision: 915 $</version>
// </file>

using System;
using System.Windows.Forms;

namespace ICSharpCode.NAntAddIn.Gui
{
	/// <summary>
	/// Represents a <see cref="NAntBuildFile"/> error in the
	/// <see cref="NAntPadTreeView"/>.
	/// </summary>
	public class NAntBuildFileErrorTreeNode : TreeNode
	{
		NAntBuildFileError buildFileError;
		public NAntBuildFileErrorTreeNode(NAntBuildFileError error)
		{
			this.Text = error.Message;
			this.ImageIndex = NAntPadTreeViewImageList.TargetErrorImage;
			this.SelectedImageIndex = NAntPadTreeViewImageList.TargetErrorImage;
			this.buildFileError = error;
		}
		
		public NAntBuildFileError Error {
			get {
				return buildFileError;
			}
		}
		
	}
}
