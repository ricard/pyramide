﻿// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="David Srbecký" email="dsrbecky@gmail.com"/>
//     <version>$Revision: 1167 $</version>
// </file>

using System;

namespace Debugger.Wrappers.MetaData
{
	struct TypeRefProps
	{
		public uint Token;
		public string Name;
	}
}
