﻿// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="David Srbecký" email="dsrbecky@gmail.com"/>
//     <version>$Revision: 1634 $</version>
// </file>

using System;
using System.Windows.Forms;

namespace Debugger.Tests.TestPrograms
{
	public class PropertyVariableForm
	{
		public static void Main()
		{
			Form form = new Form();
			System.Diagnostics.Debugger.Break();
			System.Diagnostics.Debugger.Break();
		}
	}
}
