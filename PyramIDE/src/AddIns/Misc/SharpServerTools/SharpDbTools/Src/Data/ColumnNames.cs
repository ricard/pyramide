// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Dickon Field" email=""/>
//     <version>$Revision: 1784 $</version>
// </file>

using System;

namespace SharpDbTools.Data
{
	/// <summary>
	/// Description of Columns.
	/// </summary>
	public sealed class ColumnNames
	{
		public const string InvariantName = "invariantName";
		public const string Name = "name";
		public const string ConnectionString = "connectionString";
		public const string TableName = "TABLE_NAME";
	}
}
