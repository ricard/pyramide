// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Dickon Field" email=""/>
//     <version>$Revision$</version>
// </file>

using System;

namespace SharpServerTools.Forms
{
	/// <summary>
	/// Implemented by anything that is initializable
	/// </summary>
	public interface IInitializable
	{	
		void Initialize();
	}
}
