@echo Make installation package
pushd Tools
%windir%\microsoft.net\framework\v2.0.50727\msbuild /t:MakeInstall Tools.build
@popd
@IF %ERRORLEVEL% NEQ 0 PAUSE & EXIT
@echo.
@echo.
@echo.
@echo MakeInstall.bat completed successfully.
@echo The installation package has been created.
@echo.
