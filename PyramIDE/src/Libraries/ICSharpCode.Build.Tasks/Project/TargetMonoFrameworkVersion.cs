// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Matthew Ward" email="mrward@users.sourceforge.net"/>
//     <version>$Revision: 983 $</version>
// </file>

using System;

namespace ICSharpCode.Build.Tasks
{
	public enum TargetMonoFrameworkVersion
	{
		Version11 = 0,
		Version20 = 1,
		VersionLatest = 1
	}
}
