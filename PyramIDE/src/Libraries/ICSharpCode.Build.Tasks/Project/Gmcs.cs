// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Matthew Ward" email="mrward@users.sourceforge.net"/>
//     <version>$Revision: 1157 $</version>
// </file>

using System;

namespace ICSharpCode.Build.Tasks
{
	/// <summary>
	/// MSBuild task for Mono's GMCS.
	/// </summary>
	public class Gmcs : MonoCSharpCompilerTask
	{
		protected override string ToolName {
			get {
				return "Gmcs.exe";
			}
		}
		
		protected override string GenerateFullPathToTool()
		{
			return MonoToolLocationHelper.GetPathToTool(ToolName);
		}
	}
}
