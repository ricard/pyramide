// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Daniel Grunwald" email="daniel@danielgrunwald.de"/>
//     <version>$Revision: 1742 $</version>
// </file>

using System;
using System.Collections.Generic;
using System.Windows.Forms;

using ICSharpCode.Core;
using ICSharpCode.SharpDevelop;

namespace ICSharpCode.SharpDevelop.Gui
{
	/// <summary>
	/// Control to edit a list of strings.
	/// </summary>
	public class StringListEditor : System.Windows.Forms.UserControl
	{
		public StringListEditor()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			this.ManualOrder = true;
			this.BrowseForDirectory = false;
			// init enabled states:
			ListBoxSelectedIndexChanged(null, null);
			EditTextBoxTextChanged(null, null);
			updateButton.Text = StringParser.Parse(updateButton.Text);
			removeButton.Text = StringParser.Parse(removeButton.Text);
			moveUpButton.Image   = ResourceService.GetBitmap("Icons.16x16.ArrowUp");
			moveDownButton.Image = ResourceService.GetBitmap("Icons.16x16.ArrowDown");
			deleteButton.Image   = ResourceService.GetBitmap("Icons.16x16.DeleteIcon");
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.removeButton = new System.Windows.Forms.Button();
			this.deleteButton = new System.Windows.Forms.Button();
			this.moveDownButton = new System.Windows.Forms.Button();
			this.moveUpButton = new System.Windows.Forms.Button();
			this.listBox = new System.Windows.Forms.ListBox();
			this.listLabel = new System.Windows.Forms.Label();
			this.updateButton = new System.Windows.Forms.Button();
			this.addButton = new System.Windows.Forms.Button();
			this.editTextBox = new System.Windows.Forms.TextBox();
			this.browseButton = new System.Windows.Forms.Button();
			this.TitleLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// removeButton
			// 
			this.removeButton.Location = new System.Drawing.Point(233, 53);
			this.removeButton.Name = "removeButton";
			this.removeButton.Size = new System.Drawing.Size(115, 23);
			this.removeButton.TabIndex = 5;
			this.removeButton.Text = "${res:Global.DeleteButtonText}";
			this.removeButton.Click += new System.EventHandler(this.RemoveButtonClick);
			// 
			// deleteButton
			// 
			this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.deleteButton.Location = new System.Drawing.Point(329, 164);
			this.deleteButton.Name = "deleteButton";
			this.deleteButton.Size = new System.Drawing.Size(24, 24);
			this.deleteButton.TabIndex = 10;
			this.deleteButton.Click += new System.EventHandler(this.RemoveButtonClick);
			// 
			// moveDownButton
			// 
			this.moveDownButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.moveDownButton.Location = new System.Drawing.Point(329, 134);
			this.moveDownButton.Name = "moveDownButton";
			this.moveDownButton.Size = new System.Drawing.Size(24, 24);
			this.moveDownButton.TabIndex = 9;
			this.moveDownButton.Click += new System.EventHandler(this.MoveDownButtonClick);
			// 
			// moveUpButton
			// 
			this.moveUpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.moveUpButton.Location = new System.Drawing.Point(329, 104);
			this.moveUpButton.Name = "moveUpButton";
			this.moveUpButton.Size = new System.Drawing.Size(24, 24);
			this.moveUpButton.TabIndex = 8;
			this.moveUpButton.Click += new System.EventHandler(this.MoveUpButtonClick);
			// 
			// listBox
			// 
			this.listBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
									| System.Windows.Forms.AnchorStyles.Left)
									| System.Windows.Forms.AnchorStyles.Right)));
			this.listBox.FormattingEnabled = true;
			this.listBox.Location = new System.Drawing.Point(3, 104);
			this.listBox.Name = "listBox";
			this.listBox.Size = new System.Drawing.Size(320, 160);
			this.listBox.TabIndex = 7;
			this.listBox.SelectedIndexChanged += new System.EventHandler(this.ListBoxSelectedIndexChanged);
			// 
			// listLabel
			// 
			this.listLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
									| System.Windows.Forms.AnchorStyles.Right)));
			this.listLabel.Location = new System.Drawing.Point(3, 89);
			this.listLabel.Name = "listLabel";
			this.listLabel.Size = new System.Drawing.Size(350, 23);
			this.listLabel.TabIndex = 6;
			this.listLabel.Text = "List:";
			// 
			// updateButton
			// 
			this.updateButton.Location = new System.Drawing.Point(118, 53);
			this.updateButton.Name = "updateButton";
			this.updateButton.Size = new System.Drawing.Size(115, 23);
			this.updateButton.TabIndex = 4;
			this.updateButton.Text = "${res:Global.UpdateButtonText}";
			this.updateButton.Click += new System.EventHandler(this.UpdateButtonClick);
			// 
			// addButton
			// 
			this.addButton.Location = new System.Drawing.Point(3, 53);
			this.addButton.Name = "addButton";
			this.addButton.Size = new System.Drawing.Size(115, 23);
			this.addButton.TabIndex = 3;
			this.addButton.Text = "Add Item";
			this.addButton.Click += new System.EventHandler(this.AddButtonClick);
			// 
			// editTextBox
			// 
			this.editTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
									| System.Windows.Forms.AnchorStyles.Right)));
			this.editTextBox.Location = new System.Drawing.Point(3, 26);
			this.editTextBox.Name = "editTextBox";
			this.editTextBox.Size = new System.Drawing.Size(316, 20);
			this.editTextBox.TabIndex = 1;
			this.editTextBox.TextChanged += new System.EventHandler(this.EditTextBoxTextChanged);
			// 
			// browseButton
			// 
			this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.browseButton.Location = new System.Drawing.Point(325, 24);
			this.browseButton.Name = "browseButton";
			this.browseButton.Size = new System.Drawing.Size(28, 23);
			this.browseButton.TabIndex = 2;
			this.browseButton.Text = "...";
			this.browseButton.Click += new System.EventHandler(this.BrowseButtonClick);
			// 
			// TitleLabel
			// 
			this.TitleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
									| System.Windows.Forms.AnchorStyles.Right)));
			this.TitleLabel.Location = new System.Drawing.Point(3, 10);
			this.TitleLabel.Name = "TitleLabel";
			this.TitleLabel.Size = new System.Drawing.Size(350, 23);
			this.TitleLabel.TabIndex = 0;
			this.TitleLabel.Text = "Title:";
			// 
			// StringListEditor
			// 
			this.Controls.Add(this.removeButton);
			this.Controls.Add(this.deleteButton);
			this.Controls.Add(this.moveDownButton);
			this.Controls.Add(this.moveUpButton);
			this.Controls.Add(this.listBox);
			this.Controls.Add(this.listLabel);
			this.Controls.Add(this.updateButton);
			this.Controls.Add(this.addButton);
			this.Controls.Add(this.editTextBox);
			this.Controls.Add(this.browseButton);
			this.Controls.Add(this.TitleLabel);
			this.Name = "StringListEditor";
			this.Size = new System.Drawing.Size(380, 272);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Button removeButton;
		private System.Windows.Forms.Button deleteButton;
		private System.Windows.Forms.Button moveDownButton;
		private System.Windows.Forms.Button moveUpButton;
		private System.Windows.Forms.ListBox listBox;
		private System.Windows.Forms.Label listLabel;
		private System.Windows.Forms.Button updateButton;
		private System.Windows.Forms.Button addButton;
		private System.Windows.Forms.TextBox editTextBox;
		private System.Windows.Forms.Button browseButton;
		private System.Windows.Forms.Label TitleLabel;
		#endregion
		
		public event EventHandler ListChanged;
		
		protected virtual void OnListChanged(EventArgs e)
		{
			if (ListChanged != null) {
				ListChanged(this, e);
			}
		}
		
		public bool ManualOrder {
			get {
				return !listBox.Sorted;
			}
			set {
				moveUpButton.Visible = moveDownButton.Visible = deleteButton.Visible = value;
				removeButton.Visible = !value;
				listBox.Sorted = !value;
			}
		}
		
		bool browseForDirectory;
		
		public bool BrowseForDirectory {
			get {
				return browseForDirectory;
			}
			set {
				browseForDirectory = value;
				browseButton.Visible = browseForDirectory; // || browseForFile;
			}
		}
		
		bool autoAddAfterBrowse;
		
		public bool AutoAddAfterBrowse {
			get {
				return autoAddAfterBrowse;
			}
			set {
				autoAddAfterBrowse = value;
			}
		}
		
		public string TitleText {
			get {
				return TitleLabel.Text;
			}
			set {
				TitleLabel.Text = value;
			}
		}
		
		public string AddButtonText {
			get {
				return addButton.Text;
			}
			set {
				addButton.Text = value;
			}
		}
		
		public string ListCaption {
			get {
				return listLabel.Text;
			}
			set {
				listLabel.Text = value;
			}
		}
		
		public void LoadList(IEnumerable<string> list)
		{
			listBox.Items.Clear();
			foreach (string str in list) {
				listBox.Items.Add(str);
			}
		}
		
		public string[] GetList()
		{
			string[] list = new string[listBox.Items.Count];
			for (int i = 0; i < list.Length; i++) {
				list[i] = listBox.Items[i].ToString();
			}
			return list;
		}
		
		void BrowseButtonClick(object sender, EventArgs e)
		{
			using (FolderBrowserDialog fdiag = FileService.CreateFolderBrowserDialog("${res:Dialog.ProjectOptions.SelectFolderTitle}")) {
				if (fdiag.ShowDialog() == DialogResult.OK) {
					string path = fdiag.SelectedPath;
					if (!path.EndsWith("\\") && !path.EndsWith("/"))
						path += "\\";
					editTextBox.Text = path;
					if (autoAddAfterBrowse) {
						AddButtonClick(null, null);
					}
				}
			}
		}
		
		void AddButtonClick(object sender, EventArgs e)
		{
			editTextBox.Text = editTextBox.Text.Trim();
			if (editTextBox.TextLength > 0) {
				int index = listBox.Items.IndexOf(editTextBox.Text);
				if (index < 0) {
					index = listBox.Items.Add(editTextBox.Text);
					OnListChanged(EventArgs.Empty);
				}
				listBox.SelectedIndex = index;
			}
		}
		
		void UpdateButtonClick(object sender, EventArgs e)
		{
			editTextBox.Text = editTextBox.Text.Trim();
			if (editTextBox.TextLength > 0) {
				listBox.Items[listBox.SelectedIndex] = editTextBox.Text;
				OnListChanged(EventArgs.Empty);
			}
		}
		
		void RemoveButtonClick(object sender, EventArgs e)
		{
			listBox.Items.RemoveAt(listBox.SelectedIndex);
			OnListChanged(EventArgs.Empty);
		}
		
		void MoveUpButtonClick(object sender, EventArgs e)
		{
			int index = listBox.SelectedIndex;
			object tmp = listBox.Items[index];
			listBox.Items[index] = listBox.Items[index - 1];
			listBox.Items[index - 1] = tmp;
			listBox.SelectedIndex = index - 1;
			OnListChanged(EventArgs.Empty);
		}
		
		void MoveDownButtonClick(object sender, EventArgs e)
		{
			int index = listBox.SelectedIndex;
			object tmp = listBox.Items[index];
			listBox.Items[index] = listBox.Items[index + 1];
			listBox.Items[index + 1] = tmp;
			listBox.SelectedIndex = index + 1;
			OnListChanged(EventArgs.Empty);
		}
		
		void ListBoxSelectedIndexChanged(object sender, EventArgs e)
		{
			if (listBox.SelectedIndex >= 0) {
				editTextBox.Text = listBox.Items[listBox.SelectedIndex].ToString();
			}
			moveUpButton.Enabled = listBox.SelectedIndex > 0;
			moveDownButton.Enabled = listBox.SelectedIndex >= 0 && listBox.SelectedIndex < listBox.Items.Count - 1;
			removeButton.Enabled = deleteButton.Enabled = listBox.SelectedIndex >= 0;
			updateButton.Enabled = listBox.SelectedIndex >= 0 && editTextBox.TextLength > 0;
		}
		
		void EditTextBoxTextChanged(object sender, System.EventArgs e)
		{
			addButton.Enabled = editTextBox.TextLength > 0;
			updateButton.Enabled = listBox.SelectedIndex >= 0 && editTextBox.TextLength > 0;
		}
	}
}
