﻿// <file>
//     <copyright see="prj:///doc/copyright.txt"/>
//     <license see="prj:///doc/license.txt"/>
//     <owner name="Daniel Grunwald" email="daniel@danielgrunwald.de"/>
//     <version>$Revision: 1139 $</version>
// </file>

using System;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: CLSCompliant(true)]
[assembly: StringFreezing()]

[assembly: AssemblyTitle("PyramIDE")]
[assembly: AssemblyDescription("Anubis language IDE")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyTrademark("PyramIDE is trademark of SoftArchi company")]
[assembly: AssemblyCulture("")]
