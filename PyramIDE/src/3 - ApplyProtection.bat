@echo Applying protection
Postbuild\XBuild AnubisBinding.postbuild /m Postbuild\AnubisBinding.xcmap
Postbuild\XBuild "PyramIDE.postbuild" /m "Postbuild\PyramIDE.xcmap"
@IF %ERRORLEVEL% NEQ 0 PAUSE & EXIT
@echo.
@echo.
@echo.
@echo ApplyProtection.bat completed successfully.
@echo SoftArchitect IDE assemblies have been correctly obfuscated.
@echo.
