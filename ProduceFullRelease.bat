@pushd "PyramIDE\src"
@call "0 - Clean.bat"
@IF %ERRORLEVEL% NEQ 0 PAUSE & EXIT
@call "1 - PrepareRelease.bat"
@IF %ERRORLEVEL% NEQ 0 PAUSE & EXIT
@call "2 - ReleaseBuild.bat"
@IF %ERRORLEVEL% NEQ 0 PAUSE & EXIT
@call "3 - ApplyProtection.bat"
@IF %ERRORLEVEL% NEQ 0 PAUSE & EXIT
@call "4 - MakeInstall.bat"
@IF %ERRORLEVEL% NEQ 0 PAUSE & EXIT
@popd
@echo.
@echo.
@echo.
@echo Full Release produced successfully.
@echo The installation package has been created.
@echo.
@pause